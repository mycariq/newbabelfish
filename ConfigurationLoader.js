var utils = require("./utils");
var request = require("request");
var config = require("./config");
var interval = config.Configurator.frequency;
var baseUrl = "https://" + config.API.DNS + ":" + config.API.port + "/Cariq";
var auth = "Basic " + Buffer.from(config.API.username + ":" + config.API.password).toString("base64")


'use strict'
class ConfigurationLoader {
	constructor(deviceName, supportedCommands) {
		this.configs = {};
		this.deviceName = deviceName;
		this.supportedCommands = supportedCommands;
		this.lastLoadedAt = new Date();
		this.init();
	}

	init() {
		var configurationLoader = this;

		setInterval(() => {

			//For local only
			//this.lastLoadedAt.setMinutes(this.lastLoadedAt.getMinutes() - 330);
			var fromTimestamp = utils.covertDateToString(this.lastLoadedAt);
			this.lastLoadedAt = new Date();

			var url = baseUrl + "/devices/admin/feedback/after/" + this.deviceName + "/" + fromTimestamp;

			console.log("Calling::: " + url);

			request({
				url: url,
				headers: {
					"Authorization": auth
				},
				rejectUnauthorized: false
			}, function (error, response, body) {
				if (error) {
					console.log(error);
					throw error;
				}

				var response = JSON.parse(body);
				console.log("Response from api::: " + JSON.stringify(response));

				//iterate over the response json. and set feedback to configs
				response.forEach(function (feedback) {
					configurationLoader.add(feedback.deviceId, feedback.type, feedback.payload);
				});

				console.log("After Loading:" + JSON.stringify(configurationLoader.configs));
			});
		}, interval);
	}

	add(owner, command, payload) {
		if (!this.supportedCommands.includes(command)) {
			console.log("Command '" + command + "' is not supported.")
			return;
		}

		if (!this.configs[owner])
			this.configs[owner] = {};
		this.configs[owner][command] = payload;
	}

	get(owner, command) {
		var config = this.configs[owner];
		if (config && config[command])
			return { command: command, payload: config[command] };
	}

	getFirst(owner) {
		var config = this.configs[owner];
		if (config)
			for (var key in config)
				return this.get(owner, key);

	}

	getAll(owner) {
		var ownerConfig = [];
		var config = this.configs[owner];
		if (config) {
			for (var key in config)
				ownerConfig.push(this.get(owner, key));
		}

		return ownerConfig;
	}

	remove(owner, command) {
		var config = this.get(owner, command)
		if (config) {
			delete this.configs[owner][command];
			var count = 0;
			for (var key in this.configs[owner])
				count++;
			if (count == 0)
				delete this.configs[owner]
		}
		return config;

	}

	removeFirst(owner) {
		var config = this.getFirst(owner);
		if (config)
			this.remove(owner, config.command);
		return config;
	}

	removeAll(owner) {
		var config = this.getAll(owner);
		delete this.configs[owner];
		return config;
	}

	print() {
		console.log(this.configs);
	}
}

module.exports = ConfigurationLoader;

// var cfg = new ConfigurationLoader("VT200", ["SWITCH_MAIN_SERVER","cmd1", "cmd2", "cmd3", "cmd4"]);
// cfg.add("dev1", "cmd1", "pay1");
// cfg.add("dev1", "cmd2", "pay2");
// cfg.add("dev1", "cmd3", "pay3");
// cfg.add("dev1", "cmd4", "pay4");
// cfg.add("dev2", "cmd1", "pay5");
// cfg.add("dev2", "cmd2", "pay6");
// cfg.print()

