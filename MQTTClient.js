'use strict'

var mqtt = require('mqtt');

var MQTTClient = function (url, options, subCallback) {
    var client = mqtt.connect(url, options);
    var isConnected = false;

    client.on("connect", function () {
        console.info("Connected url:"+url+" options:",options);
        isConnected = true;
    });

    client.on("error", function (err) {
        console.error("Error while connecting to " + url, err);
        throw err;
    });

    client.stream.on("error", function (err) {
        console.error("Error while connecting to " + url, err);
        throw err;
    });

    while (isConnected == false) {
        require('deasync').runLoopOnce();
    }

    this.client = client;
    if (subCallback)
        this.client.on("message", subCallback);
}

MQTTClient.prototype = {
    subscribe: function (topic) {
        this.client.subscribe(topic, function (err) {
            if (err)
                console.error("Error while subscribing. error:", err);
        });
    },

    publish: function (topic, message, options, callback) {
        this.client.publish(topic, message, options, callback);
    }
}


module.exports = MQTTClient;