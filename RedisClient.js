var redisConfig = require("./config.json").redis;
var logger = require("./logger.js");
var mylogger = logger.getLogger("redis", "redis-client.log");

// create redis database connection
const redis = require('redis');
const client = redis.createClient({
    password: redisConfig.password,
    socket: {
        host: redisConfig.host,
        port: redisConfig.port
    }
});

client.on('connect', () => {
    mylogger.debug('Redis is connected');
});

// error 
client.on('error', err => mylogger.debug('Redis Client Error', err));

// connect to DB
function connectDB(callback) {
    client.connect().then(function () {
        callback();
    });
}

function readAll(identifier, callback) {
    client.hGetAll(identifier).then(function (value) {
        callback(value);
    });
}

function read(identifier, field, callback) {
    client.hGet(identifier, field).then(function (value) {
        callback(value);
    });
}

function write(identifier, field, json) {
    client.hSet(identifier, field, json);
}

function update(identifier, field, json) {
    write(identifier, field, json);
}

function disconnect() {
    client.quit();
    mylogger.debug('Disconnected to redis DB');
}

exports.connectDB = connectDB;
exports.readAll = readAll;
exports.read = read;
exports.write = write
exports.update = update;
exports.disconnect = disconnect;