var logger = require("./logger.js");
var mylogger = logger.getLogger("throughputRecorder", "throughput.log");

var ThroughputRecord =  function (name, startTime, startCount,threshold) {
	this.name = name;
	this.startTime = startTime;
	this.startCount = startCount;
	this.threshold = threshold;
	this.endTime = null;
	this.packetCount = 0;
}

ThroughputRecord.prototype = {
 	increment: function () {
		this.packetCount++;
		if (this.packetCount % (this.startCount + this.threshold) == 0) {
			this.endTime = new Date;
			//console.log(" \n" + this.name +" Threashold Reach : packetCount = " + this.packetCount + " startTime:" + this.startTime + " endTime :" + this.endTime + " Difference : " + (this.endTime - this.startTime));
			mylogger.debug("ThroughputRecord," + this.name +"," + this.startTime + "," + this.endTime + "," +  (this.threshold * 60000) / (this.endTime - this.startTime) + "," + this.threshold + "," + this.packetCount);

		this.startTime = new Date;
 		}
	}
};

exports.ThroughputRecord = ThroughputRecord;
