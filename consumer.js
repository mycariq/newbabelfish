var Kafka = require("kafka-node");
var config = require("./config.json");
var kafkaHost = config.kafka.host;

// constructor
var Consumer = function (topic, groupIds, callback) {

	this.topic = topic;
	var consumerGroup = new Kafka.ConsumerGroup(
		{
			kafkaHost: kafkaHost,
			groupId: groupIds,
			commitOffsetsOnFirstJoin: true
		}, this.topic
	);

	consumerGroup.on("message", function (message) {
		callback(message.value);
	})
}

module.exports = Consumer;
