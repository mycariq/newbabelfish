#!/bin/sh 

echo "TempDir="${TmpDir};

#Create services directory if not exist
mkdir -p services;

#Copy required services into services folder
jq -c '.servers.babelfish.services[]' ${TmpDir}/miniTruthtable.json | while read i; do service=$(echo ${i} | jq -r .service); cp -r  staging/${service} services; done;

#Delete staging folder
rm -r staging;
