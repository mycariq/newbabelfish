var CanSignal = require("./CanSignal");
var helper = require("./helper");
var utils = require("../utils");

var CanMessage = function (dbcFileReader, json) {
    if (dbcFileReader)
        constructByDBCFile(this, dbcFileReader);
    else if (json)
        constructByJson(this, json);
}

function constructByJson(canMessage, json) {
    canMessage.canId = json.canId;
    canMessage.name = json.name;
    canMessage.dlc = json.dlc;
    canMessage.transmittingNode = json.transmittingNode;
    canMessage.canSignals = {};

    var canSignalKeys = Object.keys(json.canSignals);
    canSignalKeys.forEach(canSignalKey => {
        var canSignal = new CanSignal(undefined, undefined, json.canSignals[canSignalKey]);
        canMessage.canSignals[canSignal.name] = canSignal;
    });
}

function constructByDBCFile(canMessage, dbcFileReader) {
    var line = dbcFileReader.nextLine();

    var splittedLine = helper.splitBySpace(line);
    canMessage.canId = parseInt(splittedLine[1]);
    canMessage.name = splittedLine[2].split(":")[0];
    canMessage.dlc = parseInt(splittedLine[3]);
    canMessage.transmittingNode = splittedLine[4];
    canMessage.canSignals = {};

    dbcFileReader.commitLine();

    while (dbcFileReader.hasNext()) {
        var line = dbcFileReader.nextLine();
        if (line.startsWith(" SG_")) {
            var canSignal = new CanSignal(canMessage.name, line, undefined);
            canMessage.canSignals[canSignal.name] = canSignal;
            dbcFileReader.commitLine();
        }
        else
            break;
    }
}

CanMessage.prototype = {
    parse: function (rawValue) {
        var retValue = {};
        var keys = Object.keys(this.canSignals);
        for (var index = 0; index < keys.length; index++) {
            var key = keys[index];
            var canSignal = this.canSignals[key];
            var parsedValue = canSignal.parse(rawValue);
            if (parsedValue)
                utils.extendJson(retValue, parsedValue);
        };

        return retValue;
    },

    addComment: function (rawComment) {
        var splitted = helper.splitBySpace(rawComment);
        if (splitted[1] == "BO_")
            this.comment = splitted[3];
        if (splitted[1] == "SG_") {
            var canSignal = this.canSignals[splitted[3]];
            if (canSignal)
                canSignal.comment = splitted[4];
        }
    },

    addValues: function (rawValue) {
        var splitted = helper.splitBySpace(rawValue);

        var canSignal = this.canSignals[splitted[2]];
        if (canSignal) {
            for (var index = 3; index < splitted.length - 1; index += 2) {
                canSignal.values[splitted[index]] = splitted[index + 1];
            }
        }
    }

};

module.exports = CanMessage;