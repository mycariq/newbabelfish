var helper = require("./helper");
var utils = require("../utils");

var CanSignal = function (canMessageName, line, json) {
    if (canMessageName && line)
        constructByDBCFile(this, canMessageName, line)
    else if (json)
        constructByJson(this, json);
};

function constructByJson(canSignal, json) {
    canSignal.name = json.name;
    canSignal.canMessageName = json.canMessageName;

    canSignal.startBit = json.startBit;
    canSignal.realStartBit = json.realStartBit;
    canSignal.bitLength = json.bitLength;
    canSignal.bigIndian = json.bigIndian;
    canSignal.signedInt = json.signedInt;

    canSignal.multiplier = json.multiplier;
    canSignal.offset = json.offset;

    canSignal.min = json.min;
    canSignal.max = json.max;

    canSignal.unit = json.unit;
    canSignal.receivingNode = json.receivingNode;
    canSignal.values = json.values;
}

function constructByDBCFile(canSignal, canMessageName, line) {
    var splittedLine = helper.splitBySpace(line);
    canSignal.name = splittedLine[1];
    canSignal.canMessageName = canMessageName;

    var bitdef = splittedLine[3];
    canSignal.startBit = parseInt(bitdef.split("|")[0]);
    canSignal.bitLength = parseInt(bitdef.split("|")[1].split("@")[0]);
    canSignal.bigIndian = bitdef.split("@")[1].startsWith("0");
    canSignal.signedInt = bitdef.split("@")[1].endsWith("-");

    var multOffset = splittedLine[4].replace("(", "").replace(")", "");
    canSignal.multiplier = parseFloat(multOffset.split(",")[0]);
    canSignal.offset = parseFloat(multOffset.split(",")[1]);

    var minMax = splittedLine[5].replace("[", "").replace("]", "");
    canSignal.min = parseFloat(minMax.split("|")[0]);
    canSignal.max = parseFloat(minMax.split("|")[1]);

    canSignal.unit = splittedLine[6];
    canSignal.receivingNode = splittedLine[7];
    canSignal.values = {};
};

CanSignal.prototype = {
    parse: function (rawValue) {

        var binary;
        if (this.bigIndian)
            binary = utils.hexToBin(rawValue);
        else
            binary = utils.revHexToBin(rawValue);

        var binValue = binary.substr(this.realStartBit, this.bitLength);

        var value;
        if (this.signedInt)
            value = utils.binToSInt(binValue);
        else
            value = utils.binToInt(binValue);

        value = value * this.multiplier;

        value = value + this.offset;

        if (this.values[value])
            value = this.values[value];

        var retValue = {};
        retValue[this.canMessageName + "_" + this.name] = value;
        return retValue;
    }
};

module.exports = CanSignal;