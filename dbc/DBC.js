var DBCFileReader = require("./DBCFileReader");
var CanMessage = require("./CanMessage");
var helper = require("./helper");

var DBC = function (dbcFile, dbcJson) {
    if (dbcFile)
        constructByDBCFile(this, dbcFile)
    else if (dbcJson)
        constructByJson(this, dbcJson);
};

function constructByJson(dbc, json) {
    dbc.canMessages = {};

    var canMessageKeys = Object.keys(json.canMessages);
    canMessageKeys.forEach(canMessageKey => {
        var canMessage = new CanMessage(undefined, json.canMessages[canMessageKey]);
        dbc.canMessages[canMessage.canId] = canMessage;
    });
}


function constructByDBCFile(dbc, dbcFile) {
    dbc.canMessages = {};

    var dbcFileReader = new DBCFileReader(dbcFile);
    while (dbcFileReader.hasNext()) {
        var line = dbcFileReader.nextLine();
        //continue if empty line
        var splittedLine = (helper.splitBySpace(line));

        if (line.startsWith("BO_")) {
            var canMessage = new CanMessage(dbcFileReader, undefined);
            dbc.canMessages[canMessage.canId] = canMessage;
            //throw skip if message already exist
        }
        else if (line.startsWith("CM_")) {
            var canMessage = dbc.canMessages[splittedLine[2]];
            if (canMessage) {
                canMessage.addComment(line);
            }
        }
        else if (line.startsWith("VAL_")) {
            var canMessage = dbc.canMessages[splittedLine[1]];
            if (canMessage) {
                canMessage.addValues(line);
            }
        }

        dbcFileReader.commitLine();
    }
};

DBC.prototype = {
    parse: function parse(messageId, rawValue) {
        var canMessage = this.canMessages[messageId];
        var parsedJson = {};
        if (canMessage)
            parsedJson = canMessage.parse(rawValue);
        else
            parsedJson[messageId] = rawValue;

        return parsedJson;
    }
}

module.exports = DBC;


// var dbc = new DBC("/home/datta/Downloads/963_V016.dbc");
//console.log(JSON.stringify(dbc));

// BO_ 298 ABS_Status: 8 ABS
//  SG_ ABS_CRC_12A : 60|5@0+ (1.0,0.0) [0.0|31.0] ""  ECU
//  SG_ ABS_Alive_Counter_12A : 63|3@0+ (1.0,0.0) [0.0|6.0] ""  ECU
//  SG_ ABS_General_Failure_LED : 40|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_ABS_Status : 41|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_ABS_Warning : 42|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_ABS_LED : 43|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_MTC_Status : 44|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_MTC_Warning : 45|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_MTC_LED : 46|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_CABS_Warning : 47|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_Front_Brake_Switch_War : 33|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_Rear_ABS_Symbol : 35|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_TC_Mode_Response : 39|3@0+ (1,0) [0.0|7.0] ""  DASH
//  SG_ ABS_ABS_Mode_Response : 17|2@0+ (1,0) [0.0|3.0] ""  DASH
//  SG_ ABS_LC_Symbol : 20|3@0+ (1,0) [0.0|4.0] ""  DASH
//  SG_ ABS_MTC_State : 13|3@0+ (1,0) [0.0|7.0] ""  ECU
//  SG_ ABS_System_Status : 4|2@0+ (1,0) [0.0|3.0] ""  ECU
//  SG_ ABS_LC_Warning : 6|1@0+ (1,0) [0.0|1.0] ""  DASH
//  SG_ ABS_LC_Status : 7|1@0+ (1,0) [0.0|1.0] ""  DASH

// BO_ 301 ABS_Wheel_and_Display_Speed: 8 ABS
//  SG_ ABS_CRC_12D : 60|5@0+ (1.0,0.0) [0.0|31.0] ""  ECU
//  SG_ ABS_Alive_Counter_12D : 63|3@0+ (1.0,0.0) [0.0|6.0] ""  ECU
//  SG_ ABS_Rear_Wheel_Speed : 44|13@0+ (0.05625,0.0) [0.0|400.0] "km/h"  ECU
//  SG_ ABS_Rear_Wheel_Speed_Val : 45|1@0+ (1,0) [0.0|1.0] ""  ECU
//  SG_ ABS_Front_Wheel_Speed : 28|13@0+ (0.05625,0.0) [0.0|400.0] "km/h"  DASH,ECU,IMU
//  SG_ ABS_Front_Wheel_Speed_Val : 29|1@0+ (1,0) [0.0|1.0] ""  ECU,IMU
//  SG_ ABS_Display_Rear_Wheel_Speed : 11|12@0+ (0.1,0.0) [0.0|400.0] "km/h"  ECU
//  SG_ ABS_Display_Front_Wheel_Speed : 7|12@0+ (0.1,0.0) [0.0|400.0] "km/h"  DASH,ECU

// var dbc = new DBC("/home/datta/Downloads/963_V016.dbc", undefined);
// console.log(dbc.parse("301", "FFFFFFFFFFFFFFFF"));

// var dbcJson = {"canMessages":{"46F":{"canId":"46F","name":"TEL_GPS_Position","dlc":8,"transmittingNode":"TEL","comment":null,"canSignals":{"CNS_TEL_GPSlatitude_Est_count":{"name":"CNS_TEL_GPSlatitude_Est_count","canMessageName":"TEL_GPS_Position","startBit":7,"realStartBit":0,"bitLength":27,"bigIndian":true,"signedInt":false,"multiplier":0.000001,"offset":0,"min":0,"max":134.217727,"unit":"count","receivingNode":"TST","comment":null,"values":{}},"CNS_TEL_GPSlongitude_Est_count":{"name":"CNS_TEL_GPSlongitude_Est_count","canMessageName":"TEL_GPS_Position","startBit":27,"realStartBit":28,"bitLength":28,"bigIndian":true,"signedInt":false,"multiplier":0.000001,"offset":0,"min":0,"max":268.435455,"unit":"count","receivingNode":"TST","comment":null,"values":{}},"CNS_TEL_GPSlongitudeDirn_Est_B":{"name":"CNS_TEL_GPSlongitudeDirn_Est_B","canMessageName":"TEL_GPS_Position","startBit":63,"realStartBit":56,"bitLength":1,"bigIndian":true,"signedInt":false,"multiplier":1,"offset":0,"min":0,"max":1,"unit":"B","receivingNode":"TST","comment":null,"values":{}},"CNS_TEL_GPSlatitudeDirn_Est_B":{"name":"CNS_TEL_GPSlatitudeDirn_Est_B","canMessageName":"TEL_GPS_Position","startBit":28,"realStartBit":27,"bitLength":1,"bigIndian":true,"signedInt":false,"multiplier":1,"offset":0,"min":0,"max":1,"unit":"B","receivingNode":"TST","comment":null,"values":{}}}}}};
// var dbc = new DBC(undefined, dbcJson);
// console.log(dbc.parse("46F", "2387AB24661D1700"));
