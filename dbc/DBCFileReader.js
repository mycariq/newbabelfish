var fs = require("fs");

var DBCFileReader = function (dbcFile) {
    this.lines = fs.readFileSync(dbcFile, 'utf-8').split('\n').filter(Boolean);
    this.index = 0;
};

DBCFileReader.prototype = {
    hasNext: function () {
        return (this.index < this.lines.length);
    },
    nextLine: function () {
        //if empty line then return next line
        if (this.hasNext())
            return this.lines[this.index];
    },
    commitLine: function commitLine() {
        this.index++;
    }
}

module.exports = DBCFileReader;