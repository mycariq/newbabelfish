function splitBySpace(text) {
    var splitted = [];
    var match = text.match(/(?:[^\s"]+|"[^"]*")+/g);
    if (match)
        match.forEach(element => {
            splitted.push(element.replace(/^"/, "").replace(/"$/, "").replace(/";$/, ""));
        });

    return splitted;
}

exports.splitBySpace = splitBySpace;