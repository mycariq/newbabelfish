var exprUtils = require("./ExprUtils");

class AND {
    constructor(expr1, expr2) {
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    evaluate(json) {
        return this.expr1.evaluate(json) && this.expr2.evaluate(json);
    }

    fromJson(json) {
        this.expr1 = exprUtils.parseExpr(json.expr1);
        this.expr2 = exprUtils.parseExpr(json.expr2);
    }

    toJson() {
        return {
            type: "AND",
            expr1: this.expr1.toJson(),
            expr2: this.expr2.toJson()
        }
    }
}

module.exports = AND;