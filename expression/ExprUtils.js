var LT = require("./LT");
var LTE = require("./LTE");
var GT = require("./GT");
var GTE = require("./GTE");
var EQ = require("./EQ");
var NEQ = require("./NEQ");
var IN = require("./IN");
var LIKE = require("./LIKE");
var AND = require("./AND");
var OR = require("./OR");

function parseExpr(json) {
    if (!json)
        return;

    var expr;
    switch (json.type) {
        case "LT":
            expr = new LT();
            break;
        case "LTE":
            expr = new LTE();
            break;
        case "GT":
            expr = new GT();
            break;
        case "GTE":
            expr = new GTE();
            break;
        case "EQ":
            expr = new EQ();
            break;
        case "NEQ":
            expr = new NEQ();
            break;
        case "IN":
            expr = new IN();
            break;
        case "LIKE":
            expr = new LIKE();
            break;
        case "AND":
            expr = new AND();
            break;
        case "OR":
            expr = new OR();
            break;
        default:
            break;
    }

    if (expr)
        expr.fromJson(json);

    return expr;
}


exports.parseExpr = parseExpr;


// var exprJson = { "type": "AND", "expr1": { "type": "EQ", "param": { "message": "TEL_GPS_Position", "signal": "CNS_TEL_GPSlatitude_Est_count" }, "value": "10" }, "expr2": { "type": "AND", "expr1": { "type": "LT", "param": { "message": "TEL_GPS_Position", "signal": "CNS_TEL_GPSlongitude_Est_count" }, "value": "50" }, "expr2": { "type": "GT_EQ", "param": { "message": "TEL_GPS_Position", "signal": "CNS_TEL_GPSlongitudeDirn_Est_B" }, "value": "5" } } };

// console.log("============ LT ==============")
// exprJson = { type: "LT", param: { message: "m1", signal: "s1" }, value: 10 };
// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 5 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 15 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s2": 5 }));


// console.log("============ LTE ==============")
// exprJson = { type: "LTE", param: { message: "m1", signal: "s1" }, value: 10 };
// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 5 }));
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 10 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 15 }));


// console.log("============ AND ==============")
// //m1_s1<=10
// //AND m1_s2>20
// exprJson = { "type": "AND", "expr1": { "type": "LTE", "param": { "message": "m1", "signal": "s1" }, "value": 10 }, "expr2": { "type": "GT", "param": { "message": "m1", "signal": "s2" }, "value": 20 } }

// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 5, "m1_s2": 30 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 20 }));


// console.log("============ AND -> AND ==============")
// //m1_s2<=10
// //AND m1_s2>20
// //AND m1_s3=15
// exprJson = { "type": "AND", "expr1": { "type": "LTE", "param": { "message": "m1", "signal": "s1" }, "value": 10 }, "expr2": { "type": "AND", "expr1": { "type": "GT", "param": { "message": "m1", "signal": "s2" }, "value": 20 }, "expr2": { "type": "EQ", "param": { "message": "m1", "signal": "s3" }, "value": 15 } } }

// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 5, "m1_s2": 30, "m1_s3": 15 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 20 }));


// console.log("============ AND -> OR ==============")
// //m1_s2<=10
// //AND m1_s2>20
// //OR m1_s3=15
// exprJson = { "type": "AND", "expr1": { "type": "LTE", "param": { "message": "m1", "signal": "s1" }, "value": 10 }, "expr2": { "type": "OR", "expr1": { "type": "GT", "param": { "message": "m1", "signal": "s2" }, "value": 20 }, "expr2": { "type": "EQ", "param": { "message": "m1", "signal": "s3" }, "value": 15 } } }

// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 5, "m1_s2": 0, "m1_s3": 15 }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 20 }));


// console.log("============ LIKE ==============")
// //m1_s1 LIKE 10
// exprJson = exprJson = { type: "LIKE", param: { message: "m1", signal: "s1" }, value: 10 };

// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 201030 }));
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": "abc10xyz" }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": "1abc0xyz" }));


// console.log("============ IN ==============")
// //m1_s1 IN 10 20 30 40 50
// exprJson = exprJson = { type: "IN", param: { message: "m1", signal: "s1" }, value: [10, 20, 30, 40, "50"] };

// var expression = parseExpr(exprJson);
// console.log(expression.toJson());
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 10 }));
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": 30 }));
// console.log("Expected=>true", "Actual=>" + expression.evaluate({ "m1_s1": "50" }));
// console.log("Expected=>false", "Actual=>" + expression.evaluate({ "m1_s1": 50 }));
