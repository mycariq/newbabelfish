class LTE {
    constructor(message, signal, value) {
        this.message = message;
        this.signal = signal;
        this.value = value;
    }

    evaluate(json) {
        var paramName = this.message + "_" + this.signal;
        if (!json[paramName])
            return true;

        return json[paramName] <= this.value;
    }

    fromJson(json) {
        this.message = json.param.message;
        this.signal = json.param.signal;
        this.value = json.value;
    }

    toJson() {
        return {
            type: "LTE",
            param: {
                message: this.message,
                signal: this.signal
            },
            value: this.value
        }
    }
}

module.exports = LTE;