//'use strict';
const
	winston = require('winston');
const
	fs = require('fs');
var dateFormat = require('dateformat');

// const env = process.env.NODE_ENV || 'development';
const
	BASE_PATH = "/opt/log/forever/";

function getLogger(directory, filename) {

	// console.log(directory , filename);
	var logDir = BASE_PATH + directory + "/";
	var myFileName = logDir + "-" + filename;
	console.log("LogFileName=" + myFileName);

	// Create the log directory if it does not exist
	if (!fs.existsSync(logDir)) {
		fs.mkdirSync(logDir);
	}

	var logger = new (winston.Logger)({
		transports: [
			// colorize the output to the console
			new (winston.transports.Console)({
				timestamp: getTimeStamp,
				datePattern: 'yyyy-MM-dd',
				colorize: true,
				level: 'debug',
				formatter: customFileFormatter,
				json: false
			}), new (require('winston-daily-rotate-file'))({
				// filename: `${logDir}/-results.log`,
				// filename: `${logDir}/-${filename}`,
				filename: myFileName,
				timestamp: getTimeStamp,
				datePattern: 'yyyy-MM-dd',
				prepend: true,
				level: 'debug',
				formatter: customFileFormatter,
				json: false,
				maxFiles: 10
			})]
	});
	// winston has default console transport thats why it was logging into forever file also
	// so removed console transport
	logger.remove(winston.transports.Console);
	return logger;
}

function customFileFormatter(options) {
	// Return string will be passed to logger.
	return options.timestamp() + ',' + options.level.toUpperCase() + ',' + (undefined !== options.message ? options.message : '')
		+ (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(options.meta) : '');
}
function getTimeStamp() {
	var now = new Date();
	return dateFormat(now, "yyyy-mm-dd HH:MM:ss");
}

exports.getLogger = getLogger;