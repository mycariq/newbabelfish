// import required modules
var config = require("../config").mongo;
var MongoClient = require("./mongo-client");
var logger = require("../logger.js");
var mylogger = logger.getLogger("mongo", "mongo-batch.log");
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;
var mongoThroughputRecord = new ThroughputRecord("mongoThroughputRecord",
	new Date, 0, require("../config").ThroughputCalculator.threshold);
var deasync = require('deasync');

var insertedBatch = 0;
var filledBatch = 0;

// instantiates mongo batch
var MongoBatch = function (name, dbURL, collection) {
	this.name = name;
	this.batchSize = config.batchSize;
	this.mongo = new MongoClient(dbURL).connect();
	this.batch = [];
	this.index = 0;
	this.collection = collection;
}

// define methods
MongoBatch.prototype = {
	add: function (document) {

		if (!document) {
			//mylogger.error("(add)-document is undefined so excluding it.");
			return;
		}

		//mylogger.debug("(add)-added into batch", JSON.stringify(document));
		// add document into batch
		this.batch[this.index++] = document;
		// if batch is full then insert batch in mongo, reset index and batch
		if (this.index >= this.batchSize) {
			filledBatch++;
			mylogger.debug("FILLED-" + this.name + "-BATCH filledBatch:" + filledBatch
				+ " insertedBatch:" + insertedBatch + " batch contains " + this.batch.length + " documents");

			var oldBatch = [];
			for (var i = 0; i < this.batch.length; i++) {
				oldBatch[i] = this.batch[i];
			}

			while((filledBatch - insertedBatch)>=4000){
				mylogger.debug("Reached upper threshold. Waiting for callback from mongo. Diff="+(filledBatch - insertedBatch));
				deasync.sleep(300);
			}

			this.mongo.insertMany(this.collection, oldBatch, function (err, result) {
				insertedBatch++;
				mylogger.debug("INSERTED-BATCH filledBatch:" + filledBatch
					+ " insertedBatch:" + insertedBatch + " batch contains " + oldBatch.length + " documents");
				if (err) {
					mylogger.error("(add)-error while inserting batch:" + err);
					mylogger.error(err);
					throw Error(err);
				}
				for (var i = 0; i < oldBatch.length; i++) {
					//mylogger.debug("(INSERTED)-inserted document", JSON.stringify(oldBatch[i]));
					mongoThroughputRecord.increment();
				}
			});
			this.index = 0;
			this.batch = [];
		}
	}
}

module.exports = MongoBatch;


// var hotBatch = new MongoBatch("HOT", "mongodb://ciq-dev-srv01/current", "geobit");
// setInterval(() => {
// 	for(var i=1; i<1000; i++)
// 	hotBatch.add({"type":"data","subtype":"obd","timestamp":new Date(),"location":{"type":"Point","isReal":true,"coordinates":[74.60385833333333,17.552880000000002]},"speed":153,"rpm":93,"voltage":0.04,"ignition":0,"odometer":28958,"seatBelt":41,"fuelStatus":0,"airbag":0,"parking":0,"lowBeam":0,"highBeam":0,"door":17,"coolantTemparature":50,"waterTemparature":45,"direction":13,"brakeAbuse":0,"starterAbuse":0,"owner":"111111111113","origin":"cindriven-prod-03.internal.cloudapp.net","agent":"CIQ-IoTA4","serverTimestamp":new Date()});
// 	mylogger.debug("Added into batch");
// }, 100);