//import required modules
var utils = require("../utils.js");
var mongodb = require("mongodb");
var logger = require("../logger.js");
var mylogger = logger.getLogger("mongo", "mongo-client.log");

// Constructs mongodb
var MongoDB = function (dbURL) {
	var url = dbURL;
	var error = undefined;
	var cariqdb = undefined;

	// connect to mongodb
	mongodb.MongoClient.connect(url, {
		reconnectTries: 120,
		poolSize: 400,
		reconnectInterval: 1000,
		authSource: 'admin'
	}, function (err, db) {
		if (err) {
			mylogger.error("(MongoClient.connect)-Error in connecting to %s",
				url, err);
			throw err;
		}
		else
			mylogger.info("(MongoClient.connect)-Connected to %s", url);
		error = err;
		cariqdb = db;
	});

	// executes till callback from mongo gets called
	while (!error && !cariqdb) {
		require('deasync').runLoopOnce();
	}
	this.cariqdb = cariqdb;
};

// define methods of mongodb
MongoDB.prototype = {

	// inserts doucment into mongo and calls callback which contains error or
	// result
	insertMany: function (collection, document, callback) {

		if (!document) {
			callback(new Error("document is undefined."), undefined);
		}
		// get collection from db
		var collection = this.cariqdb.collection(collection);

		// insert document into mongo
		collection.insertMany(document, function (err, result) {
			callback(err, result);
		});
	},

	// upates document into mongo
	update: function () {
		mylogger.debug("update insert " + this.cariqdb);
	},

	// removes document from mongo
	remove: function () {
		mylogger.debug("remove insert" + this.cariqdb);
	},

	// fetches result from mongo
	select: function () {
		mylogger.debug("select insert" + this.cariqdb);
	}
}

// constructs MongoClient
var MongoClient = function (dbURL) {
	// creates new instance of mongodb
	this.mongo = new MongoDB(dbURL);
}

// Defines methods of mognoclient
MongoClient.prototype = {

	// returns already created object of mongodb
	connect: function () {
		return this.mongo;
	}
}

// exports MongoClient
module.exports = MongoClient;
