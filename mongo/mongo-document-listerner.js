//import required module
var Consumer = require("../consumer");
var config = require("../config.json").mongo;
var MongoBatch = require("./mongo-batch");
var logger = require("../logger.js");
var mylogger = logger.getLogger("mongo", "mongo-deocument-listener.log");


// to listen on this topic
var topic = config.topic;

// create mongo batch
var batch = new MongoBatch("geobit");

var consumer = new Consumer(topic, function(data) {
	mylogger.log("(Mongo consumer)-received document=", JSON.stringify(data));
	var document = data.document;
	processDocument(document);
});

// validate and insert into mongo
function processDocument(document) {
	// if document is valid then insert into mongo if not then change type of
	// packet and insert into mongo

	// set default parameters to document
	setDefaults(document);

	if (!isValid(document))
		document.type = "error";
	batch.add(document);
}

// sets default parameters to document
function setDefaults(document) {
	document.origin = require("os").hostname();
	document.serverTimestamp = new Date();
}

// validates document
function isValid(document) {
	return true;
}
