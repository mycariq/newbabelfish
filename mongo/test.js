var Producer = require("../producer");
var MongoBatch = require("./mongo-batch");
var mongoBatch = new MongoBatch("geobit");

var inserted = 0;

setInterval(function() {

	var json = {
		timestamp : new Date(),
		location : {
			type : "Point",
			isReal : false,
			coordinates : [ 0, 0 ]
		}
	};
	mongoBatch.add(json);
	inserted++;
	console.log("inserted " + inserted);
}, 3000);