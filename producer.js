var Kafka = require("kafka-node");
var config = require("./config.json");

var kafkaHost = config.kafka.host;
var kafkaClient = new Kafka.KafkaClient({ kafkaHost: kafkaHost, connectRetryOptions: { retries: 4 } });

//Wait until it gets connected to kafka
var isKafkaRunning = false;
kafkaClient.on("connect", function () {
	console.log("Connected to Kafka Server :::" + kafkaHost);
	isKafkaRunning = true;
});

while (!isKafkaRunning) {
	require('deasync').runLoopOnce();
}

// constructor
var Producer = function (topic, partitionerType) {
	this.topic = topic;

	var isMetadataUpdated = false;
	kafkaClient.refreshMetadata([this.topic], function (error) {
		if (error)
			throw error;
		console.log("Refreshed metadata");
		isMetadataUpdated = true;
	})
	while (!isMetadataUpdated) {
		require('deasync').runLoopOnce();
	}

	if (!partitionerType)
		partitionerType = 2;
	this.kafkaProducer = new Kafka.Producer(kafkaClient, {
		partitionerType: partitionerType
	});

	this.kafkaProducer.on("error", function (error) {
		console.error("Kafka Producer Throwed Error", error);
		throw error;
	});
}

Producer.prototype = {

	// produce method
	produce: function (data, callback) {
		var payload = [{ topic: this.topic, messages: data }];

		this.kafkaProducer.send(payload, function (error, result) {
			if (error) {
				console.error("Kafka Producer Throwed Error while sending", error);
				throw error;
			}

			callback(result);
		});
	},

	keyBasedProduce: function (data, key, callback) {

		var payload = [{ topic: this.topic, messages: data, key: key }];

		this.kafkaProducer.send(payload, function (error, result) {
			if (error) {
				console.error("Kafka Producer Throwed Error while sending", error);
				throw error;
			}

			callback(result);
		});
	}
}


module.exports = Producer;