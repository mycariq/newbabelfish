var https = require("https");
var fs = require("fs");
var utils = require("../utils");

// var caPath="/mnt/software/hivemq-ce-2021.3/certs/hivemq-server-cert.pem";
// var keyPath="/mnt/software/hivemq-ce-2021.3/certs/356349280000499-key-new.pem";
// var certPath ="/mnt/software/hivemq-ce-2021.3/certs/356349280000499-cert.pem"

var options = {
    username: "hardware@mycariq.com",
    password: "NJ8pn*#kg@c=*Am4",
    // ca: require("fs").readFileSync(caPath),
    // key: require("fs").readFileSync(keyPath),
    // cert: require("fs").readFileSync(certPath)
}

//#######################################
// Global variable declaration 
//#######################################
var deviceId = "356349280000499";
var interval = 5000;

//#######################################
// MQTT topic declaration 
//#######################################
var checkFotaResTopic = "/rvm/fota/update/check/res/" + deviceId;
var acknowledgeUpdateReqTopic = "/rvm/fota/update/acknowledge/req";
var acknowledgeUpdateResTopic = "/rvm/fota/update/acknowledge/res/" + deviceId;

//#######################################
// MQTT connection subscription
//#######################################
var mqttUrl = "tcp://secure-device.mycariq.com:1883";
var mqttClient = new (require("../MQTTClient"))(mqttUrl, options, packetReceiver);
mqttClient.subscribe(checkFotaResTopic);
mqttClient.subscribe(acknowledgeUpdateResTopic);

// mqttClient.publish("/rvm/fota/update/check/req", deviceId);

//#######################################
// MQTT message callback
//#######################################
function packetReceiver(topic, message) {
    var packet = message.toString("utf-8");

    switch (topic) {
        case checkFotaResTopic:
            processCampaign(message);
            break;
        case acknowledgeUpdateResTopic:
            break;
        default:
            break;
    }
}

//#######################################
// Process campaign
//#######################################
function processCampaign(message) {
    if (message == "NO_UPDATE_AVAILABLE") {
        log("No update available");
        return;
    }

    campaignId = message;
    log("Received campaign : " + campaignId);

    displayBanner("New Update Available");

    startUpdate();
}


function startUpdate() {
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "DOWNLOADING"); }, interval * 1);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "DOWNLOADED"); }, interval * 2);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "INSTALLING"); }, interval * 3);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "INSTALLED"); }, interval * 4);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "RESTARTING"); }, interval * 5);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "RESTARTED"); }, interval * 6);
    setTimeout(() => { acknowledgeUpdate(campaignId, deviceId, "DEPLOYED"); }, interval * 7);
    setTimeout(() => { displayBanner("Update Completed"); }, interval * 8);


}

function acknowledgeUpdate(campaignId, deviceId, status) {
    var packet = deviceId + "," + campaignId + "," + status;
    log("Acknowledging as " + status);
    mqttClient.publish(acknowledgeUpdateReqTopic, packet);
}


function displayBanner(text) {

    require('child_process').exec("echo  \"\033[33;5m$(figlet -w150 " + text + " )\033[0m\"", (err, stdout, stderr) => {
        if (err) {
            return;
        }
        log(stdout);
    });
}


function log(message) {
    console.log(utils.getServerTimeStamp() + " [DEBUG] " + message);
}

// download("35095308", "356349280000499");
