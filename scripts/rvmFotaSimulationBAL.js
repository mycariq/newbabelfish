var https = require("https");
var fs = require("fs");
var xor = require('bitwise-xor');
const deasync = require("deasync");
var sleep = require("deasync");
var utils = require("../utils");

const expectedArgsCount = 2;
const actualArgsCount = (process.argv.length - 2);

if (actualArgsCount < expectedArgsCount) {

    console.info("\n================================== USAGE ======================================");
    console.info("nodejs " + process.argv[1] + " <DeviceId> <Domain>");
    console.info("Domain should be one of (DRIVEN_DEV, VROOM)")
    info();

    doExit();
}

function info(){
    console.info("===============================================================================\n");
    console.info("Required Arguments : " + expectedArgsCount);
    console.info("Provided Arguments : " + actualArgsCount + "\n");

    console.info("1.Device Id : " + process.argv[2]);
    console.info("2.Domain : " + process.argv[3]);
    console.info("===============================================================================\n");
}
info();

var MQTTUrlsJson = {
    DRIVEN_DEV : "tcp://secure-device.mycariq.com:1883", 
    VROOM : "tcp://vroom-babelfish.mycariq.com:1883"
}

var options = {
    username: "hardware@mycariq.com",
    password: "NJ8pn*#kg@c=*Am4"
}

//#######################################
// Global variable declaration 
//#######################################
var deviceId = process.argv[2];
var chunkSize = 10000;
var campaignId = undefined;
var downloadToken = undefined;
var noOfBlocksToDownload = undefined;
var downloadBlockNo = undefined;
var sleepInterval = 5000;
var encodingType = undefined;//"Base64"; 
var checkSumAlgorithm = undefined;//"CRC16"; 
var domainName = process.argv[3];
var mqttUrl = MQTTUrlsJson[domainName]

// validate domain name
if(mqttUrl == null || mqttUrl.length == 0){
    console.info("Invalid Doman Name : "+domainName);
    doExit();
}


//#######################################
// Global Constants
//#######################################

const BASE_64 = "Base64", HEX = "HEX", CRC_16 = "CRC16", XOR = "XOR";

const FCS_START = 0x0000;
const FCSTAB = [0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0,
    0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81,
    0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941,
    0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400,
    0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1,
    0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781,
    0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
    0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01,
    0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0,
    0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080,
    0xE041, 0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740,
    0xA501, 0x65C0, 0x6480, 0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01,
    0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0,
    0x7A80, 0xBA41, 0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580,
    0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
    0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741, 0x5500,
    0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1,
    0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81,
    0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540,
    0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040];

//#######################################
// MQTT topic declaration 
//#######################################
var checkFotaResTopic = "/rvm/fota/update/check/res/" + deviceId;
var acknowledgeUpdateReqTopic = "/rvm/fota/update/acknowledge/req";
var acknowledgeUpdateResTopic = "/rvm/fota/update/acknowledge/res/" + deviceId;
var requestDownloadTokenReqTopic = "/rvm/fota/update/token/req";
var requestDownloadTokenResTopic = "/rvm/fota/update/token/res/" + deviceId;
var userConsentReqTopic = "/rvm/fota/update/userconsent/req";
var userConsentResTopic = "/rvm/fota/update/userconsent/res/" + deviceId;
var chunkDownloadURLReqTopic = "/rvm/fota/update/url/req";
var chunkDownloadURLResTopic = "/rvm/fota/update/url/res/" + deviceId;
var negotiateBlockReqTopic = "/rvm/fota/update/negotiate_blocks/req";
var negotiateBlockResTopic = "/rvm/fota/update/negotiate_blocks/res/" + deviceId;
var configPacketResTopic = "/driven/VIoT/v1/config/res/" + deviceId;
var configPacketAckReqTopic = "/rvm/cota/update/acknowledge/req";


//#######################################
// MQTT connection subscription
//#######################################
//var mqttUrl = "tcp://secure-device.mycariq.com:1883";
var mqttClient = new (require("../MQTTClient"))(mqttUrl, options, packetReceiver);
mqttClient.subscribe(checkFotaResTopic);
mqttClient.subscribe(requestDownloadTokenResTopic);
mqttClient.subscribe(acknowledgeUpdateResTopic);
mqttClient.subscribe(userConsentResTopic);
mqttClient.subscribe(negotiateBlockResTopic);
mqttClient.subscribe(chunkDownloadURLResTopic);
mqttClient.subscribe(configPacketResTopic);

//mqttClient.publish("/rvm/fota/update/check/req", deviceId);

//#######################################
// MQTT message callback
//#######################################
function packetReceiver(topic, message) {
    var packet = message.toString("utf-8");
    
    switch (topic) {
        case checkFotaResTopic:
            processCampaign(message);
            break;
        case requestDownloadTokenResTopic:
            processDownloadToken(message);
            break;
        case negotiateBlockResTopic:
            processBlockNegotiation(message);
            break;
        case chunkDownloadURLResTopic:
            processChunkDownloadURL(message);
            break;
        case userConsentResTopic:
            processUserConsent(message);
            break;
        case acknowledgeUpdateResTopic:
            break;
        case configPacketResTopic:
            processConfigPacket(message);
            break;

        default:
            break;
    }
}

//#######################################
// Process campaign
//#######################################
function processCampaign(message) {
   
    if (message == "NO_UPDATE_AVAILABLE") {
        log("No update available");
        return;
    }

    sleep.sleep(sleepInterval);
    campaignId = message;
    log("Received campaign : " + campaignId);

    displayBanner("New Update Available");

    acknowledgeUpdate(campaignId, deviceId, "FOTA_UPDATE_RECEIVED");

    requestDownloadToken();
}

function processDownloadToken(message) {
    sleep.sleep(sleepInterval);
    log("Received token : " + message);
    acknowledgeUpdate(campaignId, deviceId, "TOKEN_DOWNLOADED");

    downloadToken = message;

    requestBlockNegotiation();
}

function processBlockNegotiation(blocks) {
    sleep.sleep(sleepInterval);
    log("Received block count : " + blocks);
    //acknowledgeUpdate(campaignId, deviceId, "BLOCK_COUNT_NEGOTIATED");

    noOfBlocksToDownload = blocks;

    sleep.sleep(sleepInterval);
    downloadBlockNo = 1;
    requestDownloadURL();
}

function processChunkDownloadURL(downloadURL) {
    sleep.sleep(sleepInterval);
    log("Received downloadURL : " + downloadURL);
    downloadInChunk(downloadURL.toString("utf-8"));
}

function processUserConsent(userConsent) {
    sleep.sleep(sleepInterval);

    log("Received userconsent : " + userConsent);
   // acknowledgeUpdate(campaignId, deviceId, "USER_CONSENT_RECEIVED");

    deploy();
}

function processConfigPacket(message) {
    //var packet = message.toString("hex").toUpperCase();
    var base64Str= message.toString("utf-8");
    var packet = Buffer.from(base64Str, "Base64");
    packet = packet.toString("hex").toUpperCase()

    log("Received config packet :" + packet);

    var campaignIdHex = packet.substr(packet.length - 18, 16);
    var campaignNum = utils.hexToInt(campaignIdHex);

    log("Acknowleding config " + campaignNum + " as DEPLOYED");
    mqttClient.publish(configPacketAckReqTopic, deviceId + "," + campaignNum + ",DEPLOYED");
}

function deploy() {
    sleep.sleep(sleepInterval);
    acknowledgeUpdate(campaignId, deviceId, "DEPLOYED");
    sleep.sleep(sleepInterval);
    doExit();
}

function requestDownloadToken() {
    log("Requesting download token");
    mqttClient.publish(requestDownloadTokenReqTopic, deviceId + "," + campaignId);
}

function requestBlockNegotiation() {
    sleep.sleep(sleepInterval);
    log("Requesting block count negotiation");
    mqttClient.publish(negotiateBlockReqTopic, deviceId + "," + campaignId + "," + chunkSize);
}

function requestDownloadURL() {
    log("Requesting download URL for blockNo:" + downloadBlockNo + " chunkSize:" + chunkSize);
    mqttClient.publish(chunkDownloadURLReqTopic, deviceId + "," + campaignId + "," + downloadBlockNo + "," + chunkSize + "," + HEX + "," + XOR); // encodingType = HEX , checksumAlgo = XOR
    //mqttClient.publish(chunkDownloadURLReqTopic, deviceId + "," + campaignId + "," + downloadBlockNo + "," + chunkSize );
}

function requestUserConsent() {
    sleep.sleep(sleepInterval);
    log("Requesting user consent");
    mqttClient.publish(userConsentReqTopic, deviceId + "," + campaignId);

    //acknowledgeUpdate(campaignId, deviceId, "WAITING_FOR_USER_CONSENT");
}

function acknowledgeUpdate(campaignId, deviceId, status) {
    var packet = deviceId + "," + campaignId + "," + status;
    log("Acknowledging as " + status);
    mqttClient.publish(acknowledgeUpdateReqTopic, packet);
}

function displayBanner(text) {
    require('child_process').exec("echo  \"\033[33;5m$(figlet -w150 " + text + " )\033[0m\"", (err, stdout, stderr) => {
        if (err) {
            return;
        }
        log(stdout);
    });
}

function downloadInChunk(downloadURL) {

    //acknowledgeUpdate(campaignId, deviceId, "DOWNLOADING_BLOCK|" + downloadBlockNo);

    const options = {
        //hostname: 'dev-ecu.mycariq.com',
        rejectUnauthorized: false,
        //  port: 443,
        //path: downloadURL,//"/Cariq/rvm/device/communication/payload/download/chunk/" + campaignId + "/" + deviceId + "/" + offset + "/" + chunkSize,
        headers: {
            Authorization: 'Bearer ' + downloadToken
        }
    }
   
    var urlArray = downloadURL.split("/");
    encodingType = urlArray[urlArray.length-4];
    checkSumAlgorithm = urlArray[urlArray.length-3];
   
    https.get(downloadURL, options, (response) => {

        var buffer = undefined;
        response.on('data', function (chunk) {
            if (buffer == undefined)
                buffer = chunk;
            else
                buffer = Buffer.concat([buffer, chunk], buffer.length + chunk.length);
        });

        response.on('end', function () {
            if (buffer == undefined || buffer.length == 0) {
                log("No chunk available");
                return;
            }

           // acknowledgeUpdate(campaignId, deviceId, "DOWNLOADED_BLOCK|" + downloadBlockNo);

            // based on encoding type 
            var bytes = (encodingType == HEX) ? 2 : 4 ;
            var chunk = buffer.toString(encodingType);
            var actualChecksum = chunk.substring(0, bytes);
            var content = chunk.substring(bytes, chunk.length);

            // validate checksum
            var expectedChecksum = (checkSumAlgorithm == XOR) ? expectedChecksum = xorChkSum(content) : expectedChecksum = crc16ChkSum(content);
            if (actualChecksum != expectedChecksum) {
                acknowledgeUpdate(campaignId, deviceId, "INVALID_CHECKSUM_ALGO");
                return;
            }

            //acknowledgeUpdate(campaignId, deviceId, "CHECKSUM_VERIFIED|" + downloadBlockNo);

            fs.appendFileSync("/tmp/file.zip", Buffer.from(content, "hex"), { flag: (downloadBlockNo == 1) ? "w" : "a" });

            if (downloadBlockNo == noOfBlocksToDownload) {
                log("Full file downloaded");
                acknowledgeUpdate(campaignId, deviceId, "DOWNLOADED");
                requestUserConsent();
                return;
            }

            downloadBlockNo++;
            requestDownloadURL();
        });

    });

}

function xorChkSum(frame) {

    var ckSum = xor(Buffer.from('00', 'hex'), Buffer.from('00', 'hex'))

    for (var i = 0; i < frame.length; i++) {
        var tmp = frame[i] + frame[i + 1];
        i = i + 1;
        ckSum = xor(ckSum, Buffer.from(tmp, 'hex'));
    }
    return ckSum.toString('hex');
}

function crc16ChkSum(bytes) {
    var crc = 0x0000;
    for (var i = 0; i < bytes.length; i++) {
        crc = (crc >> 8) ^ FCSTAB[(crc ^ bytes[i]) & 0xff];
    }

    var hex = utils.intToHexStr(crc);
    hex = utils.addLeadingChars(hex, "0", 4);

    return hex;//convertHexToBytes(hex)
}

function convertHexToBytes(hex) {
    if (hex.length % 2 != 0)
        hex = "0" + hex;
    
   var bytes = Buffer.alloc((hex.length / 2));
    
    for (var i = 0; i < bytes.length; i++) {
        var index = i * 2;
        var num = parseInt(hex.substring(index, index + 2), 16);
        bytes[i] = num;
    }

    return bytes.toString('hex');
}

function log(message) {
    console.log(utils.getServerTimeStamp() + " [DEBUG] " + message);
}

function doExit() {
    process.exit();
}

//download("35095308", "356349280000499");
