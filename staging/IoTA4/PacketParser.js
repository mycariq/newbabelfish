var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("iotA4", "receiver.log");

var parsers = {
    "02": new (require("./OBDParser"))(),
    "03": new (require("./AlertParser"))()
};

'use strict'
class PacketParser {
    constructor() {
    }

    parse(owner, type, subtype, data) {
        //get parser from registry
        var parser = parsers[type];
        if (!parser) {
            logger.info("No parser found for ::" + type + "  data:" + data);
            return;
        }

        //parse data
        var document = parser.parse(subtype, data);
        if (!document)
            return;

        if (!(document instanceof Array))
            document = [document];

        document.forEach(docs => {
            docs.owner = owner;
            helper.addDefaults(docs);

            //persist document
            persist(docs);
        });
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;