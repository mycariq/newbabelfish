var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class TLVParser {
    constructor() {

    }

    parse(tlvs) {
        var document = {};
        Object.keys(tlvs).forEach(key => {
            switch (key) {
                case "0001":
                    document.speed = utils.hexToInt(tlvs[key]);
                    break;

                case "0002":
                    document.rpm = utils.hexToInt(tlvs[key]);
                    break;

                case "0003":
                    document.voltage = parseInt(tlvs[key]) / 100;
                    break;

                case "0004":
                    document.accelerometerX = utils.hexToFloat(tlvs[key]);
                    break;

                case "0005":
                    document.accelerometerY = utils.hexToFloat(tlvs[key]);
                    break;

                case "0006":
                    document.accelerometerZ = utils.hexToFloat(tlvs[key]);
                    break;

                case "0007":
                    var subDoc = JSON.parse(utils.hexToText(tlvs[key]));
                    utils.extendJson(document, subDoc);
                    break;
                
                case "0008":
                    document.isBLEConnected = utils.hexToInt(tlvs[key]);
                    break;

                /*****************************************/
                /************* FICOSA TLVs ***************/
                /*****************************************/
                case "0100":
                    document.ignition = utils.hexToInt(tlvs[key]);
                    break;

                case "0101":
                    document.odometer = utils.hexToInt(tlvs[key]);
                    break;

                case "0102":
                    document.seatBelt = utils.hexToInt(tlvs[key]);
                    break;

                case "0103":
                    document.fuelStatus = utils.hexToInt(tlvs[key]);
                    break;
                case "0104":
                    document.airbag = utils.hexToInt(tlvs[key]);
                    break;

                case "0105":
                    document.parking = utils.hexToInt(tlvs[key]);
                    break;

                case "0106":
                    document.lowBeam = utils.hexToInt(tlvs[key]);
                    break;

                case "0107":
                    document.highBeam = utils.hexToInt(tlvs[key]);
                    break;

                case "0108":
                    document.door = utils.hexToInt(tlvs[key]);
                    break;

                case "0109":
                    document.coolantTemparature = utils.hexToInt(tlvs[key]);
                    break;

                case "010A":
                    document.waterTemparature = utils.hexToInt(tlvs[key]);
                    break;

                case "010B":
                    document.direction = utils.hexToInt(tlvs[key]);
                    break;

                case "010C":
                    document.brakeAbuse = utils.hexToInt(tlvs[key]);
                    break;

                case "010D":
                    document.starterAbuse = utils.hexToInt(tlvs[key]);
                    break;

                /*****************************************/
                /************** REML TLVs ****************/
                /*****************************************/
                case "0200":
                    document.chargeTimeToFull = utils.hexToInt(tlvs[key]);
                    break;

                case "0201":
                    document.combinedSOC = utils.hexToInt(tlvs[key]);
                    break;

                case "0202":
                    document.rpm = utils.hexToSInt(tlvs[key]);
                    break;

                case "0203":
                    document.odometer = utils.hexToInt(tlvs[key]);
                    break;

                case "0204":
                    document.obdspeed = utils.hexToInt(tlvs[key]) * 0.1;
                    break;

                case "0205":
                    document.chargeOn = utils.hexToInt(tlvs[key]);
                    break;

                case "0206":
                    document.highBeam = utils.hexToInt(tlvs[key]);
                    break;

                case "0207":
                    document.lowBeam = utils.hexToInt(tlvs[key]);
                    break;

                case "0208":
                    document.leftIndicator = utils.hexToInt(tlvs[key]);
                    break;

                case "0209":
                    document.rightIndicator = utils.hexToInt(tlvs[key]);
                    break;

                case "020A":
                    document.gpsValid = utils.hexToInt(tlvs[key]);
                    break;

                case "020B":
                    document.keyFobCmd = utils.hexToInt(tlvs[key]);
                    break;

                case "020C":
                    document.regenerationLamp = utils.hexToInt(tlvs[key]);
                    break;

                case "020D":
                    document.totalDriveTime = utils.hexToInt(tlvs[key]);
                    break;

                case "020E":
                    document.lteConnected = utils.hexToInt(tlvs[key]);
                    break;

                case "020F":
                    document.coolantTemparature = utils.hexToSInt(tlvs[key]);
                    break;

                case "0210":
                    document.currentTimeHour = utils.hexToInt(tlvs[key]);
                    break;

                case "0211":
                    document.currentTimeMin = utils.hexToInt(tlvs[key]);
                    break;

                case "0212":
                    document.currentDriveTIme = utils.hexToInt(tlvs[key]) * 0.1;
                    break;

                case "0213":
                    document.clusterDisplay = utils.hexToInt(tlvs[key]);
                    break;


                /*****************************************/
                /************** RIVER TLVs ****************/
                /*****************************************/
                case "0300":
                    document.MOTOR1_DrvMod = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0301":
                    document.MOTOR1_Powrstg = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0302":
                    document.MOTOR1_SwState = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0303":
                    document.MOTOR1_DirTraval = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0304":
                    document.MOTOR1_ThrottlePos = utils.hexToSInt(tlvs[key]) * 1;
                    break;

                case "0305":
                    document.rpm = utils.hexToSInt(tlvs[key]) * 1;
                    break;

                case "0306":
                    document.MOTOR1_DriveManeure = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0307":
                    document.MOTOR1_Derating = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0308":
                    document.MOTOR1_DCCurr = utils.hexToSInt(tlvs[key]) * 1;
                    break;

                case "0309":
                    document.MOTOR2_BatVtg = utils.hexToInt(tlvs[key]) * 0.1;
                    break;

                case "030A":
                    document.MOTOR2_MotTemp = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "030B":
                    document.MOTOR2_MotControllerTemp = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "030C":
                    document.Motor2_DIstate = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "030D":
                    document.Motor2_DOstate = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "030E":
                    document.MOTOR3_ErrCode0 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "030F":
                    document.MOTOR3_ErrCode1 = utils.hexToInt(tlvs[key]) * 1;
                    break;
                case "0310":
                    document.MOTOR3_ErrCode2 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0311":
                    document.MOTOR3_ErrCode3 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0312":
                    document.MOTOR3_ErrCode4 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0313":
                    document.MOTOR3_ErrAction = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0314":
                    document.MOTOR4_MotV1 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0315":
                    document.MOTOR4_MotV2 = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0316":
                    document.MOTOR4_RefMTrq = utils.hexToSInt(tlvs[key]) * 0.1;
                    break;

                case "0317":
                    document.MOTOR4_ActmTrq = utils.hexToSInt(tlvs[key]) * 0.1;
                    break;
                case "0318":
                    document.MOTOR4_SMPstge = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0319":
                    document.MOTOR4_SMSubPStg = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031A":
                    document.MOTOR4_SMTrac = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031B":
                    document.MOTOR4_SMDirTrac = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031C":
                    document.VCU201_BattMode = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031D":
                    document.VCU_vcubyte = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031E":
                    document.VCU_vcupublic = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "031F":
                    document.VCUAUTH_AuthCode = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0320":
                    document.MCU1_mcuvol = utils.hexToInt(tlvs[key]) * 0.01;
                    break;

                case "0321":
                    document.MCU1_CellV14 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0322":
                    document.MCU1_CellV13 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0323":
                    document.MCU1_CellV12 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0324":
                    document.MCU1_CellV11 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0325":
                    document.MCU1_CellV10 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0326":
                    document.MCU1_CellV9 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0327":
                    document.MCU1_CellV8 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0328":
                    document.MCU1_CellV7 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0329":
                    document.MCU1_CellV6 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032A":
                    document.MCU1_CellV5 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032B":
                    document.MCU1_CellV4 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032C":
                    document.MCU1_CellV3 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032D":
                    document.MCU1_CellV2 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032E":
                    document.MCU1_CellV1 = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "032F":
                    document.MCU1_ChrgTimeToFull = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0330":
                    document.MCU1_ChrgVtg = utils.hexToInt(tlvs[key]) * 0.001;
                    break;

                case "0331":
                    document.MCU1_ChrgCurrent = utils.hexToInt(tlvs[key]) * 0.1;
                    break;

                case "0332":
                    document.MCU1_Thermistor3Temp = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0333":
                    document.MCU1_Thermistor2Temp = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0334":
                    document.MCU1_GaugeTemp = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0335":
                    document.MCU1_ThermistorTemp = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0336":
                    document.MCU1_TS3 = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0337":
                    document.MCU1_TS2 = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0338":
                    document.MCU1_TS1 = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "0339":
                    document.MCU1_SOH = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "033A":
                    document.MCU1_ASOC = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "033B":
                    document.MCU1_RSOC = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "033C":
                    document.MCU1_MaxErr = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "033D":
                    document.MCU1_AvgCurr = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "033E":
                    document.MCU1_Curr = utils.hexToSInt(tlvs[key]) * 0.01;
                    break;

                case "033F":
                    document.MCU1_BattMode = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0340":
                    document.MCU1_DummyByte = utils.hexToInt(tlvs[key]) * 1;
                    break;

                case "0341":
                    document.MCU1_PubKey = utils.hexToInt(tlvs[key]) * 1;
                    break;

                default:
                    document[key] = tlvs[key];
                    break;
            }
        });

        return document;
    }
}

module.exports = TLVParser;