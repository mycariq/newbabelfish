var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("iotA4", "receiver.log");
var parser = new (require("./PacketParser"))();
var helper = require("./helper");
var utils = require("../../utils");

var options = {
	username: "hardware@mycariq.com",
	password: "NJ8pn*#kg@c=*Am4",
	ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}


var mqttClient = new (require("../../MQTTClient"))(mqttConfig.url, options, packetReceiver);
mqttClient.subscribe("$share/babelfish//driven/IoTA4/#");

function packetReceiver(topic, message) {
	var packet = message.toString("hex").toUpperCase();
	logger.info("Received topic: " + topic + " packet: " + packet);
	processPacket({}, packet);
}

function processPacket(socket, packet) {

	//Remove escape sequence
	var packet = helper.removeEscapeSequence(packet);

	//Validate packet
	if (!helper.isValidPacket(packet))
		logger.info("Invalid packet: " + packet);

	//Extract owner, type, subtype, content
	var index = 4;
	var ownerLength = utils.hexToInt(packet.substr(2, 2)) * 2;
	var owner = packet.substr(index, ownerLength);
	var contentLength = utils.hexToInt(packet.substr(index += ownerLength, 2)) * 2;
	var type = packet.substr(index += 2, 2);
	var subtype = packet.substr(index += 2, 2);;
	var content = packet.substr(index += 2, contentLength - 4);

	//Process packet
	logger.info("Processing", owner, type, subtype, content);
	parser.parse(owner, type, subtype, content);


	//Send response if any
}

//var obdPacket = "2808123456789012345625020021092305595918311354073512427E00040001013D150002021122000302125000040111000029";
//var alertPacket = "280812345678901234561C030121092305595918311354073512427E0002000101250002021122000029";
//mqttClient.publish("/driven/IoTA4/" + Math.random(), Buffer.from(obdPacket, "hex"));