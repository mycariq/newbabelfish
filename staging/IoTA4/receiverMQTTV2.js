var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("iotA4", "receiver.log");
var parser = new (require("./PacketParser"))();
var helper = require("./helper");
var utils = require("../../utils");

var options = {
	username: "hardware@mycariq.com",
	password: "NJ8pn*#kg@c=*Am4",
	ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}


var mqttClient = new (require("../../MQTTClient"))(mqttConfig.url, options, packetReceiver);
mqttClient.subscribe("$share/babelfish//driven/IoTA4V2/#");

function packetReceiver(topic, message) {
	var packet = message.toString("hex").toUpperCase();
	logger.info("Received topic: " + topic + " packet: " + packet);

	var packets = packet.replace(/2928/g, "29|28").split("|");
	packets.forEach(packet => {
		logger.info("Processing splitted packet = " + packet);
		processPacket({}, packet);
	});
}

function processPacket(socket, packet) {

	//Remove escape sequence
	var packet = helper.removeEscapeSequence(packet);

	//Validate packet
	if (!helper.isValidPacket(packet))
		logger.info("Invalid packet: " + packet);

	//Extract owner, type, subtype, content
	var index = 4;
	var ownerLength = utils.hexToInt(packet.substr(2, 2)) * 2;
	var owner = utils.hexToText(packet.substr(index, ownerLength));
	var contentLength = utils.hexToInt(packet.substr(index += ownerLength, 4)) * 2;
	var type = packet.substr(index += 4, 2);
	var subtype = packet.substr(index += 2, 2);;
	var content = packet.substr(index += 2, contentLength - 4);

	//Process packet
	logger.info("Processing", owner, type, subtype, content);
	parser.parse(owner, type, subtype, content);


	//Send response if any
}

// var obdPacket = "280A37333837393732373733002E020022050213110117176683074222758E000100010100000029";
// var alertPacket = "280A37333837393732373733002E020022050213073417177517074224770E000100010100000029";
//mqttClient.publish("/driven/IoTA4V2/" + Math.random(), Buffer.from(obdPacket, "hex"));

//packetReceiver("test", obdPacket);