var Server = require("../../server");
var logger = require("../../logger.js").getLogger("iotA4", "receiver.log");
var parser = new (require("./PacketParser"))();
var helper = require("./helper");
var utils = require("../../utils");

new Server("IoTA4", require("../../config.json").IoTA4V2.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet: " + packet);

	//process packet
	var packets = helper.splitPacket(packet);
	packets.forEach(packet => {
		logger.info("Processing splitted packet = " + packet);
		processPacket({}, packet);
	});

});

function processPacket(socket, packet) {

	//Remove escape sequence
	var packet = helper.removeEscapeSequence(packet);

	//Validate packet
	if (!helper.isValidPacket(packet)) {
		logger.info("Invalid packet: " + packet);
		return;
	}

	//Extract owner, type, subtype, content
	var index = 4;
	var ownerLength = utils.hexToInt(packet.substr(2, 2)) * 2;
	var owner = utils.hexToText(packet.substr(index, ownerLength));
	var contentLength = utils.hexToInt(packet.substr(index += ownerLength, 4)) * 2;
	var type = packet.substr(index += 4, 2);
	var subtype = packet.substr(index += 2, 2);;
	var content = packet.substr(index += 2, contentLength - 4);
	logger.info(contentLength)

	//Process packet
	logger.info("Processing", owner, type, subtype, content);
	parser.parse(owner, type, subtype, content);


	//Send response if any
}

//var obdPacket = "2806323939303338002E020022081718134818352485073465011E0001000101000000292806323939303338002E020022081718141618352934073465961E0001000101000000292806323939303338002E020022081718144418353383073466910E0001000101000000292806323939303338002E020022081718151218353832073467860E0001000101000000292806323939303338002E020022081718154018354281073468810E000100010100000029";
//var alertPacket = "280812345678901234661C030121092305595918311354073512427E0002000101250002021122000029";
//processPacket({}, obdPacket);


// 28 head
// 08 owner length
// 1234567890123456 owner
// 1C content length
// 0200 type subtype 
// 211231235959 timestamp 
// 18311354 latitude
// 073512427 longitude
// E gps metadata 1110  <1 located, 0 unlocate><1 N, 0 S><1 E, 0 W><0 GPS located, 1 LBS located>
// 0002 tlv count 
// 0001 01 25 speed tlv
// 0002 02 1122 rpm tlv
// 0000 checksum
// 29 tail