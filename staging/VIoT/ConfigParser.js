var utils = require("../../utils");
var logger = require("../../logger.js").getLogger("VIoT", "receiver.log");
var helper = require("./helper");

'use strict'
class ConfigParser {
    constructor() {
    }

    parse(owner, packet) {
    }

    preparePacket(config) {

        var noOfConfigs = 0;
        var configContent = "";

        config.configParams.forEach(configParam => {
            if (!configParam.isEnabled || configParam.isEnabled == false)
                return;

            var messageId = configParam.cotaParam.canId;
            if (!messageId)
                return;

            configContent += utils.addLeadingChars(messageId, "0", 8);
            var frequencyHex = utils.intToHexStr(configParam.frequency);
            configContent += utils.addLeadingChars(frequencyHex, "0", 6);

            noOfConfigs++;
        });

        if (noOfConfigs == 0)
            return;

        configContent = utils.intToHexStr(noOfConfigs) + configContent;

        configContent += utils.intToHexStr(config.dbcIdentifier);// dbc id
        var currentTimeHex = utils.intToHexStr(Math.floor(new Date().getTime() / 1000));
        configContent += utils.addLeadingChars(currentTimeHex, "0", 8);

        configContent += utils.addLeadingChars(utils.intToHexStr(config.campaignId), "0", 16);

        var checksum = helper.computeChecksum(configContent);
        var contentLength = utils.intToHexStr(configContent.length / 2);

        var configPacket = "01" //header id
            + utils.intToHexStr(config.dbcName.length)
            + utils.textToHex(config.dbcName) //dbc version name
            + utils.addLeadingChars(contentLength, "0", 4)
            + configContent
            + checksum;

        return configPacket;
    }
}



var messageIds = {
    ABS_Angle_Information: "12F",
    ABS_MTC_Control_Info: "128",
    ABS_Pressure_Info: "290",
    ABS_Status: "12A",
    ABS_Wheel_and_Display_Speed: "12D",
    ABS_WSS_DTC: "12E",
    ECU_Engine_Data: "540",
    ECU_RPM_and_others: "120",
    ECU_Engine_Torque_Info: "121",
    ECU_Gear_Pos_Clutch_Switch: "129",
    LAS_DRS_TxID1_3D_Sensor: "176",
    LAS_DRS_TxID2_3D_Sensor: "177"
}


module.exports = ConfigParser;