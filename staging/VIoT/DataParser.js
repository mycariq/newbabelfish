var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var logger = require("../../logger.js").getLogger("VIoT", "receiver.log");
var utils = require("../../utils")
var helper = require("./helper");

'use strict'
class DataParser {
    constructor() {
    }

    parse(owner, packet, dbcParamMaps) {
        var documents = {};

        var dbcIdentifier = utils.hexToInt(packet.substr(2, 2));
        var dbcParamMap = dbcParamMaps[dbcIdentifier];

        var offset = 4;
        var packetLength = utils.hexToInt(packet.substr(offset, 4));
        offset += 4;
        var noOfMessages = utils.hexToInt(packet.substr(offset, 2));

        offset += 2;
        for (var i = 1; i <= noOfMessages; i++) {
            var valueLength = utils.hexToInt(packet.substr(offset + 24, 2));
            var ttlvLenght = 24 + 2 + (valueLength * 2);
            var ttlv = packet.substr(offset, ttlvLenght);
            offset += ttlvLenght;

            var ttlvDoc = parseTTLV(ttlv, dbcParamMap);
            addIntoDoc(documents, owner, ttlvDoc);
        }

        //var parsedDocuments = Object.values(documents);
        var keys = Object.keys(documents);
        var parsedDocuments = []
        keys.forEach(function (key) {
            parsedDocuments.push(documents[key]);
        });


        postProcess(parsedDocuments, dbcParamMaps[dbcIdentifier]);
        parsedDocuments = filter(parsedDocuments, dbcParamMaps[dbcIdentifier]);
        persist(parsedDocuments);
    }
}

function parseTTLV(ttlv, dbcParamMap) {
    var dbc;
    if (dbcParamMap) {
        dbc = dbcParamMap.dbc;
    }

    var type = ttlv.substr(0, 8).replace(/^0+/, "");
    var timestamp = new Date(utils.hexToInt(ttlv.substr(8, 16)));
    var valueLength = utils.hexToInt(ttlv.substr(24, 2));
    var value = ttlv.substr(26, valueLength * 2);

    if (isNaN(timestamp.getTime()) == true)
        return;

    var parsedJson = {};
    if (dbc)
        parsedJson = dbc.parse(type, value);
    else
        parsedJson[type] = value;
    parsedJson.timestamp = timestamp;
    return parsedJson;
}

function filter(documents, dbcParamMap) {
    if (!dbcParamMap || !dbcParamMap.expression)
        return documents;
    var expression = dbcParamMap.expression;

    var filteredDocs = [];

    documents.forEach(document => {
        if (expression.evaluate(document))
            filteredDocs.push(document);
    });

    return filteredDocs;
}

function postProcess(documents, dbcParamMap) {
    if (!dbcParamMap || !dbcParamMap.paramMapping)
        return;
    var paramMapping = dbcParamMap.paramMapping;

    documents.forEach(document => {
        Object.keys(paramMapping).forEach(function (paramMapKey) {
            if (document[paramMapKey]) {
                document[paramMapping[paramMapKey]] = document[paramMapKey];
            }
        });

        if (document.latitude && document.longitude) {
            document.location = utils.getGeoJson(document.latitude, document.longitude, true);
            delete document.latitude;
            delete document.longitude;
        }
    });
}

function addIntoDoc(documents, owner, ttlvDoc) {
    if (!ttlvDoc || !ttlvDoc.timestamp)
        return;

    var timestampKey = ttlvDoc.timestamp.getTime();
    var document = documents[timestampKey];
    if (!document) {
        document = { owner: owner, type: "data", subtype: "obdgps" };
        documents[timestampKey] = document;
    }

    utils.extendJson(document, ttlvDoc);
}

function persist(documents) {
    documents.forEach(function (document) {
        helper.addDefaults(document);
        logger.info("Parsed packet " + JSON.stringify(document));
        kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });
}

module.exports = DataParser;