var utils = require("../../utils");
var origin = require("os").hostname();
var xor = require('bitwise-xor');

function parseOwnerFromTopic(topic) {
    var splitted = topic.split("/");
    return splitted[splitted.length - 1];
}

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-VIoT";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function computeChecksum(hexStr) {

    var checksum = xor(Buffer.from('00', 'hex'), Buffer.from('00', 'hex'))

    for (var i = 0; i < hexStr.length; i++) {
        var byte = hexStr[i] + hexStr[i + 1];
        i = i + 1;
        checksum = xor(checksum, Buffer.from(byte, 'hex'));
    }
    return checksum.toString('hex').toUpperCase();
}


exports.parseOwnerFromTopic = parseOwnerFromTopic;
exports.addDefaults = addDefaults;
exports.computeChecksum = computeChecksum;