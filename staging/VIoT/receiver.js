var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("VIoT", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");
var exprUtils = require("../../expression/ExprUtils");
var DBC = require("../../dbc/DBC");

var dataParser = new (require("./DataParser.js"))();
var configParser = new (require("./ConfigParser"))();

var options = {
    username: "hardware@mycariq.com",
    password: "NJ8pn*#kg@c=*Am4",
    ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}

var mqttClient = new (require("../../MQTTClient"))(mqttConfig.url, options, packetReceiver);

var shareTopic = "$share/babelfish/";
var dataReqTopic = "/driven/VIoT/v1/data/req";
var dataResTopic = "/driven/VIoT/v1/data/res/";
var configReqTopic = "/driven/VIoT/v1/config/req";
var configResTopic = "/driven/VIoT/v1/config/res/";
var configJsonReqTopic = "/driven/VIoT/v1/config/json/req";
var configJsonResTopic = "/driven/VIoT/v1/config/json/res";
var dbcReqTopic = "/rvm/dbc/req";
var dbcResTopic = "/rvm/dbc/res";

var dbcParamMaps = {};

mqttClient.subscribe(shareTopic + dataReqTopic + "/#");
mqttClient.subscribe(shareTopic + configReqTopic + "/#");
mqttClient.subscribe(shareTopic + configJsonResTopic);
mqttClient.subscribe(dbcResTopic);

mqttClient.publish(dbcReqTopic, "");

function packetReceiver(topic, message) {
    console.log("Received ", topic, message);

    if (topic.startsWith(dataReqTopic))
        processDataReq(topic, message);

    if (topic.startsWith(configReqTopic))
        processConfigReq(topic, message);

    if (topic.startsWith(configJsonResTopic))
        processConfigJsonRes(topic, message);

    if (topic.startsWith(dbcResTopic))
        processDBCRes(topic, message);
}

function processDataReq(topic, message) {
    var packet = message.toString("hex").toUpperCase();

    var owner = helper.parseOwnerFromTopic(topic);
    logger.debug("Received data packet owner:" + owner + " packet:" + packet);
    if (isValidDataPacket(packet))
        dataParser.parse(owner, packet, dbcParamMaps);
}

function processConfigReq(topic, message) {
    var packet = message.toString("hex");
    logger.debug("Received config request topic:" + topic + " packet:" + packet);
}

function processConfigJsonRes(topic, message) {
    var packet = message.toString("utf-8");
    logger.debug("Received config json topic:" + topic + " packet:" + packet);

    var config = JSON.parse(packet);
    var owners = config.owners;

    var configPacket = configParser.preparePacket(config);
    if (!configPacket)
        return;

    logger.debug("Prepared config packet owners:" + owners + " packet:" + configPacket.toUpperCase());

    var hexBuf = Buffer.from(configPacket, "hex");
    var base64 = hexBuf.toString("Base64");

    logger.debug("Prepared base64 config packet owners:" + owners + " packet:" + base64);

    owners.forEach(owner => {
        mqttClient.publish(configResTopic + owner, Buffer.from(base64, "utf-8"));
    });

}

function processDBCRes(topic, message) {

    var json = JSON.parse(message);
    logger.debug("Received DBC param mapping dbcIdentifier:" + json.dbcIdentifier + "  current identifiers:" + Object.keys(dbcParamMaps));

    var dbc = new DBC(undefined, json.dbc);
    var conditionalLogging = json.conditionalLogging;
    var expression = exprUtils.parseExpr(conditionalLogging);

    dbcParamMaps[json.dbcIdentifier] = {
        dbcIdentifier: json.dbcIdentifier,
        dbc: dbc,
        paramMapping: transposeMapping(json.paramMapping),
        expression: expression
    }
}

function transposeMapping(paramMapping) {
    if (!paramMapping)
        return;

    var newParamMapping = {};
    Object.keys(paramMapping).forEach(function (key) {
        var valJson = paramMapping[key];
        newParamMapping[valJson["message"] + "_" + valJson["signal"]] = key;
    });

    return newParamMapping;
}

function isValidDataPacket(packet) {
    return true;
}



var packet = "";
packet += "01FF000003";
packet += "0000012D" + "00000185395CA8E0" + "08" + "FFFFFFFFFFFFFFFF";
packet += "00000128" + "00000185395CA8E0" + "08" + "FFFFFFFFFFFFFFFF";
packet += "0000046F" + "00000185395CA8F1" + "08" + "2387AB24661D1700";
packet += "00000000";


var config = {
    "owners": ["356349280000499"],
    "campaignId": 35192909,
    "dbcName": "BAL_TEST",
    "dbcIdentifier": 1,
    "configParams":
        [
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "12A", "name": "ABS_Status" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "12D", "name": "ABS_Wheel_and_Display_Speed" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "5A0", "name": "ABS_Feature_Control" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "12E", "name": "ABS_WSS_DTC" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "12F", "name": "ABS_Angle_Information" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "5A2", "name": "CCU_Feature_Control" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "4E8", "name": "ECU_Handshake" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "4E9", "name": "IMMO_Handshake" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "290", "name": "ABS_Pressure_Info" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "6A6", "name": "CCU_TbT_Navigation" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "6A7", "name": "DASH_UC_CONN" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "6A8", "name": "CCU_UC_CONN" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "450", "name": "DASH_Meter_Info" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "4EA", "name": "IMMO_Status" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "176", "name": "LAS_DRS_TxlD1_3D_Sensor" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "551", "name": "DASH_Driv_Sens_Req" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "177", "name": "LAS_DRS_TxlD2_3D_Sensor" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "55A", "name": "DASH_Bike_Info" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "5B0", "name": "ECU_Feature_Control" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "6B9", "name": "CCU_Status" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "120", "name": "ECU_Engine_RPM_and_others" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "560", "name": "HCU_Heating" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "121", "name": "ECU_Engine_Torque_Info" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "540", "name": "ECU_Engine_Data" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "541", "name": "DASH_Engine_Data" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "128", "name": "ABS_MTC_Control_Info" } },
            { "frequency": 15000, "isEnabled": true, "cotaParam": { "canId": "129", "name": "ECU_Gear_Pos_Clutch_Switch" } }]
};

// setInterval(() => {
    // mqttClient.publish(dataReqTopic + "/123456789", Buffer.from(packet, "hex"));
// }, 5000);

// mqttClient.publish(configJsonResTopic, Buffer.from(JSON.stringify(config), "utf-8"));
