var utils = require("../../utils");
var helper = require("./helper");
var tlvParser = new (require("./TLVParser"))();

'use strict'
class OBDParser {
    constructor() {

    }

    parse(subtype, packet) {
        var document = { type: "data", subtype: "obd" };
        var index = 0;
        document.timestamp = helper.parseTimestamp(packet.substr(index, 12));
        document.location = helper.parseLocation(packet.substr(index += 12, 18));

        var tlvCount = utils.hexToInt(packet.substr(index += 18, 4));
        var tlvs = helper.parseTLVs(tlvCount, packet.substr(index += 4));

        utils.extendJson(document,tlvParser.parse(tlvs));
        return document;
    }
}

module.exports = OBDParser;