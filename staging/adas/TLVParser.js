var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class TLVParser {
    constructor() {

    }

    parse(tlvs) {
        var document = {};
        Object.keys(tlvs).forEach(key => {
            switch (key) {
                case "0001":
                    document.speed = utils.hexToInt(tlvs[key]);
                    break;

                case "0002":
                    document.rpm = utils.hexToInt(tlvs[key]);
                    break;

                case "0003":
                    document.voltage = parseInt(tlvs[key]) / 100;
                    break;

                case "0004":
                    document.xTimestamp = utils.hexToInt(tlvs[key]);
                    break;

                case "0005":
                    document.fcw = utils.hexToInt(tlvs[key]);
                    break;

                case "0006":
                    document.objectID = utils.hexToInt(tlvs[key]);
                    break;

                case "0007":
                    document.distance = utils.hexToFloat(tlvs[key]);
                    break;

                case "0008":
                    document.fcwLevel = utils.hexToInt(tlvs[key]);
                    break;

                case "0009":
                    document.ttc = utils.hexToFloat(tlvs[key]);
                    break;

                case "000A":
                    document.egoVelocity = utils.hexToFloat(tlvs[key]);
                    break;

                case "000B":
                    document.targetVelocity = utils.hexToFloat(tlvs[key]);
                    break;

                case "000C":
                    document.relativeVelocity = utils.hexToFloat(tlvs[key]);
                    break;

                case "000D":
                    document.ldw = utils.hexToInt(tlvs[key]);
                    break;

                case "000E":
                    document.coordinatesX = utils.hexToFloat(tlvs[key]);
                    break;

                case "000F":
                    document.coordinatesY = utils.hexToFloat(tlvs[key]);
                    break;

                case "0010":
                    document.ldwLevel = utils.hexToInt(tlvs[key]);
                    break;

                case "0011":
                    document.drowsiness = utils.hexToFloat(tlvs[key]);
                    break;

                case "0012":
                    document.combi1 = utils.hexToInt(tlvs[key]);
                    break;

                case "0013":
                    document.combi2 = utils.hexToInt(tlvs[key]);
                    break;

                case "0014":
                    document.combiType = utils.hexToInt(tlvs[key]);
                    break;

                case "0015":
                    document.warnType = utils.hexToInt(tlvs[key]);
                    break;

                case "0016":
                    document.level = utils.hexToInt(tlvs[key]);
                    break;

                case "0017":
                    document.distraction = utils.hexToFloat(tlvs[key]);
                    break;

                case "0018":
                    document.rollingStop = utils.hexToInt(tlvs[key]);
                    break;

                case "0019":
                    document.ldwSide = utils.hexToInt(tlvs[key]);
                    break;

                case "001A":
                    document.driverID = utils.hexToInt(tlvs[key]);
                    break;

                case "001B":
                    document.genericInt32 = utils.hexToSInt(tlvs[key]);
                    break;

                case "001C":
                    document.genericFloat32 = utils.hexToFloat(tlvs[key]);
                    break;

                case "001D":
                    document.genericUint32 = utils.hexToInt(tlvs[key]);
                    break;

                default:
                    document[key] = tlvs[key];
                    break;
            }
        });

        return document;
    }
}

module.exports = TLVParser;