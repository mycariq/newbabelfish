var utils = require("../../utils");
var origin = require("os").hostname();

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-IoTA4";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function isValidPacket(packet) {
    //Check head and tail. If not 28, 29 then return false
    var head = packet.substr(0, 2);
    var tail = packet.substr(packet.length - 2);
    if ("28" != head || "29" != tail)
        return false;

    //If checksum is not matching then return false
    //Finally return true
    return true;
}

function removeEscapeSequence(packet) {
    /* If there is no 3D in packet, return it no need to process for escape */
    if (packet.indexOf("3D") < 0)
        return packet;

    var escapePacket = "";

    for (var i = 0; i < packet.length; i += 2) {
        if (packet.substr(i, 2) === "3D") {
            escapePacket += utils.xorHex(packet.substr(i, 2), packet.substr(i + 2, 2));
            i += 2;
        } else
            escapePacket += packet.substr(i, 2);
    }
    return escapePacket.toUpperCase();
}

function parseTimestamp(timestamp) {
    return new Date("20" + timestamp.substr(0, 2) + "-" + timestamp.substr(2, 2) + "-"
        + timestamp.substr(4, 2) + " " + timestamp.substr(6, 2) + ":"
        + timestamp.substr(8, 2) + ":" + timestamp.substr(10, 2)) + " UTC";
}

function parseLocation(locationStr) {
    var direction = utils.hexToBin(locationStr.substr(17, 1));
    var latDirection = (direction.substr(2, 1) === "1") ? "N" : "S";
    var longDirection = (direction.substr(1, 1) === "1") ? "E" : "W";

    var latitude = parseLatitude((locationStr.substr(0, 4) + "." + locationStr.substr(4, 4))
        + latDirection);
    var longitude = parseLongitude((locationStr.substr(8, 5) + "." + locationStr.substr(13, 4))
        + longDirection);

    return utils.getGeoJson(latitude, longitude, true);
}

function parseLatitude(packet) {
    var direction = packet.substr(packet.length - 1, 1);
    var degree = parseInt(packet.substr(0, 2));
    var remaining = parseFloat(packet.substr(2, packet.length - 3)) / 60;

    var latitude = degree + remaining;
    if (direction == 'S')
        latitude *= (-1);

    return latitude;

}

function parseLongitude(packet) {
    var direction = packet.substr(packet.length - 1, 1);
    var degree = parseInt(packet.substr(0, 3));
    var remaining = parseFloat(packet.substr(3, packet.length - 4)) / 60;

    var longitude = degree + remaining;
    if (direction == 'W')
        longitude *= (-1);

    return longitude;
}

function parseTLVs(tlvCount, packet) {
    var tlvs = {};
    var index = 0;
    for (var i = 1; i <= tlvCount; i++) {
        var type = packet.substr(index, 4);
        var length = utils.hexToInt(packet.substr(index += 4, 2)) * 2;
        var value = packet.substr(index += 2, length);
        tlvs[type] = value;
        index += length;
    }
    return tlvs;
}


exports.addDefaults = addDefaults;
exports.isValidPacket = isValidPacket;
exports.removeEscapeSequence = removeEscapeSequence;
exports.parseTimestamp = parseTimestamp;
exports.parseLocation = parseLocation;
exports.parseTLVs = parseTLVs;