var helper=require("./helper");
var utils = require("../../utils");

var logger = require("../../logger.js").getLogger("autocop", "receiver.log");

'use strict';
class DataFrameProcessor{
	constructor(){
	}
	
	process(frame){
		logger.info("processing:"+frame);
		var packets = frame.split(",");
		var document = {};
		document.owner = packets[1];
		document.timestamp =helper.getTimestamp(packets[2], packets[3]);
		document.location = utils.getGeoJson(parseFloat(packets[4]),parseFloat(packets[5]),true);
		document.type="data";
		document.subtype="gps";
		document.speed =  parseFloat(packets[6]);
		document.direction = parseInt(packets[7]);
		document.altitude = parseInt(packets[8]);
		document.noOfSatelites = parseInt(packets[9]);
		document.gpsStatus = parseInt(packets[10]);
		document.gpsSignalStrength = parseInt(packets[11]);
		document.digitalInputStatus = packets[12];
		document.digitalOutputStatus = parseInt(packets[13]);
		document.analogInputValue = parseFloat(packets[14]);
		document.voltage = parseFloat(packets[15]);
		document.internalBatteryVoltage = parseFloat(packets[16]);
		document.pcbTemperature = parseFloat(packets[17]);
		document.reserved = packets[18];
		document.movingStatusFromMSensor = packets[19];
		document.accelGValue = parseFloat(packets[20]);
		document.tiltValue = packets[21];
		document.tripIndication = packets[22];
		document.virtualOdoMeter = parseInt(packets[23]);
		document.serialNo = parseInt(packets[24]);
		document.checksum = packets[25];
		return document;
	}
}; 

module.exports=DataFrameProcessor;