var dataFrameProcessor=new (require("./DataFrameProcessor"))();
var alertFrameProcessor=new (require("./AlertFrameProcessor"))();

var logger = require("../../logger.js").getLogger("autocop", "receiver.log");

'use strict';
class FrameProcessor{
	
	constructor(){
	}
	
	process(frame){
		var document=undefined;
		
		if(frame && frame.length >3){
			logger.info("Processing frame:"+frame);
			frame=frame.substring(1,frame.length-1);

			switch(frame.split(",")[0]){
				case "TLN":
					document = dataFrameProcessor.process(frame);
					break;
	
				case "TLB":
					document = dataFrameProcessor.process(frame);
					break;
					
				case "TLA":
					document = alertFrameProcessor.process(frame);
					break;
					
				case "TLL":
					document = alertFrameProcessor.process(frame);
					break;
					
				default:
					logger.info("Processor not found for this frame...");
			}
		}
		else{
			logger.info("Failed to process frame coz of either it is undefined or has less chars...");
		}
		return document;
	}
}

module.exports=FrameProcessor;