function getTimestamp(dateStr, timeStr) {
	return new Date("20" + dateStr.substr(4, 2) + "-" + dateStr.substr(2, 2) + "-" + dateStr.substr(0, 2) + "T" + timeStr.substr(0, 2) + ":" + timeStr.substr(2, 2) + ":" + timeStr.substr(4, 2));
}

exports.getTimestamp = getTimestamp;