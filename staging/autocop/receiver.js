var logger = require("../../logger.js").getLogger("autocop", "receiver.log");
var utils = require("../../utils");

var Server = require("../../server");
var frameProcessor = new (require("./FrameProcessor"))();

var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

var feedbackSender = require("./FeedbackSender");

new Server("autocop", require("../../config.json").autocop.port).start(function (err, data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info("Received:" + data);
	//split multiple frames
	var frames = data.toString().split(";");
	var frameCount = frames.length - 1;

	//iterate over frames and process each frame
	for (var i = 0; i < frameCount; i++) {
		processFrame(frames[i], socket);
	}

	//create response for acknowledgement write into socket
	//var response = "@O#" + (frameCount) + "$";
	var response = "@O#OK$";
	logger.info("Responding:" + response);
	socket.write(response);
});

function processFrame(frame, socket) {
	var document = frameProcessor.process(frame);
	if (!document) return;
	if (document instanceof Array) {
		document.forEach(function (doc) {
			putMetadata(doc);
			feedbackSender.send(doc.owner, socket);
			kafkaProducer.keyBasedProduce(JSON.stringify(doc), doc.owner, function (result) {
				logger.info("KafkaResult:" + JSON.stringify(result));
			});
		});
	} else {
		putMetadata(document);
		feedbackSender.send(document.owner, socket);
		kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
			logger.info("KafkaResult:" + JSON.stringify(result));
		});
	}
	logger.info("Processed: " + JSON.stringify(document));
}

function putMetadata(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "Autocop";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = utils.getGeoJson(0.0, 0.0, false);
		}
	}
}


//var alertPacket="(TLA,355234057855757,260619,113719,+13.674950,+79.502624,0,0,126,15,1,5,100,00,0.0,24.62,8.46,38.75,0.0,0,1.00,-71,0,1,0,0,0,0,000000000,0,0,0,0,0,0,0,1,0000000,0,66,104)";
//processFrame(alertPacket,{});