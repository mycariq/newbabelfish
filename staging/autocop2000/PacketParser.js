var utils = require("../../utils");
'use strict'
class PacketParser{
	constructor(){
	}
	
	parse(imei, timestamp, serialNo, packet,monitoring){
		var documents=[];
		
		var dataDocument = {};
		dataDocument.owner=imei;
		dataDocument.type="data";
		dataDocument.subtype="obdgps";
		dataDocument.timestamp=timestamp;
		dataDocument.serialNo=serialNo;
		dataDocument.smsCount=utils.hexToInt(packet.substr(0,3));
		dataDocument.voltage=utils.hexToInt(packet.substr(3,3))/100;
		dataDocument.internalVoltage=utils.hexToInt(packet.substr(6,3))/100;
		dataDocument.externalAnalogVoltage=utils.hexToInt(packet.substr(9,3))/100;
		dataDocument.direction=utils.hexToInt(packet.substr(12,3));
		dataDocument.location=utils.getGeoJson(utils.hexToFloat(packet.substr(15,8)),utils.hexToFloat(packet.substr(23,8)),true);
		dataDocument.speed=utils.hexToInt(packet.substr(31,2));
		dataDocument.distance=utils.hexToInt(packet.substr(33,6));
		dataDocument.averageGValue=utils.hexToInt(packet.substr(39,2));
		dataDocument.accelerationX=utils.hexToInt(packet.substr(41,2));
		dataDocument.accelerationY=utils.hexToInt(packet.substr(43,2));
		dataDocument.accelerationZ=utils.hexToInt(packet.substr(45,2));
		parseCanMessages(dataDocument,packet.substr(47));
		require("./helper").addDefaults(dataDocument);
		documents.push(dataDocument);
		
		documents.push(...parseMonitoring(monitoring,dataDocument));
		
		return documents;
	}
}

function createBaseDocument(dataDocument){
	var document={};
	document.owner = dataDocument.owner;
	document.timestamp = dataDocument.timestamp;
	document.serialNo = dataDocument.serialNo;
	document.location = dataDocument.location;
	require("./helper").addDefaults(document);
	return document;
}

function parseMonitoring(monitoring,dataDocument){
	var documents=[];
	var binary=utils.hexToBin(monitoring);
	
	var statusDoc=utils.extendJson(createBaseDocument(dataDocument),{"type":"data","subtype":"tlc200Status"});
	
	dataDocument.backlog=(binary[0]=='0');
	statusDoc.gpsStatus=(binary[1]=='1')?"fixed":"notFixed";
	statusDoc.ignitionStatus=(binary[4]=='1')?"on":"otherwise";
	//plug in reminder alert
	if(binary[7]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"plugInReminder"}));
	statusDoc.digitalInputStatus=(binary[8]=='1')?"active":"inactive";
	//tampering alert
	if(binary[11]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"tampering"}));
	//gps module fault alert
	if(binary[14]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"gpsModuleFault"}));
	//towing alert
	if(binary[15]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"towing"}));
	//pull out reminder alert
	if(binary[16]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"pullOutReminder"}));
	//low battery alert
	if(binary[17]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"lowBatteryVoltage"}));
	//server not reachable alert
	if(binary[18]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"serverNotReachable"}));
	//suddenSpeededUp alert
	if(binary[19]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"suddenSpeededUp"}));
	//rapidDeceleration alert
	if(binary[20]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"rapidDeceleration"}));
	//sharpTurn alert
	if(binary[21]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"sharpTurn"}));
	//highRpm alert
	if(binary[22]=='1')
		documents.push(utils.extendJson(createBaseDocument(dataDocument),{"type":"alert","subtype":"highRpm"}));
	statusDoc.sleepModeStatus=binary[24]+binary[25];
	statusDoc.idlingStatus=(binary[26]=='1')?"M-Sensor has detected motion":"M-Sensor has detected stop";
	statusDoc.digitalOutputStatus=(binary[27]=='1')?"active":"inactive";
	
	documents.push(statusDoc);
	return documents;
}

function parseCanMessages(document,canData){
	document.totalCanMsgs=utils.hexToInt(canData.substr(0,2));
	for(var index=2,i=1;index<canData.length;i++){
		var canSize=utils.hexToInt(canData.substr(index,2));
		var msg = canData.substr(index,canSize*2+2);
		parsePid(document,msg);
		index+=(2+canSize*2	);
	}
}

function parsePid(document,packet){
	console.log(packet);
	var length = utils.hexToInt(packet.substr(0,2))*2;
	if(length != packet.substr(2).length){
		console.log("Can not parse pid ==="+packet);
		return;
	}
	var pid=packet.substr(2,4);
	var value=packet.substr(6);
	
	switch(pid){
		case "4104":
			document.engineLoad=utils.hexToInt(value.substr(0,2))*100/255;
			break;
		case "4105":
			document.coolantTemparature=utils.hexToInt(value.substr(0,2))-40;
			break;
		case "410C":
			document.rpm=((utils.hexToInt(value.substr(0,2))*256)+utils.hexToInt(value.substr(2,2)))/4;
			break;
		case "4110":
			document.mafAirFlowRate=((utils.hexToInt(value.substr(0,2))*256)+utils.hexToInt(value.substr(2,2)))/100;
			break;
		case "4111":
			document.throttlePosition=utils.hexToInt(value.substr(0,2))*100/255;
			break;
		case "411F":
			document.runTimeSinceEngineStart=(utils.hexToInt(value.substr(0,2))*256)+utils.hexToInt(value.substr(2,2));
			break;
		case "412F":
			document.fuelLevelInput=utils.hexToInt(value.substr(0,2))*100/255;
			break;
		case "4131":
			document.distanceTraveledSinceCodesCleared=(utils.hexToInt(value.substr(0,2))*256)+utils.hexToInt(value.substr(2,2));
			break;
		default:	
			if(document.yetToImplement)
				document.yetToImplement=document.yetToImplement+"|"+pid+value;
			else
				document.yetToImplement=pid+value;
	}
}

module.exports=PacketParser;