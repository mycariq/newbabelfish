var packetParser =new (require("./PacketParser"))();
var helper = require("../autocop/helper");
var utils = require("../../utils");

'use strict'
class PacketProcessor{
	constructor(){
	}
	process(imei,packet){
		var timestamp=helper.getTimestamp(packet.substr(2,6),packet.substr(8,6));
		var monitoring=packet.substr(16,8);
		var serialNo=utils.hexToInt(packet.substr(24,3));
		var obdGps=packet.substr(27);
		return packetParser.parse(imei,timestamp,serialNo,obdGps,monitoring);
	}
}

//var documents=new PacketProcessor().process("053454354550","72010116000000020810402000102E50F0000000000000000000000000000000006400FA40140003410D3304410C3344034104330341053300044110334403410B330341113304413133440341003303410D3304410C3344034104330341053300044110334403410B33034111330441313344");
//documents.forEach(function(doc){console.log(JSON.stringify(doc));});
module.exports=PacketProcessor;
//require('sleep').sleep(6000000000000);