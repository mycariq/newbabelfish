function addDefaults(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "Autocop2000";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = require("../../utils").getGeoJson(0.0, 0.0, false);
		}
	}
}

exports.addDefaults = addDefaults;