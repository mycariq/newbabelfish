var Server = require("../../server");
var utils = require("../../utils");
var packetProcessor = new (require("./PacketProcessor"))();
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

var feedbackSender = require("./FeedbackSender");

new Server("autocop2000", require("../../config.json").autocop2000.port).start(function (err, data, id, socket) {
	console.log("=====RAW BUFFER===");
	console.log(data);
	var rawData = data.toString("hex").toUpperCase();
	console.log(new Date, "Received:" + rawData);

	var frame = {
		packets: []
	};
	frame.expectedBytes = utils.hexToInt(rawData.substr(0, 3));
	frame.receivedBytes = (rawData.length - 4) / 2;
	frame.packetCount = utils.hexToInt(rawData.substr(3, 1));
	frame.imei = rawData.substr(4, 16).substr(1, 15);
	frame.packets = getPackets(rawData.substr(20));
	console.log(JSON.stringify(frame));

	processFrame(frame);

	//send feedback to device
	feedbackSender.send(frame.imei, socket);

	//create response for acknowledgement write into socket
	var response = "@O#" + (frame.packetCount) + "$";
	console.log("Responding:" + response);
	socket.write(response);
});

function getPackets(rawPacket) {
	var packets = [];
	for (var index = 0; index < rawPacket.length;) {
		var totalBytes = utils.hexToInt(rawPacket.substr(index, 2));
		packets.push(rawPacket.substr(index, totalBytes * 2 + 2));
		index += totalBytes * 2 + 2;
	}
	return packets;
}

function processFrame(frame) {
	frame.packets.forEach(function (packet) {
		var document = packetProcessor.process(frame.imei, frame.packets[0]);
		persist(document);
	});
}

function persist(document) {
	if (document instanceof Array) {
		document.forEach(function (doc) {
			kafkaProducer.keyBasedProduce(JSON.stringify(doc), doc.owner, function (result) {
				console.info("KafkaResult:" + JSON.stringify(result));
			});
		});
	} else {
		kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
			console.info("KafkaResult:" + JSON.stringify(result));
		});
	}
	console.log("Processed: " + JSON.stringify(document));
}