var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "data", subtype: "gps" };
        document.start = helper.hexToAscii(packet, 0, 2);
        document.packetIdentifier = helper.hexToAscii(packet, 2, 12);
        document.vendorId = helper.hexToAscii(packet, 14, 12);
        document.firmwareVersion = helper.hexToAscii(packet, 26, 12);
        document.registrationNo = helper.hexToAscii(packet, 38, 30);
        document.imei = helper.hexToAscii(packet, 68, 30);
        document.owner = document.imei;
        document.operatorName = helper.hexToAscii(packet, 98, 24);
        document.packetType = helper.hexToAscii(packet, 122, 4);
        document.packetStatus = helper.hexToAscii(packet, 126, 2);
        document.tamperAlert = helper.hexToAscii(packet, 128, 2);
        document.lac = packet.substr(130, 4);
        document.cellId = packet.substr(134, 4);
        document.nmr = packet.substr(138, 32);
        document.alertBytes = helper.hexToAscii(packet, 170, 6);
        document.timestamp = helper.parseTimestamp(packet.substr(176, 12));
        document.location = packet.substr(188, 20);
        document.speed = utils.hexToInt(packet.substr(208, 4)) / 10;
        document.heading = packet.substr(212, 4);
        document.noOfSattelite = packet.substr(216, 2);
        document.altitude = packet.substr(218, 4);
        document.pdop = packet.substr(222, 4);
        document.hdop = packet.substr(226, 4);
        document.status = packet.substr(230, 2);
        document.voltage = packet.substr(232, 4);
        document.internalBatteryVoltage = packet.substr(236, 2);
        document.adc1 = packet.substr(238, 4);
        document.adc2 = packet.substr(242, 4);
        document.gsmSignalStrength = packet.substr(246, 2);
        document.mcc = packet.substr(248, 4);
        document.mnc = packet.substr(252, 4);
        document.digitalInputStatus = packet.substr(256, 2);
        document.driveBehaviour = packet.substr(258, 2);
        document.alertId = packet.substr(260, 2);
        document.frameNo = packet.substr(262, 6);
        document.distanceTravelled = packet.substr(268, 6);
        document.smsCount = packet.substr(274, 4);
        document.pcbTemperature = packet.substr(278, 4);
        document.bytesReserved = packet.substr(282, 4);
        document.checksum = packet.substr(286, 2);
        document.end = helper.hexToAscii(packet, 288, 2);

        return document;
    }
}

module.exports = GpsParser;