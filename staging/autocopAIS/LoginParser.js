var utils = require("../../utils");
var helper = require("./helper");
'use strict'
class LoginParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "data", subtype: "login" };
        document.timestamp = new Date();
        document.start = helper.hexToAscii(packet, 0, 2);
        document.deviceName = helper.hexToAscii(packet, 2, 30);
        document.imei = helper.hexToAscii(packet, 32, 30);
        document.owner = document.imei;
        document.firmwareVersion = helper.hexToAscii(packet, 62, 12);
        document.protocol = helper.hexToAscii(packet, 74, 12);
        document.location = helper.getLocation(packet, 86, 42);
        document.batteryPercentage = helper.hexToAscii(packet, 128, 4);
        document.lowBatteryThreshold = helper.hexToAscii(packet, 132, 4);
        document.memoryPercentage = helper.hexToAscii(packet, 136, 4);
        document.dataUploadRateIngitionOn = helper.hexToAscii(packet, 140, 4);
        document.dataUploadRateIngitionOff = helper.hexToAscii(packet, 144, 4);
        document.digitalInputStatus = packet.substr(148, 6);
        document.digitalOutputStatus = packet.substr(154, 4);
        document.analogInputStatus = packet.substr(158, 4);

        return document;
    }
}

module.exports = LoginParser;