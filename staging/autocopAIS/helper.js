var utils = require("../../utils");
var origin = require("os").hostname();;

//adds default values to mongo document
function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-Autocop-AIS";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function hexToAscii(packet, startIndex, noOfChars) {
    var substr = packet.substr(startIndex, noOfChars);
    if (!substr) return;

    return utils.hexToText(substr);
}

function getLocation(packet, startIndex, noOfChars) {
    var locString = hexToAscii(packet, startIndex, noOfChars);

    var latDir = locString.substr(0, 1);
    var lat = parseFloat(locString.substr(1, 9));

    var longDir = locString.substr(10, 1);
    var long = parseFloat(locString.substr(11, 10));

    return utils.getGeoJson(lat, long, true);
}

function parseTimestamp(datetime) {
    console.log(datetime)
    var timestampStr = "20" + datetime.substr(4, 2) + "-" + datetime.substr(2, 2) + "-" + datetime.substr(0, 2);
    timestampStr += " " + datetime.substr(6, 2) + ":" + datetime.substr(8, 2) + ":" + datetime.substr(10, 2) + " UTC";
    return new Date(timestampStr);
}

exports.addDefaults = addDefaults;
exports.hexToAscii = hexToAscii;
exports.getLocation = getLocation;
exports.parseTimestamp = parseTimestamp;