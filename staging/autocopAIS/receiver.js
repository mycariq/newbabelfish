var Server = require("../../server");
var utils = require("../../utils");
var packetParser = new (require("./PacketParser"))();
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("autocopAIS", "receiver.log");

new Server("autocopAIS", require("../../config.json").autocopAIS.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info(data);
	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet : " + packet);

	processPacket(socket, packet);
});

function processPacket(socket, packet) {
	var packetType;
	if (packet.length == 162) {
		packetType = "login";
		sendToDevice(socket, "@O#OK$");
	}
	else if (packet.length == 108) {
		packetType = "health";
		sendToDevice(socket, "@O#OK$");
	}
	else if (packet.length == 290) {
		packetType = "gps";
		sendToDevice(socket, "@O#1$");
	}
	else if (packet.length == 314) {
		packetType = "emergency";
		sendToDevice(socket, "@O#OK$");
	}
	else
		packetType = "other";
	logger.info("Packet type:" + packetType + "  packet:" + packet);
	packetParser.parse(packetType, packet);
}

function sendToDevice(socket, packet) {
	if (socket) {
		logger.info("Sending to device : " + packet);
		socket.write(new Buffer(packet, "utf8"));
	}
}

var login = "244D483132484431323334252525252533353233353330383137393735323147315F31314456312E302E304E31382E353335373539453037332E3835313531363030333331323630333030323230303030";
var gps = "24415554434F50617574636F7047315F3131444D483132484431323334252525252533353233353330383137393735323149444541252525252525252554414C4F273F1813273F1812273F0520273F051F273F05214040402801191234173000000000300000000000000000020000270E270E78003F00000000001F013F00163F000000001C000000003B001F01233F2A";
//gps="2448656164657256656E646F7247335F3031412525252525252525252525252525253330303330303330303330303330304944454125252525252525254E524C4F61FDA0C961FD420761FD000061FDFFFF61FD00004040400304181350104E414F515345429B7D5C0064009B0E03B000F2007C7800892900000000190194002D99240700009A00009A008C0019007B6E2A";
//processPacket(undefined, gps);
