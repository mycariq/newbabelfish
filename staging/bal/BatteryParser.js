var helper = require("./helper");

'use strict'
class BatteryParser {
    constructor() {

    }

    parse(data) {
        var document = { owner: data[1], type: "data", subtype: "voltage" };
        document.vendorId = data[2];
        document.vin = data[3];
        document.isBacklog = (data[4] == "H");
        document.publishTimestamp = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.gpsFix = data[9];
        document.location = helper.parseLocation(data[10], data[11], data[12], data[13]);
        document.direction = data[14];
        document.speed = parseFloat(data[15]);
        document.timeToCharge = data[16];
        document.chargeCount = data[17];
        document.cycleType = data[18];
        document.chargerACVoltage = data[19];
        document.chargerDCVoltage = data[20];
        document.chargeDCCurrent = data[21];
        document.stateOfCharge = data[22];
        document.stateOfHelath = data[23];
        document.contractorStatus = data[24];
        document.packVoltage = data[25];
        document.packDischargeCurrent = data[26];
        document.packChargeCurrent = data[27];
        document.healthState = data[28];
        document.cycleCount = data[29];
        document.minTemperature = data[30];
        document.minTemperatureIdentifier = data[31];
        document.maxTemperature = data[32];
        document.maxTemperatureIdentifier = data[33];
        document.minCellVoltage = data[34];
        document.minCellVoltageIdentifier = data[35];
        document.maxCellVoltage = data[36];
        document.maxCellVoltageIdentifier = data[37];
        document.cellVoltageGroup = data[38];
        document.throughPut = data[39];
        document.compressorCurrent = data[40];
        document.condenserCurrent = data[41];
        document.evaporatorCurrent = data[42];
        document.auxBatteryConnectedState = data[43];
        document.auxBatteryVoltage = data[44];
        return document;
    }
}

module.exports = BatteryParser;