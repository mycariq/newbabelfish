var helper = require("./helper");

'use strict'
class ComponentIDParser {
    constructor() {

    }

    parse(data) {
        var document = { owner: data[1], type: "data", subtype: "componentID" };
        document.vendorId = data[2];
        document.vin = data[3];
        document.isBacklog = (data[4] == "H");
        document.publishTimestamp = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.telHW = data[9];
        document.telSW = data[10];
        document.vcuHW = data[11];
        document.vcuSW = data[12];
        document.batteryHW = data[13];
        document.batterySW = data[14];
        document.bmsHW = data[15];
        document.bmsSW = data[16];
        document.mcuHW = data[17];
        document.mcuSW = data[18];
        document.motorHW = data[19];
        document.motorSW = data[20];
        document.esclHW = data[21];
        document.esclSW = data[22];
        document.firmwareVersion = data[23];
        document.vehicleRegistrationNo = data[24];
        return document;
    }
}

module.exports = ComponentIDParser;