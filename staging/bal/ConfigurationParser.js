var helper = require("./helper");

'use strict'
class ConfigurationParser {
    constructor() {

    }

    parse(data) {
        var document = { owner: data[1], type: "data", subtype: "configuration" };
        document.timestamp = helper.parseTimeStamp(data[2], data[3]);
        document.vehicleRegistrationNo = data[4];
        document.timeZoneOffset = data[5];
        document.notMovingLocationPakcetInterval = data[6];
        document.movingLocationPacketInterval = data[7];
        document.internalBatteryLocationPacketInterval = data[8];
        document.vehicleChargingLocationPacketInterval = data[9];
        document.notMovingHealthPakcetInterval = data[10];
        document.movingHealthPacketInterval = data[11];
        document.internalBatteryHealthPacketInterval = data[12];
        document.vehicleChargingHealthPacketInterval = data[13];
        document.notMovingBatteryPakcetInterval = data[14];
        document.movingBatteryPacketInterval = data[15];
        document.internalBatteryBatteryPacketInterval = data[16];
        document.vehicleChargingBatteryPacketInterval = data[17];
        document.sleepModeFrequency = data[18];
        document.reserved = data[19];
        document.overSpeedLimit = data[20];
        document.harshAccelerationThreshold = data[21];
        document.harshBrakingThreshold = data[22];
        document.harshCorneringThreshold = data[23];
        document.errorCodes = data[24];
        document.firmwareVersion = data[25];
        return document;
    }
}

module.exports = ConfigurationParser;