var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class HealthParser {
    constructor() {

    }

    parse(data) {
        var document = { owner: data[1], type: "data", subtype: "heartbeat" };
        document.vendorId = data[2];
        document.vin = data[3];
        document.isBacklog = (data[4] == "H");
        document.publishDate = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.gpsFix = data[9];
        document.location = helper.parseLocation(data[10], data[11], data[12], data[13]);
        document.direction = data[14];
        document.speed = parseFloat(data[15]);
        document.ignitionStatus = data[16];
        document.chargingStatus = data[17];
        document.odometerReading = data[18];
        document.errorCodes = data[19];
        document.internalBatteryState = data[20];
        document.internalBatteryVoltage = data[21];
        document.vehicleMode = data[22];
        document.fobKeyBatteryState = data[23];
        document.dcdcPowerGoodStatus = data[24];
        document.socJumps = data[25];
        document.ambientTemperature = data[26];
        document.energy = data[27];
        document.obdSpeed = data[28];
        document.singleTripDistance = data[29];
        document.distanceToEmpty = data[30];
        document.efficiency = data[31];
        document.motorTemperature = data[32];
        document.mcuTemperature = data[33];
        document.motorCurrent = data[34];
        document.motorVoltage = data[35];
        document.motorTorque = data[36];
        document.rpm = data[37];
        document.brakeOnTime = data[38];
        document.throttlePosition = data[39];
        document.throttleBrakeOverlap = data[40];
        return document;
    }
}

module.exports = HealthParser;