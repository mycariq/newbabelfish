var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LocationParser {
    constructor() {

    }

    parse(data) {
        var index = 1;
        var document = { owner: data[index++] };
        document.vendorId = data[index++];
        document.vin = data[index++];
        document.isBacklog = (data[index++] == "H");
        document.publishDate = helper.parseTimeStamp(data[index++], data[index++]);
        document.timestamp = helper.parseTimeStamp(data[index++], data[index++]);
        addTypeAndSubtype(document, data[index++], data[index++]);
        document.gpsFix = data[index++];
        document.location = helper.parseLocation(data[index++], data[index++], data[index++], data[index++]);
        document.altitude = data[index++];
        document.direction = data[index++];
        document.speed = parseFloat(data[index++]);
        document.signalQuality = data[index++];
        document.signalStrength = data[index++];
        document.gsmConnect = data[index++];
        document.gprsConnect = data[index++];
        document.noOfSatellite = data[index++];
        document.pdop = data[index++];
        document.hdop = data[index++];
        document.networkOperator = data[index++];
        document.immobilizationStatus = data[index++];
        document.geoFenceStatus = data[index++];
        document.rolloverStatus = data[index++];
        document.unauthorizedMovementStatus = data[index++];
        document.vehicleAuthenticationStatus = data[index++];
        document.vehileMovingStatus = data[index++];
        document.ignitionStatus = data[index++];
        document.noOfSatelliteConnected = data[index++];
        document.accuracy = data[index++];
        document.fixType = data[index++];

        //document.fixType = data[index++].substr(0, 1);
        return document;
    }
}
function addTypeAndSubtype(document, packetType, alertType) {
    switch (packetType) {
        case "NR":
            document.type = "data";
            document.subtype = "gps";
            document.history = (alertType == "02");
            break;
        case "VS":
            document.type = "alert";
            document.subtype = "vehicleSecurity";
            break;
        case "IN":
            document.type = "alert";
            document.subtype = "ignitionOn";
            break;
        case "IF":
            document.type = "alert";
            document.subtype = "ignitionOff";
            break;
        default:
            break;
    }
}

module.exports = LocationParser;