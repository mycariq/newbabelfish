var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("bal", "receiver.log");;

var parsers = {
    "$BAL_LOC": new (require("./LocationParser"))(),
    "$BAL_VHEALTH": new (require("./HealthParser"))(),
    "$BAL_TRIP": new (require("./TripParser"))(),
    "$BAL_VBATTERY": new (require("./BatteryParser"))(),
    "$BAL_CID": new (require("./ComponentIDParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(packetArray) {
        //get parser from registry
        var command = packetArray[0];
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for command:" + command + " packet:" + packetArray);
            return;
        }

        //parse data
        var document = parser.parse(packetArray);
        if (!document) {
            logger.info("No document returned by parser : " + command + " packet: " + packetArray);
            return;
        }

        //persist document
        persist(document);
    }
}

function persist(documents) {
    if (!Array.isArray(documents)) {
        documents = [documents];
    }

    documents.forEach(function (document) {
        //add defaults
        helper.addDefaults(document);

        logger.info("Parsed packet " + JSON.stringify(document));
        kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });

}

module.exports = PacketParser;