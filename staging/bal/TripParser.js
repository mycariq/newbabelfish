var helper = require("./helper");

'use strict'
class TripParser {
    constructor() {

    }

    parse(data) {
        var document = { owner: data[1], type: "data", subtype: "trip" };
        document.vendorId = data[2];
        document.vin = data[3];
        document.isBacklog = (data[4] == "H");
        document.publishDate = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.startTimestamp = helper.parseTimeStamp(data[9], data[11]);
        document.endTimestamp = helper.parseTimeStamp(data[10], data[12]);
        document.startLocation = helper.parseLocation(data[13], data[14], data[15], data[16]);
        document.startDirection = data[17];
        document.endLocation = helper.parseLocation(data[18], data[19], data[20], data[21]);
        document.endDirection = data[22];
        document.startOdometerReading = data[23];
        document.endOdometerReading = data[24];
        document.obdSpeed = data[25];
        return document;
    }
}

module.exports = TripParser;