var utils = require("../../utils");

var origin = require("os").hostname();;

//adds default values to mongo document
function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-BAL";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function parseTimeStamp(dateStr, timeStr) {
    var timestampStr = parseInt("20"+dateStr.substr(4)) + "-" + parseInt(dateStr.substr(2, 2)) + "-" + parseInt(dateStr.substr(0, 2));
    timestampStr += " " + parseInt(timeStr.substr(0, 2)) + ":" + parseInt(timeStr.substr(2, 2)) + ":" + parseInt(timeStr.substr(4) + " UTC");
    return new Date(timestampStr +" UTC");
}

function parseLocation(latStr, latDir, longStr, longDir) {
    var latitude = parseFloat(latStr);
    var longitude = parseFloat(longStr);
    if (latDir == "S")
        latitude = latitude * -1;

    if (longDir == "W")
        longitude = longitude * -1;

    return utils.getGeoJson(latitude, longitude, true);
}

exports.parseTimeStamp = parseTimeStamp;
exports.addDefaults = addDefaults;
exports.parseLocation = parseLocation;