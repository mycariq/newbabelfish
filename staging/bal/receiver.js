const SUPPORTED_PACKET_TYPES = ["$BAL_LOC", "$BAL_VHEALTH", "$BAL_TRIP", "$BAL_VBATTERY", "$BAL_CID", "$BAL_DEVICECFG"];
const TAIL = "*";

var Server = require("../../server");
var logger = require("../../logger.js").getLogger("bal", "receiver.log");
var parser = new (require("./PacketParser"))();

new Server("bal", require("../../config.json").bal.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	//convert packet into utf-8
	var packet = data.toString("utf-8");
	logger.info("Received raw packet : " + packet);

	//process packet
	processPacket(socket, packet);

});

function processPacket(socket, packet) {
	//validate packet
	if (!isValidPacket(packet))
		return;

	//split packet by comma to separate each value
	var packetArray = packet.split(",");

	//parse packet
	parser.parse(packetArray);

	//send response
	//TODO if required then only
}

function isValidPacket(packet) {
	var head = packet.split(",")[0];
	var tail = packet.substring(packet.length - 1);
	var crc = packet.substring(packet.length - 17, packet.length - 9);

	//check head or packet type is not valid then return false
	if (!SUPPORTED_PACKET_TYPES.includes(head)) {
		logger.info("Invalid head/packet type of :" + packet);
		return false;
	}

	//if tail is not equal to *<CR><LF> then return false
	if (TAIL != tail) {
		logger.info("Invalid tail of :" + packet);
		return false;
	}

	//if crc is not matching then return false
	//TODO take checksum algorithm from varror

	//finally if everything well then return true

	return true;
}


//var locationPacket = "$BAL_LOC,358014094486940,010720,154402,NR,01,1,18.681022,N,73.731079,E,621.9,325.62,054,00,,0,0,3,,0.53,,0,0,0,0,0,0,0*";
// var healthPacket = "$BAL_VHEALTH,865734020000749,VARROC,MD200000000000000,L,26032020,142248,26032020,142246,1,18.556662,N,073.793187,E,087,120.4,1,1,999999,FFFFFFFFFFFFFFFF,1,4.1,7,1,1,6000,-10,10000,111.9,150.5,150,25,111,111,111,150,50.5,10000,600,100,6000,00004C12*<CR><LF>";
// var tripPacket = "$BAL_TRIP,865734020000749,VARROC,MD200000000000000,L,26032020,142248,26032020,142246,10032020,10032020,235959,235959,89.99999,N,179.99999,E,359,89.99999,N,179.99999,E,359,999998,999999,111.9,00004C12*<CR><LF>";
// var batteryPacket = "$BAL_VBATTERY,865734020000749,VARROC,MD200000000000000,L,26032020,142248,26032020,142246,1,18.556662,N,073.793187,E,087,120.4,999,99999,3,600,150,200,100,100,F,150.1,200,99,1,2000,-10,F,-10,F,4.15,F,4.15,F,,9999,9999,1.11,1.11,1,12.5,00004C12*<CR><LF>";
//console.log(locationPacket);
//processPacket({}, locationPacket);

