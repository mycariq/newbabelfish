var helper=require("./helper");
var utils = require("../../utils");

var logger = require("../../logger.js").getLogger("cariqVirtual", "receiver.log");

'use strict';
class AlertFrameProcessor{
	constructor(){
	}
	
	process(frame){
		logger.info("processing:"+frame);
		var packets = frame.split(",");
		var documents = [];
		
		processData(documents,packets);
		processOverSpeedAlert(documents,packets);
		processInput2MissUseAlert(documents,packets);
		processReconnectTamperingAlert(documents,packets);
		processLowBatteryAlert(documents,packets);
		processPullOutAlert(documents,packets);
		processGpsModuleFailureAlert(documents,packets);
		processTowingAlert(documents,packets);
		processServerNotReachableAlert(documents,packets);
		processSleepModeAlert(documents,packets);
		processSMSCountAlert(documents,packets);
		
		return documents;
	}
}; 

function processData(documents,packets){
	var document = {};
	document.owner = packets[1];
	document.timestamp = helper.getTimestamp(packets[2], packets[3]);
	document.location = utils.getGeoJson(parseFloat(packets[4]),parseFloat(packets[5]),true);
	document.type = "data";
	document.subtype = "gps";
	document.pickFrom = "TLA";
	document.speed = parseFloat(packets[6]);
	document.direction = parseInt(packets[7]);
	document.altitude = parseInt(packets[8]);
	document.noOfSatelites = parseInt(packets[9]);
	document.gpsStatus = parseInt(packets[10]);
	document.gpsSignalStrength = parseInt(packets[11]);
	document.digitalInputStatus = packets[12];
	document.digitalOutputStatus = parseInt(packets[13]);
	document.analogInputValue = parseFloat(packets[14]);
	document.voltage = parseFloat(packets[15]);
	document.internalBatteryVoltage = parseFloat(packets[16]);
	document.pcbTemperature = parseFloat(packets[17]);
	document.reserved = packets[18];
	document.movingStatusFromMSensor = packets[19];
	document.accelGValue = parseFloat(packets[20]);
	document.tiltValue = packets[21];
	document.tripIndication = packets[22];
	document.virtualOdoMeter = parseInt(packets[23]);
	document.serialNo = parseInt(packets[39]);
	document.checksum = packets[40];
	
	documents.push(document);
}

function processOverSpeedAlert(documents,packets){
	if(parseInt(packets[24]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "overSpeed";
		documents.push(document);
	}
}

function processInput2MissUseAlert(documents,packets){
	if(parseInt(packets[25]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "input2MissUse";
		documents.push(document);
	}
}

function processReconnectTamperingAlert(documents,packets){
	if(parseInt(packets[30]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "plugInReminder";
		documents.push(document);
	}
	if(parseInt(packets[30]) == 2){
		var document = createBasicDocument(packets);
		document.subtype = "tampering";
		documents.push(document);
	}
}
function processLowBatteryAlert(documents,packets){
	if(parseInt(packets[31]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "lowBatteryVoltage";
		documents.push(document);
	}
}

function processPullOutAlert(documents,packets){
	if(parseInt(packets[32]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "pullOutReminder";
		documents.push(document);
	}
}

function processGpsModuleFailureAlert(documents,packets){
	if(parseInt(packets[33]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "gpsModuleFault";
		documents.push(document);
	}	
}

function processTowingAlert(documents,packets){
	if(parseInt(packets[34]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "towing";
		documents.push(document);
	}
}

function processServerNotReachableAlert(documents,packets){
	if(parseInt(packets[35]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "serverNotReachable";
		documents.push(document);
	}
}

function processSleepModeAlert(documents,packets){
	if(parseInt(packets[36]) == 1){
		var document = createBasicDocument(packets);
		document.subtype = "sleepMode";
		document.data = "active";
		documents.push(document);
	}
	if(parseInt(packets[36]) == 2){
		var document = createBasicDocument(packets);
		document.subtype = "sleepMode";
		document.data = "basic";
		documents.push(document);
	}
	if(parseInt(packets[36]) == 8){
		var document = createBasicDocument(packets);
		document.subtype = "sleepMode";
		document.data = "deep";
		documents.push(document);
	}
	if(parseInt(packets[36]) == 5){
		var document = createBasicDocument(packets);
		document.subtype = "sleepMode";
		document.data = "batteryBackup";
		documents.push(document);
	}
}

function processSMSCountAlert(documents,packets){
		var document = createBasicDocument(packets);
		document.subtype = "smsCount";
		document.data = parseInt(packets[38]);
		documents.push(document);
}

function createBasicDocument(packets){
	var document = {};
	document.owner = packets[1];
	document.timestamp = helper.getTimestamp(packets[2], packets[3]);
	document.location = utils.getGeoJson(parseFloat(packets[4]),parseFloat(packets[5]),true);
	document.type = "alert";
	document.serialNo = parseInt(packets[39]);
	document.checksum = packets[40];
	return document;
}

module.exports=AlertFrameProcessor;