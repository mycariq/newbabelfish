var configurationLoader = require("../../ConfigurationLoader");

var commands = {
	mobilize : {
		on : "@C#112,Q,0,g,o$;",
		off : "@C#112,Q,1,g,o$;"
	}
};

function send(owner, socket) {
	//send mobiliser
	var mobiliseConf = configurationLoader.get(owner, "MOBILIZE_SWITCH");
	if (mobiliseConf) {
		if(mobiliseConf.MOBILIZE == "ON"){
			console.log("Sending mobilise on command to device="+owner+" command="+commands.mobilize.on);
			socket.write(commands.mobilize.on);
		}
		if(mobiliseConf.MOBILIZE == "OFF"){
			console.log("Sending mobilise off command to device="+owner+" command="+commands.mobilize.off);
			socket.write(commands.mobilize.off);
		}
		configurationLoader.remove(owner, "MOBILIZE_SWITCH");
	}
}

exports.send = send;
