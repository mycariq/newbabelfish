var configurationLoader = new (require("../../ConfigurationLoader"))("CIQ-Concox-PT06", "immobilize");
var logger = require("../../logger.js").getLogger("concoxPT06", "receiver.log");
var helper = require("./helper.js");
var utils = require("../../utils");

function send(owner, socket) {
	//send mobiliser
	var mobiliseConf = configurationLoader.get(owner, "immobilize");
	if (mobiliseConf) {
		var start = mobiliseConf.payload.start;
		logger.info("Mobilize command - owner = " + owner + " command = " + start);
		// validate
		if (start != 1 && start != 0)
			return;

		// send command to device
		sendTodevice(socket, prepareMobilize(start));

		configurationLoader.remove(owner, "immobilize");
	}
}

function prepareMobilize(isOn) {

	var packet = "";
	var start = "7878";
	var end = "0D0A";
	var protocolNo = "80";
	var serverFlag = "00000000";
	var mobilizeCommand = (isOn == 1) ? "52454C41592C3123" : "52454C41592C3023";
	var language = ""; // "0002"
	var serialNo = "0000"; // "00A0"

	var commandLenght = (serverFlag + mobilizeCommand + language).length / 2;
	packet += protocolNo;
	packet += utils.intToHexStr(commandLenght);
	packet += serverFlag;
	packet += mobilizeCommand;
	packet += language;
	packet += serialNo;

	var packetLength = utils.intToHexStr(packet.length / 2 + 2);
	packet = packetLength + packet;
	packet = packet + helper.calculateChecksum(packet);
	packet = start + packet + end;

	return packet;
}

function sendTodevice(socket, packet) {
	logger.info("Sending mobilize command to device=" + socket.owner + " packet : " + packet);
	socket.write(Buffer.from(packet, "hex"));
}

exports.send = send;
