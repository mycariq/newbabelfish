var logger = require("../../logger").getLogger("concoxPT06", "receiver.log");
'use strict'
class ICCIDParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "metadata", subtype: "imeiimsi" };
        document.timestamp = new Date();
        document.protocolNo = content.substr(0, 2);
        document.subProtocolNo = content.substr(2, 2);
        document.imei = content.substr(4, 16);
        document.imsi = content.substr(20, 16);
        document.iccid = content.substr(36, 22).replace("F","");
        
        return document;
    }
}

module.exports = ICCIDParser;