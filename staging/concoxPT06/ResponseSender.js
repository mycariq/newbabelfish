var utils = require("../../utils");
var helper = require("./helper");
var logger = console;

'use strict';
class ResponseSender {
    constructor() {

    }

    send(socket, deviceId, command, serialNumber) {

        var response = undefined;

        switch (command) {
            case "01":
                response = command + serialNumber;
                break;
            case "13":
                response = command + serialNumber;
                break;
            case "26":
                response = command + serialNumber;
                break;

        }
        sendResponse(socket, deviceId, response)
    }
}

function sendResponse(socket, deviceId, response) {
    if (!response)
        return;

    //calculate packet length in hex format and add into response
    var packetLength = utils.intToHexStr((response.length / 2) + 2);
    response = packetLength + response;

    //calculate checksum of reponse and append to response
    response = response + helper.calculateChecksum(response);

    //append head and tail to response
    response = "7878" + response + "0D0A";

    //send response to device
    logger.info("Sending response to " + deviceId + " Response : " + response);

    if (socket)
        socket.write(new Buffer(response, "hex"));

}

module.exports = ResponseSender;