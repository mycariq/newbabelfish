var Server = require("../../server.js");
var logger = require("../../logger.js").getLogger("concoxPT06", "receiver.log");
var helper = require("./helper.js");
var responseSender = new (require("./ResponseSender.js"))();
var packetParser = new (require("./PacketParser.js"))();
var feedbackSender = require("./FeedbackSender");

new Server("concoxPT06", require("../../config.json").concoxPT06.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	//convert received packet to hex string
	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet:" + packet);

	//validate packet. return if it is invalid
	if (!isValidPacket(packet))
		return;

	//fetch device id from packet if it is login packet and store on socket
	if (packet.substr(6, 2) == "01")
		socket.owner = packet.substr(9, 15);

	//process packet
	processPacket(socket, packet);
});

function processPacket(socket, packet) {

	//create packet format json from packet
	var packetJson = {};
	packetJson.head = packet.substr(0, 4);
	packetJson.packetLength = packet.substr(4, 2);
	packetJson.command = packet.substr(6, 2);
	packetJson.content = packet.substring(8, packet.length - 12);
	packetJson.serialNo = packet.substr(packet.length - 12, 4);
	packetJson.crc = packet.substr(packet.length - 8, 4);
	packetJson.tail = packet.substr(packet.length - 4, 4)
	logger.info("DeviceId: " + socket.owner + "  Raw json : " + JSON.stringify(packetJson));

	//parse packet
	packetParser.parse(socket.owner, packetJson.command, packetJson.content, packetJson.serialNo);
	//send response
	responseSender.send(socket, socket.owner, packetJson.command, packetJson.serialNo);

	// feedback send 
	feedbackSender.send(socket.owner, socket);
}

function isValidPacket(packet) {
	// check invalid packet
	if (!packet) {
		logger.info("invalid packet");
		return false;
	}
	
	//fetch head, tail crc from packet
	var head = packet.substr(0, 4);
	var tail = packet.substr(packet.length - 4, 4);
	var crc = packet.substr(packet.length - 8, 4);

	//validate head and tail
	if (("7878" != head && "7979" != head) || "0D0A" != tail) {
		logger.info("Head and Start are not matching. Hence ignoring packet: " + packet);
		return false;
	}

	//validate crc
	if (crc != helper.calculateChecksum(packet.substring(4, packet.length - 8))) {
		logger.info("CRC is not matching. hence ignoring packet: " + packet);
		return false;
	}

	return true;	
}
