var logger = require("../../logger").getLogger("concoxQ2", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");
var voltageLevel = require("./Voltage.json");
var gsmSignal = require("./GsmSignal.json");

'use strict'
class HeartbeatParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "data", subtype: "heartbeat" };
        document.timestamp = new Date();
        document.terminalInfo = content.substr(0, 2);
        document.voltageLevel = voltageLevel[content.substr(2, 2)];
        document.gsmSignal = gsmSignal[content.substr(4, 2)];
        document.language = content.substr(6, 4);
        return document;
    }
}

module.exports = HeartbeatParser;