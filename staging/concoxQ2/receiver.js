var Server = require("../../server");
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger").getLogger("concoxQ2", "receiver.log");;
var responseSender = new (require("./ResponseSender"))();
var packetParser = new (require("./PacketParser"))();

new Server("concoxQ2", require("../../config.json").concoxQ2.port).start(function (err,
	data, id, socket) {

	//convert received packet to hex string
	var packet = data.toString("hex").toUpperCase();
	logger.info("received: " + packet);

	//validate packet. return if it is invalid
	if (!isValidPacket(packet))
		return;

	//fetch device id from packet if it is login packet and store on socket
	if (packet.substr(6, 2) == "01")
		socket["deviceId"] = packet.substr(9, 16);

	//process packet
	processPacket(socket, socket["deviceId"], packet);
});

function isValidPacket(packet) {
	//fetch head, tail crc from packet
	var head = packet.substr(0, 4);
	var tail = packet.substr(packet.length - 4, 4);
	var crc = packet.substr(packet.length - 8, 4);

	//validate head and tail
	if ("7878" != head || "0D0A" != tail) {
		logger.info("Head and Start are not matching. Hence ignoring packet: " + packet);
		return false;
	}

	//validate crc
	if (crc != helper.calculateChecksum(packet.substring(4, packet.length - 8))) {
		logger.info("CRC is not matching. hence ignoring packet: " + packet);
		return false;
	}
	return true;
}

function processPacket(socket, deviceId, packet) {

	//create packet format json from packet
	var packetJson = {};
	packetJson.head = packet.substr(0, 4);
	packetJson.packetLength = packet.substr(4, 2);
	packetJson.command = packet.substr(6, 2);
	packetJson.content = packet.substring(8, packet.length - 12);
	packetJson.serialNo = packet.substr(packet.length - 12, 4);
	packetJson.crc = packet.substr(packet.length - 8, 4);
	packetJson.tail = packet.substr(packet.length - 4, 4)
	logger.info("DeviceId: " + deviceId + "  Raw json : " + JSON.stringify(packetJson));

	//parse packet
	packetParser.parse(deviceId, packetJson.command, packetJson.content, packetJson.serialNo);
	//send response
	responseSender.send(socket, deviceId, packetJson.command, packetJson.serialNo);
}


var login = "7878110103587390521043703002212100292F7E0D0A";
var heartbeat = "78780A138405040002001F094D0D0A";
var gps = "7878222812080706231BC401FE081607E9F5900814D201941B008E009747000000001C90F70D0A";
var chinaGPS="78782222120B1D050120C5026C14000C38DD4001149001CC002866000F6F00000100F356CD0D0A";
var alert = "7878252612080A052129C401FE0AB607E9F5E002042A0901941B008E009747800604200200B2BE460D0A";
//processPacket(undefined, "0587390521043703", chinaGPS);