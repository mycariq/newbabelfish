const request = require('request');
var kueURL = require('kue'), jobsDASURL = kueURL.createQueue();

var throughputConfig = require("../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];

const DAS_TOPIC = "das-urls";
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;
var dasQThroughputRecord  = new ThroughputRecord("dasMessangerThroughputRecord", new Date, 0, threshold);

var logger = require("../logger.js");
var mylogger = logger.getLogger("das", "dasMessanger.log");

mylogger.debug("================ Started dasMessanger =============");

/* This is callback trick to keep hold on URL String */
function myrequest(URL,callback) {
	request(URL, function(error, response, body) {
		callback(URL, error, response, body);
	} );

}

/* Here is actual Processing of URLJob */
jobsDASURL.process(DAS_TOPIC, 20, function (job, done) {
 	mylogger.debug("(jobsDASURL.process)-Processing Job Id : " + job.id + " URL is : " + job.data.URL);
	var myURL = job.data.URL;
	myrequest(myURL, function(myURL, error, response, body) {
		//console.log(myScriptName + new Date + ": "  + "URL:"+myURL);
		//console.log(myScriptName + new Date + ": "  + "Error:" + error);
		//console.log(myScriptName + new Date + ": "  + "Response:" + response);
		//console.log(myScriptName + new Date + ": "  + "Body:" + body);

		if (!error && response.statusCode == 200) {
			mylogger.debug("(jobsDASURL.process)-Body = " + body + " StatusCode = " + response.statusCode);
			dasQThroughputRecord.increment();
		}
		else {
			if (error)
				mylogger.error("(jobsDASURL.process)-DAS does not accept this URL : " + myURL + error + error.stack);
			else
				mylogger.error("(jobsDASURL.process)-DAS does not accept this URL : "+ myURL+ " **No Error but StatusCode : " + response.statusCode);

			if ( response && response.statusCode == "400") {
				mylogger.error("(jobsDASURL.process)-This is special case 400 ! Wrong URL gets generated!");
			} else {
				/* We need to recreate JOB for processing this URL */
 				var job = jobsDASURL.create(DAS_TOPIC, { URL: myURL });
				mylogger.debug("(jobsDASURL.process)-Reinserting Failed DASUrl JOB : " + myURL );
 				job.save();
			}
		}

	});
 	done && done();
});

