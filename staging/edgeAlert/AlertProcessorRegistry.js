
const alertProcessors = new Map();

function add(alertName,processorName){
    alertProcessors.set(alertName,processorName);
}

function get(alertName){
    return alertProcessors.get(alertName);
}

function getAll(settings) {
    var processors = new Map();
    for (var alertName in settings) {
        var process = alertProcessors.get(alertName);
        if (process)
            processors.set(alertName, process);
    }
    return processors;
}

add('Speed',require("../edgeAlert/OverSpeedAlertProcessor.js"));
add('Geofencing',require("../edgeAlert/GeofenceAlertProcessor.js"));

exports.get = get;
exports.getAll = getAll;
