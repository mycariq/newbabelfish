var https = require("https");
var logger = require("../../logger.js");
var apiConfig = require("../../config.json").API;
var mylogger = logger.getLogger("edgeAnalytics", "receiverEdgeAlertProcessor.log");

const ALERT_SETTING = "ALERT_SETTING_";
const ALERT_STATE = "ALERT_STATE_";

function fetchSetting(deviceId, redisClient, callback) {
    var identifier = ALERT_SETTING + deviceId;
    redisClient.readAll(identifier, function (result, error) {
        if (error) {
            callback(undefined, error);
            return;
        }

        // get setting from redis
        if (JSON.stringify(result) != '{}') {
            callback(result);
        } else {
            // update setting 
            updateSetting(deviceId, callback, function(){
                callback();
            });
        }
    });
}

function fetchState(deviceId, alertName, redisClient, callback) {
    var identifier = ALERT_STATE + deviceId;
    redisClient.read(identifier, alertName, function (result, error) {
        if (error) {
            callback(alertName, undefined, error);
            return;
        }
        callback(alertName, result);
    });
}

function updateState(redisClient, deviceId, alertName, state) {
    var identifier = ALERT_STATE + deviceId;
    redisClient.update(identifier, alertName, JSON.stringify(state));
}

function updateSetting(deviceId, callback) {

    var host = apiConfig.DNS;
    var port = apiConfig.port;
    var basicAuth = 'Basic cmVkaXM6NjA3ZWU0NjE0NWY2OTJhYWEwYmY0NmExZDQ4ZDFjN2E=';
    var URL = 'https://' + host + ':' + port + '/Cariq/settings/update/redis/edgeAlert/' + deviceId;
    const options = {
        rejectUnauthorized: false,
        method: 'POST',
        headers: {
            Authorization: basicAuth
        }
    }
    
    //mylogger.debug("updateSetting - deviceId : " + deviceId);

    var req = https.request(URL, options, (response) => {
        response.on('data', function (data) {
            //mylogger.debug("updateSetting - response : " + data);
            callback();
        });
        response.on('error', (error) => {
            mylogger.debug("updateSetting - error : " + error);
            callback();
        });
    });

    req.on('error', (error) => {
        mylogger.debug("updateSetting - error : " + error);
        callback();
    });

    req.end();
}

exports.fetchSetting = fetchSetting;
exports.fetchState = fetchState;
exports.updateState = updateState;