

function isGpsPacket(document) {
    if (document.subtype == "gps" || document.subtype == "obd" || document.subtype == "obdgps")
        return true;

    return false;
}

function calculateDistance(lat1, lat2, lng1, lng2) {

    lng1 = lng1 * Math.PI / 180;
    lng2 = lng2 * Math.PI / 180;
    lat1 = lat1 * Math.PI / 180;
    lat2 = lat2 * Math.PI / 180;

    // Haversine formula 
    let dlng = lng2 - lng1;
    let dlat = lat2 - lat1;
    let a = Math.pow(Math.sin(dlat / 2), 2)
        + Math.cos(lat1) * Math.cos(lat2)
        * Math.pow(Math.sin(dlng / 2), 2);

    let c = 2 * Math.asin(Math.sqrt(a));

    // Radius of earth in kilometers. Use 3956 for miles
    let r = 6371;
    // calculate the result
    return (c * r);
}

class GeoLocation {

    constructor(document) {
        this.timestamp = document.timestamp;
        var location = document.location;
        this.isReal = document.location.isReal;
        var coordinates = location.coordinates;
        if (coordinates.length < 2)
            return;

        this.latitude = coordinates[1];
        this.longitude = coordinates[0];
    }

    get getTimeStamp() {
        return this.timestamp;
    }

    get getLatitude() {
        return this.latitude;
    }

    get getLongitude() {
        return this.longitude;
    }

    isValid() {
        return (this.latitude != null && this.longitude != null && this.timestamp != null && this.isReal);
    }
}

exports.isGpsPacket = isGpsPacket;
exports.calculateDistance = calculateDistance;
exports.GeoLocation = GeoLocation;