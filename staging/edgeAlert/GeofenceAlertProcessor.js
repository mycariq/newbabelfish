var edgeAlertProcessor = require("../edgeAlert/receiverEdgeAlertProcessor.js");
var settingHandler = require("../edgeAlert/AlertSettingHandler.js");
var edgeAlertHelper = require("../edgeAlert/EdgeAlertHelper.js");

var logger = require("../../logger.js");
var mylogger = logger.getLogger("edgeAnalytics", "receiverEdgeAlertProcessor.log");

function doProcess(document, alertName, setting, state, redisClient) {

    // if pcaket is not gps then return
    if (!edgeAlertHelper.isGpsPacket(document))
        return;

    // mylogger.debug("GeofenceAlertProcessor - Document : " + JSON.stringify(document));
    // mylogger.debug("GeofenceAlertProcessor - Setting : " + JSON.stringify(setting));
    // mylogger.debug("GeofenceAlertProcessor - State : " + JSON.stringify(state));

    var owner = document.owner;
    let geoLocation = new edgeAlertHelper.GeoLocation(document);
    if (!geoLocation.isValid())
        return;

    //mylogger.debug("GeofenceAlertProcessor - geoLocation : " + geoLocation.getLatitude+","+geoLocation.getLongitude);

    // if setting josn is empty then return 
    if (setting == null)
        return;

    // if setting josn is empty then return 
    let alertState = new State(owner, alertName, state, redisClient);
    if (state == null) {
        //mylogger.debug("GeofenceAlertProcessor - updateLocation : " + geoLocation.getLatitude);
        alertState.updateLocation(geoLocation.getLatitude, geoLocation.getLongitude);
        return;
    }
    //mylogger.debug("GeofenceAlertProcessor - prevLatLng : " + alertState.getPrevLat+","+alertState.getPrevLng);

    let alertSetting = new Setting(setting);
    var circularRegions = [];
    if (null != alertSetting.getCircle) {
        for (const json of alertSetting.getCircle) {
            circularRegions.push(new CircularRegion(json));
        }
    }
    var rectangleRegions = [];
    if (null != alertSetting.getRectangle) {
        for (const json of alertSetting.getRectangle) {
            rectangleRegions.push(new RectangleRegion(json));
        }
    }
    //mylogger.debug("GeofenceAlertProcessor - circularRegions : " + JSON.stringify(circularRegions));
    //mylogger.debug("GeofenceAlertProcessor - rectangleRegions : " + JSON.stringify(rectangleRegions));

    // processor alert
    processAlert(document, geoLocation, alertState, circularRegions, rectangleRegions);

    // update state location
    alertState.updateLocation(geoLocation.getLatitude, geoLocation.getLongitude);
}

function processAlert(document, geoLocation, alertState, circularRegions, rectangleRegions) {
    //mylogger.debug("GeofenceAlertProcessor - processAlert : " + JSON.stringify(document));
    // check packet is inside or not 
    circularRegions.forEach(element => {
        // isInside : true
        if (element.isInside(geoLocation.getLatitude, geoLocation.getLongitude) && !element.isInside(alertState.getPrevLat, alertState.getPrevLng)) {
            generateAlert(document, element.getRegionId, true, true, alertState);
        }
        //mylogger.debug("GeofenceAlertProcessor - processAlert circularRegions : "+element.getLat+","+element.getLng);
        // isInside : false
        if (!element.isInside(geoLocation.getLatitude, geoLocation.getLongitude) && element.isInside(alertState.getPrevLat, alertState.getPrevLng)) {
            generateAlert(document, element.getRegionId, false, true, alertState);
        }
    });

    //mylogger.debug("GeofenceAlertProcessor - processAlert circularRegions completed");
    // check packet is inside or not 
    rectangleRegions.forEach(element => {
        // isInside : true
        if (element.isInside(geoLocation.getLatitude, geoLocation.getLongitude) && !element.isInside(alertState.getPrevLat, alertState.getPrevLng)) {
            generateAlert(document, element.getRegionId, true, false, alertState);
        }

        //mylogger.debug("GeofenceAlertProcessor - processAlert rectangleRegions : "+element.getLat1+","+element.getLat2+","+element.getLng1+","+element.getLng2);
        // isInside : false
        if (!element.isInside(geoLocation.getLatitude, geoLocation.getLongitude) && element.isInside(alertState.getPrevLat, alertState.getPrevLng)) {
            generateAlert(document, element.getRegionId, false, false, alertState);
        }
    });
    //mylogger.debug("GeofenceAlertProcessor - processAlert completed");
}

function generateAlert(document, regionId, entry, isCircularRegion, alertState) {
    var data = {};
    data.regionId = regionId;
    data.entry = entry;
    document.data = JSON.stringify(data);
    document = updateDocument(document);

   // mylogger.debug("GeofenceAlertProcessor - generateAlert : " + JSON.stringify(document) + " isCircularRegion : " + isCircularRegion + " alertState : " + JSON.stringify(alertState));
    // push into kafka
    edgeAlertProcessor.persist(document);
}

function updateDocument(document) {
    document.type = "alert";
    document.subtype = "geoFence";
    return document;
}

class Setting {
    constructor(setting) {
        try {
            setting = JSON.parse(setting);
            this.circle = setting.Circle;
            this.rectangle = setting.Rectangle;
        } catch (e) {
            mylogger.debug("GeofenceAlertProcessor - Setting exception : " + e);
        }
    }

    get getCircle() {
        return this.circle
    }

    get getRectangle() {
        return this.rectangle;
    }
}

class State {
    constructor(owner, alertName, state, redisClient) {
        this.owner = owner;
        this.alertName = alertName;
        this.redisClient = redisClient;

        if (null == state) {
            state = '{"prevLocation": {"lat":null,"lng":null}}';
        }
        this.state = JSON.parse(state);
    }

    get getObj() {
        return this;
    }

    get getPrevLat() {
        return this.state.prevLocation.lat;
    }

    get getPrevLng() {
        return this.state.prevLocation.lng;
    }

    updateLocation(lat, lng) {
        this.state.prevLocation.lat = lat;
        this.state.prevLocation.lng = lng;
        settingHandler.updateState(this.redisClient, this.owner, this.alertName, this.state);
    }
}

class CircularRegion {
    constructor(region) {
        this.regionId = region.regionId;
        this.radius = region.radius;
        this.lat = region.lat;
        this.lng = region.lng;
    }

    get getLat() {
        return this.lat;
    }

    get getLng() {
        return this.lng;
    }

    get getRadius() {
        return this.radius;
    }

    get getRegionId() {
        return this.regionId;
    }

    isInside(lat, lng) {
        //mylogger.debug("CircularRegion - isInside - lat,lng : " + lat+","+lng);
        var distance = edgeAlertHelper.calculateDistance(this.lat, lat, this.lng, lng);
        //mylogger.debug("CircularRegion - isInside - distance : " + distance);
        return distance < this.radius;
    }
}

class RectangleRegion {
    constructor(region) {
        this.regionId = region.regionId;
        this.lat1 = region.lat1;
        this.lat2 = region.lat2;
        this.lng1 = region.lng1;
        this.lng2 = region.lng2;
    }

    get getLat1() {
        return this.lat1;
    }

    get getLat2() {
        return this.lat2;
    }

    get getLng1() {
        return this.lng1;
    }

    get getLng2() {
        return this.lng2;
    }

    get getRegionId() {
        return this.regionId;
    }

    isInside(lat, lng) {
        //mylogger.debug("RectangleRegion - isInside - lat,lng : " + lat+","+lng);
        return ((lat > this.lat1 && lat < this.lat2) && (lng > this.lng1 && lng < this.lng2));
    }
}

exports.doProcess = doProcess;