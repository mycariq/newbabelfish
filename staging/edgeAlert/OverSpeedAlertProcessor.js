var edgeAlertProcessor = require("../edgeAlert/receiverEdgeAlertProcessor.js");
var settingHandler = require("../edgeAlert/AlertSettingHandler.js");
var edgeAlertHelper = require("../edgeAlert/EdgeAlertHelper.js");

var logger = require("../../logger.js");
var mylogger = logger.getLogger("edgeAnalytics", "receiverEdgeAlertProcessor.log");

// const variables;
const threshold = 5;

function doProcess(document, alertName, setting, state, redisClient) {
    
   // mylogger.debug("OverSpeedAlertProcessor - doProcess : " + JSON.stringify(setting));

    // if pcaket is not gps then return
    if (!edgeAlertHelper.isGpsPacket(document))
        return;

    // if setting josn is empty then return 
    if (setting == null)
        return;

    var owner = document.owner;
    let alertSetting = new Setting(setting);
    let alertState = new State(owner, alertName, state, redisClient);
    // check when last alert send
    if (!isAlertableTime(alertSetting, alertState, document))
        return;

    var speed = document.speed;
    // if speed is null or 0 then return
    if (speed == null || speed < 1) {
        alertState.updateOccurance(0);
        return;
    }

    if (speed != null && alertSetting.getOverSpeed != null && speed > alertSetting.getOverSpeed) {
        alertState.updateOccurance((++alertState.occurance));
    } else {
        alertState.updateOccurance(0);
        return;
    }

    if (isAlertable(alertState)) {
        document = updateDocument(document);
        // update setting
        alertState.updateLastOccuranceTime(document.timestamp);

        //mylogger.debug("OverSpeedAlertProcessor - generateAlert : " + JSON.stringify(document));
        
        // push into kafka
        edgeAlertProcessor.persist(document);
        return;
    }
}

function isAlertableTime(alertSetting, alertState, document) {

    if (null == alertState.getLastOccuranceTime) {
        return true;
    }
    
    var currentTime = new Date();//document.timestamp;
    var durationInMiliSec = (alertSetting.getDurationInMinutes * (60 * 1000));
    let timeDiffMS = currentTime.getTime() - alertState.getLastOccuranceTime.getTime();
    if (timeDiffMS > durationInMiliSec)
        return true;

    return false;
}

function isAlertable(alertState) {
    if (alertState.getOccurance >= threshold) {
        return true;
    }
    return false;
}

function updateDocument(document) {
    document.type = "alert";
    document.subtype = "overSpeed";
    return document;
}

class Setting {
    constructor(setting) {
        setting = JSON.parse(setting);
        this.overSpeed = setting.maxSpeed;
        this.durationInMinutes = setting.speedAlertDurationInMinutes;
    }

    get getOverSpeed() {
        return this.overSpeed;
    }

    get getDurationInMinutes() {
        return this.durationInMinutes;
    }
}

class State {
    constructor(owner, alertName, state, redisClient) {
        this.owner = owner;
        this.alertName = alertName;
        this.redisClient = redisClient;

        if (!state) {
            state = '{"occurance": 0,"lastOccuranceTime": null}';
        }
        this.state = JSON.parse(state);
        this.occurance = this.state.occurance;
        this.lastOccuranceTime = this.state.lastOccuranceTime;
    }

    get getOccurance() {
        return this.occurance;
    }

    get getLastOccuranceTime() {
        return (null == this.lastOccuranceTime) ? null : new Date(this.lastOccuranceTime);
    }

    get getObj() {
        return this;
    }

    updateOccurance(occurance) {
        this.state.occurance = occurance;
        // update occurance
        settingHandler.updateState(this.redisClient, this.owner, this.alertName, this.state);
    }

    updateLastOccuranceTime(lastOccuranceTime) {
        this.state.occurance = 0;
        this.state.lastOccuranceTime = lastOccuranceTime;
        // update lastOccuranceTime
        settingHandler.updateState(this.redisClient, this.owner, this.alertName, this.state);
    }
}

exports.doProcess = doProcess;