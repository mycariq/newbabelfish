var redisClient = require("../../RedisClient.js");
var settingHandler = require("../edgeAlert/AlertSettingHandler.js");
var alertProcessorRegistry = require("../edgeAlert/AlertProcessorRegistry.js");

var kafkaProducer;
var kafkaConsumer;

//connected to redis DB
redisClient.connectDB(function () {
    kafkaConsumer = new (require("../../consumer"))("GeoBit", "receiverEdgeAlertProcessor", process);
    kafkaProducer = new (require("../../producer.js"))("EdgeAlert", 3);
});

var logger = require("../../logger.js");
var mylogger = logger.getLogger("edgeAnalytics", "receiverEdgeAlertProcessor.log");

var packetLockStatus = {};
// test devices
//const testDevices = ["111111111118","631036605979","694036871992","111111111133","10171864313293"];

function process(message) {

    //prepare doccument
    var document = prepare(message);
    // get device Id 
    var deviceId = document.owner;
    // if (!isTestDevice(deviceId))
    //     return;

    // mylogger.debug("process : " + JSON.stringify(document));
    // mylogger.debug("packetLockStatus : " + JSON.stringify(packetLockStatus[deviceId]));

    // if packet is lock then return
    if (isPacketLock(deviceId)) {
        setTimeout(() => {
            process(JSON.stringify(document));
        }, 200);
        return;
    } else {
        lockPacket(deviceId);
    }

    // get setttings for this device
    settingHandler.fetchSetting(deviceId, redisClient, function (settings, error) {
        if (error) {
            unLockPacket(deviceId);
            return;
        }

        if (settings) {
            var completedCount = 0;
            // get all alert processor
            let alertProcessors = alertProcessorRegistry.getAll(settings);
            if (alertProcessors.size < 1) {
                unLockPacket(deviceId);
                return;
            }
            try {
                for (var entry of alertProcessors.entries()) {
                    var alertName = entry[0];
                    let alertProcessor = entry[1];
                    settingHandler.fetchState(deviceId, alertName, redisClient, function (alertName, state, error) {
                        if (error) {
                            mylogger.debug("Exception : " + error);
                        }
                        else {
                            //mylogger.debug("Before process : " + JSON.stringify(state));
                            alertProcessor.doProcess(document, alertName, settings[alertName], state, redisClient);
                            //mylogger.debug("After process : " + JSON.stringify(state));
                        }
                        completedCount++;
                        if (alertProcessors.size == completedCount) {
                            unLockPacket(deviceId);
                        }
                    });
                }
            } catch (e) {
                unLockPacket(deviceId);
                mylogger.debug("Exception : " + e);
            }
        } else {
            unLockPacket(deviceId);
        }
    });
}

function isPacketLock(deviceId) {
    // is first packet
    if (!packetLockStatus[deviceId]) {
        packetLockStatus[deviceId] = { isLock: true, isLastUpdated: new Date() };
        return false;
    }

    // if packet is lock and not older than 1 min
    var status = packetLockStatus[deviceId];
    if (status.isLock && !isPacketOlder(deviceId, status))
        return true;

    return false;
}

function lockPacket(deviceId) {
    updateLockStatus(deviceId, true);
}

function unLockPacket(deviceId) {
    updateLockStatus(deviceId, false);
}

function updateLockStatus(deviceId, isLock) {
    var status = packetLockStatus[deviceId];
    status.isLock = isLock;
    status.isLastUpdated = new Date();
    packetLockStatus[deviceId] = status;
}

function isPacketOlder(deviceId, status) {
    var isLastUpdatedDate = new Date(status.isLastUpdated);
    var currentDate = new Date();
    var dateDiffInMin = Math.round((currentDate.getTime() - isLastUpdatedDate.getTime()) / (1000 * 3600));
    if (dateDiffInMin > 1) {
        unLockPacket(deviceId, false);
    }

    return false;
}

function persist(edgeAlert) {
    // edge alert push on kafka topic
    kafkaProducer.keyBasedProduce(JSON.stringify(edgeAlert), edgeAlert.owner, function (result) {
        mylogger.debug("EdgeAlertProcessor - KafkaResult : " + JSON.stringify(result));
    });
}

function prepare(message) {
    var document = JSON.parse(message);
    document.timestamp = new Date(document.timestamp);
    document.serverTimestamp = new Date(document.serverTimestamp);
    return document;
}

function isTestDevice(deviceId) {
    return testDevices.includes(deviceId);
}

// var document = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:05:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.8782616,28.3059299]},"speed":46,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document1 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:06:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.8720616,28.305855]},"speed":47,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document2 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:07:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.8720599,28.30629]},"speed":48,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document3 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":49,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document4 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":50,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document5 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":51,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document6 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":52,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document7 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":53,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document8 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":54,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document9 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:08:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.87139,28.3067933]},"speed":55,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';
// var document10 = '{"type":"data","subtype":"gps","timestamp":"Wed Jun 05 2024 07:09:53 GMT+0000 (Coordinated Universal Time) UTC","location":{"type":"Point","isReal":true,"coordinates":[76.8704483,28.3070816]},"speed":44,"owner":"354018113535680","origin":"cin-cariq-dev-admin","agent":"CIQ-IoTA4","serverTimestamp":"2024-06-05T07:05:53.808Z"}';

// process(document);
// process(document1);
// process(document2);
// process(document3);
// process(document4);
// process(document5);
// process(document6);
// process(document7);
// process(document8);
// process(document9);
// process(document10);

exports.process = process;
exports.persist = persist;