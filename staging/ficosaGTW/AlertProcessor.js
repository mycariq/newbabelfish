'use strict'
class AlertProcessor {
    constructor() {
        this.alertJSON = {
            brakeAbuse: { consecutiveCount: 0, consecutiveCountThreshold: 3 },
            highTemperatureOfCoolant: { consecutiveCount: 0, consecutiveCountThreshold: 10 },
            suddenSpeededUp: { consecutiveCount: 0, consecutiveCountThreshold: 8 },
            rapidDeceleration: { consecutiveCount: 0, consecutiveCountThreshold: 8 },
            sharpTurn: { consecutiveCount: 0, consecutiveCountThreshold: 8 }
        }
    }

    process(document) {

        Object.keys(this.alertJSON).forEach(alertKey => {
            var json = this.alertJSON[alertKey];

            var matching = false;
            switch (alertKey) {

                case "brakeAbuse":
                    matching = (document.parking == 2 && document.ignition == 1);
                    break;

                case "highTemperatureOfCoolant":
                    matching = (document.coolantTemparature >= 84 && document.ignition == 1);
                    break;

                case "suddenSpeededUp":
                    matching = (document.LAC > 0.13 && document.ignition == 1);
                    break;

                case "rapidDeceleration":
                    matching = (document.LAC < -0.23 && document.ignition == 1);
                    break;

                case "sharpTurn":
                    matching = ((document.TAC < -0.50 || document.TAC > 0.50) && document.ignition == 1);
                    break;

                default:
                    break;
            }

            if (matching)
                json.consecutiveCount++;
            else
                json.consecutiveCount = 0;

            if (json.consecutiveCount >= json.consecutiveCountThreshold) {
                json.alertTimestamp = document.timestamp;
                json.location = document.location;
            }
        });
    }

    finish() {
        var documents = [];
        Object.keys(this.alertJSON).forEach(alertKey => {
            var json = this.alertJSON[alertKey];

            if (json.alertTimestamp) {
                var alertDocument = {
                    type: "alert",
                    subtype: alertKey,
                    timestamp: json.alertTimestamp,
                    location: json.location
                };
                documents.push(alertDocument);
            }
        });
        return documents;
    }
}


module.exports = AlertProcessor;