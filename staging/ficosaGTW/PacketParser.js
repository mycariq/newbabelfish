var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("ficosaGTW", "receiver.log");

var parsers = {
    "probe": new (require("./ProbeParser"))(),
    "tow-regular": new (require("./TowRegularParser"))(),
    "tow-away": new (require("./TowAwayParser"))(),
    "ecall": new (require("./EcallParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(packet) {
        //get parser from registry
        var command = packet.service;
        var owner = packet.tcu;
        var vin = packet.vin;

        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  packet:" + packet);
            return;
        }

        //parse data
        var document = parser.parse(packet);
        if (!document)
            return;

        //persist document
        persist(owner, vin, document);
    }
}

function persist(owner, vin, document) {
    if (!document)
        return;

    if (!(document instanceof Array))
        document = [document];

    document.forEach(doc => {
        doc.owner = owner;
        doc.vin = vin;
        helper.addDefaults(doc);
        logger.info("Parsed packet " + JSON.stringify(doc));
        kafkaProducer.keyBasedProduce(JSON.stringify(doc), doc.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });
}

module.exports = PacketParser;