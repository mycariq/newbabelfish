var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class ProbeParser {
    constructor() {

    }

    parse(packet) {

        var parsedData = parseData(packet);
        var parsedEvents = parseEvents(packet);

        var documents = [];
        documents.push(...parsedData);
        documents.push(...parsedEvents);

        return documents;
    }

}

function parseData(packet) {
    var documents = [];

    var timestamp = packet.timestamps.device;
    var gtwTimestamp = new Date(packet.timestamps.gtw);

    var data = packet.data;

    var alertProcessor = new (require("./AlertProcessor"))();

    for (var index = 0; index < data.GPS.length; index++) {
        var document = { type: "data", subtype: "obdgps" };
        document.timestamp = new Date(timestamp + (index * 1000));
        document.gtwTimestamp = gtwTimestamp;
        document.ignition = (data.IGC.status == true) ? 1 : 0;
        var gps = data.GPS[index];
        if (gps.length > 0) {
            document.location = utils.getGeoJson(gps[0], gps[1], true);
            document.direction = gps[3];
            document.speed = gps[4];
        }
        document.rpm = data.RPM.avg;
        document.odometer = data.MLG;
        document.seatBelt = utils.binToInt(("" + data.SBD.value + data.SBF.value + data["2SL"].value + data["2SC"].value + data["2SR"].value).replace(/2/g, '1'));
        document.fuelStatus = data.FLW.value;
        document.airbag = 0;
        document.parking = (data.PKB[index] == 2) ? 1 : 0;
        document.lowBeam = data.LWB.value;
        document.highBeam = data.HHB.value;
        document.door = utils.binToInt(("" + data.FRDW.value + data.FLDW.value + data.RLDW.value + data.RRDW.value + + data.BTDW.value).replace(/2/g, '1'));
        document.coolantTemparature = data.ECT;
        document.waterTemparature = data.WWT.value;
        document.voltage = data.VBT;
        document.brakeAbuse = 0;
        document.starterAbuse = 0;

        //Remaining saving as it is
        document.BST = data.BST;
        document.GAD = data.GAD[index];
        document.SFR = data.SFR[index];
        document.SFL = data.SFL[index];
        document.SPD = data.SPD[index];
        document.SRL = data.SRL[index];
        document.SRR = data.SRR[index];
        document.LAC = data.LAC[index];
        document.TAC = data.TAC[index];
        document.YAW = data.YAW[index];
        document.DSP = data.DSP[index];
        document.MRG = data.MRG;
        document.ERS = data.ERS;
        document.FCP = data.FCP;
        document.SWA = data.SWA[index];
        document.SWR = data.SWR[index];
        document.SWO = data.SWO[index];
        document.MET = data.MET[index];
        document.RWS = data.RWS[index];
        document.RPO = data.RPO[index];
        document.EDR = data.EDR[index];
        document.RKEB = data.RKEB;
        document.DDL = data.DDL;
        document.RFL = data.RFL;
        document.FFL = data.FFL;
        document.PSL = data.PSL;
        document.WGL = data.WGL;
        document.AWR = data.AWR;
        document.WLW = data.WLW;
        document.BTW = data.BTW;
        document.WDW = data.WDW;
        document.GLW = data.GLW;

        documents.push(document);

        alertProcessor.process(document);
    }

    documents.push(...alertProcessor.finish());
    return documents;
}

function parseEvents(packet) {
    var documents = [];
    var timestamp = packet.timestamps.device;

    packet.events.forEach(event => {
        var document = undefined;
        switch (event.id) {

            case "IGN_ON":
                document = { subtype: "ignitionOn", ignition: 1 };
                break;

            case "IGN_OFF":
                var noOfValues = packet.data.GPS.length;
                if (noOfValues > 0)
                    timestamp += (noOfValues - 1) * 1000;
                document = { subtype: "ignitionOff", ignition: 0 };
                break;

            case "TOW_CONFIRMED_BY_GPS":
                document = { subtype: "towing" }
                break;

            case "CRASH":
                document = { subtype: "collision" }
                break;

            case "PANIC_BUTTON":
                document = { subtype: "sos" }
                break;

            case "ACCELEROMETER_TRIGGER":
                document = { subtype: "vibration" }
                break;

            default:
                break;
        }

        if (document) {
            document.type = "alert";
            document.timestamp = new Date(timestamp);
            document.gtwTimestamp = new Date(packet.timestamps.gtw);
            if (event.gps)
                document.location = utils.getGeoJson(event.gps[0], event.gps[1], true);
            documents.push(document);
        }
    });

    return documents;
}

module.exports = ProbeParser;