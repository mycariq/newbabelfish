var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class TowRegularParser {
    constructor() {

    }

    parse(packet) {

        var parsedEvents = parseEvents(packet);

        var documents = [];
        documents.push(...parsedEvents);

        return documents;
    }

}

function parseEvents(packet) {
    var documents = [];

    packet.events.forEach(event => {
        var document = undefined;
        switch (event.id) {

            case "IGN_ON":
                document = { subtype: "ignitionOn", ignition: 1 };
                break;

            case "IGN_OFF":
                document = { subtype: "ignitionOff", ignition: 0 };
                break;

            case "TOW_CONFIRMED_BY_GPS":
                document = { subtype: "towing" }
                break;

            case "CRASH":
                document = { subtype: "collision" }
                break;

            case "PANIC_BUTTON":
                document = { subtype: "sos" }
                break;

            case "ACCELEROMETER_TRIGGER":
                document = { subtype: "vibration" }
                break;

            default:
                break;
        }

        if (document) {
            document.type = "alert";
            document.timestamp = new Date(packet.timestamps.device);
            document.gtwTimestamp = new Date(packet.timestamps.gtw);
            if (event.gps)
                document.location = utils.getGeoJson(event.gps[0], event.gps[1], true);
            documents.push(document);
        }
    });

    return documents;
}


module.exports = TowRegularParser;