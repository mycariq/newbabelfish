var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class TowRegularParser {
    constructor() {

    }

    parse(packet) {

        var parsedData = parseData(packet);
        var parsedEvents = parseEvents(packet);

        var documents = [];
        documents.push(...parsedData);
        documents.push(...parsedEvents);

        return documents;
    }

}

function parseData(packet) {

    var document = { type: "data", subtype: "heartbeat" };
    document.timestamp = new Date(packet.timestamps.device);
    document.gtwTimestamp = new Date(packet.timestamps.gtw);

    var data = packet.data;
    var gps = data.GPS;
    if (gps.length > 0) {
        document.location = utils.getGeoJson(gps[0], gps[1], true);
        document.direction = gps[3];
        document.speed = gps[4];
    }

    document.ignitionCount = data.IGC;
    document.RAT = data.RAT;
    document.voltage = data.VBT;
    document.ARV = data.ARV;
    document.ignition = 0;

    return [document];
}

function parseEvents(packet) {
    var documents = [];

    packet.events.forEach(event => {
        var document = undefined;
        switch (event.id) {

            case "IGN_ON":
                document = { subtype: "ignitionOn", ignition: 1 };
                break;

            case "IGN_OFF":
                document = { subtype: "ignitionOff", ignition: 0 };
                break;

            case "TOW_CONFIRMED_BY_GPS":
                document = { subtype: "towing" }
                break;

            case "CRASH":
                document = { subtype: "collision" }
                break;

            case "PANIC_BUTTON":
                document = { subtype: "sos" }
                break;

            case "ACCELEROMETER_TRIGGER":
                document = { subtype: "vibration" }
                break;

            default:
                break;
        }

        if (document) {
            document.type = "alert";
            document.timestamp = new Date(packet.timestamps.device);
            document.gtwTimestamp = new Date(packet.timestamps.gtw);
            if (event.gps)
                document.location = utils.getGeoJson(event.gps[0], event.gps[1], true);
            documents.push(document);
        }
    });

    return documents;
}


module.exports = TowRegularParser;