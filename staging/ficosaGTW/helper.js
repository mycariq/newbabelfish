var utils = require("../../utils");
var origin = require("os").hostname();

exports.addDefaults = function (document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-FICOSA-GTW";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;

}
