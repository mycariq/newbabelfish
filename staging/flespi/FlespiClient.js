'use strict'

var Connection = require('flespi-io-js/dist/node.js')
var deasync = require('deasync');

var logger = require("../logger.js").getLogger("flespi", "receiver.log");

//constructor
var FlespiClient = function (token) {
    this.token = token;
}

FlespiClient.prototype = {
    connect: function () {
        this.connection = new Connection({
            token: this.token,
            socketConfig: {
                mqttSettings: {
                    resubscribe: true,
                }
            }
        });

        var connack = undefined, error = undefined;

        this.connection.socket.on('connect', function (conn) {
            connack = conn;
        });

        this.connection.socket.on('error', function (err) {
            error = err;

        });

        while (!error && !connack) {
            deasync.runLoopOnce();

        }

        if (error)
            throw new Error("Error while connecting : " + JSON.stringify(error));
        logger.info("Connected ::: " + JSON.stringify(connack));
    },

    subscribe: function (channelIds, handler) {
        var channelIdsArr = channelIds.split(",");
        var subscribeArray = [];

        for (var index = 0; index < channelIdsArr.length; index++) {
            subscribeArray[index] = {
                name: "flespi/message/gw/channels/" + channelIdsArr[index] + "/#",
                handler: function (message, topic) {
                    handler(topic, message.toString("utf-8"));
                }
            }
        }

       console.log(subscribeArray);
        this.connection.socket.subscribe(subscribeArray);
    }
}

module.exports = FlespiClient;


// var client = new FlespiClient("Vztx4bEDqAtiAzHDy6eZS0tJ8mbaTSCEfTZ9UtA8RThhjOJFILurBNYxYZxFF09I");
// client.connect();
// client.subscribe("$share/group_name/#", function (topic, message) {
//     console.log("================");
//     console.log("topic:::::", topic);
//     console.log("message:::::", message);
// })

// console.log("Done");