'use strict'

var utils = require("../utils");

var GpsParser = function () {
}

GpsParser.prototype = {

    parse(packet) {
        var document = { type: "data", subtype: "gps" };
        document.owner = packet.ident;
        document.location = utils.getGeoJson(packet["position.latitude"], packet["position.longitude"], true);
        document.speed = packet["position.speed"];
        document.voltage = packet["external.powersource.voltage"];
        document = utils.extendJson(document, packet);
        document.timestamp = new Date(Math.floor(packet.timestamp * 1000));
        return document;
    }
}

module.exports = GpsParser;