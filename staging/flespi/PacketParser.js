'use strict'
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../logger.js").getLogger("flespi", "receiver.log");

var gpsParser = new (require("./GpsParser"))();

class PacketParser {
	constructor() {
	}

	parse(packet) {

		var document = gpsParser.parse(packet);

		helper.addDefaults(document);

		logger.info("Parsed :::" + JSON.stringify(document));
		persist(document);
	}
}

function persist(document) {
	logger.info("Persisting into mongo Doc :::" + JSON.stringify(document));
	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;