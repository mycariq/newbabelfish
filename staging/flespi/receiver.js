
//Add following library into package.json
//"flespi-io-js": "git+https://github.com/flespi-software/flespi-io-js.git",
var FlespiClient = require("./FlespiClient");
var packetParser = new (require("./PacketParser"))();
var logger = require("../logger.js").getLogger("flespi", "receiver.log");
var config = require("../config").flespi;

var token = config.token;
var channels = config.channels;

if (channels && token) {

    var flespiClient = new FlespiClient("Vztx4bEDqAtiAzHDy6eZS0tJ8mbaTSCEfTZ9UtA8RThhjOJFILurBNYxYZxFF09I");
    flespiClient.connect();
    flespiClient.subscribe(channels, function (topic, message) {
        logger.info("Received packet:" + message + " from topic:" + topic);
        packetParser.parse(JSON.parse(message));
    });
}
