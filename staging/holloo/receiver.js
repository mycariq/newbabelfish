var Server = require("../server");
var logger = require("../logger.js").getLogger("holloo", "receiver.log");
//var parser = new (require("./PacketParser"))();

new Server("holloo", require("../config.json").holloo.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info("===========================");
	logger.info(data);
	var packet = data.toString("utf-8");
	logger.info("Hex str:" + data.toString("hex"));
	logger.info("Received raw packet : " + packet);

});

var gps = "$LOCATION,865734020000749,L,17-04-2006 14:22:48,17-04-2006 14:22:45,NR,0,18.556662,N,073.793187,E,120.4,087,12,2.24,3.64,0545.23,68,404,10,00D6,CFBD,0,0,0,0,010.5,000123,0000,0000,087965,MH-12AB-1234,VT01001_260718,4C*<CR><LF>";

//processPacket({}, maintenance);
