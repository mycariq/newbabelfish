var utils = require("../utils.js");
var status = require("./status.json");

function getTimeStamp(hex) {
	var str = utils.hexToText(hex);
	var timeStamp = "20";
	timeStamp += str.substr(0, 2) + "-" + str.substr(2, 2) + "-"
			+ str.substr(4, 2) + " ";
	timeStamp += str.substr(6, 2) + ":" + str.substr(8, 2) + ":"
			+ str.substr(10, 2);
	return timeStamp;
}

function getSerialNumber(packet) {
	return packet.substr(14, 12);
}

function getDataByTLV(data) {
	var result = {};
	for (var i = 0; i < data.length; i++) {
		var size = utils.hexToInt(data.substr(i + 4, 4)) * 2;
		result[data.substr(i, 4)] = data.substr(i + 8, size - 8);
		if (data.substr(i, 4) === "C0C0")
			break;
		i = i + size - 1;
	}
	return result;
}

function getStatus(hex) {
	var bin = utils.hexToBin(hex);
	var result = {};

	for (var i = 0; i < bin.length; i++) {
		subStatus = status[i];
		if (!subStatus)
			continue;
		result[subStatus.key] = subStatus[bin[i]];
	}
	return result;
}

exports.getTimeStamp = getTimeStamp;
exports.getDataByTLV = getDataByTLV;
exports.getSerialNumber = getSerialNumber;
exports.getStatus = getStatus;
