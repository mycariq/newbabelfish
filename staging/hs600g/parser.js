var utils = require("../utils.js");
var helper = require("./helper");
var config = require("../config")["hs600g"];
var packetType = require("./packet-type.json");

var logger = require("../logger.js");
var mylogger = logger.getLogger("hs600g", "parser.log");


var port = config["port"];
const
DAS_BASEURL = "http://gearbox.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/pushToBC/{id}/{timeStamp}/{packet}";
// DAS_BASEURL =
// "http://localhost:8080/DataAcquisitionModule/dataacquisitions/pushToBC/{id}/{timeStamp}/{packet}";
// new Consumer(port, function(data) {
// 	var message = data.packet;
// 	mylogger.denug("(Consumer)-" + JSON.stringify(message));
// 	var command = message.command;
// 	mylogger.debug("(Consumer)-command" + command);
// 	if (command === "AA00")
// 		parsePositionPacket(message);
// 	else if (command === "AA12")
// 		parseFaultPacket(message);
// 	else if (command === "AA02")
// 		parseLoginPacket(message);
// 	else if (command === "AA14")
// 		parseHandshakePacket(message);
// 	else
// 		mylogger.error("(Consumer)-Unparsed=" + JSON.stringify(message));
// });

function parseHandshakePacket(message) {
	var result = {};
	result.id = message.id;
	result.timeStamp = utils.getServerTimeStamp();
	result.odo_speed = "0000";
	result.rpm = "0000";
	mylogger.debug("(parseHandshakePacket)-" + JSON.stringify(result));
	utils.sendPacket(createUrl(result));
}

function parseLoginPacket(message) {
	var packet = message.packet;
	var tlv = helper.getDataByTLV(packet.substring(26, packet.length - 2));
	var result = {};
	result.id = message.id;
	result.timeStamp = utils.getServerTimeStamp();
	result.odo_speed = "0000";
	result.rpm = "0000";
	if (tlv["0001"])
		result.firmwareVersion = utils.hexToText(tlv["0001"]);
	if (tlv["0002"])
		result.vin = utils.hexToText(tlv["0002"]);
	if (tlv["0003"])
		result.imei = utils.hexToText(tlv["0003"]);
	if (tlv["0005"])
		result.parameterRequestIdentification = tlv["0005"];
	if (tlv["0006"])
		result.APNUserName = utils.hexToText(tlv["0006"]);
	if (tlv["0007"])
		result.APNPassword = utils.hexToText(tlv["0007"]);
	if (tlv["0008"])
		result.deviceType = tlv["0008"];
	if (tlv["0009"])
		result.simNumber = utils.hexToText(tlv["0009"]);
	if (tlv["000A"])
		result.apn = utils.hexToText(tlv["000A"]);

	mylogger.debug("(parseLoginPacket)-" + JSON.stringify(result));
	utils.sendPacket(createUrl(result));
}
function parsePositionPacket(message) {
	var packet = message.packet;
	var result = {};

	result.id = message.id;
	result.serialNumber = helper.getSerialNumber(packet);
	result["status " + packet.substr(26, 4)] = helper.getStatus(packet.substr(
			26, 4));
	result.type = packetType[packet.substr(30, 4)] + " " + packet.substr(30, 4);
	result.timeStamp = helper.getTimeStamp(packet.substr(34, 24));
	result.latitude = packet.substr(66, 8);
	result.longitude = packet.substr(58, 8);
	result.gps_speed = packet.substr(74, 4);
	result.direction = packet.substr(78, 4);
	result.altitude = packet.substr(82, 4);
	result.odo_speed = packet.substr(86, 4);
	result.TLV = packet.substring(90, packet.length - 2);
	var tlvResult = helper.getDataByTLV(result.TLV);
	mylogger.debug("(parsePositionPacket)-tlv_result=" + JSON.stringify(tlvResult));

	if (tlvResult["0001"]) {
		var obd = tlvResult["0001"];
		result.obd = obd;
		result.coolant_temperature = obd.substr(0, 2);
		result.rpm = obd.substr(2, 4);
		result.average_speed = obd.substr(6, 2);
		result.obd_fuel_consumption = obd.substr(8, 4);
		result.obd_fuel_consumption_per_100km = obd.substr(12, 4);
		result.interval_milage = obd.substr(16, 4);
		result.voltage = obd.substr(20, 4);
		result.fuel_tank_volume = obd.substr(24, 2);
		result.trip_id = obd.substr(26, 8);
	}

	mylogger.debug("(parsePositionPacket)-" + JSON.stringify(result));

	utils.sendPacket(createUrl(result));
}

function parseFaultPacket(message) {
	var packet = message.packet;
	var result = {};
	result.id = message.id;
	result.version = packet.substr(26, 4);
	result.errors = getErrors(packet.substring(30, packet.length - 2));
	mylogger.debug("(parseFaultPacket)-" + JSON.stringify(result));

	var url = DAS_BASEURL.replace("{id}", result.id).replace("{timeStamp}",
			utils.getServerTimeStamp().replace(" ", "%20")).replace("{packet}",
			"HS06" + result.errors);
	utils.sendPacket(url);
}

function getErrors(errorsString) {
	var errors = "";
	var codes = {};
	codes["0"] = "P0";
	codes["1"] = "P1";
	codes["2"] = "P2";
	codes["3"] = "P3";

	codes["4"] = "C0";
	codes["5"] = "C1";
	codes["6"] = "C2";
	codes["7"] = "C3";

	codes["8"] = "B0";
	codes["9"] = "B1";
	codes["A"] = "B2";
	codes["B"] = "B3";

	codes["C"] = "U0";
	codes["D"] = "U1";
	codes["E"] = "U2";
	codes["F"] = "U3";
	for (var i = 0; i < errorsString.length; i += 4) {
		var error = errorsString.substr(i, 4);
		errors += codes[error.substr(0, 1)] + error.substr(1, 3) + "|";

	}
	return errors;
}

function getLongitude(packet) {
	return parseInt(packet.substr(58, 8), 16) / 100000;
}

function getLatitude(packet) {
	return parseInt(packet.substr(66, 8), 16) / 100000;
}

function createUrl(json) {
	var timeStamp = json.timeStamp.replace(' ', '%20');
	return DAS_BASEURL.replace("{id}", json.id).replace("{timeStamp}",
			timeStamp).replace("{packet}", createPacket(json));
	// return DAS_BASEURL
	// + json["id"]
	// + "/"
	// + timeStamp
	// + "/speed="
	// + json["odo_speed"]
	// + ",Cariq_Latitude="
	// + ((json["latitude"] == 0 || json["latitude"] > 100) ? "NULL"
	// : json["latitude"])
	// + ",Cariq_Longitude="
	// + ((json["longitude"] == 0 || json["longitude"] > 100) ? "NULL"
	// : json["longitude"]);
}

function createPacket(json) {
	return ((json.latitude) ? "HS01" + json.latitude + "," : "")
			+ ((json.longitude) ? "HS02" + json.longitude + "," : "")
			+ ((json.odo_speed) ? "HS03" + json.odo_speed + "," : "")
			+ ((json.rpm) ? "HS04" + json.rpm + "," : "")
			+ ((json.voltage) ? "HS05" + json.voltage + "," : "")
			+ ((json.vin) ? "41Y5" + json.vin + "," : "");
}
