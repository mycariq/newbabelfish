//import server module
var Server = require("../server.js");

var config = require("../config")["hs600g"];

var utils = require("../utils.js");
var helper = require("./helper");
var port = config["port"];

var logger = require("../logger.js");
var mylogger = logger.getLogger("hs600g", "receiver.log");


new Server("hs600g", port).start(function (err, data, id, socket) {

	//if error is occured then log it exit from function
	if (err) {
		mylogger.error("(Server)-Unexpected Error : " + err);
		return;
	}
	//if data is undefined then exit from function
	if (!data)
		return;

	var packet = new Buffer(data, "utf8").toString("hex").toUpperCase();

	// if has error then handles
	mylogger.debug("(Server)-Packet received:" + packet);

	packet = escapePackets(packet);
	mylogger.debug("(Server)-After escape=" + packet);

	log(packet);

	var packets = getPackets(packet);
	mylogger.debug("(Server)-After separating=" + packet);
	var id;
	for (var i = 0; i < packets.length; i++) {
		var packet = packets[i];
		var command = getCommandType(packet);
		mylogger.debug("(Server)-command=" + command);

		var newId = getPacketResponse(command, packet, socket);
		if (newId)
			id = newId;

		if (isQueueable(command)) {
			mylogger.debug("(Server)-packet is queueable");
			var result = {};
			// result["packet"] = packet.replace("DBDC", "C0");
			// result["packet"] = packet.replace("DBDD", "DB");
			result["packet"] = packet;
			result["id"] = id;
			result["command"] = command;
			mylogger.debug("(Server)-result" + result);
			producer.produce({
				packet: result
			});
		}
	}
	var response = [];
	response["id"] = id;
	return response;
});

function getPackets(packet) {
	var packets = [];
	var index = 0;
	packets[index] = "";
	for (var i = 0; i < packet.length; i += 2) {
		if (packet.substr(i, 2) === "C0") {
			if (packet.substr(i + 2, 2) === "C0") {
				packets[index++] += "C0";
				packets[index] = "C0";
				i += 2;
			} else
				packets[index] += packet.substr(i, 2);
		} else
			packets[index] += packet.substr(i, 2);
	}
	return packets;
}

function escapePackets(packet) {
	if (packet.indexOf("DB") < 0)
		return packet;
	var escapePacket = "";
	for (var i = 0; i < packet.length; i += 2) {
		if (packet.substr(i, 2) === "DB") {
			if (packet.substr(i + 2, 2) === "DC") {
				escapePacket += "C0";
				i += 2;
			} else if (packet.substr(i + 2, 2) === "DD") {
				escapePacket += "DB";
				i += 2;
			} else
				escapePacket += packet.substr(i, 2);
		} else
			escapePacket += packet.substr(i, 2);
	}
	return escapePacket.toUpperCase();
}
function log(packet) {
	var command = getCommandType(packet);
	var msg = {};
	if (command === "AA02") {
		msg.type = "login";
		msg.packet = packet;
		logger.info(msg);
	} else {
		type = packet.substr(30, 4);
		if (type === "0000") {
			msg.type = "live";
			msg.packet = packet;
			msg.time = helper.getTimeStamp(packet.substr(34, 24));
			logger.info(msg);
		}
		if (type === "0001") {
			msg.type = "backlog";
			msg.packet = packet;
			msg.time = helper.getTimeStamp(packet.substr(34, 24));
			logger.info(msg);
		}
		if (type === "0002") {
			msg.type = "logout";
			msg.packet = packet;
			msg.time = helper.getTimeStamp(packet.substr(34, 24));
			logger.info(msg);
		}
	}
}

function getPacketResponse(command, packet, socket) {
	var newId;
	if (command === "AA02") {
		socket.write(new Buffer(getLoginResponse(packet), "hex"));
		newId = getImeiNumber(packet);
		mylogger.debug("(getPacketResponse)-Writing set request==" + getSetRequest());
		socket.write(new Buffer(getSetRequest(), "hex"));
	}
	if (command === "AA00") {
		socket.write(new Buffer(getPositionResponse(packet), "hex"));
	}
	if (command === "AA12") {
		socket.write(new Buffer(getFaultResponse(packet), "hex"));
	}
	return newId;
}

function getSetRequest() {
	var packet = /* cmdCrcSeqNum */"AA04000000000001"
		+ /* sleepWakeUp */"00030006001E"
		+ /* reportinterval */"000200060005"
		+ /* harsh acceleration */"000800060003"
		+ /* harsh deceleration */"000900060005"
		+ /* harsh turn sensitivity */"001000060005";
	var hexLength = ((4 + 4 + packet.length) / 2).toString(16);
	var packetLength = hexLength;
	for (var i = 0; i < (4 - hexLength.length); i++)
		packetLength = "0" + packetLength;
	return "C00000" + packetLength + packet + "C0";
}

function getCommandType(packet) {
	return packet.substr(10, 4);
}

function getLoginResponse(packet) {
	var command = "FF03";
	var serialNumber = helper.getSerialNumber(packet);
	var response = "00";
	var packetLength = (4 + 6 + 2 + command.length + serialNumber.length + response.length) / 2;
	return "C0" + "010000" + utils.intToHexStr(packetLength) + command
		+ serialNumber + response + "C0";
}

function getFaultResponse(packet) {
	var command = "FF13";
	var response = "00";
	var packetLength = (4 + 6 + 2 + command.length + response.length) / 2;
	return "C0" + "010000" + utils.intToHexStr(packetLength) + command
		+ response + "C0";
}

function getPositionResponse(packet) {
	var command = "FF01";
	var serialNumber = helper.getSerialNumber(packet);
	var packetLength = (4 + 6 + 2 + command.length + serialNumber.length) / 2;
	return "C0" + "010000" + utils.intToHexStr(packetLength) + command
		+ serialNumber + "C0";
}

function isQueueable(command) {
	if (command === "AA00" || command === "AA12" || command === "AA02"
		|| command === "AA14")
		return true;
	return false;
}

function getImeiNumber(packet) {
	var result = helper.getDataByTLV(packet.substring(26, packet.length - 2));
	mylogger.debug("(getImeiNumber)-login_result=" + JSON.stringify(result));

	if (result["0003"]) {
		return new Buffer(result["0003"], "hex").toString("utf-8");
	}

	return undefined;
}
