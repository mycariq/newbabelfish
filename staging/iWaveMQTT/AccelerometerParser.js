var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class AccelerometerParser {
	constructor() {
	}

	parse(data) {
		var document = { type: "data", subtype: "accelerometer" };
		document.owner = data.imei.toString();
		document.timestamp = new Date(data.ts);
		document.accelerometerX = data["acc:x-axis"];
		document.accelerometerY = data["acc:y-axis"];
		document.accelerometerZ = data["acc:z-axis"];
		return document;
	}
}

module.exports = AccelerometerParser;