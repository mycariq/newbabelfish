var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class DtcParser {
	constructor() {
	}

	parse(data) {
		var document = { type: "data", subtype: "dtc" };
		document.owner = data.imei.toString();
		document.timestamp = new Date(data.ts);
		document.errorCount = data.Error_Count;
		document.errorCodeDTC = data.DTC.join("|");
		return document;
	}
}

module.exports = DtcParser;