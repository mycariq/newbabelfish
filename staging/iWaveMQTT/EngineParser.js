var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class EngineParser {
	constructor() {
	}

	parse(data) {
		var document = { type: "data", subtype: "obd" };
		document.owner = data.imei.toString();
		document.timestamp = new Date(data.ts);
		document.rpm = data.rpm;
		document.speed = data.speed;
		document.engineLoad = data.engine_load;
		document.engineTemp = data.engine_temp;
		document.voltage = data.bat_volt;
		return document;
	}
}

module.exports = EngineParser;