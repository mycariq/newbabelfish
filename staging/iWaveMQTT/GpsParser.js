var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
	constructor() {
	}
	parse(data) {
		var document = { type: "data", subtype: "gps" };
		document.owner = data.imei.toString();
		document.timestamp = new Date(data.ts);
		document.location = utils.getGeoJson(data.lt, data.ln, true);
		document.speed = data.speed;
		document.v = data.v;
		return document;
	}
}


module.exports = GpsParser;