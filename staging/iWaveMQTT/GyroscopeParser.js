var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GyroscopeParser {
	constructor() {
	}

	parse(data) {
		var document = { type: "data", subtype: "gyroscope" };
		document.owner = data.imei.toString();
		document.timestamp = new Date(data.ts);
		document.gyroscopeX = data["gyro:x-axis"];
		document.gyroscopeY = data["gyro:y-axis"];
		document.gyroscopeZ = data["gyro:z-axis"];
		return document;
	}
}

module.exports = GyroscopeParser;