var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var logger = require("../../logger.js").getLogger("iWaveMQTT", "receiver.log");
var helper = require("./helper");

var parsers = {
    "accelerometer": new (require("./AccelerometerParser"))(),
    "gps": new (require("./GpsParser"))(),
    "dtc": new (require("./DtcParser"))(),
    "engine": new (require("./EngineParser"))(),
    "gyroscope": new (require("./GyroscopeParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(command, data) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  packet:" + JSON.stringify(data));
            return;
        }

        //parse data
        var document = parser.parse(data);
        if (!document)
            return;

        //add owner and defaults
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;