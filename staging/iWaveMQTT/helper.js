var logger = require("../../logger.js").getLogger("iWave", "receiver.log");
var utils = require("../../utils");
var origin = require("os").hostname();

function addDefaults(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "iWaveMQTT";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = utils.getGeoJson(0.0, 0.0, false);
		}
	}
	return document;
}

exports.addDefaults = addDefaults;