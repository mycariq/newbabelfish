var mqttUrl = require("../../config.json").mqtt.url;
var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("iWaveMQTT", "receiver.log");
var parser = new (require("./PacketParser"))();

var options = {
	username: "hardware@mycariq.com",
	password: "NJ8pn*#kg@c=*Am4",
	ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}

//Unsecured
var mqttClient = new (require("../../MQTTClient"))(mqttUrl, options, packetReceiver);
mqttClient.subscribe("/driven/iWaveHardware/#");

//TLS
//var tlsMqttClient = new (require("../MQTTClient"))(mqttUrl.replace("1883", "8883").replace("mqtt", "mqtts"), options, packetReceiver);
//tlsMqttClient.subscribe("/tls/driven/iWaveHardware/#");


function packetReceiver(topic, message) {
	var packet = message.toString("utf-8");
	logger.info("Received topic: " + topic + " packet: " + packet);
	processPacket(topic, packet);
}

function processPacket(topic, packet) {
	//validate packet
	if (!isValidPacket(packet))
		return;

	packet = JSON.parse(packet);
	var command = packet.type;

	//parse packet
	parser.parse(command, packet);

	//send response
	//TODO if required then only
}

function isValidPacket(packet) {
	return true;
}

//var accelerometer = { "ts": "2021-03-19T05:47:46.466Z", "imei": 864004043113015, "type": "accelerometer", "acc:x-axis": 0.691057, "acc:y-axis": 2.167025, "acc:z-axis": 9.797942 };
//var gyroscope = { "ts": "2021-03-19T05:47:48.469Z", "imei": 864004043113015, "type": "gyroscope", "gyro:x-axis": 0.014841, "gyro:y-axis": -0.032130, "gyro:z-axis": 0.013617 };
//var engine = { "ts": "2021-03-19T05:48:19.587Z", "imei": 864004043113015, "type": "engine", "rpm": 10546, "speed": 166, "engine_load": 19, "engine_temp": 215, "bat_volt": 12.04 };
//var gps = {"ts":"2021-03-19T05:48:03.507Z","imei":864004043113015, "type":"gps", "lt":12.912602,"ln":77.614768,"speed":123, "v":1}
//var dtc = {"ts":"2019-07-04T14:42:38.870Z","imei":864004043113015, "type":"dtc", "Error_Count": 2, "DTC":["P0004","P0005"]};

//mqttClient.publish("/driven/iWaveHardware/" + Math.random(), JSON.stringify(dtc));