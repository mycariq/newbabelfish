//to start execute following command from root dir of babelfish
//forever -a -l /opt/log/forever/ibm.log -o /opt/log/forever/ibm.out -e /opt/log/forever/ibm.err --uid ibmIot start ibm/consumer.js

//create client
var MongoClient = require("../mongo/mongo-client");
var mongoClient = new MongoClient().connect();

//logger
var logger = require("../logger.js").getLogger("ibm", "consumer.log");

//import library
var Client = require("ibmiotf");

//configuration details
var appClientConfig = {
	"org" : "eogc2l",
	"id" : "ibmIOT",
	"auth-key" : "a-eogc2l-1dvvod32n5",
	"auth-token" : "FIP*uaCj(5O--m1ZWw"
}

//create instance of client and connect to ibm
var client = new Client.IotfApplication(appClientConfig);
client.connect();

//proceed further if connection established successfully
client.on("connect", function() {
	logger.debug("connected successfully..!!");
	client.subscribeToDeviceEvents();

});

//log data on receive
client.on("deviceEvent", function(deviceType, deviceId, eventType, format, payload) {
	logger.debug("Received::: deviceType:" + deviceType + " deviceId:" + deviceId + " eventType:" + eventType + " format:" + format + " payload:" + payload);
	processPacket(JSON.parse(payload.toString()));
});

//if error occurred while connecting then log error and throw
client.on("error", function(err) {
	logger.error("Error while connecting..", err);
});

//process packet and store into mongo
function processPacket(payload) {
	var packet = payload.d;
	//create mongo document 
	var document = {};
	document.agent = "GoldBell";
	document.type = "data";
	document.subtype = "obdgps";
	document.serverTimestamp = new Date;
	document.origin = require("os").hostname();
	document.owner = "" + packet.RMUId;
	document.timestamp = new Date(packet.GPSDateTime);
	document.speed = packet.Speed;
	document.location = {
		"type" : "Point",
		"isReal" : true,
		"coordinates" : [ packet.lng, packet.lat ]
	};
	document.rpm = packet.props.engineRPM;
	document.voltage = packet.props.InputVoltage;
	logger.debug("Mongo Doc:", JSON.stringify(document));

	//insert into mongo
	logger.debug("inserting document:" + JSON.stringify(document));
	mongoClient.insert("geobit", document, function(err, result) {
		logger.debug("Document inserted..!!");
		if (err) {
			logger.error("error while inserting document:" + err);
			logger.error(err);
			throw Error(err);
		}
	});
}