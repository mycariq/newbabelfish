var helper = require("./helper");
var utils = require("../../utils");

'use strict'
class GpsExpandedParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "gps" };
        var latitude = parseFloat(helper.getValue(data, 5));
        var longitude = parseFloat(helper.getValue(data, 6));
        document.location = utils.getGeoJson(latitude, longitude, true);
        document.timestamp = helper.getTimestamp(helper.getValue(data, 7));
        document.gpsStatus = helper.getValue(data, 8);
        document.gsmSignal = helper.getValue(data, 9);
        document.speed = parseFloat(helper.getValue(data, 10));
        document.accumulatedDistance = helper.getValue(data, 11);
        document.degree = helper.getValue(data, 12);
        document.noOfSatellite = helper.getValue(data, 13);
        document.hdop = helper.getValue(data, 14);
        document.voltageEquivalentOfAnalog = parseFloat(helper.getValue(data, 17));
        document.digitalInputStatus1 = helper.getValue(data, 18);
        document.tamper = helper.getValue(data, 19);
        document.overSpeedStarted = helper.getValue(data, 20);
        document.overSpeedEnd = helper.getValue(data, 21);
        document.immobilizer = helper.getValue(data, 24);
        document.powerStatus = helper.getValue(data, 25);
        document.digitalInputStatus2 = helper.getValue(data, 26);
        document.ignitionStatus = helper.getValue(data, 29);
        document.internalBatteryLow = helper.getValue(data, 36);
        document.anglePollingBit = helper.getValue(data, 37);
        document.digitalOutputStatus1 = helper.getValue(data, 42);
        document.harshAcceleration = helper.getValue(data, 44);
        document.harshBraking = helper.getValue(data, 45);
        document.digitalInputStatus3 = helper.getValue(data, 47);
        document.digitalInputStatus4 = helper.getValue(data, 48);
        document.digitalOutputStatus2 = helper.getValue(data, 49);
        document.voltage = parseFloat(helper.getValue(data, 50)) / 1000;
        document.internalVoltage = parseFloat(helper.getValue(data, 51)) / 1000;

        return document;
    }
}

module.exports = GpsExpandedParser;