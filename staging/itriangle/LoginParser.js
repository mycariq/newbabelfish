var helper = require("./helper");

'use strict'
class LoginParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "login" };
        document.timestamp = new Date();
        document.firmwareVersion = helper.getValue(data, 5);
        document.serverAddress = helper.getValue(data, 6);
        document.port = helper.getValue(data, 7);
        document.apn = helper.getValue(data, 8);
        document.ignitionOnReportingInterval = helper.getValue(data, 9);
        document.ignitionOffReportingInterval = helper.getValue(data, 10);
        document.adminNo1 = helper.getValue(data, 11);
        document.adminNo2 = helper.getValue(data, 12);
        document.gmtOffset = helper.getValue(data, 13);
        document.reserved = helper.getValue(data, 14);
        document.overSpeedLimit = helper.getValue(data, 15);
        document.overSpeedDuration = helper.getValue(data, 16);
        document.gpsFixStatus = helper.getValue(data, 17);
        document.ignitionStatus = helper.getValue(data, 18);
        return document;
    }
}

module.exports = LoginParser;