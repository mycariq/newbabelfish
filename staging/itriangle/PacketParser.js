var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger").getLogger("itriangle", "receiver.log");;

var parsers = {
    "1": new (require("./GpsExpandedParser"))(),
    "101": new (require("./GpsExpandedParser"))(),//backlog packet
    "3": new (require("./AlertParser"))("powerFail"),
    "4": new (require("./AlertParser"))("DigitalInput1High"),
    "5": new (require("./AlertParser"))("DigitalInput1Low"),
    "6": new (require("./AlertParser"))("DigitalInput2High"),
    "7": new (require("./AlertParser"))("DigitalInput2Low"),
    "8": new (require("./AlertParser"))("OverspeedStart"),
    "9": new (require("./AlertParser"))("OverspeedEnd"),
    "10": new (require("./AlertParser"))("wakeupVoltage"),
    "11": new (require("./AlertParser"))("dormancyVoltage"),
    "12": new (require("./AlertParser"))("rapidDeceleration"),
    "13": new (require("./AlertParser"))("internalBatteryLow"),
    "15": new (require("./LoginParser"))(),
    "17": new (require("./AlertParser"))("suddenSpeededUp"),
    "19": new (require("./AlertParser"))("anglePolling"),
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(data) {
        //get parser from registry
        var parser = parsers[data[2]];
        if (!parser) {
            logger.info("No parser found for " + data[2]);
            return;
        }

        //parse data
        var document = parser.parse(data);
        if (!document)
            return;

        //add owner and defaults
        document = utils.extendJson({ owner: data[1] }, document);
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}
module.exports = PacketParser;