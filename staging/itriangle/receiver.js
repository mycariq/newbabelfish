var Server = require("../../server");
var utils = require("../../utils");
var logger = require("../../logger").getLogger("itriangle", "receiver.log");;
var packetParser = new (require("./PacketParser"))();

new Server("itriangle", require("../../config.json").itriangle.port).start(function (err,
	data, id, socket) {
	//convert hex buffer into ascii/utf-8
	var rawPacket = data.toString();
	logger.info("received: " + rawPacket);

	processPacket(socket, rawPacket);
});

function processPacket(socket, rawPacket) {
	//split packet into array by ',' separator
	var packArray = rawPacket.split(",");
	packetParser.parse(packArray);

}


//var loginPacket = "$$CLIENT_1NS,180225591,15,1_36T02B0164M_9,dev-babelfish.mycariq.com,62999,mycariq.com,T1:30 S,T2:1 M,Ad1:+919164061023,Ad2:+919164061023,TOF:19800 S,,OSC:75 KM,OST:10 S,GPS:NO,Ignition:OFF,*67";
//var gpsExpandedPacket = "$$CLIENT_1NS,180225591,1,0.000000,0.000000,800106053201,V,31,0,0,0,0,0.000000,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12445,4003,*2B";
// var engineOnAlert="$$CLIENT_1NS,180225591,10,0.000000,0.000000,180803061631,V,31,0,0,31,0,0.000000,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12084,4003,*2B";
// var engineOffAlert="$$CLIENT_1NS,180225591,11,0.000000,0.000000,180803061643,V,31,0,0,31,0,0.000000,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12021,3997,*22";
// var backlog = "$$CLIENT_1NS,180225591,101,18.569878,73.762726,180802153525,A,0,0,0,183,6,1.950000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,12127,3981,*07";
//processPacket(undefined, gpsExpandedPacket);
