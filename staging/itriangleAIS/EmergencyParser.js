var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class EmergencyParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "emergency" };
        document.timestamp = new Date();
        document.vendorId = data[1];
        document.messageType = data[2];
        document.owner = data[3];
        document.packetType = data[4];
        document.timestamp = helper.getTimestamp(data[5]);
        document.gpsValidity = data[6];
        document.location = helper.getGeoJson(data[7], data[8], data[9], data[10]);
        document.altitude = data[11];
        document.speed = parseFloat(data[12]);
        document.distance = data[13];
        document.provider = data[14];
        document.registrationNo = data[15];
        document.replyNo = data[16];
        return document;
    }
}

module.exports = EmergencyParser;