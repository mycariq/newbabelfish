var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.vendorId = data[1];
        document.firmwareVersion = data[2];
        document.packetType = data[3];
        document.messageId = parseInt(data[4]);
        addPacketType(document);
        document.packetStatus = data[5];
        document.owner = data[6];
        document.registrationNo = data[7];
        document.gpsFix = data[8];
        document.timestamp = helper.getTimestamp(data[9] + data[10]);
        document.location = helper.getGeoJson(data[11], data[12], data[13], data[14]);
        document.speed = parseFloat(data[15]);
        document.heading = data[16];
        document.noOfSattelite = data[17];
        document.altitude = data[18];
        document.pdop = data[19];
        document.hdop = data[20];
        document.networkOperator = data[21];
        document.ignitionStatus = data[22];
        document.mainPowerStatus = data[23];
        document.voltage = parseFloat(data[24]);
        document.internalVoltage = parseFloat(data[25]);
        document.emergencyStatus = data[26];
        document.tamperAlert = data[27];

        if (document.messageId >= 3) {
            var newDocument = utils.extendJson({}, document);
            newDocument.type = "data";
            newDocument.subtype = "gps";
            return [document, newDocument];
        }
        return document;
    }
}

function addPacketType(document) {
    if (document.messageId == 1 || document.messageId == 2) {
        document.type = "data";
        document.subtype = "gps";
        return;
    }

    document.type = "alert";
    switch (document.messageId) {

        case 3:
            document.subtype = "pullOutReminder";
            break;

        case 4:
            document.subtype = "lowBatteryVoltage";
            break;

        case 5:
            document.subtype = "lowBatteryVoltageGone";
            break;

        case 6:
            document.subtype = "dormancyEnergize";
            break;

        case 7:
            document.subtype = "ignitionOn";
            break;

        case 8:
            document.subtype = "ignitionOff";
            break;

        case 9:
            document.subtype = "gpsBoxOpened";
            break;

        case 10:
            document.subtype = "emergencyOn";
            break;

        case 11:
            document.subtype = "emergencyOff";
            break;

        case 12:
            document.subtype = "paramChangedOverTheAir";
            break;

        case 13:
            document.subtype = "rapidDeceleration";
            break;

        case 14:
            document.subtype = "suddenSpeededUp";
            break;

        case 15:
            document.subtype = "sharpTurn";
            break;

        case 16:
            document.subtype = "emergencyButtonTampered";
            break;

        default:
            document.subtype = "unknown";
    }
}

module.exports = GpsParser;