var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class HealthParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "health" };
        document.timestamp = new Date();
        document.vendorId = data[1];
        document.firmwareVersion = data[2];
        document.owner = data[3];
        document.batteryPercentage = data[4];
        document.lowBatteryThresholdPercentage = data[5];
        document.memoryPercentage1 = data[6];
        document.memoryPercentage2 = data[7];
        document.dataUpdateRateIgnitionOn = data[8];
        document.dataUpdateRateIgnitionOff = data[9];
        document.digitalInputStatus = data[10];
        document.analogInput1Status = data[11];
        document.analogInput2Status = data[12];
        return document;
    }
}

module.exports = HealthParser;