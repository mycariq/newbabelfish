var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LoginParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "login" };
        document.timestamp=new Date();
        document.vendorId = data[1];
        document.registrationNo = data[2];
        document.owner = data[3];
        document.imei = data[3];
        document.firmwareVersion = data[4];
        document.protocolVersion = data[5];
        document.location = helper.getGeoJson(data[6], data[7], data[8], data[9]);

        return document;
    }
}

module.exports = LoginParser;