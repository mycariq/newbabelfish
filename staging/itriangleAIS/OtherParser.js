var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class OtherParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "yet_to_implement" };
        document.timestamp = new Date();
        document.data = data.join(",");
        return document;
    }
}

module.exports = OtherParser;