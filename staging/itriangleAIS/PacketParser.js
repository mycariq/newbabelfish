var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("itriangleAIS", "receiver.log");;

var parsers = {
    "login": new (require("./LoginParser"))(),
    "gps": new (require("./GpsParser"))(),
    "health": new (require("./HealthParser"))(),
    "emergency": new (require("./EmergencyParser"))(),
    "other": new (require("./OtherParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(command, data) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for " + command + " Packet:" + data);
            return;
        }

        //parse data
        var document = parser.parse(data);
        if (!document) {
            logger.info("No document returned by parser : " + command + " packet: " + data);
            return;
        }

        //persist document
        persist(document);
    }
}

function persist(documents) {
    if (!Array.isArray(documents)) {
        documents = [documents];
    }

    documents.forEach(function (document) {
        logger.info("Parsed packet " + JSON.stringify(document));
        //add defaults
        helper.addDefaults(document);
        kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });

}

module.exports = PacketParser;