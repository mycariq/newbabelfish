var utils = require("../../utils");
var origin = require("os").hostname();;

//adds default values to mongo document
function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-iTriangle-AIS";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function getGeoJson(lat, latDir, long, longDir) {
    var latitude = parseFloat(lat);
    var longitude = parseFloat(long);

    return utils.getGeoJson(latitude, longitude, true);
}

function getTimestamp(datetime) {
    var timestampStr = datetime.substr(4, 4) + "-" + datetime.substr(2, 2) + "-" + datetime.substr(0, 2);
    timestampStr += " " + datetime.substr(8, 2) + ":" + datetime.substr(10, 2) + ":" + datetime.substr(12, 2) + " UTC";

    return new Date(timestampStr);
}

exports.addDefaults = addDefaults;
exports.getGeoJson = getGeoJson;
exports.getTimestamp = getTimestamp;