var Server = require("../../server");
var logger = require("../../logger.js").getLogger("itriangleAIS", "receiver.log");
var parser = new (require("./PacketParser"))();

new Server("itriangleAIS", require("../../config.json").itriangleAIS.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info("===========================");
	logger.info(data);
	var packet = data.toString("utf-8");
	logger.info("Hex str:" + data.toString("hex"));
	logger.info("Received raw packet : " + packet);

	var packets = packet.split("\n");
	for (var i = 0; i < packets.length; i++) {
		if (packets[i])
			processPacket(socket, packets[i]);
	}
});

function processPacket(socket, packet) {

	logger.info("Parsing :" + packet);
	var packetArray = packet.split(",");

	var packetType = undefined;
	var arrLength = packetArray.length;
	if (packetArray.length == 10)
		packetType = "login";
	else if (arrLength > 48 && arrLength < 55)
		packetType = "gps";
	else if (arrLength > 12 && arrLength < 15)
		packetType = "health";
	else if (arrLength > 16 && arrLength < 20)
		packetType = "emergency";
	else
		packetType = "other";

	parser.parse(packetType, packetArray);
}

var login = "$Header,iTriangle,KA01I2000,861693034634154,1_37T02B0164MAIS,AIS140,12.976545,N,77.549759,E*50";
var gps = "$Header,iTriangle,1_37T02B0164MAIS,BR,6,L,861693034634154,KA01I2000,1,09112017,160702,12.976593,N,77.549782,E,25.1,344,15,911.0,1.04,0.68,Airtel,1,1,11.8,3.8,1,C,24,404,45,61b4,9ad9,31,9adb,61b4,35,ffff,0000,33,ffff,0000,31,ffff,0000,0001,00,000014,0.0,0.1,4,()*1E";
var health = "$Header,iTriangle,1_37T02B0164MAIS,861693034634154,65,40,0.33,0.33,10,60,0001,0.0,0.0*5A";
var emergency = "$Header,iTriangle,EMR,861693034634154,NM,09112017155133,A,12.976495,N,77.549713,E,906.0,0.0 ,23,G,KA01I2000,+919844098440*4B";

//processPacket({}, gps);
