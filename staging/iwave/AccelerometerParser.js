var utils = require("../../utils");

'use strict'
class AccelerometerParser{
	constructor(){
	}
	
	parse(owner,data){
		var splits = utils.hexToText(data).split(",");
		var document = { owner:owner, type:"data", subtype:"accelerometer" };
		document.accelerometerX = splits[0];
		document.accelerometerY = splits[1];
		document.accelerometerZ = splits[2];
		document.timestamp = new Date();
		return require("./helper").addDefaults(document);
	}
}

module.exports = AccelerometerParser;