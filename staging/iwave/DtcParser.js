var utils = require("../../utils");

'use strict'
class DtcParser{
	constructor(){
	}
	
	parse(owner,data){
		var splits = utils.hexToText(data).split(",");
		var document = { owner:owner, type:"data", subtype:"dtc", "timestamp":new Date() };
		document.errorCodeDTC = splits[0];
		return require("./helper").addDefaults(document);
	}
}

module.exports = DtcParser;