var utils = require("../../utils");

'use strict'
class GpsParser{
	constructor(){
	}
	parse(owner, data){
		var splits = utils.hexToText(data).split(",");
		var document = { owner:owner, type:"data", subtype:"gps" };
		var timestamp = new Date(splits[0]);
//		timestamp.setMinutes(timestamp.getMinutes() - 330);
		document.timestamp = timestamp;
		document.location = utils.getGeoJson((splits[1])?parseFloat(splits[1]):0.0,(splits[2])?parseFloat(splits[2]):0.0,true);
		document.speed = (splits[3])?parseFloat(splits[3]):undefined;
		document.direction = (splits[4])?parseFloat(splits[4]):undefined;
		return require("./helper").addDefaults(document);
	}
}


module.exports = GpsParser;