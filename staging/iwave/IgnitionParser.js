var utils = require("../../utils");

'use strict'
class IgnitionParser{
	constructor(){
	}
	
	parse(owner,isIgnitionOn,data){
		var splits = utils.hexToText(data).split(",");
		var document = { owner:owner, type:"alert", subtype:(isIgnitionOn)?"ignitionOn":"ignitionOff" };
		document.timestamp = new Date(splits[0]);
		return require("./helper").addDefaults(document);
	}
}

module.exports = IgnitionParser;