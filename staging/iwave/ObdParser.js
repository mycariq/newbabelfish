var utils = require("../../utils");

'use strict'
class ObdParser{
	constructor(){
	}
	
	parse(owner,data){
		var splits = utils.hexToText(data).split(",");
		var document = { owner:owner, type:"data", subtype:"obd", timestamp:new Date() };
		for(var index=1;index<splits.length;){
			var mode=splits[index];
			var pid=utils.intToHexStr(parseInt(splits[index+1])).toUpperCase();
			var value=splits[index+2];
			parsePid(document,"4"+mode+pid,value);
			index+=3;
		}
		return require("./helper").addDefaults(document);
	}
}

function parsePid(document,pid,value){
	switch(pid){
	case "4104":
		document.engineLoad = parseFloat(value);
		break;
	case "410C":
		document.rpm = parseFloat(value);
		break;
		case "410D":
			document.obdSpeed = parseFloat(value);
			break;
		case "4131":
			document.distanceTraveledSinceCodesCleared = parseFloat(value);
			break;
		case "4146":
			document.ambientAirTemperature = parseFloat(value);
			break;
		default:
				if(document.yetToImplement)
					document.yetToImplement = document.yetToImplement +"|"+pid+","+value;
				else
					document.yetToImplement = pid+","+value;
	}
}

module.exports = ObdParser;