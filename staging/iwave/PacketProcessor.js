var gpsParser = new (require("./GpsParser"))();
var ignitionParser = new (require("./IgnitionParser"))();
var accelerometerParser = new (require("./AccelerometerParser"))();
var dtcParser = new (require("./DtcParser"))();
var obdParser = new (require("./ObdParser"))();
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);


'use strict'
class PacketProcessor{
	
	constructor(){
	}
	
	process(owner, command, data){
		switch(command){
		case "65":
			persist(gpsParser.parse(owner,data));
			break;
		case "66":
			persist(ignitionParser.parse(owner,true,data));
			break;
		case "67":
			persist(ignitionParser.parse(owner,false,data));
			break;
		case "68":
			persist(obdParser.parse(owner,data));
			break;
		case "69":
			persist(accelerometerParser.parse(owner,data));
			break;
		case "6a":
			persist(dtcParser.parse(owner,data));
			break;
		default:
			console.log("========Parser not found=======");
		}
	}
}

function persist(document){
	console.log("parsed:::",JSON.stringify(document));
	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

var packet = undefined;
// GPS packet
// ********************************
// without lat long- this packet will come when gps is not locked
packet="1b0065323031372d30382d30385431353a35373a32332e3230305a00";
// with lat long
//packet="430065323031372d30382d30385432313a34353a31322e3932355a2c31322e3931323537322c37372e3631353131362c302e3030303030302c3332322e363030303030";

// Ignition On Packet
// **********************************
//packet = "1b0066323031372d30382d30385432313a33323a33342e3238355a";

// Ignition Off Packet
// ***********************************
//packet = "1b0067323031372d30382d30385432313a33323a33342e3238355a"
	
// Car status
// ***********************************
//packet = "500068302c312c37302c32302e3030303030302c312c342c31392e3630373834332c312c34392c302e3030303030302c312c31322c31323231382e3735303030302c312c31332c37342e303030303030";

// Accelerometer
// ***********************************
// packet = "12006936353230382c3331322c3439323030";

// DTC
// *********************************
// packet = "80006a5030303032";

//new PacketProcessor().process("55554354",packet.substr(4,2),packet.substr(6));
//require("sleep").sleep(5244546);
module.exports = PacketProcessor;