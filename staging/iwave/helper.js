function addDefaults(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "iWave";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = require("../../utils").getGeoJson(0.0, 0.0, false);
		}
	}
	return document;
}

exports.addDefaults = addDefaults;