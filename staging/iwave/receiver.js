var Server = require("../../server");
var utils = require("../../utils");
var packetProcessor = new (require("./PacketProcessor"))();

new Server("iwave", require("../../config.json").iwave.port).start(function(err,
		data, id, socket) {
	console.log("=====RAW BUFFER===");
	console.log(data);
	var rawPacket = data.toString("hex").toUpperCase();
	var packet = {
		length : utils.hexToInt(utils.reverseHex(rawPacket.substr(0, 4))),
		command : rawPacket.substr(4, 2),
		data : rawPacket.substr(6),
		owner : id
	}

	console.log("received:" + rawPacket);
	console.log("raw json=" + JSON.stringify(packet));

	var response = [];

	if (packet.command == "6C")
		response["id"] = utils.hexToText(packet.data);
	else
		packetProcessor.process(packet.owner, packet.command, packet.data);

	response["message"] = new Buffer(packet.command + "00", "hex");

	console.log("Sending response", response["message"]);
	return response;
});
