var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("jIoT", "receiver.log");
var utils = require("../../utils");
var origin = require("os").hostname();
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);


//#######################################
// MQTT declarations
//#######################################
var options = {
    username: "hardware@mycariq.com",
    password: "NJ8pn*#kg@c=*Am4",
    ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}
// MQTT topic  
var drivenJIoTReqTopic = "/driven/JIoT";

// MQTT connection subscription
var mqttClient = new (require("../../MQTTClient"))(mqttConfig.url, options, packetReceiver);
mqttClient.subscribe(drivenJIoTReqTopic);

//#######################################
// MQTT message callback
//#######################################
function packetReceiver(topic, message) {
    switch (topic) {
        case drivenJIoTReqTopic:
            processResponse(message);
            break;

        default:
            break;
    }
}

function processResponse(message) {
    var jsonString = message.toString("utf-8");
    var document = JSON.parse(jsonString);

    document.type = "data";
    document.subtype = "obdgps";

    // add location
    document["location"] = utils.getGeoJson(document["latitude"], document["longitude"], true);

    // remove latitude and longitude from document
    delete document["latitude"];
    delete document["longitude"];

    // add defaults
    addDefaults(document);

    // persist 
    persist(document);
}

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-VIoT";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function persist(document) {
    logger.debug("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.debug("KafkaResult:" + JSON.stringify(result));
    });
}

