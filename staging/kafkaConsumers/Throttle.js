var logger = console;
var lastDataJson = {};

'use strict'
class Throttle {
    constructor(thresholdJson) {
        this.thresholdJson = thresholdJson;
    }

    process(packet, callback) {
        //get owner from packet
        var owner = packet.owner;

        //get entry from last packet json
        var ownerLastDataJson = lastDataJson[owner];

        //if entry does not exist then add empty json
        if (ownerLastDataJson == undefined) {
            ownerLastDataJson = {};
            lastDataJson[owner] = ownerLastDataJson;
        }

        var supress = false;
        var subtype = packet.subtype;
        var threshold = this.thresholdJson[subtype];
        var packetTimestamp = packet.timestamp;

        //if subtype does not exist into threshold json then
        //mark supress as false

        //if threshold is exist then only go for throtlling
        if (threshold) {

            //if last timestamp does not exist into owner json then set as 1st epoch
            var lastReceivedOn = ownerLastDataJson[subtype];
            if (lastReceivedOn == undefined)
                lastReceivedOn = new Date(1);

            //Find out time difference. if exceeding threshold then set updated into ownerLastDataJson
            var timeDiff = getTimeDiff(packetTimestamp, lastReceivedOn);
            if (timeDiff >= threshold * 1000) {
                ownerLastDataJson[subtype] = packetTimestamp;
            }

            //If not exceeding then mark supress as true for not to persist into Mongo.
            else
                supress = true;
        }

        //Mark supress to false for VIoT device data
        if (packet.agent && (packet.agent == "CIQ-VIoT" || packet.agent == "CIQ-Thinkrace-VT200") || packet.agent == "CIQ-Chip")
            supress = false;

        //call callback
        callback(supress, packet);
    }
}


function getTimeDiff(date1, date2) {
    if (date1 == undefined || date2 == undefined)
        return 0;
    var diff = date1 - date2;

    if (diff < 0)
        return diff * -1;

    return diff;
}


module.exports = Throttle;

// var throttle = new Throttle({ gps: 15, obdgps: 15, obd: 15, heartbeat: 3600 });
// setInterval(() => {
//     var packet = { owner: "123456", subtype: "gps", timestamp: new Date() };
//     throttle.process(packet, function (supress, packet) {
//         logger.info(supress, packet.subtype, packet.timestamp);
//     });
// }, 2000);