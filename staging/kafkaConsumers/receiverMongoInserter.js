var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    Object.keys(cluster.workers).forEach(function (id) {
        console.log("I am running with ID : " + cluster.workers[id].process.pid);
    });

    cluster.on('exit', function (worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died.');
    });
    return;
}

var mongoConfig = require("../../config").mongo;
var logger = require("../../logger").getLogger("kafkaConsumers", "receiverMongoInserter.log");
var hotBatch = new (require("../../mongo/mongo-batch"))("HOT", mongoConfig.hotDBURL, "geobit");
var coldBatch = new (require("../../mongo/mongo-batch"))("COLD", mongoConfig.coldDBURL, "geobit");
var throttle = new (require("./Throttle"))({ gps: 15, obdgps: 15, obd: 15, heartbeat: 60 });
var origin = require("os").hostname();

//Heatsink logging. Do not log other than parsed packet
var heatSinkLogger = require("../../logger").getLogger("heatsink", "heatsink.log");
var isHeatsinkEnabled = require("../../config").heatsink.isEnabled;

var kafkaProducer = new (require("../../producer"))("EdgeAlert", 3);
var kafkaConsumer = new (require("../../consumer"))("GeoBit", "MongoInserter", function (message) {
    var document = JSON.parse(message);
    document.timestamp = new Date(document.timestamp);
    document.serverTimestamp = new Date(document.serverTimestamp);
    document.kafkaTimestamp = new Date();
    document.latency = (document.kafkaTimestamp - document.serverTimestamp) / 1000;
    document.kafkaOrigin = origin;

    var location = document.location;

    //Ignore packets if location is out of range
    var coordinates = location.coordinates;
    if (coordinates.length < 2) {
        // console.log("ignoring packet: ",document);
        return;
    }
    var latitude = coordinates[1];
    var longitude = coordinates[0];
    if (latitude == undefined || longitude == undefined || latitude < -90 || latitude > 90 || longitude < -180 || longitude > 180) {
        // console.log("ignoring packet: " , document);
        return;
    }

    //Heatsink logging. Do not log other than parsed packet
    if (isHeatsinkEnabled)
        heatSinkLogger.info(JSON.stringify(document));

    throttle.process(document, throttleCallback);

});

var throttleCallback = function throttleCallback(supress, document) {

    //logger.info("Throttled ::: supress:" + supress + " Packet:" + JSON.stringify(document));
    if (!supress) {
        hotBatch.add(document);
        coldBatch.add(document);

        if (document.type == "alert") {
            kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
                //logger.info("KafkaResult:" + JSON.stringify(result));
            });
        }
    }
}