var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.type = "data";
        document.subtype = "alert";
        
        data.forEach(function (info) {
            var packetType = info.substr(0, 1);   
            var response = parser.parse(packetType, info);
            if (response != undefined) {
                var keys = Object.keys(response);
                keys.forEach(function (data) {
                    document[data] = response[data];
                });
            }               
        });
    
        return document;
    }
}

module.exports = AlertParser;