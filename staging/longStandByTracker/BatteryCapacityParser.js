var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class BatteryCapacityParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.type = "data";
        document.subtype = "gps";
        document.timestamp = new Date();
        document.voltage = helper.getVoltage(data.substr(2));

        return document;
    }
}

module.exports = BatteryCapacityParser;