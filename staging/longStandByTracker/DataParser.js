var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class DataParser {
    constructor() {

    }

    parse(packetType, data) {   
        var document = {};
        var info = data.substr(1);

        switch (packetType) {
            case "A": getGPSLocation(document, info); break;
            case "B": getAlarm(document, info); break;
            case "E": getTimeStamp(document, info); break;
            case "F": getSpeed(document, info); break;
            case "G": getHeightData(document, info); break; 
            case "I": getWifiInfo(document, info); break;
            case "M": getBatteryCapacity(document, info); break;
            case "N": getGsmSignalStrength(document, info); break;
            case "O": getGpsSignalStrength(document, info); break;
            case "P": getSingleBaseStationLocation(document, info); break;
            case "T": getMessageSerialNo(document, info); break;
            case "W": getAlarmBitExtension(document, info); break;
            case "X": getMultiBaseStationLocation(document, info); break;
            case "X": getAdditionalInfo(document, info); break;
            default: return;
        }

        return document;
    }
}

function getGPSLocation(document, info) {
    var direction = parseInt(info.substr(23, 1));
    var locString = info.substr(6, 17);
    // get latitude direction
    var latDegree = parseFloat(locString.substr(0, 2));
    var latMinutes = locString.substr(2, 6);
    var latMinute = parseFloat(latMinutes.substr(0, 2)) + "." + parseFloat(latMinutes.substr(2));

    // get longitude direction
    var lngDegree = parseFloat(locString.substr(8, 3));
    var longMinutes = locString.substr(11);
    var lngMinute = parseFloat(longMinutes.substr(0, 2)) + "." + parseFloat(longMinutes.substr(2));

    document.timestamp = helper.parseTimestamp(info);
    document.speed = helper.round(parseFloat(info.substr(24, 2)) * 3.70, 2); //knots × 1.852
    //document.gpsdirection = info.substr(26, 2);
    document.location = helper.getLocationFromDegree(lngDegree, lngMinute, latDegree, latMinute); 
    return document;
}

function getAlarm(document, info) {
    return undefined;
}

function getTimeStamp(document, info) {
    var timestampStr = "20" + info.substr(0, 2) + "-" + info.substr(2, 2) + "-" + info.substr(4, 2);
    timestampStr += " " + info.substr(6, 2) + ":" + info.substr(8, 2) + ":" + info.substr(10) + " UTC";

    document.timestamp = new Date(timestampStr);
    return document;
}

function getSpeed(document, info) {
    // kilometers per hour = knots × 1.852
    var knots = parseFloat(info.substr(0, 3) + "." + info.substr(3));

    document.obdspeed = helper.round(knots * 1.852, 2);
    return document;
}

function getHeightData(document, info) {
    return undefined;
}

function getWifiInfo(document, info) {
    return undefined;
}

function getBatteryCapacity(document, info) {
    // Equation of a Straight Line: y = mx + c 
    // battery alert below 15% same like mobile 
    // m=gradient, angle of the line to x-axis
    // c=is intercept on y-axis 
    var m = 0.035;
    var c = 10.50;

    var batteryCapacity = parseFloat(info.substr(0, 2) + '.' + info.substr(2));

    // var capacity = parseFloat(value.substr(0, 2) + '.' + value.substr(2)) - 25;
    // return round(((capacity / 3.75) * 0.1) + 11, 2);
    document.voltage = helper.round(batteryCapacity * m + c, 2);
    return document;
}

function getGsmSignalStrength(document, info) {
    //document.gsmsignal = parseFloat(info);
    return undefined;
}

function getGpsSignalStrength(document, info) {
    //document.gpssatellites = parseFloat(info.substr(0, 2));
    return undefined;
}

function getSingleBaseStationLocation(document, info) {
    return undefined;
}

function getMessageSerialNo(document, info) {
    return undefined;
}

function getAlarmBitExtension(document, info) {
    return undefined;
}

function getMultiBaseStationLocation(document, info) {
    return undefined;
}

function getAdditionalInfo(document, info) {
    return undefined;
}

module.exports = DataParser;