var utils = require("../../utils");
var helper = require("./helper");
var parser = new (require("./DataParser"))();

'use strict'
class GpsParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.type = "data";
        document.subtype = "gps";
        
        data.forEach(function (info) {
            var packetType = info.substr(0, 1);   
            var response = parser.parse(packetType, info);
            if (response != undefined) {
                var keys = Object.keys(response);
                keys.forEach(function (data) {
                    document[data] = response[data];
                });
            }               
        });
            
        // document.timestamp = helper.parseTimestamp(data);
        // document.location = helper.getLocation(data, 8, 17);
        // document.speed = parseFloat(data.substr(26, 2));

        return document;
    }
}

module.exports = GpsParser;