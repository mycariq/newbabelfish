var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsPositionParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.type = "data";
        document.subtype = "gps";
        document.timestamp = helper.parseTimestamp(data);
        document.location = helper.getLocation(data, 8, 17);
        document.speed = parseFloat(data.substr(26, 2));

        return document;
    }
}

module.exports = GpsPositionParser;