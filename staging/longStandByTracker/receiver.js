var Server = require("../../server");
var logger = require("../../logger.js").getLogger("longStandByTracker", "receiver.log");
var parser = new (require("./PacketParser"))();

new Server("longStandByTracker", require("../../config.json").longStandByTracker.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packetData = data.toString("utf-8");
	logger.info("Received raw packet : " + packetData);

	var packets = packetData.split("#*");
	packets.forEach(function (packet) {
		if (packet) { // remove first (*), last(#) char if present
			packet = (packet.charAt(0) == '*') ? packet.substr(1) : packet;
			packet = (packet.charAt(packet.length - 1) == '#') ? packet.substr(0, packet.length - 1) : packet;
		}
		processPacket(socket, packet);
	});
});

function processPacket(socket, packet) {
	//logger.info("Parsing packet: " + packet);
	var packetArray = packet.split("&");

	// get owner and command
	var info = packetArray[0].split(",");
	var owner = info[0].substr(5, 15);
	var command = info[1];

	// remove first element from array 
	packetArray.shift();
	parser.parse(owner, command, packetArray);
}

//processPacket({}, "VK201867282032059749,AB&A1212481834200007345765970000170919&X404,22,60719,13012,82;201,8147,92;201,14978,93;60719,1993,93;60719,4662,94;60719,13011,97;60719,54401,98&B0100000000&W10&N17&Z10&T0020&b2");