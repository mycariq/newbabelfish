var helper = require("./helper");
var logger = require("../../logger.js").getLogger("longStandByTrackerG07", "receiver.log");
var utils = require("../../utils");

'use strict'
class DataParser {
    constructor() {
    }

    parse(data) {
        var packet = data.toString("hex").toUpperCase();

        return parseData(packet);
    }
}

function parseData(packet) {

    var document = { type: "data", subtype: "gps" };

    document.owner = packet.substr(2, 10);
    document.timestamp = helper.parseTimestamp(packet.substr(12, 6), packet.substr(18, 6));
    document.voltage = helper.getVoltage(utils.hexToInt(packet.substr(32, 2)));
    document.location = getLocation(packet.substr(24, 8), packet.substr(34, 10));
    // speed converting knots to km/hr
    document.speed = utils.hexToInt(packet.substr(44, 3)) * 1.852;
    document.direction = packet.substr(47, 3);
    document.vehicleStatus = packet.substr(50, 8);
    document.mileage = utils.hexToInt(packet.substr(58, 8));
    document.reserved = packet.substr(66, 8);
    document.mcc = packet.substr(74, 4);
    document.mnc = packet.substr(78, 2);
    document.lac = packet.substr(80, 4);
    document.cellId = packet.substr(84, 4);
    document.recordNumber = utils.hexToInt(packet.substr(88, 2));

    return document;
}

function getLocation(latitude, longitude) {
    var direction = utils.hexToInt(longitude.substr(9));

    // get latitude direction
    var latDegree = latitude.substr(0, 2);
    var latMinutes = latitude.substr(2);
    var latMinute = parseFloat(latMinutes.substr(0, 2)) + "." + parseFloat(latMinutes.substr(2));

    // get longitude direction
    var lngDegree = longitude.substr(0, 3);
    var longMinutes = longitude.substr(3);
    var lngMinute = parseFloat(longMinutes.substr(0, 2)) + "." + parseFloat(longMinutes.substr(2, 4));

    return helper.getLocationFromDegree(lngDegree, lngMinute, latDegree, latMinute);
}

module.exports = DataParser;