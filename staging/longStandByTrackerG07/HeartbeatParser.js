var helper = require("./helper");
var logger = require("../../logger.js").getLogger("longStandByTrackerG07", "receiver.log");

'use strict'
class HeartbeatParser {
    constructor() {
    }

    parse(data) {
        var packetData = data.toString("utf-8");
        logger.info("Received heartbeat packet: " + packetData);

        var document = [];
        var packets = packetData.split("#*");
        packets.forEach(function (packet) {
            if (packet) { // remove first (*), last(#) char if present
                packet = (packet.charAt(0) == '*') ? packet.substr(1) : packet;
                packet = (packet.charAt(packet.length - 1) == '#') ? packet.substr(0, packet.length - 1) : packet;
            }

            var packetArray = packet.split(",");
            document.push(processData(packetArray));
        });

        return document;
    }
}

function processData(content) {

    var document = { type: "data", subtype: "gps" };

    document.owner = content[1];
    document.timestamp = helper.parseTimestamp(content[3], content[11]);
    document.isValid = (content[4] == "A");
    document.location = helper.getLocation(content[5], content[7]);
    document.speed = parseFloat(content[9]);
    document.direction = content[10];
    document.vehicleStatus = content[12];
    document.mcc = content[13];
    document.mnc = content[14];
    document.lac = content[15];
    document.cellId = content[16];
    if (content[2] == "V5")
        document.voltage = helper.getVoltage(parseFloat(content[18]));

    return document;
}

module.exports = HeartbeatParser;