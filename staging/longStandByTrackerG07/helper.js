var utils = require("../../utils");
var logger = require("../../logger.js").getLogger("longStandByTrackerG07", "receiver.log");
var origin = require("os").hostname();;

//adds default values to mongo document
function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-longStandByTrackerG07";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function getLocation(latitude, longitude) {
    // get latitude direction
    var latDegree = parseFloat(latitude.substr(0, 2));
    var latMinute = latitude.substr(2);

    // get longitude direction
    var lngDegree = parseFloat(longitude.substr(0, 3));
    var lngMinute = longitude.substr(3);

    return getLocationFromDegree(lngDegree, lngMinute, latDegree, latMinute);
}

function getLocationFromDegree(lngDegree, lngMinute, latDegree, latMinute) {
    var output = {};

    // "073° 49.1011' 18° 29.5301'"
    var coords = lngDegree + "° " + lngMinute + "' " + latDegree + "° " + latMinute + "'";

    //Get coords string and strip unneccessary chars
    query = coords.toLowerCase().replace(/[^\-a-z0-9\.\ \,\;]+/g, "");
    //Get coordinate parts
    var regularExp = /([\-a-z]?)(\d+\.?\d*)([a-z]?)/g;
    var matches;
    var parts = [];

    while ((matches = regularExp.exec(query)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (matches.index === regularExp.lastIndex) {
            regularExp.lastIndex++;
        }
        var part = [];
        // The result can be accessed through the `m`-variable.
        matches.forEach((match, groupIndex) => {
            part.push(match);
        });
        parts.push(part);
    }

    //Get coordinate point by counting parts
    var point = {}
    if (parts.length == 4) {
        //Lat long in DM
        point = convertDEGToDMS(parts);
    } else {
        point.error = true
        point.error_msg = "Cannot determine coordinate type";
    }

    //Check for match
    if (point.error) {
        //Return error
        output.success = false;
        output.error_msg = point.error_msg;
    } else {
        //Setup base result
        output.success = true;
        output.result = {
            lng: point.lng,
            lat: point.lat
        };
        lat = point.lat;
        lng = point.lng;
        output.display = `${lng} ${lat}`;
    }

    if (output.success == false) {
        logger.error(output.error_msg);
        return utils.getGeoJson(0.0, 0.0, false);
    }

    return utils.getGeoJson(output.result.lat, output.result.lng, true);
}

function convertDEGToDMS(coords) {

    //Parse coords
    var dlng = parseFloat(coords[0][2]);
    var mlng = parseFloat(coords[1][2]);
    var dlat = parseFloat(coords[2][2]);
    var mlat = parseFloat(coords[3][2]);

    //Parse hemispheres
    if (
        coords[0][1] == "w" ||
        coords[0][3] == "w" ||
        coords[1][3] == "w" ||
        coords[0][1] == "-"
    )
        dlng *= -1;
    if (
        coords[2][1] == "s" ||
        coords[2][3] == "s" ||
        coords[3][3] == "s" ||
        coords[2][1] == "-"
    )
        dlat *= -1;

    //Validate results
    if (dlat >= 90 || dlat <= -90) {
        return {
            error: true,
            error_msg: "Latitude degrees out of bounds [Expected:-90,90 Value: " +
                String(dlat) +
                "]"
        };
    }
    if (dlng >= 180 || dlng <= -180) {
        return {
            error: true,
            error_msg: "Longitude degrees out of bounds [Expected:-180,180 Value: " +
                String(dlng) +
                "]"
        };
    }
    if (mlat < 0 || mlat >= 60) {
        return {
            error: true,
            error_msg: "Latitude minutes out of bounds [Expected:0,60 Value: " +
                String(mlat) +
                "]"
        };
    }
    if (mlng < 0 || mlng >= 60) {
        return {
            error: true,
            error_msg: "Longitude minutes out of bounds [Expected:0,60 Value: " +
                String(mlng) +
                "]"
        };
    }

    //Calculate coords
    var lng = dlng + (Math.sign(dlng) * mlng) / 60;
    var lat = dlat + (Math.sign(dlat) * mlat) / 60;

    //Return coords
    return {
        lat: lat,
        lng: lng,
        error: false
    };
}

function parseTimestamp(time, date) {
    var timestampStr = "20" + date.substr(4) + "-" + date.substr(2, 2) + "-" + date.substr(0, 2);
    timestampStr += " " + time.substr(0, 2) + ":" + time.substr(2, 2) + ":" + time.substr(4, 2) + " UTC";

    return new Date(timestampStr);
}

function getVoltage(capacity) {
    // Equation of a Straight Line: y = mx + c 
    // battery alert below 15% same like mobile 
    // m=gradient, angle of the line to x-axis
    // c=is intercept on y-axis 
    var m = 0.035;
    var c = 10.50;

    return round(capacity * m + c, 2);
}

function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

exports.addDefaults = addDefaults;
exports.getLocation = getLocation;
exports.getLocationFromDegree = getLocationFromDegree;
exports.convertDEGToDMS = convertDEGToDMS;
exports.parseTimestamp = parseTimestamp;
exports.getVoltage = getVoltage;
exports.round = round;