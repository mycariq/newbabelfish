var Server = require("../../server");
var logger = require("../../logger.js").getLogger("longStandByTrackerG07", "receiver.log");
var parser = new (require("./PacketParser"))();

new Server("longStandByTrackerG07", require("../../config.json").longStandByTrackerG07.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info("==========================================");
	logger.info(data);
	var dataPacket = data.toString("hex").toUpperCase();
	var command = dataPacket.slice(0, 2);
	
	processPacket(socket, command, data);
});

function processPacket(socket, command, packet) {
	parser.parse(command, packet);
}

//var heartbeart = "HQ,7028172317,V1,110150,A,1834.1462,N,07345.7591,E,000.00,000,130220,FFFFBBFF,404,90,3111,27708";
//var heartBeat = "2A48512C373032383137323331372C56312C3233353335342C562C313833372E383134342C4E2C30373334352E313637382C452C3030302E30302C3030302C3034303332302C46464646424246462C3430342C32322C36303637382C3636363223";
//processPacket({}, "2A", heartBeat);