var kueURL = require('kue'), jobsURL = kueURL.createQueue();
var xor = require('bitwise-xor');
var utils = require("../utils");
var config = require("../config")["napino"];
var dasDetails = require("../config")["DAS"];
var mongoConfig = require("../config")["mongo"];
var port = config["port"];
var throughputConfig = require("../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];
var dasDNS = "http://" + dasDetails["DNS"] + ":" + dasDetails["port"];
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;
var mongoQThroughputRecord = new ThroughputRecord("mongoQThroughputRecord",
	new Date, 0, threshold);
var parserQThroughputRecord = new ThroughputRecord("parserQThroughputRecord",
	new Date, 0, threshold);
var jAlarmFile = require("./alarm");
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

var HashMap = require('hashmap');
var mapForEOLAlerts = new HashMap();

var logger = require("../logger.js");
var mylogger = logger.getLogger("napino", "parser.log");


const
	DAS_BASEURL_PARSED = dasDNS
		+ "/DataAcquisitionModule/dataacquisitions/push/parsedData/{id}/{timeStamp}/{packet}";

const
	DAS_TOPIC = "das-urls";
var mongoTopic = mongoConfig["topic"];

const
	mongo_document_agent = "Napino";
const
	derived_towing_alert_speed = 15;

mylogger.debug("DAS_BASEURL_PARSED : " + DAS_BASEURL_PARSED);

var NAPINO_REALTIME_VEHICAL_DATA = "55";
var NAPINO_ALERT_DATA = "66";
var NAPINO_SAVED_DATA = "77";
var NAPINO_DIAGNOSTIC_DATA = "88";

var DATATYPE_START = 0;
var DATATYPE_LENGTH = 2;

var IMEI_START = 2;
var IMEI_LENGTH = 30;

var ALERTTYPE_START = 32;
var ALERTTYPE_LENGTH = 2;
var ALERTDATA_START = 34;
var ALERTDATA_LENGTH = 2;

var BATTERY_VOLTAGE_START = 34;
var BATTERY_VOLTAGE_LENGTH = 8;

var _35_BYTE_ALERT_LATITUDE_START = 34;
var _35_BYTE_ALERT_LONGITUDE_START = 44;

var _36_BYTE_ALERT_LATITUDE_START = 36;
var _36_BYTE_ALERT_LONGITUDE_START = 46;

var SPEED_START = IMEI_START + IMEI_LENGTH;
var SPEED_LENGTH = 2;

var RPM_START = SPEED_START + SPEED_LENGTH;
var RPM_LENGTH = 4;

var FUEL_LEVEL_COUNT_START = RPM_START + RPM_LENGTH;
var FUEL_LEVEL_COUNT_LENGTH = 4;

var XAXIS_START = FUEL_LEVEL_COUNT_START + FUEL_LEVEL_COUNT_LENGTH;
var XAXIS_LENGTH = 4;

var YAXIS_START = XAXIS_START + XAXIS_LENGTH;
var YAXIS_LENGTH = 4;

var ZAXIS_START = YAXIS_START + YAXIS_LENGTH;
var ZAXIS_LENGTH = 4;

var NO_TIMES_STARTER_PRESS_START = ZAXIS_START + ZAXIS_LENGTH;
var NO_TIMES_STARTER_PRESS_LENGTH = 8;

var NO_TIMES_BRAKE_PRESS_START = NO_TIMES_STARTER_PRESS_START
	+ NO_TIMES_STARTER_PRESS_LENGTH;
var NO_TIMES_BRAKE_PRESS_LENGTH = 8;

var BITWISE_FORMAT_START = NO_TIMES_BRAKE_PRESS_START
	+ NO_TIMES_BRAKE_PRESS_LENGTH;
var BITWISE_FORMAT_LENGTH = 4;

var NO_TIMES_SIDESTAND_START = BITWISE_FORMAT_START + BITWISE_FORMAT_LENGTH;
var NO_TIMES_SIDESTAND_LENGTH = 8;

var NO_TIMES_CLUTCH_PRESS_START = NO_TIMES_SIDESTAND_START
	+ NO_TIMES_SIDESTAND_LENGTH;
var NO_TIMES_CLUTCH_PRESS_LENGTH = 8;

var NO_TIMES_TURNSWITCH_PRESS_START = NO_TIMES_CLUTCH_PRESS_START
	+ NO_TIMES_CLUTCH_PRESS_LENGTH;
var NO_TIMES_TURNSWITCH_PRESS_LENGTH = 8;

var NO_TIMES_HEADLAMP_START = NO_TIMES_TURNSWITCH_PRESS_START
	+ NO_TIMES_TURNSWITCH_PRESS_LENGTH;
var NO_TIMES_HEADLAMP_LENGTH = 8;

var NO_TIMES_NEUTRAL_START = NO_TIMES_HEADLAMP_START + NO_TIMES_HEADLAMP_LENGTH;
var NO_TIMES_NEUTRAL_LENGTH = 8;

var NO_TIMES_TPS_START = NO_TIMES_NEUTRAL_START + NO_TIMES_NEUTRAL_LENGTH;
var NO_TIMES_TPS_LENGTH = 8;

var NO_TIMES_I3S_START = NO_TIMES_TPS_START + NO_TIMES_TPS_LENGTH;
var NO_TIMES_I3S_LENGTH = 8;

var IGNITION_ANGLE_START = NO_TIMES_I3S_START + NO_TIMES_I3S_LENGTH;
var IGNITION_ANGLE_LENGTH = 8;

var LATITUDE_START = IGNITION_ANGLE_START + IGNITION_ANGLE_LENGTH;
var LATITUDE_LENGTH = 8;

var DIR_LATITUDE_START = LATITUDE_START + LATITUDE_LENGTH;
var DIR_LATITUDE_LENGTH = 2;

var LONGITUDE_START = DIR_LATITUDE_START + DIR_LATITUDE_LENGTH;
var LONGITUDE_LENGTH = 8;

var DIR_LONGITUDE_START = LONGITUDE_START + LONGITUDE_LENGTH;
var DIR_LONGITUDE_LENGTH = 2;

var TS_LENGTH = 12;
var CS_LENGTH = 4;

var bit0 = 1;
var bit1 = 2;
var bit2 = 4;
var bit3 = 8;
var bit4 = 16;
var bit5 = 32;
var bit6 = 64;
var bit7 = 128;

function isBitOn(number, position) {
	switch (position) {
		case 0:
			if ((number & bit0) == bit0)
				return 1;
			break;
		case 1:
			if ((number & bit1) == bit1)
				return 1;
			break;
		case 2:
			if ((number & bit2) == bit2)
				return 1;
			break;
		case 3:
			if ((number & bit3) == bit3)
				return 1;
			break;
		case 4:
			if ((number & bit4) == bit4)
				return 1;
			break;
		case 5:
			if ((number & bit5) == bit5)
				return 1;
			break;
		case 6:
			if ((number & bit6) == bit6)
				return 1;
			break;
		case 7:
			if ((number & bit7) == bit7)
				return 1;
			break;
	}
	return 0;
}

function parseRawPacket(packet) {

	var packetType = packet.substr(DATATYPE_START, DATATYPE_LENGTH);

	switch (packetType) {
		case NAPINO_REALTIME_VEHICAL_DATA:
			return parseRealTimeVehicalData(packet);

		case NAPINO_ALERT_DATA:
			return parseAlert(packet);

		case NAPINO_DIAGNOSTIC_DATA:
			return parseDiagnostic(packet);

		case NAPINO_SAVED_DATA:
			return parseSavedData(packet);

		default:
			mylogger.error("(Consumer)-Unknown Packet,ANOMALY,UNPARSED_PACKET," + packet);
	}
	return null;
}

function parseSignedTwoByte(napinoData) {
	var buf = new Buffer(napinoData, "hex");
	return (buf.readInt16LE(0));
}

function parseFourByteFloat(napinoData) {
	var buf = new Buffer(napinoData, "hex");
	return (buf.readFloatLE(0));
}

function parseFourByte(napinoData) {
	// console.log(myScriptName + new Date + "This is 4 byte :" + napinoData);
	var buf = new Buffer(napinoData, "hex");
	return (buf.readInt32LE(0));
}

function parseTwoByte(napinoData) {
	var buf = new Buffer(napinoData, "hex");
	return (buf.readInt16LE(0));
}

// prependZero
function prependZero(a) {
	if (a < 10)
		return ("0" + a);
	return a;
}

// Napino : 0C1E141B0610
// CarIQ : 2016-06-04%2012:49:21
function getTimestamp(timestamp) {
	var hh = prependZero(parseInt(timestamp.substr(0, 2), 16));
	var mm = prependZero(parseInt(timestamp.substr(2, 2), 16));
	var ss = prependZero(parseInt(timestamp.substr(4, 2), 16));
	var dd = prependZero(parseInt(timestamp.substr(6, 2), 16));
	var nn = prependZero(parseInt(timestamp.substr(8, 2), 16));
	var yy = prependZero(parseInt(timestamp.substr(10, 2), 16));

	return utils.convertISTStrToUTCStr("20" + yy + "-" + nn + "-" + dd + " "
		+ hh + ":" + mm + ":" + ss);
}

/*
 * Parse Diagnostic Data
 */
function parseDiagnostic(packet) {

	var jPacketJSON = {};
	var jResultJSONForDas = {};

	var allData = "";

	jPacketJSON.devID = (new Buffer(packet.substr(IMEI_START, IMEI_LENGTH),
		"hex").toString("ascii"));
	jPacketJSON.chkSum = packet.substr((packet.length - 4), CS_LENGTH);
	// jPacketJSON.bitwiseFormat = packet.substr(32, 6);

	// console.log("\n Bitwise = " + jPacketJSON.bitwiseFormat);

	var bitWise16 = parseInt(new Buffer(packet.substr(32, 2), "ascii"), 16);
	var bitWise17 = parseInt(new Buffer(packet.substr(34, 2), "ascii"), 16);
	var bitWise18 = parseInt(new Buffer(packet.substr(36, 2), "ascii"), 16);

	// console.log("\n Bitwise 16= " + bitWise16);
	// console.log("\n Bitwise 17= " + bitWise17);
	// console.log("\n Bitwise 18= " + bitWise18);

	jPacketJSON.leftSideIndicatorLampStatus = isBitOn(bitWise16, 0);
	jPacketJSON.rightSideIndicatorLampStatus = isBitOn(bitWise16, 1);
	jPacketJSON.batteryChargingSystemStatus = isBitOn(bitWise16, 2);
	jPacketJSON.ignitionSystemStatus = isBitOn(bitWise16, 3);
	jPacketJSON.sideStandSwitchStatus = isBitOn(bitWise16, 4);
	jPacketJSON.starterSystemStatus = isBitOn(bitWise16, 5);
	jPacketJSON.brakeSwitchStatus = isBitOn(bitWise16, 6);
	jPacketJSON.engineTempStatus = isBitOn(bitWise16, 7);

	jPacketJSON.brakeLightStatus = isBitOn(bitWise17, 0);
	jPacketJSON.loBeamHeadlampStatus = isBitOn(bitWise17, 1);
	jPacketJSON.hiBeamHeadlampStatus = isBitOn(bitWise17, 2);

	var bikeDTC = "";
	/* Start : Construct Napino ERROR Codes */
	if (jPacketJSON.leftSideIndicatorLampStatus == 0)
		bikeDTC = bikeDTC + "N0001|";

	if (jPacketJSON.rightSideIndicatorLampStatus == 0)
		bikeDTC = bikeDTC + "N0002|";

	if (jPacketJSON.batteryChargingSystemStatus == 0)
		bikeDTC = bikeDTC + "N0003|";

	if (jPacketJSON.ignitionSystemStatus == 0)
		bikeDTC = bikeDTC + "N0004|";

	if (jPacketJSON.sideStandSwitchStatus == 0)
		bikeDTC = bikeDTC + "N0005|";

	if (jPacketJSON.starterSystemStatus == 0)
		bikeDTC = bikeDTC + "N0006|";

	if (jPacketJSON.brakeSwitchStatus == 0)
		bikeDTC = bikeDTC + "N0007|";

	if (jPacketJSON.engineTempStatus == 0)
		bikeDTC = bikeDTC + "N0008|";

	if (jPacketJSON.brakeLightStatus == 0)
		bikeDTC = bikeDTC + "N0009|";

	if (jPacketJSON.loBeamHeadlampStatus == 0)
		bikeDTC = bikeDTC + "N000A|";

	if (jPacketJSON.hiBeamHeadlampStatus == 0)
		bikeDTC = bikeDTC + "N000B|";

	if (bikeDTC == "") {
		mylogger.debug("(parseDiagnostic)-Clearing ALL ERROR(s)");
		bikeDTC = "P0000";
	}
	/* End : Construct Napino ERROR Codes */

	// console.log(myScriptName + new Date + ": " + " Packet JSON = " +
	// JSON.stringify(jPacketJSON));
	allData = allData + "leftSideIndicatorLampStatus="
		+ jPacketJSON.leftSideIndicatorLampStatus + ",";
	allData = allData + "rightSideIndicatorLampStatus="
		+ jPacketJSON.rightSideIndicatorLampStatus + ",";
	allData = allData + "batteryChargingSystemStatus="
		+ jPacketJSON.batteryChargingSystemStatus + ",";
	allData = allData + "ignitionSystemStatus="
		+ jPacketJSON.ignitionSystemStatus + ",";
	allData = allData + "sideStandSwitchStatus="
		+ jPacketJSON.sideStandSwitchStatus + ",";
	allData = allData + "starterSystemStatus="
		+ jPacketJSON.starterSystemStatus + ",";
	allData = allData + "brakeSwitchStatus=" + jPacketJSON.brakeSwitchStatus
		+ ",";
	allData = allData + "engineTempStatus=" + jPacketJSON.engineTempStatus
		+ ",";
	allData = allData + "brakeLightStatus=" + jPacketJSON.brakeLightStatus
		+ ",";
	allData = allData + "loBeamHeadlampStatus="
		+ jPacketJSON.loBeamHeadlampStatus + ",";
	allData = allData + "hiBeamHeadlampStatus="
		+ jPacketJSON.hiBeamHeadlampStatus + ",";
	if (bikeDTC != "") {
		allData = allData + "ErrorCode_DTC=" + bikeDTC + ",";
	}

	jResultJSONForDas.id = jPacketJSON.devID;
	jResultJSONForDas.timeStamp = utils.getServerTimeStamp();
	jResultJSONForDas.all_data = allData;

	return jResultJSONForDas;
}

/*
 * Parse Saved Data
 */
function parseSavedData(packet) {

	var totalLength = packet.length;
	var startFrom = null;
	var nextPacket = null;
	var nextPacketLength = null;

	startFrom = 2;

	while (startFrom < totalLength) {
		var nextPacketType = packet.substr(startFrom, 2);
		mylogger.debug("(parseSavedData)-***** nextPacketType" + nextPacketType);
		switch (nextPacketType) {
			case "55":
				mylogger.debug("(parseSavedData)-This is 0x55");
				nextPacketLength = 87;
				break;

			case "66":
				mylogger.debug("(parseSavedData)-This is 0x66");
				var alertType = packet.substr(startFrom + 32, 2);

				if (alertType == "00" || alertType == "17" || alertType == "19"
					|| alertType == "1A") {
					nextPacketLength = 35;
				} else if (alertType == "01" || alertType == "02"
					|| alertType == "05" || alertType == "06"
					|| alertType == "07" || alertType == "0C"
					|| alertType == "0D") {
					nextPacketLength = 36;
				} else if (alertType == "03") {
					nextPacketLength = 29;
				} else if (alertType == "04" || alertType == "08"
					|| alertType == "09" || alertType == "0A"
					|| alertType == "0B" || alertType == "0E"
					|| alertType == "18") {
					nextPacketLength = 25;
				} else {
					mylogger.error("(parseSavedData),ANOMALY,Something is wrong in alertType - Returning from here!!!!");
					return;
				}
				break;

			case "88":
				mylogger.debug("(parseSavedData)-This is 0x88");
				nextPacketLength = 21;
				break;

			default:
				mylogger.error("(parseSavedData),ANOMALY,Something is wrong in packetType - Returning from here!!!!");
				return;
		}

		nextPacket = packet.substr(startFrom, (nextPacketLength * 2));
		mylogger.debug("(parseSavedData)-NextPacket = " + nextPacket);

		processPacket(nextPacket);
		startFrom = startFrom + nextPacket.length;
	}

	mylogger.debug("(parseSavedData)-Out of parseSavedData");
	return null;
}

/*
 * Parse Accidental Alert Data
 */
function parseAlert(packet) {

	var jPacketJSON = {};
	var jResultJSONForDas = {};

	var allData = "";

	jPacketJSON.devID = (new Buffer(packet.substr(IMEI_START, IMEI_LENGTH),
		"hex").toString("ascii"));

	var alertType = packet.substr(ALERTTYPE_START, ALERTTYPE_LENGTH);

	if (jAlarmFile[alertType])
		jPacketJSON.alertDescription = jAlarmFile[alertType];
	else
		jPacketJSON.alertDescription = "NotDefinedYet";

	mylogger.debug("(parseAlert)-********>>>>" + jPacketJSON.alertDescription);

	jPacketJSON.deviceTimestamp = getTimestamp(packet.substr(
		(packet.length - 16), TS_LENGTH));
	jPacketJSON.chkSum = packet.substr((packet.length - 4), CS_LENGTH);
	jPacketJSON.alertData = null;
	jPacketJSON.latitude = 0;
	jPacketJSON.longitude = 0;

	switch (alertType) {
		case "00": // Accidental Alert Packet
		case "17": // Panic Alert
		case "19": // Ignition ON Alert
		case "1A": // Towing Alert
			jPacketJSON.latitude = parseFourByteFloat(packet.substr(
				_35_BYTE_ALERT_LATITUDE_START, LATITUDE_LENGTH));
			jPacketJSON.dirLatitude = packet.substr(_35_BYTE_ALERT_LATITUDE_START
				+ LATITUDE_LENGTH, DIR_LATITUDE_LENGTH);
			if (jPacketJSON.dirLatitude != "4E")
				jPacketJSON.latitude = jPacketJSON.latitude * (-1);

			jPacketJSON.longitude = parseFourByteFloat(packet.substr(
				_35_BYTE_ALERT_LONGITUDE_START, LONGITUDE_LENGTH));
			jPacketJSON.dirLongitude = packet.substr(_35_BYTE_ALERT_LONGITUDE_START
				+ LONGITUDE_LENGTH, DIR_LONGITUDE_LENGTH);
			if (jPacketJSON.dirLongitude != "45")
				jPacketJSON.longitude = jPacketJSON.longitude * (-1);
			break;

		case "01": // Over Speeding Alert
		case "02": // Harsh Braking Alert
		case "05": // Low Fuel Level Alert
		case "06": // Fuel Fill Alert
		case "07": // Fuel Theft Alert
		case "0C": // Turn left at high speed Alert
		case "0D": // Turn right at high speed Alert
			jPacketJSON.alertData = parseInt(packet.substr(ALERTDATA_START,
				ALERTDATA_LENGTH), 16);

			jPacketJSON.latitude = parseFourByteFloat(packet.substr(
				_36_BYTE_ALERT_LATITUDE_START, LATITUDE_LENGTH));
			jPacketJSON.dirLatitude = packet.substr(_36_BYTE_ALERT_LATITUDE_START
				+ LATITUDE_LENGTH, DIR_LATITUDE_LENGTH);
			if (jPacketJSON.dirLatitude != "4E")
				jPacketJSON.latitude = jPacketJSON.latitude * (-1);

			jPacketJSON.longitude = parseFourByteFloat(packet.substr(
				_36_BYTE_ALERT_LONGITUDE_START, LONGITUDE_LENGTH));
			jPacketJSON.dirLongitude = packet.substr(_36_BYTE_ALERT_LONGITUDE_START
				+ LONGITUDE_LENGTH, DIR_LONGITUDE_LENGTH);
			if (jPacketJSON.dirLongitude != "45")
				jPacketJSON.longitude = jPacketJSON.longitude * (-1);
			break;

		case "03": // Low Battery Alert
			jPacketJSON.alertData = parseFourByteFloat(packet.substr(
				BATTERY_VOLTAGE_START, BATTERY_VOLTAGE_LENGTH));
			break;

		case "04": // Battery Disconnection Alert
		case "08": // Clutch Pressed in Running Condition Alert
		case "09": // Self-Switch Pressed in Engine Running Condition Alert
		case "0A": // Continuous WOT with brake pressed Condition Alert
		case "0B": // Idling for long time Alert
		case "0E": // Rear Seat Passenger Fall Down Alert
		case "18": // Side Stand Down in running condition Alert
			break;

		default:
			mylogger.error("(parseAlert)-Unknown ALERT Packet,ANOMALY,UNKNOWN_CMD," + alertType);
			return null;
	}

	// console.log(myScriptName + new Date + ": " + " Packet JSON = " +
	// JSON.stringify(jPacketJSON));
	allData = allData + "alert." + jPacketJSON.alertDescription + "="
		+ ((jPacketJSON.alertData) ? jPacketJSON.alertData : 0) + ",";
	if (jPacketJSON.latitude != 0)
		allData = allData + "alert.latitude=" + jPacketJSON.latitude + ",";
	if (jPacketJSON.longitude != 0)
		allData = allData + "alert.longitude=" + jPacketJSON.longitude + ",";

	jResultJSONForDas.id = jPacketJSON.devID;
	jResultJSONForDas.timeStamp = jPacketJSON.deviceTimestamp;
	jResultJSONForDas.all_data = allData;

	return jResultJSONForDas;
}

/*
 * Send EOL indications as ALERTs
 */
function doProcessEOLPacket(dID, dTS, dLatitude, dLongitude, eolAlertName,
	eolCount) {
	var url_EOL;
	var eol_alert_data;

	/*
	 * Check if we really need to send EOL alert for this switch Logic is we
	 * will be sending EOL alert for one time for specified threashold i.e. for
	 * demo every 20th occurance. But only ones for each 20th occurance. Note
	 * that this is for demo only, we may remove this code in real production
	 */
	var devIdAndEOLName = dID + "_" + eolAlertName;
	var oldEOLcount = mapForEOLAlerts.get(devIdAndEOLName);
	if (oldEOLcount) {
		mylogger.debug("(doProcessEOLPacket)-EOLAlert:" + devIdAndEOLName + " OldAlertCout=" + oldEOLcount + " CurAlertCount=" + eolCount);
		if (oldEOLcount == eolCount) {
			mylogger.debug("(doProcessEOLPacket)-!!! NO NEED TO SEND this eolAlert !!! ");
			return;
		}
	}
	mapForEOLAlerts.set(devIdAndEOLName, eolCount);
	/* End of EOL Alert Logic */

	url_EOL = DAS_BASEURL_PARSED.replace("{id}", dID).replace("{timeStamp}",
		dTS.replace(" ", "%20"));
	eol_alert_data = "alert." + eolAlertName + "=" + eolCount + ",";
	if (dLatitude != 0) {
		eol_alert_data = eol_alert_data + "alert.latitude=" + dLatitude + ","
			+ "alert.longitude=" + dLongitude + ",";
	}

	url_EOL = url_EOL.replace("{packet}", eol_alert_data);
	mylogger.debug("(doProcessEOLPacket)-Created EOL URL = " + url_EOL);
	processURL(url_EOL);
}

/*
 * Parse RealTime Vehical Data
 */
function parseRealTimeVehicalData(packet) {

	var jPacketJSON = {};
	var jResultJSONForDas = {};

	var allData = "";

	jPacketJSON.devID = (new Buffer(packet.substr(IMEI_START, IMEI_LENGTH),
		"hex").toString("ascii"));
	jPacketJSON.speed = parseInt(packet.substr(SPEED_START, SPEED_LENGTH), 16);
	jPacketJSON.rpm = parseTwoByte(packet.substr(RPM_START, RPM_LENGTH));
	jPacketJSON.fuelLevelCount = parseTwoByte(packet.substr(
		FUEL_LEVEL_COUNT_START, FUEL_LEVEL_COUNT_LENGTH));
	jPacketJSON.xAxis = parseSignedTwoByte(packet.substr(XAXIS_START,
		XAXIS_LENGTH));
	jPacketJSON.yAxis = parseSignedTwoByte(packet.substr(YAXIS_START,
		YAXIS_LENGTH));
	jPacketJSON.zAxis = parseSignedTwoByte(packet.substr(ZAXIS_START,
		ZAXIS_LENGTH));
	jPacketJSON.noTimesStarterPress = parseFourByte(packet.substr(
		NO_TIMES_STARTER_PRESS_START, NO_TIMES_STARTER_PRESS_LENGTH));
	jPacketJSON.noTimesBrakePress = parseFourByte(packet.substr(
		NO_TIMES_BRAKE_PRESS_START, NO_TIMES_BRAKE_PRESS_LENGTH));

	// jPacketJSON.bitwiseFormat = packet.substr(BITWISE_FORMAT_START,
	// BITWISE_FORMAT_LENGTH);

	// console.log("\n Bitwise = " + jPacketJSON.bitwiseFormat);

	// var hexData = new Buffer("AA", "ascii");

	var bitWise35 = parseInt(new Buffer(packet.substr(BITWISE_FORMAT_START, 2),
		"ascii"), 16);
	var bitWise36 = parseInt(new Buffer(packet.substr(BITWISE_FORMAT_START + 2,
		2), "ascii"), 16);

	// console.log("\n Bitwise 35= " + bitWise35);
	// console.log("\n Bitwise 36= " + bitWise36);

	jPacketJSON.batteryStatus = isBitOn(bitWise35, 0);
	jPacketJSON.batteryDisconneced = isBitOn(bitWise35, 1);
	jPacketJSON.leftIndicatorStatus = isBitOn(bitWise35, 2);
	jPacketJSON.rightIndicatorStatus = isBitOn(bitWise35, 3);
	jPacketJSON.sideStandStatus = isBitOn(bitWise35, 4);
	jPacketJSON.hiBeamHeadlampStatus = isBitOn(bitWise35, 5);
	jPacketJSON.lowBeamHeadlampStatus = isBitOn(bitWise35, 6);
	jPacketJSON.clutchStatus = isBitOn(bitWise35, 7);

	jPacketJSON.brakeStatus = isBitOn(bitWise36, 0);
	jPacketJSON.throttleSwitchStatus = isBitOn(bitWise36, 1);
	jPacketJSON.gearStatus = isBitOn(bitWise36, 2);

	jPacketJSON.i3sStatus = isBitOn(bitWise36, 3);
	jPacketJSON.mobilizeStatus = isBitOn(bitWise36, 4);

	jPacketJSON.noTimesSidestand = parseFourByte(packet.substr(
		NO_TIMES_SIDESTAND_START, NO_TIMES_SIDESTAND_LENGTH));
	jPacketJSON.noTimesClutchPress = parseFourByte(packet.substr(
		NO_TIMES_CLUTCH_PRESS_START, NO_TIMES_CLUTCH_PRESS_LENGTH));
	jPacketJSON.noTimesTurnswitchPress = parseFourByte(packet.substr(
		NO_TIMES_TURNSWITCH_PRESS_START, NO_TIMES_TURNSWITCH_PRESS_LENGTH));
	jPacketJSON.noTimesHeadlamp = parseFourByte(packet.substr(
		NO_TIMES_HEADLAMP_START, NO_TIMES_HEADLAMP_LENGTH));
	jPacketJSON.noTimesNeutral = parseFourByte(packet.substr(
		NO_TIMES_NEUTRAL_START, NO_TIMES_NEUTRAL_LENGTH));

	jPacketJSON.noTimesTPS = parseFourByte(packet.substr(NO_TIMES_TPS_START,
		NO_TIMES_TPS_LENGTH));
	jPacketJSON.noTimesI3S = parseFourByte(packet.substr(NO_TIMES_I3S_START,
		NO_TIMES_I3S_LENGTH));

	jPacketJSON.ignitionAngle = parseFourByteFloat(packet.substr(
		IGNITION_ANGLE_START, IGNITION_ANGLE_LENGTH));

	jPacketJSON.latitude = parseFourByteFloat(packet.substr(LATITUDE_START,
		LATITUDE_LENGTH));
	jPacketJSON.dirLatitude = packet.substr(DIR_LATITUDE_START,
		DIR_LATITUDE_LENGTH);
	if (jPacketJSON.dirLatitude != "4E")
		jPacketJSON.latitude = jPacketJSON.latitude * (-1);

	jPacketJSON.longitude = parseFourByteFloat(packet.substr(LONGITUDE_START,
		LONGITUDE_LENGTH));
	jPacketJSON.dirLongitude = packet.substr(DIR_LONGITUDE_START,
		DIR_LONGITUDE_LENGTH);
	if (jPacketJSON.dirLongitude != "45")
		jPacketJSON.longitude = jPacketJSON.longitude * (-1);

	jPacketJSON.deviceTimestamp = getTimestamp(packet.substr(
		(packet.length - 16), TS_LENGTH));
	jPacketJSON.chkSum = packet.substr((packet.length - 4), CS_LENGTH);

	// console.log(myScriptName + new Date + ": " + " Packet JSON = " +
	// JSON.stringify(jPacketJSON));

	allData = allData + "speed=" + jPacketJSON.speed + ",";
	allData = allData + "rpm=" + jPacketJSON.rpm + ",";
	allData = allData + "fuelLevelCount=" + jPacketJSON.fuelLevelCount + ",";
	allData = allData + "xAxis=" + jPacketJSON.xAxis + ",";
	allData = allData + "yAxis=" + jPacketJSON.yAxis + ",";
	allData = allData + "zAxis=" + jPacketJSON.zAxis + ",";
	allData = allData + "noTimesStarterPress="
		+ jPacketJSON.noTimesStarterPress + ",";
	if (jPacketJSON.noTimesStarterPress > 0
		&& jPacketJSON.noTimesStarterPress % 20 == 0) {
		mylogger.debug("Generate EOL Alert for StartSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForStartSwitch", jPacketJSON.noTimesStarterPress);
	}
	allData = allData + "noTimesBrakePress=" + jPacketJSON.noTimesBrakePress
		+ ",";
	if (jPacketJSON.noTimesBrakePress > 0
		&& jPacketJSON.noTimesBrakePress % 20 == 0) {
		mylogger.debug("Generate EOL Alert for BrakeSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForBrakeSwitch", jPacketJSON.noTimesBrakePress);
	}

	// allData = allData + "bitwiseFormat=" + jPacketJSON.bitwiseFormat + ",";

	allData = allData + "batteryStatus=" + jPacketJSON.batteryStatus + ",";
	allData = allData + "batteryDisconneced=" + jPacketJSON.batteryDisconneced
		+ ",";
	allData = allData + "leftIndicatorStatus="
		+ jPacketJSON.leftIndicatorStatus + ",";
	allData = allData + "rightIndicatorStatus="
		+ jPacketJSON.rightIndicatorStatus + ",";
	allData = allData + "sideStandStatus=" + jPacketJSON.sideStandStatus + ",";
	allData = allData + "hiBeamHeadlampStatus="
		+ jPacketJSON.hiBeamHeadlampStatus + ",";
	allData = allData + "lowBeamHeadlampStatus="
		+ jPacketJSON.lowBeamHeadlampStatus + ",";
	allData = allData + "clutchStatus=" + jPacketJSON.clutchStatus + ",";
	allData = allData + "brakeStatus=" + jPacketJSON.brakeStatus + ",";
	allData = allData + "throttleSwitchStatus="
		+ jPacketJSON.throttleSwitchStatus + ",";
	allData = allData + "gearStatus=" + jPacketJSON.gearStatus + ",";
	allData = allData + "i3sStatus=" + jPacketJSON.i3sStatus + ",";
	allData = allData + "mobilizeStatus=" + jPacketJSON.mobilizeStatus + ",";

	allData = allData + "noTimesSidestand=" + jPacketJSON.noTimesSidestand
		+ ",";
	if (jPacketJSON.noTimesSidestand > 0
		&& jPacketJSON.noTimesSidestand % 20 == 0) {
		mylogger.debug("Generate EOL Alert for SideStand");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForSidestandSwitch", jPacketJSON.noTimesSidestand);
	}

	allData = allData + "noTimesClutchPress=" + jPacketJSON.noTimesClutchPress
		+ ",";
	if (jPacketJSON.noTimesClutchPress > 0
		&& jPacketJSON.noTimesClutchPress % 20 == 0) {
		mylogger.debug("Generate EOL Alert for CluchSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForCluchSwitch", jPacketJSON.noTimesClutchPress);
	}

	allData = allData + "noTimesTurnswitchPress="
		+ jPacketJSON.noTimesTurnswitchPress + ",";
	if (jPacketJSON.noTimesTurnswitchPress > 0
		&& jPacketJSON.noTimesTurnswitchPress % 20 == 0) {
		mylogger.debug("Generate EOL Alert for TurnSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForTurnSwitch", jPacketJSON.noTimesTurnswitchPress);
	}

	allData = allData + "noTimesHeadlamp=" + jPacketJSON.noTimesHeadlamp + ",";
	if (jPacketJSON.noTimesHeadlamp > 0
		&& jPacketJSON.noTimesHeadlamp % 20 == 0) {
		mylogger.debug("Generate EOL Alert for HeadLampSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForHeadLampSwitch", jPacketJSON.noTimesHeadlamp);
	}

	allData = allData + "noTimesNeutral=" + jPacketJSON.noTimesNeutral + ",";
	if (jPacketJSON.noTimesNeutral > 0 && jPacketJSON.noTimesNeutral % 20 == 0) {
		mylogger.debug("Generate EOL Alert for NeutralSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude,
			"eolForNeutralSwitch", jPacketJSON.noTimesNeutral);
	}

	allData = allData + "noTimesTPS=" + jPacketJSON.noTimesTPS + ",";
	if (jPacketJSON.noTimesTPS > 0 && jPacketJSON.noTimesTPS % 20 == 0) {
		mylogger.debug("Generate EOL Alert for TPSSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude, "eolForTPSSwitch",
			jPacketJSON.noTimesTPS);
	}

	allData = allData + "noTimesI3S=" + jPacketJSON.noTimesI3S + ",";
	if (jPacketJSON.noTimesI3S > 0 && jPacketJSON.noTimesI3S % 20 == 0) {
		mylogger.debug("Generate EOL Alert for I3SSwitch");
		doProcessEOLPacket(jPacketJSON.devID, jPacketJSON.deviceTimestamp,
			jPacketJSON.latitude, jPacketJSON.longitude, "eolForI3SSwitch",
			jPacketJSON.noTimesI3S);
	}

	allData = allData + "ignitionAngle=" + jPacketJSON.ignitionAngle + ",";

	if (jPacketJSON.latitude != 0)
		allData = allData + "Latitude=" + jPacketJSON.latitude + ",";
	if (jPacketJSON.longitude != 0)
		allData = allData + "Longitude=" + jPacketJSON.longitude + ",";

	jResultJSONForDas.id = jPacketJSON.devID;
	jResultJSONForDas.timeStamp = jPacketJSON.deviceTimestamp;
	jResultJSONForDas.all_data = allData;

	return jResultJSONForDas;
}

function mongoProducer(document) {
	// console.log("==========================================================");
	// console.log("MONGO==>" + JSON.stringify(mongo_document));
	// console.log("==========================================================");

	// <DR> queue is removed bcoz it is converting mongo document to string
	// var job = jobsURL.create(mongoTopic, { document: mongo_document });
	// job.save();

	// set default values
	document.origin = require("os").hostname();
	document.serverTimestamp = new Date();

	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
	mongoQThroughputRecord.increment();
}

/*
 * Here we are mainitaining another Queue of "URL" Strings with "Topic"
 * DAS_TOPIC
 */
function processURL(URL) {
	URL = URL.replace(/ /g, "%20"); // Replace SPACES with PERCENTAGE
	var job = jobsURL.create(DAS_TOPIC, {
		URL: URL
	});
	job.save();
	parserQThroughputRecord.increment();
}

/*
 * Parse the RAW Packet
 */
function processPacket(packet) {

	/* Do the basic Parsing */
	var jResultJSONForDas = parseRawPacket(packet);
	if (jResultJSONForDas != null) {
		processURL(creatUrl(jResultJSONForDas));
	}
}

/*
 * This function is responsible for creating required URL which will be send to
 * DAS
 */
function creatUrl(json) {

	var url;
	url = DAS_BASEURL_PARSED.replace("{id}", json.id).replace("{timeStamp}",
		json.timeStamp.replace(" ", "%20"));
	url = url.replace("{packet}", json.all_data);

	mylogger.debug("(creatUrl)-Created URL = " + url);

	return url;
}
