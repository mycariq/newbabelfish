//import server module
const https = require('https');
var Server = require("../server.js");
var config = require("../config")["napino"];
var utils = require("../utils");
var xor = require('bitwise-xor');

var port = config["port"];
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;

var logger = require("../logger.js");
var mylogger = logger.getLogger("napino", "receiver.log");

mylogger.debug("(Server)- Started on port :" + port);	

/* ============================== Start : Code for Feedback loop ============================*/

var configurator = require("../config")["Configurator"];
var frequncy = configurator["frequency"];
//var frequncy = 1000;
var configCmds = ["MOBILIZE_SWITCH"];
var HashMap = require('hashmap');
var mapOfConfigurations = new HashMap();

setInterval(checkConfiguration, frequncy);

var apiDetails = require("../config")["API"];

var optionsHost=apiDetails["DNS"];
var optionsPort=apiDetails["port"];
var optionsPath='/Cariq/devices/admin/feedback/after/';

mylogger.info("Options Host - Port - Path :" + optionsHost + "-" + optionsPort + "-" + optionsPath);

//console.log("\n Options :" + options.path);
var beginDate = new Date();
//beginDate.setHours(beginDate.getHours() - 5); // Need to remove this code
//beginDate.setMinutes(beginDate.getMinutes() - 30); // Need to remove this code
var beginDateTime = utils.covertDateToString(beginDate).replace(/ /g, "%20");
mylogger.debug("beginDateTime:" + beginDateTime);
var updatedOn = beginDateTime;

var options = {
   host: '',
   port: '',
   path: '',
   // authentication headers
   headers: {
      'Authorization' : 'Basic dGhpbmtkajowZGQ1NDkzOGQ2ZTllMzc5MzJlOTcxNTk1NDRiODMyNg=='
   }   
};


function getConfigJSON() {

  var configJSON = {};
  
  options.path = optionsPath + updatedOn;
  options.host = optionsHost;
  options.port = optionsPort;
  //mylogger.debug("(getConfigJSON)-optionsHost:" + options.host + " " + options.port + " " + options.path);
  request = https.get(options, function(res){
  var body = "";
  res.on('data', function(data) {
    body += data;
  });
   res.on('end', function() {
   configJSON = JSON.parse(body);

   if (configJSON.length > 0) {
   		mylogger.debug("(getConfigJSON)-configJSON:" + JSON.stringify(configJSON));
//  PUSH in MAP

  for(var myKey in configJSON) {
     //console.log("--------------");
     var subJson = configJSON[myKey];
     //console.log("\n DEV:" + subJson.deviceId);
     //console.log("\n CMD:" + subJson.command);
     var paramJson =  subJson.parameter;

/*
   if (paramJson.SSID)
   	console.log("SSID:" + paramJson.SSID);

   if (paramJson.SERVER_NAME)
   	console.log("SERVER_NAME:" + paramJson.SERVER_NAME);

   if (paramJson.PORT)
   	console.log("PORT:" + paramJson.PORT);
   
   if (paramJson.APN)
   	console.log("APN:" + paramJson.APN);
   
   if (paramJson.WIFI_ONOFF)
   	console.log("WIFI_ONOFF:" + paramJson.WIFI_ONOFF);

   if (paramJson.PWD)
   	console.log("PWD:" + paramJson.PWD);

*/

   var myJson = {};
   myJson.id = subJson.deviceId;
   //myJson.id = "123456789012345"; // This is HACK for testing....
   myJson.cmdname = subJson.command;
   myJson.cmdparam ="";

   switch (subJson.command) {
	case "MOBILIZE_SWITCH":
		myJson.cmdparam = paramJson.MOBILIZE_ONOFF;
		break;
	default:
		mylogger.error("(getConfigJSON)-!!!!! Something is Wrong,ANOMALY,INVALID_COMMAND,Command is not MOBILIZE_SWITCH");
		break;
   }
   mylogger.debug("(getConfigJSON)-Inserting this JSON in MAP: " + JSON.stringify(myJson));
   mapOfConfigurations.set(myJson.id+myJson.cmdname, myJson);
  }
//  End : PUSH in MAP
    } //else {
	//	//console.log(myScriptName+ "" + new Date + " : No NEW Configuration to Apply !!!");
    //}
	

  })
   res.on('error', function(e) {
      mylogger.error("(getConfigJSON)-Got Error for request,ANOMALY,GETCONFIG_ERROR," + e.message);
   });
});

request.on('error', function(e) {
  mylogger.error("(getConfigJSON)-Problem with request,ANOMALY,GETCONFIG_PROBLEM," + e.message);
});


//console.log("\n Options :" + options.path);
var d2 = new Date();
//d2.setHours(d2.getHours() - 5); // Need to remove this code
//d2.setMinutes(d2.getMinutes() - 30); // Need to remove this code
var currDateTime = utils.covertDateToString(d2).replace(/ /g, "%20");
//console.log("currDateTime:" + currDateTime);
//console.log("\n Options :" + options.path);
updatedOn = currDateTime;


}



function checkConfiguration() {
	//mylogger.debug("(checkConfiguration)-Get Configuration JSON from Platform");
	var configJSON = {};
        getConfigJSON();
}

/* ============================== End : Code for Feedback loop ============================*/

var throughputConfig = require("../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];

var receiverThroughputRecord  = new ThroughputRecord("receiverThroughputRecord", new Date, 0, threshold);

/*
 * Verify If checksum is right
 **/
function verifyCheckSum(packet) {
	return true;
}

new Server("napino", port).start(function(err, data, id, socket) {
	
	//if error is occured then log it exit from function
	if(err){
		mylogger.error("(Server)-Unexpected Error,ANOMALY,UNEXPECTED_ERROR," +  err);
		return ;
	}
	//if data is undefined then exit from function
	if(!data)
		return;

	var packet = new Buffer(data, "utf8").toString("hex").toUpperCase();
	mylogger.debug("(Server)-Received Raw Packet (Writing to Q),RAW_DATA,DATA," + packet);
	receiverThroughputRecord.increment();

	var responseToClient = "41434B";
	if (verifyCheckSum(packet))
	{
			   var devID;
           var cmdID = (new Buffer (packet.substr(0, 2), "hex").toString("ascii"));
	   //console.log("###################### cmdID ####" , cmdID);
	   if ( cmdID == "w")
           	devID = (new Buffer (packet.substr(4, 30), "hex").toString("ascii"));
	   else
           	devID = (new Buffer (packet.substr(2, 30), "hex").toString("ascii"));

	   mylogger.debug("(Server)-DeviceID = " + devID);

	   var cmdsLength = configCmds.length;
           // Check for all possible  deviceID-command combination , get out of this loop once found Configuration.... 
           for (var iTest = 0 ; iTest < cmdsLength ; iTest++) {
	            var configObject = mapOfConfigurations.get(devID+configCmds[iTest]);
		    if (configObject) {
			mylogger.debug("(Server)-We need to apply this configuration,ACTION,APPLY_CONFIG," + JSON.stringify(configObject));
			mylogger.debug("(Server)-CMD = " + configObject.cmdname);
			mylogger.debug("(Server)-CMDPARAM = " + configObject.cmdparam);
			mapOfConfigurations.remove(devID+configCmds[iTest]);

			switch (configObject.cmdname) {
				case "MOBILIZE_SWITCH":
					//console.log(myScriptName + new Date + " We need to APPLY this ############# HOW ????");
					if (configObject.cmdparam == "ON")
						responseToClient = responseToClient + "000100"; 
					if (configObject.cmdparam == "OFF")
						responseToClient = responseToClient + "000200"; 
					break;

				default:
					mylogger.error("(Server),ANOMALY,UNDEFINED_CONFIG,This configuration command is YET to implement");
					break;
			}
			break ; // One Configuration at one time :)
		}
            }
	}
	else
		responseToClient = "455252";

	if (socket.write(new Buffer(responseToClient, 'hex')))
		mylogger.debug("(Server)-ResponseToClient write DONE,ACTION,WRITE_TO_DEVICE_PASS,"  + devID + ",Response :" + responseToClient);
	else
		mylogger.debug("(Server)-ResponseToClient write DONE,ANOMALY,WRITE_TO_DEVICE_FAIL,"  + devID + ",Response :" + responseToClient);

});
