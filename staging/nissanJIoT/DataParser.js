var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("NissanJIoT", "receiver.log");

'use strict'
class DataParser {
    constructor() {

    }

    parse(owner, document) {
        document.type = "data";
        document.subtype = "obdgps";
        document.owner = owner;
        document.vin = "MDHFDAD15K2003700";

        // add location
        if (!document["latitude"])
            document.latitude = 0.0;
        if (!document["longitude"])
            document.longitude = 0.0;
        document["location"] = utils.getGeoJson(document["latitude"], document["longitude"], true);

        // remove latitude and longitude from document
        delete document["latitude"];
        delete document["longitude"];

        // seatBelt 
        if (document.seatBelt)
            document.seatBelt = utils.binToInt(document.seatBelt);

        // door 
        if (document.door) {
            var door = document.door;
            var fr = door.substr(1, 1);
            var fl = door.substr(0, 1);
            var rl = door.substr(2, 1);
            rl = (rl == "2") ? "1" : "0";
            var rr = door.substr(3, 1);
            rr = (rr == "2") ? "1" : "0";
            var boot = "0";
            document.door = fr + fl + rl + rr + boot;
            document.door = utils.binToInt(document.door);
        }
          
        // lowBeam
        if (!document.lowBeam)
            document.lowBeam = 0;

        // highBeam
        if (!document.highBeam)
            document.highBeam = 0;

        // fuelStatus
        if (!document.fuelStatus)
            document.fuelStatus = 0;

        // airbag
        if (!document.airbag)
            document.airbag = 0;

        // parking
      //  if (!document.parking)
            document.parking = 0;

        // add defaults
        helper.addDefaults(document);

        // persist 
        persist(document);
    }
}

function persist(document) {
    logger.debug("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.debug("KafkaResult:" + JSON.stringify(result));
    });
}


module.exports = DataParser;