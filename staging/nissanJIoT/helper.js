var origin = require("os").hostname();

function parseOwnerFromTopic(topic) {
    var splitted = topic.split("/");
    return splitted[splitted.length - 1];
}


function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-VIoT";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

exports.parseOwnerFromTopic = parseOwnerFromTopic;
exports.addDefaults = addDefaults;