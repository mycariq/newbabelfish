//Load config
var mqttConfig = require("../../config.json").mqtt;
var logger = require("../../logger.js").getLogger("NissanJIoT", "receiver.log");
var helper = require("./helper");
var dataParser = new (require("./DataParser"))();


//Create MQTT options
var options = {
    username: "hardware@mycariq.com",
    password: "NJ8pn*#kg@c=*Am4",
    ca: require("fs").readFileSync(require.resolve("../../certs/" + mqttConfig.caFileName) + "")
}


//Connect to MQTT Broker
var mqttClient = new (require("../../MQTTClient"))(mqttConfig.url, options, packetReceiver);

//Subscribe to the topics
var shareTopic = "$share/babelfish/";
var dataReqTopic = "/nissan/JIoT"
mqttClient.subscribe(shareTopic + dataReqTopic + "/#");

//Packet receiver callback
function packetReceiver(topic, message) {

    var hexPacket = message.toString("hex");
    logger.debug("Received raw hex packet:" + hexPacket + " topic:" + topic);

    var asciiPacket = message.toString("utf-8");
    logger.debug("Received raw ascii packet:" + asciiPacket);

    var jsonPacket = JSON.parse(asciiPacket);
    logger.debug("Received JSON packet:" + JSON.stringify(jsonPacket));

    if (topic.startsWith(dataReqTopic))
        processDataPacket(topic, jsonPacket);
}

function processDataPacket(topic, jsonPacket) {
    if (!isValidDataPacket(jsonPacket)) {
        logger.debug("Data packet is not valid:" + JSON.stringify(jsonPacket));
    }
    else {
        var owner = helper.parseOwnerFromTopic(topic);
        dataParser.parse(owner, jsonPacket);
    }
}

function isValidDataPacket(jsonPacket) {
    return true;
}


// var packet = { "type": "data", "subtype": "obdgps", "timestamp": "2023-11-21 12:20:48", "ignition": "0", "seatBelt": "00000", "parking": "0", "door": "0000", "deviceId": "ID", "vin": "VIN", "engineStt": "0" };
// var packet = JSON.stringify(packet);
// packetReceiver("/nissan/JIoT/357376060895268", Buffer.from(packet, "utf-8"));