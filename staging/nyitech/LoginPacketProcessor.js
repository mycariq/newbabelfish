var utils = require("../utils");
var helper = require("./helper");

'use strict'
class LoginPacketProcessor {
    constructor() {

    }
    process(document, data) {
        var index = 0;
        document.type = "data";
        document.subtype = "login";
        document.timestamp = helper.UtcTime(data.substr(data.length - 12));
        helper.GpsInfo(document, data.substr(index, 42));
        document.obdModuleVersion = data.substr(index += 42, 8);
        document.unitFirmwareVersion = data.substr(index += 8, 8);
        document.unitHardwareVersion = data.substr(index += 8, 8);
        document.editedParameterNumber = data.substr(index += 8, 2);
        document.editedParameterNumberArray = data.substring(index += 2, data.length - 12);
    }
}

module.exports = LoginPacketProcessor;