var helper = require("./helper");
var utils = require("../utils");

'use strict'
class ObdGpsPacketProcessor {
    constructor() {

    }
    process(document, data) {
        var index = 0;
        document.type = "data";
        document.subtype = "obdgps";

        document.timestamp = helper.UtcTime(data.substr(index, 12));
        document.dataSwich = data.substr(index += 12, 6);
        helper.GpsInfo(document, data.substr(index += 6, 42));

        var noOfPids = utils.hexToInt(data.substr(index += 42, 2));
        index += 2;
        for (var pidNo = 1; pidNo <= noOfPids; pidNo++) {
            var pid = "41" + data.substr(index, 2);
            var lenght = utils.hexToInt(data.substr(index += 4, 2));
            var pidValue = utils.reverseHex(data.substr(index += 2, lenght * 2));
            utils.parsePid(document, pid, pidValue);
            index += lenght * 2;
        }

        document.tripFuelConsumption = data.substr(index, 8);
        document.tripMileage = data.substr(index += 8, 8);
        document.tripDuration = data.substr(index += 8, 8);

        var gSensorDataLength = utils.hexToInt(utils.reverseHex(data.substr(index += 8, 4)));
        var gSensorData = data.substr(index += 4, gSensorDataLength * 2);
        helper.GSensorInfo(document, gSensorData);

        document.voltage = utils.hexToInt(utils.reverseHex(data.substr(index += gSensorDataLength * 2, 4))) / 10;
        document.protocolCode = utils.hexToInt(utils.reverseHex(data.substr(index += 4, 4)));
        document.accOn = (utils.hexToInt(utils.reverseHex(data.substr(index += 4, 2))) == 1) ? true : false;
    }
}

module.exports = ObdGpsPacketProcessor;