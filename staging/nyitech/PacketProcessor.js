var loginPacketProcessor = new (require("./LoginPacketProcessor"))();
var obdGpsPacketProcessor = new (require("./ObdGpsPacketProcessor"))();
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

'use strict'
class PacketProcessor {
    constructor() {

    }
    process(owner, command, data) {

        switch (command) {
            case "1001":
                var document = { owner: owner };
                loginPacketProcessor.process(document, data);
                require("./helper").addDefaults(document);
                persist(document);
                break;
            case "2001":
                var document = { owner: owner };
                obdGpsPacketProcessor.process(document, data);
                require("./helper").addDefaults(document);
                persist(document);
                break;
            default:
                console.log("No processor found for" + command);
        }
    }
}

function persist(document) {
    console.log("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketProcessor;
