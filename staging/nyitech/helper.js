var utils = require("../utils");

function addDefaults(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "NYitech";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = require("../utils").getGeoJson(0.0, 0.0, false);
		}
	}
	return document;
}

function UtcTime(data) {
	var day = utils.hexToInt(data.substr(0, 2));
	var month = utils.hexToInt(data.substr(2, 2));
	var year = "20" + utils.hexToInt(data.substr(4, 2));
	var hour = utils.hexToInt(data.substr(6, 2));
	var minute = utils.hexToInt(data.substr(8, 2));
	var second = utils.hexToInt(data.substr(10, 2));
	return new Date(year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second);
}

function GpsInfo(document, data) {

	var index = 12;
	document.status = utils.reverseStr(utils.hexToBin(data.substr(index, 2)));

	var latitude = utils.hexToInt(utils.reverseHex(data.substr(index += 2, 8))) / 3600000;
	if (document.status[2] == '0')
		latitude = latitude * -1;
	var longitude = utils.hexToInt(utils.reverseHex(data.substr(index += 8, 8))) / 3600000;
	if (document.status[3] == "0")
		longitude = longitude * -1;
	document.location = utils.getGeoJson(latitude, longitude, true);

	document.speed = utils.hexToInt(utils.reverseHex(data.substr(index += 8, 4))) * 0.036;
	document.direction = data.substr(index += 4, 4);
	document.high = data.substr(index += 4, 4);
}

exports.GSensorInfo = function (document, data) {
	document.accelerometerX = [];
	document.accelerometerY = [];
	document.accelerometerZ = [];
	for (var i = 0; i < data.length / 12; i++) {
		var startIndex = i * 12;
		document.accelerometerX[i] = utils.hexToSInt(utils.reverseHex(data.substr(startIndex + 0, 4))) * 3.9065 / 1000;
		document.accelerometerY[i] = utils.hexToSInt(utils.reverseHex(data.substr(startIndex + 4, 4))) * 3.9065 / 1000;
		document.accelerometerZ[i] = utils.hexToSInt(utils.reverseHex(data.substr(startIndex + 8, 4))) * 3.9065 / 1000;
	}
}

exports.addDefaults = addDefaults;
exports.UtcTime = UtcTime;
exports.GpsInfo = GpsInfo;