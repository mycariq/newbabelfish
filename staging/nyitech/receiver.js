var utils = require("../utils");
var Server = require("../server");
var packetProcessor = new (require("./PacketProcessor"))();

new Server("nyitech", require("../config.json").nyitech.port).start(function (err, data, id, socket) {

	var rawPacket = data.toString("hex").toUpperCase();
	console.log("Received raw packet ::: " + rawPacket);

	processPacket(rawPacket);

});

function processPacket(rawPacket) {
	var packets = rawPacket.replace(/0D0A4040/g, '0D0A===========4040').split("===========");
	packets.forEach(function (packet) {
		console.log("Processing ::: " + packet);
		var packetJson = {
			header: packet.substr(0, 4),
			length: utils.hexToInt(utils.reverseHex(packet.substr(4, 4))),
			owner: utils.hexToText(packet.substr(8, 24)),
			command: utils.reverseHex(packet.substr(32, 4)),
			data: packet.substring(36, packet.length - 8),
			crc: packet.substring(packet.length - 8, packet.length - 4),
			tail: packet.substring(packet.length - 4, packet.length),
			raw: packet
		}

		packetProcessor.process(packetJson.owner, packetJson.command, packetJson.data);

	});
}


//var loginPacket = "404044003247512D313630313031313801101D0B110622220F4614FC033CE6D30F17000000A215010103090402090001010000064261636465621D0B11062222E69F0D0A";
//var obdGpsLivePacket = "404083003247512D31363031303131380120010C11070B1A808080010C11070B1A0F7E0CFC034A0BD40FD601D60B7B1606052001810B2001730C2002A2120D2001110F200149102002A5089A000000B4190000F28709001E00BFFFDDFFF300B5FFE0FFD800C6FFD2FFCE005DFFB5FFE7008EFFEBFFF0008E00FFFF010100004AEB0D0A";
//processPacket(loginPacket);
//processPacket(obdGpsLivePacket);