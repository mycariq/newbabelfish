var logger = require("../../logger").getLogger("pictorPS25", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");
var alert = require("./Alert.json");
var gsmSignal = require("./GsmSignal.json");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "alert", subtype: alert[content.substr(60,2)] };
        if(!document.subtype)
            return;

        document.timestamp = helper.parseTimestamp(content.substr(0, 12));
        document.noOfSatellite = utils.hexToInt(content.substr(13, 1));
        var latitude = utils.hexToInt(content.substr(14, 8)) / 1800000;
        var longitude = utils.hexToInt(content.substr(22, 8)) / 1800000;
        document.location = utils.getGeoJson(latitude, longitude, true);
        document.speed = utils.hexToInt(content.substr(30, 2));
        document.course = content.substr(32, 4);
        document.lbslength = content.substr(36,2);
        document.mcc = content.substr(38, 4);
        document.mnc = content.substr(42, 2);
        document.lac = content.substr(44, 4);
        document.cellId = content.substr(48, 6);
        helper.parseTerminalInfo(document,utils.hexToBin(content.substr(54,2)));
        document.voltage = helper.voltage(content.substr(56, 2));     
        document.gsmSignal = gsmSignal[content.substr(58, 2)];

        return document;
    }
}

module.exports = AlertParser;