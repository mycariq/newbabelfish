var logger = require("../../logger").getLogger("pictorPS25", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LocationParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "data", subtype: "gps" };
        document.timestamp = helper.parseTimestamp(content.substr(0, 12));
        document.noOfSatellite = utils.hexToInt(content.substr(13, 1));
        var latitude = utils.hexToInt(content.substr(14, 8)) / 1800000;
        var longitude = utils.hexToInt(content.substr(22, 8)) / 1800000;
        document.location = utils.getGeoJson(latitude, longitude, true);
        document.speed = utils.hexToInt(content.substr(30, 2));
        document.course = content.substr(32, 4);
        document.mcc = content.substr(36, 4);
        document.mnc = content.substr(40, 2);
        document.lac = content.substr(42, 4);
        document.cellId = content.substr(46, 6);
        return document;
    }
}

module.exports = LocationParser;