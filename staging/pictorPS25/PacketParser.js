var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger").getLogger("pictorPS25", "receiver.log");

var parsers = {
    "12": new (require("./LocationParser"))(),
    "16": new (require("./AlertParser"))(),
    "13": new (require("./HeartbeatParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(deviceId, command, content, serialNo) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for " + command);
            return;
        }

        //parse data
        var document = parser.parse(content);
        if (!document)
            return;

        //add owner and defaults
        document = utils.extendJson({ owner: deviceId }, document);
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;