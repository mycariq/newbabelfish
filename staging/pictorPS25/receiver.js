var Server = require("../../server");
var helper = require("./helper");
var logger = require("../../logger").getLogger("pictorPS25", "receiver.log");
var responseSender = new (require("./ResponseSender"))();
var packetParser = new (require("./PacketParser"))();

new Server("pictorPS25", require("../../config.json").pictorPS25.port).start(function (err,
	data, id, socket) {

	//convert received packet to hex string
	var packet = data.toString("hex").toUpperCase();
	logger.info("received: " + packet);

    //validate packet. return if it is invalid
	if (!isValidPacket(packet))
			return;

	//process packet
	processPacket(socket, socket["deviceId"], packet);

	});


function isValidPacket(packet) {
		//fetch head, tail crc from packet
		var head = packet.substr(0, 4);
		var tail = packet.substr(packet.length - 4, 4);
		var crc = packet.substr(packet.length - 8, 4);
	
		//validate head and tail
		if ("7878" != head || "0D0A" != tail) {
			logger.info("Head and Start are not matching. Hence ignoring packet: " + packet);
			return false;
		}
	
		//validate crc
		if (crc != helper.calculateChecksum(packet.substring(4, packet.length - 8))) {
			logger.info("CRC is not matching. hence ignoring packet: " + packet);
			return false;
		}
		return true;
}

function processPacket(socket, deviceId, packet) {

	//create packet format json from packet
	var packetJson = {};
	packetJson.deviceId = deviceId;
	packetJson.head = packet.substr(0, 4); //start
	packetJson.packetLength = packet.substr(4, 2); //packet length
	packetJson.command = packet.substr(6, 2); //protocol number
	packetJson.content = packet.substring(8, packet.length - 12); //information content
	packetJson.serialNo = packet.substr(packet.length - 12, 4); // serial number
	packetJson.crc = packet.substr(packet.length - 8, 4); //error check
	packetJson.tail = packet.substr(packet.length - 4, 4) //stop bit

	if (packetJson.command == "01") {
		deviceId = packetJson.content.substr(1);
		socket["deviceId"] = deviceId;
	}

	logger.info("DeviceId: " + deviceId + "  Raw json : " + JSON.stringify(packetJson));

	//parse packet
	packetParser.parse(deviceId, packetJson.command, packetJson.content, packetJson.serialNo);
	//send response
	responseSender.send(socket, deviceId, packetJson.command, packetJson.serialNo);
}