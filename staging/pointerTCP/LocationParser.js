var logger = require("../../logger.js").getLogger("pointerTCP", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LocationParser {
    constructor() {
    }

    parse(packet) {

        var document = { type: "data", subtype: "gps" };

        document.header = utils.hexToText(packet.substr(0, 8));
        document.messageType = utils.hexToInt(packet.substr(8, 2));
        document.owner = utils.revHexToIntStr(packet.substr(10, 8));
        var communicationControlFieldHex = utils.revHexToBin(packet.substr(18, 4));
        document.communicationControlField = communicationControlFieldHex;
        var byte_11 = communicationControlFieldHex.substr(8);
        var byte_10 = communicationControlFieldHex.substr(0, 8);
        // read top to bottom 
        // 00000100 (11th Byte)
        // 0(MSB) = No hibernation;
        // 0 = Momentary speed;
        // 0 = Buisness Mode;
        // 01000 = 8(decimal) = h;
        //document.hibernationHex = byte_11.substr(0, 1);
        document.hibernation = (byte_11.substr(0, 1) == "0") ? false : true;
        
        // read bottom to top
        // 00000000 (10th Byte):
        // 0(LSB) = Active Transmission;
        // 0 = Garmin Disabled;
        // 0= Garmin Not Connected;
        // 0 = Direct from RAM;
        // 01 = PSP Mode is enabled(seebytes 33-38)
        // 0 = Not CAN originated Speed
        // 0(MSB) = Not CAN originated Odometer
        //document.transmissionHex = byte_10.substr(byte_10.length -1);
        document.transmission = (byte_10.substr(byte_10.length -1) == "0") ? "active" : "passive";
        //document.messageSourceHex = byte_10.substr(5, 1); 
        document.messageSource = (byte_10.substr(5, 1) == "0") ? "direct" : "memory";

        document.messageNumerator = utils.hexToInt(packet.substr(22, 2));
        document.hardwareVersion = utils.hexToInt(packet.substr(24, 2));
        document.softwareVersion = utils.hexToInt(packet.substr(26, 2));
        document.protocolVersion = utils.hexToInt(packet.substr(28, 2));
        document.unitStatus = utils.hexToInt(packet.substr(30, 2));
        document.gsmOperator = utils.revHexToInt(packet.substr(32, 2));
        document.transmissionReasonSpecificData = utils.hexToInt(packet.substr(34, 2));
        document.transmissionReason = utils.hexToInt(packet.substr(36, 2));
        document.unitModeOfOperation = utils.hexToInt(packet.substr(38, 2));
        document.unitIOStatus = utils.hexToInt(packet.substr(40, 10));
        var analogInputValues = packet.substr(50, 8); //4 bytes (main power level, battery voltage, input voltage 1, input voltage 2)
        document.voltage = parseFloat((utils.hexToInt(analogInputValues.substr(0, 2)) * 0.1176470588235).toFixed(2));
        document.batteryVoltage = parseFloat((utils.hexToInt(analogInputValues.substr(2, 2)) * 0.01647058823).toFixed(2));
        // depends on cofiguration
        //document.inputVoltage = utils.hexToInt(analogInputValues.substr(4, 2));
        //document.inputVolage2 = utils.hexToInt(analogInputValues.substr(6, 2));
        document.milageCounter = utils.revHexToInt(packet.substr(58, 6));
        document.multiPurposeField = utils.hexToInt(packet.substr(64, 12));
        document.lastGPSFix = utils.revHexToBin(packet.substr(76, 4));
        document.locationStatus = utils.hexToInt(packet.substr(80, 2));
        document.mode = utils.hexToInt(packet.substr(82, 4));
        document.noOfSatellitesUsed = utils.hexToInt(packet.substr(86, 2));
        var longitude = packet.substr(88, 8);
        var latitude = packet.substr(96, 8);
        document.location = helper.getGeoJson(latitude, longitude);
        document.altitude = utils.revHexToInt(packet.substr(104, 8)) * 0.01;
        document.speed = utils.revHexToInt(packet.substr(112, 8)) * 0.036;
        document.speedDirection = (utils.revHexToInt(packet.substr(120, 4)) * (180 / 3.14) * 0.001).toFixed(2);
        document.timestamp = helper.getTimeStamp(packet.substr(124, 14));

        return document;
    }
}

module.exports = LocationParser;