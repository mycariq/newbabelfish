var logger = require("../../logger.js").getLogger("pointerTCP", "receiver.log");
var utils = require("../../utils");
var origin = require("os").hostname();

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-pointerTCP";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function getGeoJson(latHex, longHex) {

    // calculate latitude
    var lat = utils.revHexToInt(latHex);
    var latRadians = lat / 100000000;
    var latitude = latRadians * 180 / 3.1415926535;

    //  calculate longitude
    var lng = utils.revHexToInt(longHex);
    var lngRadians = lng / 100000000;
    var longitude = lngRadians * 180 / 3.1415926535;
    return utils.getGeoJson(latitude, longitude, true);
}

function getTimeStamp(timestamp) {
    var seconds = utils.hexToInt(timestamp.substr(0, 2));
    var minutes = utils.hexToInt(timestamp.substr(2, 2));
    var hours = utils.hexToInt(timestamp.substr(4, 2));
    var day = utils.hexToInt(timestamp.substr(6, 2));
    var month = utils.hexToInt(timestamp.substr(8, 2));
    var yearStr = timestamp.substr(10);
    var year;
    if (2 == yearStr.length)
        year = 2000 + utils.revHexToInt(yearStr);
    else if (4 == yearStr.length)
        year = utils.revHexToInt(yearStr);
    else
        return;

    var timestampStr = year + "-" + month + "-" + day;
    timestampStr += " " + hours + ":" + minutes + ":" + seconds + " UTC";
    return new Date(timestampStr);
}

// Addictive CheckSum Algorithm
function calculateCheckSum(packet) {
    var checkSum = 0;
    var myBuffer = [];

    for (var i = 0; i < packet.length; i += 2) {
        myBuffer[i / 2] = (parseInt(packet.charAt(i), 16) << 4) + (parseInt(packet.charAt(i + 1), 16));
    }

    myBuffer.forEach(element => {
        checkSum += (0xff & element);
    });

    //console.log("Checksum: " + utils.intToHexStr(checkSum & 0xFF));
    return utils.intToHexStr(checkSum & 0xFF);
}

// Generate Authcode using the algorithm shared in the form of excel sheet
function generateAuthCode(unitNo, lookupArray) {
	// Convert to Hex
	var hexUnit = parseInt(unitNo).toString(16);

	// Pad it to 8 length by adding 0s in front
	var paddingLength = 8 - hexUnit.length;
	for (let i = 0; i < paddingLength; i++)
		hexUnit = '0' + hexUnit;

	// Arrange it by taking 2 characters from last to first
	var seed = hexUnit[6] + hexUnit[7] + hexUnit[4] + hexUnit[5] + hexUnit[2] + hexUnit[3] + hexUnit[0] + hexUnit[1];

	var authkey = "";
	for (let i = 0; i < 8; i++) {
		var seedch = seed[i];
		// seed character is hex, get its integer value to lookup into static array
		var seedchInt = parseInt(seedch, 16);
		var ch = lookupArray[seedchInt];
		authkey = authkey + ch;
	}

	return authkey;
}

exports.addDefaults = addDefaults;
exports.getGeoJson = getGeoJson;
exports.getTimeStamp = getTimeStamp;
exports.calculateCheckSum = calculateCheckSum;
exports.generateAuthCode = generateAuthCode;