var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("pointerTLS1_1", "receiver.log");

var parsers = {
    "MCGP": new (require("./LocationParser"))(),
    "CSA": new (require("./AlertParser"))(),
};

'use strict'
class PacketParser {
    constructor() {
    }

    parse(command, data) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  data:" + data);
            return;
        }

        //parse data
        var documents = parser.parse(data);
        if (!documents)
            return;

        //persist document
        persist(documents);
    }
}

function persist(documents) {
    if (!Array.isArray(documents)) {
        documents = [documents];
    }

    documents.forEach(function (document) {
        //add defaults
        helper.addDefaults(document);

        logger.info("Parsed packet " + JSON.stringify(document));
        kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });
}

module.exports = PacketParser;