var Server = require("../../server");
var logger = require("../../logger.js").getLogger("pointerTLS1_1", "receiver.log");
var parser = new (require("./PacketParser"))();
var utils = require("../../utils");
var helper = require("./helper");

//constant auth table
const authTable = [ 2, 1, 8, 0, 0, 1, 7, 9, 1, 2, 4, 1, 7, 1, 5, 8 ];
const ByteSize = 2;

new Server("pointerTLS1_1", require("../../config.json").pointerTLS1_1.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet: " + packet);

	// we are parsing only type 0 packet and packet length 140
	var packetArray = getPacketArray(packet);
	logger.info("packetArray = ", packetArray);

	//processing packet
	packetArray.forEach(function (newPacket) {
		processPacket(socket, newPacket);
	});
});

// packet fragmentation
function getPacketArray(packet) {
	var responseArray = [];
	let index = 0;
	logger.info("packet lenght = ", packet.length);

	// fragmentation of multiple packets
	while(index < packet.length) {
		// gps packet
		var header = utils.hexToText(packet.substr(index, 8));
		if ("MCGP" == header) {
			logger.info("found MCGP header");

			var startIndex = index;
			// including gps packet 70 bytes
			index += 70 * ByteSize;

			var subPacket = packet.substring(startIndex, index);
			responseArray.push(subPacket);
			logger.info("pushing sub packet = ", subPacket);
			logger.info("index = ", index);
			continue;
		}

		// alert event packet
		header = utils.hexToText(packet.substr(index, 6));
		if ("CSA" == header) {
			logger.info("found CSA header");
			var startIndex = index;

			index += 3 * ByteSize; // 3 bytes header

			var subPacketSize = utils.hexToInt(packet.substr(index, 2));
			logger.info("SubpacketSize = ", subPacketSize);

			index += 2 * ByteSize; // Increment 2 bytes of subpacketSize

			index += subPacketSize * ByteSize;
			var subPacket = packet.substring(startIndex, index);
			responseArray.push(subPacket);
			logger.info("pushing sub packet = ", subPacket);
			logger.info("index = ", index);
			continue;
		}

		console.log("UNKOWN HEADER!!!");
		break;
	}

	// returning final packet array
	return responseArray;
}

function processPacket(socket, packet) {

	//validate packet
	if (!isValidPacket(packet))
		return;

	var command = getCommand(packet);
	parser.parse(command, packet);

	var responsePacket = getAcknowledgementPacket(command, packet);
	if (responsePacket)
		sendToDevice(socket, responsePacket);
}

function getCommand(packet) {
	if (!packet)
		return;

	var header = utils.hexToText(packet.substr(0, 8));
	if ("MCGP" == header)
		return "MCGP";

	header = utils.hexToText(packet.substr(0, 6));
	if ("CSA" == header)
		return "CSA";

	return;
}

function getAcknowledgementPacket(command, packet) {

	if ("MCGP" == command) {
		var packetHeader = packet.substr(0, 8);
		var owner = packet.substr(10, 8);
		var messageType = "04"; // default
		var commandNumerator = "00"; // default
		// calculating auth code from auth table
		var authenticationCode = helper.generateAuthCode(utils.revHexToIntStr(owner), authTable);
		var actionCode = "00"; // default
		var messageNumerator = packet.substr(22, 2);
		var unUsedBytes = "0000000000000000000000"; // default

		// generating acknowledgement packet
		var content = messageType + owner + commandNumerator + authenticationCode
			+ actionCode + messageNumerator + unUsedBytes;

		var checkSum = helper.calculateCheckSum(content);
		var response = packetHeader + content + checkSum;
		response = response.toUpperCase();
		return response;
	}

	// alert event packet
	if ("CSA" == command) {
		var packetHeader = packet.substr(0, 6);
		var msgLength = "0D00"; // fixed 13 bytes ack packet
		var numerator = packet.substr(10, 4);
		var messageType = "09"; // default
		var owner = packet.substr(16, 8);
		var moduleId = "09"; // default
		var moduleLength = "03"; // default
		var ackMsgType = "00"; // default
		var spare = "0000"; // deafult

		// generating acknowledgement packet
		var content = msgLength + numerator + messageType + owner + moduleId
			+ moduleLength + ackMsgType + spare;

		var checkSum = helper.calculateCheckSum(content);
		var response = packetHeader + content + checkSum;
		response = response.toUpperCase();
		return response;
	}

	return;
}

function sendToDevice(socket, packet) {
	if (!packet)
		return;

	logger.info("Sending response : " + packet);

	if (!socket)
		return;
	socket.write(new Buffer(packet, "hex"));
}

function isValidPacket(packet) {
	if (!packet) {
		logger.info("invalid packet");
		return false;
	}

	// calculating checksum for MCGP packet
	var header = utils.hexToText(packet.substr(0, 8));
	if ("MCGP" == header) {
		var checkSum = packet.substr(packet.length - 2);
		var content = packet.slice(8, packet.length - 2);

		// validating checksum
		var expectedChecksum = helper.calculateCheckSum(content).toUpperCase();
		if (checkSum != expectedChecksum) {
			logger.info("MCGP - Invalid checksum :: expected:" + expectedChecksum + " packet:" + packet);
			return false;
		}
	}

	header = utils.hexToText(packet.substr(0, 6));
	if ("CSA" == header) {
		var checkSum = packet.substr(packet.length - 2);
		var content = packet.slice(6, packet.length - 2);

		// validating checksum
		var expectedChecksum = helper.calculateCheckSum(content).toUpperCase();
		if (checkSum != expectedChecksum) {
			logger.info("CSA - Invalid checksum :: expected:" + expectedChecksum + " packet:" + packet);
			return false;
		}
	}

	return true;
}

// device - 1638061
var location_1 = "4D43475000ADFE180008012C542764029D002C0063800000EE6EF4A4B677750000000000000084790004020CC2B92F09231756024C0400000700000065141D04060F0BE5076C";
// device - 2235943
var location_2 = "4D43475000271E220008011B542764029D002C0063C00000E379F4AEB64D2A0000000000000084790004020B5E88EF0776D70901A4060000720200006A0B0C04060F0BE50773";
// device - 2235943
var location_3 = "4D43475000271E220008011F542764029D002C0063C00000E379F4AEB64D2A0000000000000084790004020AFA88EF07CED509016C07000083020000670B1104060F0BE50744";
// device - 2235943
var location_4 = "4D43475000271E2200080122542764029D002C0063C00000E378F4AEB64D2A0000000000000084790004020B8F89EF0781D409016C07000018020000770B1304060F0BE50736";
// device - 2149612
var location_5 = "4D43475000ECCC20000812D2542464029E002C00638000002476F6A8B65DB700F7F9E6F021478479A004020C33C55708271A5801BC0200006900000026021C04060F0BE5079A";

// real pointer device packets for testing
// var arr = [location_1, location_2, location_3, location_4, location_5];
// arr.forEach(packet => {
// 	processPacket({}, packet);
// });

