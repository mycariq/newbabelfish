var fs = require("fs");
var tls = require("tls");

var options = {
    //key: fs.readFileSync('private-key.pem'),
    // rejectUnauthorized: false,
    // ca: fs.readFileSync(require.resolve('./CA_sha2_2048.crt')),
    // cert: fs.readFileSync(require.resolve('./CA_sha2_2048.crt'))
   // ca:fs.readFileSync(require.resolve('../iWaveMQTT/server.pem'))
//    key: fs.readFileSync(require.resolve('./private-key.pem')),
    ca: fs.readFileSync(require.resolve('./public-cert.pem')),
    rejectUnauthorized: false
};


var client = tls.connect(7060, /*"dev.nissanconnect.in"*/"localhost", options, function () {

    if (client.authorized) {
        console.log("Connection authorized by a Certificate Authority.");
    } else {
        console.log("Connection not authorized: " + client.authorizationError)
    }

    client.write("I am the client sending you a message.");

});

client.on("data", function (data) {

    console.log('Received: %s [it is %d bytes long]',
        data.toString().replace(/(\n)/gm, ""),
        data.length);

    client.end();

});

client.on('close', function () {

    console.log("Connection closed");

});

client.on('error', function (error) {

    console.error(error);
    client.destroy();

});