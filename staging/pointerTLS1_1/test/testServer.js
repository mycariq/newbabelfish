var logger = console;// require("../logger.js").getLogger("pointerTLSProto", "receiver.log");
var fs = require("fs");
var tls = require("tls");

var options = {
    //key: fs.readFileSync('private-key.pem'),
    // requestCert: false,
    // ca: fs.readFileSync(require.resolve('./CA_sha2_2048.crt')),
    // cert: fs.readFileSync(require.resolve('./CA_sha2_2048.crt'))
    //ca: fs.readFileSync(require.resolve('../iWaveMQTT/server.pem'))

    key: fs.readFileSync(require.resolve('./private-key.pem')),
    cert: fs.readFileSync(require.resolve('./public-cert.pem')),
    rejectUnauthorized: false
};

var key = fs.readFileSync(require.resolve('./private-key.pem'));
var cert = fs.readFileSync(require.resolve('./public-cert.pem'));

var TLSServer = require("../../tlsServer");
// var logger = require("../logger.js").getLogger("pointerTLS1_1", "receiver.log");
// var parser = new (require("./PacketParser"))();
// var utils = require("../utils");
// var helper = require("./helper");

new TLSServer("pointerTLS1_1", require("../../config.json").pointerTLS1_1.port, key, cert).start(function (err,
    data, id, socket) {
    logger.info("[7060] Received=" + data.toString("hex").toUpperCase());
    logger.info("[7060] Received=" + data.toString("utf-8").toUpperCase());
});


// var server7060 = tls.createServer(options, function (socket) {

//     socket.on('data', function (data) {

//         logger.info("[7060] Received=" + data.toString("hex").toUpperCase());
//         logger.info("[7060] Received=" + data.toString("utf-8").toUpperCase());

//     });

//     // Let us know when the transmission is over
//     socket.on('end', function () {

//         logger.info("[7060] Socket is ended");

//     });

// });

// // Start listening on a specific port and address
// server7060.listen(7060, function () {
//     logger.info("[7060] Server started. Listening on 7060");
// });



// var server6050 = tls.createServer(options, function (socket) {

//     socket.on('data', function (data) {

//         logger.info("[6050] Received=" + data.toString("hex").toUpperCase());

//     });

//     // Let us know when the transmission is over
//     socket.on('end', function () {

//         logger.info("[6050] Socket is ended");

//     });

// });

// // Start listening on a specific port and address
// server6050.listen(6050, function () {
//     logger.info("[6050] Server started. Listening on 6050");
// });
