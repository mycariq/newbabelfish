var logger = require("../../logger.js").getLogger("pointerTLS1_2", "receiver.log");
var utils = require("../../utils");
var helper = require("./helper");

// constants
// HA, HB, HT alert
const subReasonMap = new Map();
subReasonMap.set(0, "Reserved");
subReasonMap.set(1, "Green Severity");
subReasonMap.set(2, "Yellow Severity");
subReasonMap.set(3, "Red Severity");

// Crash alert
const crashSubReasonMap = new Map();
crashSubReasonMap.set(0, "EDR Light");
crashSubReasonMap.set(1, "EDR Heavy");
crashSubReasonMap.set(2, "Maneuver Light");
crashSubReasonMap.set(3, "Maneuver Heavy");

// Idling alert
const idlingSubReasonMap = new Map();
idlingSubReasonMap.set(0, "Short Idling Start");
idlingSubReasonMap.set(1, "Long Idling Start");
idlingSubReasonMap.set(2, "Idling Ended ");

// Ignition ON/OFF alert
const ignitionMap = new Map();
ignitionMap.set(0, "ignitionOff");
ignitionMap.set(1, "ignitionOn");

const ignitionSubReasonMap = new Map();
ignitionSubReasonMap.set(0, "Ignition Input");
ignitionSubReasonMap.set(1, "Accelerometer Ignition Sensor");
ignitionSubReasonMap.set(2, "Accelerometer Movement Sensor");
ignitionSubReasonMap.set(3, "Accelerometer Sensor and Voltage");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "alert" };

        var eventReason = parseInt(packet.substr(28, 2));
        var eventSubReason = parseInt(packet.substr(30, 2));
        var event = getEvent(eventReason, eventSubReason);
        logger.info("Alert Event = ", event);
        if (!event.reason)
            return;

        document.subtype = event.reason;
        document.header = utils.hexToText(packet.substr(0, 6));
        document.packetLength = utils.revHexToInt(packet.substr(6, 4));
        document.numerator = utils.revHexToInt(packet.substr(10, 4));
        var messageTypeBinary = utils.hexToBin(packet.substr(14, 2));
        document.messageType = parseInt(messageTypeBinary.substr(messageTypeBinary.length - 3));
        document.owner = utils.revHexToIntStr(packet.substr(16, 8));
        document.moduleId = utils.revHexToInt(packet.substr(24, 2));
        document.moduleLenght = utils.revHexToInt(packet.substr(26, 2));
        document.eventReason = parseInt(packet.substr(28, 2));
        document.eventSubReason = parseInt(packet.substr(30, 2));
        document.eventNumerator = utils.revHexToInt(packet.substr(32, 4));
        document.operationalMode = utils.hexToBin(packet.substr(36, 2));
        document.spare1 = parseInt(packet.substr(38, 2));
        document.driverId = utils.revHexToIntStr(packet.substr(40, 12));
        document.tripId = utils.revHexToIntStr(packet.substr(52, 6));
        document.maneuverId = utils.revHexToIntStr(packet.substr(58, 6));
        document.maneuverDataUsage = parseInt(packet.substr(64, 2));
        document.bitMask = utils.hexToBin(packet.substr(66, 2));
        document.spare2 = parseInt(packet.substr(68, 2));
        document.hdop = parseInt(packet.substr(70, 2));
        document.mode1 = parseInt(packet.substr(72, 2));
        document.mode2 = parseInt(packet.substr(74, 2));
        document.noOfSatellitesUsed = utils.revHexToInt(packet.substr(76, 2));
        var longitude = packet.substr(78, 8);
        var latitude = packet.substr(86, 8);
        document.location = helper.getGeoJson(latitude, longitude);
        document.altitude = utils.revHexToInt(packet.substr(94, 8)) * 0.01;
        document.speed = utils.revHexToInt(packet.substr(102, 2));
        document.speedDirection = (utils.revHexToInt(packet.substr(104, 4)) * (180 / 3.14) * 0.001).toFixed(2);
        document.timestamp = helper.getTimeStamp(packet.substr(108, 12));

        return document;
    }
}

function getEvent(eventReason, eventSubReason) {
    logger.info("eventReason = ", eventReason, ", eventSubReason = ", eventSubReason);

    var event = {};
    switch (eventReason) {
        case 2:
            event.reason = "crash";
            event.subReason = crashSubReasonMap.get(eventSubReason);
            break;

        case 6:
            event.reason = "suddenSpeededUp";
            event.subReason = subReasonMap.get(eventSubReason);
            break;

        case 7:
            event.reason = "rapidDeceleration";
            event.subReason = subReasonMap.get(eventSubReason);
            break;

        case 8:
            event.reason = "sharpTurn"; // suddenLaneChange
            event.subReason = subReasonMap.get(eventSubReason);
            break;

        case 13:
            event.reason = "idling";
            event.subReason = subReasonMap.get(eventSubReason);
            break;

        case 18:
            var subResonBin = utils.hexToBin(utils.intToHexStr(eventSubReason));
            event.reason = ignitionMap.get(parseInt(subResonBin.substr(4, 7)));
            event.subReason = ignitionSubReasonMap.get(parseInt(subResonBin.substr(0, 3)));
            break;

        default:
            break;
    }

    return event;
}

module.exports = AlertParser;