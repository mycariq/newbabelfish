var TLSServer = require("../tlsServer");
var logger = require("../logger.js").getLogger("pointerTLS1_2", "receiver.log");
var parser = new (require("./PacketParser"))();
var utils = require("../utils");
var helper = require("./helper");
var fs = require("fs");
// var key = fs.readFileSync(require.resolve('./server_2048.pem'));
// var cert = fs.readFileSync(require.resolve('./CA_sha2_2048.crt'));

var tlsOptions = {
	key: fs.readFileSync(require.resolve('../pointerTLS1_1/private.key')),
	cert: fs.readFileSync(require.resolve('../pointerTLS1_1/new.crt')),
	rejectUnauthorized: false,
	//ciphers: "TLS_RSA_WITH_AES_256_CBC_SHA256:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!SRP:!CAMELLIA"
	secureProtocol:'TLSv1_2_method'
	//ciphers: "TLS_CHACHA20_POLY1305_SHA256"
}



new TLSServer("pointerTLS1_2", require("../config.json").pointerTLS1_2.port, tlsOptions).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet: " + packet);

	//process packet
	processPacket(socket, packet);
});

function processPacket(socket, packet) {

	//validate packet
	if (!isValidPacket(packet))
		return;

	var packetJSON = {};
	packetJSON.systemCode = packet.substr(0, 8);
	packetJSON.messageType = packet.substr(8, 2);
	packetJSON.owner = packet.substr(10, 8);
	packetJSON.messageType = "04";
	// TODO: not understabd commnad numerator field
	packetJSON.commandNumerator = "00";
	packetJSON.authenticationCode = "00000000";
	packetJSON.actionCode = "00";
	packetJSON.messageNumerator = packet.substr(22, 2);
	packetJSON.unUsedBytes = "0000000000000000000000";

	var command = utils.hexToInt(packet.substr(8, 2));
	parser.parse(command, packet);

	sendPacketResponse(socket, packetJSON);
}

function sendPacketResponse(socket, packetJSON) {
	var response = undefined;

	// generating acknowledgement packet
	var content = packetJSON.messageType + packetJSON.owner
		+ packetJSON.commandNumerator + packetJSON.authenticationCode + packetJSON.actionCode
		+ packetJSON.messageNumerator + packetJSON.unUsedBytes;

	var checkSum = helper.calculateCheckSum(content);
	response = packetJSON.systemCode + content + checkSum;
	response = response.toUpperCase();

	sendToDevice(socket, response);
}

function sendToDevice(socket, packet) {
	if (!packet)
		return;

	logger.info("Sending response : " + packet);

	if (!socket)
		return;
	socket.write(new Buffer(packet, "hex"));
}

function isValidPacket(packet) {
	if (!packet) {
		logger.info("invalid packet");
		return false;
	}

	// calculating checksum
	var checkSum = packet.substr(packet.length - 2);
	var content = packet.slice(8, packet.length - 2);

	// validating checksum
	var expectedChecksum = helper.calculateCheckSum(content);
	if (checkSum != expectedChecksum) {
		logger.info("Invalid checksum :: expected:" + expectedChecksum + " packet:" + packet);
		return false;
	}

	return true;
}

// device - 1638061
var location_1 = "4D43475000ADFE180008012C542764029D002C0063800000EE6EF4A4B677750000000000000084790004020CC2B92F09231756024C0400000700000065141D04060F0BE5076C";
// device - 2235943
var location_2 = "4D43475000271E220008011B542764029D002C0063C00000E379F4AEB64D2A0000000000000084790004020B5E88EF0776D70901A4060000720200006A0B0C04060F0BE50773";
// device - 2235943
var location_3 = "4D43475000271E220008011F542764029D002C0063C00000E379F4AEB64D2A0000000000000084790004020AFA88EF07CED509016C07000083020000670B1104060F0BE50744";
// device - 2235943
var location_4 = "4D43475000271E2200080122542764029D002C0063C00000E378F4AEB64D2A0000000000000084790004020B8F89EF0781D409016C07000018020000770B1304060F0BE50736";
// device - 2149612
var location_5 = "4D43475000ECCC20000812D2542464029E002C00638000002476F6A8B65DB700F7F9E6F021478479A004020C33C55708271A5801BC0200006900000026021C04060F0BE5079A";

// real pointer device packets for testing
var arr = [location_1, location_2, location_3, location_4, location_5];
// arr.forEach(packet => {
// 	processPacket({}, packet);
// });
