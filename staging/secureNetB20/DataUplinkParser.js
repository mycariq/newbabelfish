var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("secureNetB20", "receiver.log");

'use strict'
class DataUplinkParser {
    constructor() {
    }

    parse(packet) {
        var passthroughJSON = {};
        passthroughJSON.passthroughType = packet.substr(0, 2);
        if (passthroughJSON.passthroughType != "F8") {
            logger.info("Does not support passthrough type:" + passthroughJSON.passthroughType);
            return;
        }
        passthroughJSON.content = packet.substr(2);

        passthroughJSON.passthroughDataID = passthroughJSON.content.substr(0, 4);
        if (passthroughJSON.passthroughDataID != "0008") {
            logger.info("Does not support passthrough data id:" + passthroughJSON.passthroughDataID);
            return;
        }
        passthroughJSON.passthroughDataLength = utils.hexToInt(passthroughJSON.content.substr(4, 4));
        passthroughJSON.passthroughData = passthroughJSON.content.substr(8, passthroughJSON.passthroughDataLength * 2);
        var document = parsePassthroughData(passthroughJSON.passthroughData);
        passthroughJSON.doc = document;

        return document;
    }
}

function parsePassthroughData(passthroughData) {
    var passthroughPid = passthroughData.substr(0, 2);
    var passthroughPidData = passthroughData.substr(2);

    switch (passthroughPid) {
        case "01":
            return parseTripStart(passthroughPidData);
            break;

        case "02":
            return parseTripEnd(passthroughPidData);
            break;

        case "03":
            return parseCollision(passthroughPidData);
            break;

        case "04":
            return parseRollover(passthroughPidData);
            break;

        case "05":
            return parseOverSpeed(passthroughPidData);
            break;

        case "06":
            return parseRashAcceleration(passthroughPidData);
            break;

        case "07":
            return parseRashDecceleration(passthroughPidData);
            break;

        case "08":
            return parseSharpTurn(passthroughPidData);
            break;

        case "09":
            return parseSuddenLaneChange(passthroughPidData);
            break;

        case "0E":
            return parseScreeching(passthroughPidData);
            break;

        default:
            break;
    }
}

function parseTripStart(data) {
    var document = { type: "alert", subtype: "ignitionOn", ignition: 1 };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.totalMileage = utils.hexToInt(data.substr(36, 8)) * 10;
    return document;
}

function parseTripEnd(data) {
    var document = { type: "alert", subtype: "ignitionOff", ignition: 0 };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.totalMileage = utils.hexToInt(data.substr(44, 8)) * 10;
    return document;
}

function parseCollision(data) {
    var document = { type: "alert", subtype: "collision" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    return document;
}

function parseRollover(data) {
    var document = { type: "alert", subtype: "rollover" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    return document;
}

function parseOverSpeed(data) {
    var document = { type: "alert", subtype: "overSpeed" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.overSpeedTime = utils.hexToInt(data.substr(44, 8));
    document.overSpeedMileage = utils.hexToInt(data.substr(52, 8));
    document.topSpeed = utils.hexToInt(data.substr(60, 8)) / 10;
    return document;
}

function parseRashAcceleration(data) {
    var document = { type: "alert", subtype: "suddenSpeededUp" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    document.currentSpeed = utils.hexToInt(data.substr(52, 8));
    document.speed1SecondsAgo = utils.hexToInt(data.substr(60, 8));
    document.speed2SecondsAgo = utils.hexToInt(data.substr(68, 8));
    document.speed3SecondsAgo = utils.hexToInt(data.substr(76, 8));
    document.speed4SecondsAgo = utils.hexToInt(data.substr(84, 8));
    document.speed5SecondsAgo = utils.hexToInt(data.substr(92, 8));
    return document;
}

function parseRashDecceleration(data) {
    var document = { type: "alert", subtype: "rapidDeceleration" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    document.currentSpeed = utils.hexToInt(data.substr(52, 8));
    document.speed1SecondsAgo = utils.hexToInt(data.substr(60, 8));
    document.speed2SecondsAgo = utils.hexToInt(data.substr(68, 8));
    document.speed3SecondsAgo = utils.hexToInt(data.substr(76, 8));
    document.speed4SecondsAgo = utils.hexToInt(data.substr(84, 8));
    document.speed5SecondsAgo = utils.hexToInt(data.substr(92, 8));
    return document;
}

function parseSharpTurn(data) {
    var document = { type: "alert", subtype: "sharpTurn" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    document.currentSpeed = utils.hexToInt(data.substr(52, 8));
    document.speed1SecondsAgo = utils.hexToInt(data.substr(60, 8));
    document.speed2SecondsAgo = utils.hexToInt(data.substr(68, 8));
    document.speed3SecondsAgo = utils.hexToInt(data.substr(76, 8));
    document.speed4SecondsAgo = utils.hexToInt(data.substr(84, 8));
    document.speed5SecondsAgo = utils.hexToInt(data.substr(92, 8));
    document.currentDirectionAngle = utils.hexToInt(data.substr(100, 8));
    document.directionAngle1SecondsAgo = utils.hexToInt(data.substr(108, 8));
    document.directionAngle2SecondsAgo = utils.hexToInt(data.substr(116, 8));
    document.directionAngle3SecondsAgo = utils.hexToInt(data.substr(124, 8));
    document.directionAngle4SecondsAgo = utils.hexToInt(data.substr(132, 8));
    document.directionAngle5SecondsAgo = utils.hexToInt(data.substr(140, 8));
    return document;
}

function parseSuddenLaneChange(data) {
    var document = { type: "alert", subtype: "suddenLaneChange" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    document.currentSpeed = utils.hexToInt(data.substr(52, 8));
    document.speed1SecondsAgo = utils.hexToInt(data.substr(60, 8));
    document.speed2SecondsAgo = utils.hexToInt(data.substr(68, 8));
    document.speed3SecondsAgo = utils.hexToInt(data.substr(76, 8));
    document.speed4SecondsAgo = utils.hexToInt(data.substr(84, 8));
    document.speed5SecondsAgo = utils.hexToInt(data.substr(92, 8));
    document.currentDirectionAngle = utils.hexToInt(data.substr(100, 8));
    document.directionAngle1SecondsAgo = utils.hexToInt(data.substr(108, 8));
    document.directionAngle2SecondsAgo = utils.hexToInt(data.substr(116, 8));
    document.directionAngle3SecondsAgo = utils.hexToInt(data.substr(124, 8));
    document.directionAngle4SecondsAgo = utils.hexToInt(data.substr(132, 8));
    document.directionAngle5SecondsAgo = utils.hexToInt(data.substr(140, 8));
    return document;
}

function parseScreeching(data) {
    var document = { type: "alert", subtype: "screeching" };
    document.timestamp = helper.getTimestamp(data.substr(0, 12));
    document.strokeNo = data.substr(12, 8);
    document.location = helper.getGeoJson(data.substr(20, 8), data.substr(28, 8));
    document.tripMileage = utils.hexToInt(data.substr(36, 8));
    document.vehicleSignalState = data.substr(44, 8);
    document.currentSpeed = utils.hexToInt(data.substr(52, 8));
    document.speed1SecondsAgo = utils.hexToInt(data.substr(60, 8));
    document.speed2SecondsAgo = utils.hexToInt(data.substr(68, 8));
    document.speed3SecondsAgo = utils.hexToInt(data.substr(76, 8));
    document.speed4SecondsAgo = utils.hexToInt(data.substr(84, 8));
    document.speed5SecondsAgo = utils.hexToInt(data.substr(92, 8));
    return document;
}

module.exports = DataUplinkParser;