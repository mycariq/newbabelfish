var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "data", subtype: "gps" };
        document.warningMark = utils.hexToBin(packet.substr(0, 8));
        document.state = packet.substr(8, 8);
        document.location = helper.getGeoJson(packet.substr(16, 8), packet.substr(24, 8));
        document.altitude = packet.substr(32, 4);
        document.speed = utils.hexToInt(packet.substr(36, 4)) / 10;
        document.direction = packet.substr(40, 4);
        document.timestamp = helper.getTimestamp(packet.substr(44, 12));
        var tlvData = packet.substr(56);
        var tlvArray = helper.createTLVArray(2, tlvData);
        parseTLVArray(document, tlvArray);

        var documents = parseAlerts(document.warningMark, document.timestamp, document.location);
        documents.push(document);
        return documents;
    }
}

function parseAlerts(warningMark, timestamp, location) {
    var documents = [];
    for (var index = 0; index < warningMark.length; index++) {
        if (warningMark[index] == "1") {
            var alertName = helper.alertJson[index];
            if (!alertName)
                continue;
            var document = { type: "alert", timestamp: timestamp, location: location, subtype: alertName };
            documents.push(document);
        }
    }
    return documents;
}


function parseTLVArray(document, tlvArray) {
    if (!tlvArray)
        return;

    tlvArray.forEach(tlv => {
        var type = tlv.type;
        var value = tlv.value;

        switch (type) {
            // case "01":
            //     document.mileage = utils.hexToInt(value) / 100;
            //     break;

            // case "02":
            //     document.fuelVolume = value;
            //     break;

            // case "03":
            //     document.drivingRecordFunction = value;
            //     break;

            // case "04":
            //     document.alarmEvent = value;
            //     break;

            // case "25":
            //     document.extendedVehicleSignalStatus = value;
            //     break;

            // case "2A":
            //     document.ioStatus = value;
            //     break;

            // case "2B":
            //     document.analog = value;
            //     break;

            // case "30":
            //     document.signalStrength = value;
            //     break;

            // case "31":
            //     document.satelliteNo = value;
            //     break;

            case "E3":
                document.voltage = utils.hexToInt(value.substr(4,4)) / 100;
                break;

            case "F3":
                var obdTLVArray = helper.createTLVArray(4, value);
                parseTLVArray(document, obdTLVArray);
                break;

            // case "D0":
            //     document.doorLock = value;
            //     break;

            // case "D1":
            //     document.windowStatus = value;
            //     break;

            // case "D2":
            //     document.seatBeltPosition = value;
            //     break;

            // case "D3":
            //     document.vehicleAlarm = value;
            //     break;

            // case "D4":
            //     document.vehicleFaultState = value;
            //     break;

            // case "D5":
            //     document.vehicleGearState = value;
            //     break;

            // case "D6":
            //     document.tyrePressure = value;
            //     break;

            // case "C0":
            //     document.temperature = value;
            //     break;

            //obd data cases
            case "0002":
                document.obdSpeed = value;
                break;

            case "0003":
                document.rpm = utils.hexToInt(value);
                break;

            case "0004":
                document.voltage = utils.hexToInt(value) / 1000;
                break;
            // case "0005":
            //     document.totalVehicleMileage = value;
            //     break;
            // case "0006":
            //     document.idleInstantaneousFuel = value;
            //     break;
            // case "0007":
            //     document.instantaneousFuelConsumption = value;
            //     break;

            case "0008":
                document.engineLoad = value;
                break;

            case "0009":
                document.coolantTemperature = utils.hexToInt(value);
                break;
            // case "000B":
            //     document.intakeManifoldAbsolutePressure = value;
            //     break;
            // case "000C":
            //     document.inletTemperature= value;
            //     break;
            // case "000D":
            //     document.inletFlow = value;
            //     break;
            // case "000E":
            //     document.absoluteThrottlePosition = value;
            //     break;
            // case "000F":
            //     document.ignitionAdvanceAngle = value;
            //     break;

            case "0050":
                document.vin = value;
                break;

            case "0051":
                document.dtc = value;
                break;
            // case "0052":
            //     document.id = value;
            //     break;
            // case "0100":
            //     document.totalTripMileage = value;
            //     break;
            // case "0101":
            //     document.accumulatedMileage = value;
            //     break;
            // case "0102":
            //     document.totalFuelConsumption = value;
            //     break;
            // case "0103":
            //     document.accumulatedFuelConsumption = value;
            //     break;
            // case "0104":
            //     document.averageFuelConsumption = value;
            //     break;
            // case "0105":
            //     document.timeUnit = value;
            //     break;
            // case "0106":
            //     document.highSpeedTimesAndHighSpeed = value;
            //     break;
            // case "0107":
            //     document.drivingTimeAtHighSpeed = value;
            //     break;
            // case "0108":
            //     document.totalUltraLongIdleTime = value;
            //     break;
            // case "0109":
            //     document.totalIdleTime = value;
            //     break;
            // case "010A":
            //     document.totalIdleFuelConsumption = value;
            //     break;
            // case "010B":
            //     document.totalDurationOfFatigueDriving = value;
            //     break;
            // case "010C":
            //     document.averageSpeed = value;
            //     break;
            // case "010D":
            //     document.maximumSpeed = value;
            //     break;
            // case "010E":
            //     document.tripMaximumSpeed = value;
            //     break;
            // case "010F":
            //     document.highestTemerature = value;
            //     break;
            // case "0110":
            //     document.highestVoltage = value;
            //     break;
            // case "0111":
            //     document.speedStatistics = value;
            //     break;
            // case "0112":
            //     document.urgentAccelerationTimes = value;
            //     break;
            // case "0113":
            //     document.statisticsOfTimesOfSharp = value;
            //     break;
            // case "0114":
            //     document.statisticsOnTheNumberOfSharp = value;
            //     break;
            // case "0115":
            //     document.statisticsOnTheNumberOfAburpt = value;
            //     break;
            // case "0116":
            //     document.tripBrakeTimesStatistics = value;
            //     break;

            default:
                break;
        }
    });

}

module.exports = GpsParser;