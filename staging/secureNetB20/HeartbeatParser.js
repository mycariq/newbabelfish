var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class HeartbeatParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "data", subtype: "heartbeat", timestamp: new Date() };
        return document;
    }
}

module.exports = HeartbeatParser;