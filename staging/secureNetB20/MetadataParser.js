var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class MetadataParser {
    constructor() {

    }

    parse(packet) {
        var document = { type: "metadata", subtype: "imeiimsi", timestamp: new Date() };
        var index = 0;
        document.terminalType = packet.substr(index, 4);
        document.manufacturerId = packet.substr(index += 4, 10);
        document.terminalType2 = packet.substr(index += 10, 40);
        document.terminalId = packet.substr(index += 40, 14);
        document.iccid = packet.substr(index += 14, 20);
        var hardwareVersionLength = utils.hexToInt(packet.substr(index += 20, 2));
        document.hardwareVersion = utils.hexToText(packet.substr(index += 2, hardwareVersionLength * 2));
        var firmwareVersionLength = utils.hexToInt(packet.substr(index += hardwareVersionLength * 2, 2));
        document.firmwareVersion = utils.hexToText(packet.substr(index += 2, firmwareVersionLength * 2));
        document.gnssModule = packet.substr(index += firmwareVersionLength * 2, 2);
        document.communicationModule = packet.substr(index += 2, 2);

        return document;
    }
}

module.exports = MetadataParser;