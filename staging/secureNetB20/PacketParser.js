var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("secureNetB20", "receiver.log");

var parsers = {
    //  "0100": new (require("./LoginParser"))(),
    "0002": new (require("./HeartbeatParser")),
    "0200": new (require("./GpsParser"))(),
    "0900": new (require("./DataUplinkParser"))(),
    "0107": new (require("./MetadataParser"))(),
    "0704": new (require("./PositioningDataParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(owner, command, packet) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  packet:" + packet);
            return;
        }

        //parse data
        var document = parser.parse(packet);
        if (!document)
            return;

        //persist document
        persist(owner, document);
    }
}

function persist(owner, document) {
    if (!document)
        return;

    if (!(document instanceof Array))
        document = [document];

    document.forEach(doc => {
        doc.owner = owner;
        helper.addDefaults(doc);
        logger.info("Parsed packet " + JSON.stringify(doc));
        kafkaProducer.keyBasedProduce(JSON.stringify(doc), doc.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
        });
    });
}

module.exports = PacketParser;