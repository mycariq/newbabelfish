var utils = require("../../utils");
var helper = require("./helper");
var gpsParser = new (require("./GpsParser"));

'use strict'
class PositioningDataParser {
    constructor() {
    }

    parse(packet) {

        var dataPacketCount = utils.hexToInt(packet.substr(0, 4));
        var backlog = utils.hexToInt(packet.substr(4, 2));
        packet = packet.substr(6);

        var documents = [];
        for (let index = 1; index <= dataPacketCount; index++) {
            let dataLenght = utils.hexToInt(packet.substr(0, 4));
            let data = packet.substr(4, dataLenght * 2);
            
            let parsedDocuments = gpsParser.parse(data);
            parsedDocuments.forEach(parsedDocument => {
                parsedDocument.backlog = backlog;
                documents.push(parsedDocument);
            });
            
            packet = packet.substr(4 + dataLenght * 2);
        }
        
        return documents;
    }
}

module.exports = PositioningDataParser;