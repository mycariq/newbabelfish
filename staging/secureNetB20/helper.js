var utils = require("../../utils");
var origin = require("os").hostname();


var alertJson = {
    "0": "sos",
    "1": "overSpeed",
    //"2": "Tired driving The sign is maintained until the",
    //"3": "Danger warning Reset after receiving reply.",
    //"4": "GnssModuleFails",
    //"5": "GnssAntennaFail",
    //"6": "ShortCircuitOfGnssAntenna",
    "7": "lowBatteryVoltage",
    "8": "pullOutReminder",
    //"9": "terminal LCD or monitor fault The sign is maintained until the",
    //"10": "TTSModuleFault",
    //"11": "camera failure The sign is maintained until the",
    //"12": "fault of IC card module of",
    //"13": "speed warning The sign is maintained until the",
    "14": "rashDriving",
    "15": "vibration",
    //"16":"keep",
    //"17":"keep",
    //"18": "Accumulative driving overtime The sign is maintained until the",
    //"19": "stop over time The sign is maintained until the",
    //"20": "access area Reset after receiving reply.",
    //"21": "access route Reset after receiving reply.",
    //"22": "Insufficient/too long driving",
    //"23": "route deviation alarm The sign is maintained until the",
    //"24": "vehicle VSS failure The sign is maintained until the",
    //"25": "Abnormal vehicle oil volume The sign is maintained until the",
    //"26": "Vehicle theft (through vehicle",
    //"27": "Illegal ignition of vehicle Reset after receiving reply.",
    //"28": "Illegal displacement of",
    "29": "collision",
    "30": "rollover",
    //"31": "illegal door opening alarm (if"
}




function getContentLength(hex) {
    var bin = utils.hexToBin(hex);
    var contentLength = utils.binToInt(bin.substr(6, 10)) * 2;
    return contentLength;
}

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-SecureNet-B20";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;

}

/**
 * get geo json
 * @param {*} latHex 
 * @param {*} longHex 
 */
function getGeoJson(latHex, longHex) {
    var latitude = utils.hexToInt(latHex) / 1000000;
    var longitude = utils.hexToInt(longHex) / 1000000;
    // if geo location is in china then set 0,0
    if (isChinaLocation(latitude, longitude))
        return utils.getGeoJson(0, 0, false);

    return utils.getGeoJson(latitude, longitude, true);
}

function isChinaLocation(latitude, longitude) {
    // geo region of china location
    var lLat = 9.984839;
    var lLng = 96.009240;
    var uLat = 38.063019;
    var uLng = 123.494562;

    return (latitude > lLat && latitude < uLat) && (longitude > lLng && longitude < uLng)
}

function getTimestamp(timestampStr) {
    var year = "20" + timestampStr.substr(0, 2);
    var month = timestampStr.substr(2, 2);
    var day = timestampStr.substr(4, 2);
    var hour = timestampStr.substr(6, 2);
    var minute = timestampStr.substr(8, 2);
    var seconds = timestampStr.substr(10, 2);
    // timestampStr is 8 hours ahead from UTC so need to change in UTC+08(subtracts 8 hours from timestampStr)
    return new Date(year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + seconds + " UTC+08");
}

function createTLVArray(typeLength, tlvData) {

    if (!tlvData || tlvData.length < 4)
        return undefined;

    var tlvArray = [];

    for (var index = 0; index < tlvData.length;) {
        var type = tlvData.substr(index, typeLength);
        var length = utils.hexToInt(tlvData.substr(index + typeLength, 2)) * 2;
        var value = tlvData.substr(index + typeLength + 2, length);
        tlvArray.push({ type: type, length: length, value: value });
        index += typeLength + 2 + length;
    }

    return tlvArray;
}

function calculateChecksum(content) {
    var buffer = new Buffer(content, "hex");
    var checksum = buffer[0];
    for (var i = 1; i < buffer.length; i++) {
        checksum ^= buffer[i];
    }
    return utils.intToHexStr(checksum);
}

function restoreEscapeSequece(packet) {
    var newPacket = "";
    for (var index = 0; index < packet.length; index += 2) {
        if (packet.substr(index, 4) == "7D02") {
            newPacket += "7E";
            index += 2;
        }
        else if (packet.substr(index, 4) == "7D01") {
            newPacket += "7D";
            index += 2;
        }
        else
            newPacket += packet.substr(index, 2);
    }

    return newPacket;
}

exports.getContentLength = getContentLength;
exports.addDefaults = addDefaults;
exports.getGeoJson = getGeoJson;
exports.getTimestamp = getTimestamp;
exports.createTLVArray = createTLVArray;
exports.alertJson = alertJson;
exports.calculateChecksum = calculateChecksum;
exports.restoreEscapeSequece = restoreEscapeSequece;
