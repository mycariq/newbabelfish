const SWITCH_MAIN_SERVER = "SWITCH_MAIN_SERVER";

var Server = require("../../server");
var logger = require("../../logger.js").getLogger("secureNetB20", "receiver.log");
var helper = require("./helper");
var packetParser = new (require("./PacketParser"))();
var utils = require("../../utils");
var configurationLoader = new (require("../../ConfigurationLoader"))("B20", [SWITCH_MAIN_SERVER]);
var port = require("../../config.json").secureNetB20.port;

new Server("secureNetB20", port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet : " + packet);
	processPacket(packet, socket);
});

function processPacket(packet, socket) {

	logger.info("Before Escape: " + packet);
	packet = helper.restoreEscapeSequece(packet);
	logger.info("After Escape: " + packet);

	//validate packet
	if (!isValidPacket(packet)) {
		logger.info("Invalid Packet : " + packet);
		return;
	}

	var packetJson = {};
	packetJson.head = packet.substr(0, 2);
	packetJson.command = packet.substr(2, 4);
	packetJson.messageBodyProp = packet.substr(6, 4);
	packetJson.owner = packet.substr(10, 12);
	packetJson.messageNo = packet.substr(22, 4);
	var contentLength = helper.getContentLength(packetJson.messageBodyProp);
	packetJson.content = packet.substr(26, contentLength);
	packetJson.checksum = packet.substr(packet.length - 4, 2);
	packetJson.tail = packet.substr(packet.length - 2, 2);

	logger.info("Packet JSON: " + JSON.stringify(packetJson));

	packetParser.parse(packetJson.owner, packetJson.command, packetJson.content);

	sendPacketResponse(socket, packetJson);
}

function sendPacketResponse(socket, packetJson) {
	var response = undefined;
	var owner = packetJson.owner;

	switch (packetJson.command) {
		case "0100":
			response = "8100" + "000F" + owner + "0000" + packetJson.messageNo + "00" + utils.textToHex(owner);
			response = "7E" + response + helper.calculateChecksum(response) + "7E";
			sendToDevice(socket, response);

			//query device metadata
			var metadataPacket = "8107" + "0000" + owner + "0000";
			metadataPacket = "7E" + metadataPacket + helper.calculateChecksum(metadataPacket) + "7E";
			sendToDevice(socket, metadataPacket);
			break;

		default:
			response = "8001" + "0005" + owner + "0000" + packetJson.messageNo + packetJson.command + "00";
			response = "7E" + response + helper.calculateChecksum(response) + "7E";
			sendToDevice(socket, response);

	}

	//device feedbacks
	var feedback = configurationLoader.removeFirst(owner);
	
	if (!feedback)
		return;
	switch (feedback.command) {
		case SWITCH_MAIN_SERVER:
			var content = "03";
			//domain
			var domain = utils.textToHex(feedback.payload.babelfishDomain);
			content += "0013" + utils.intToHexStr(domain.length / 2) + domain;
			//port
			content += "0018" + "03" +"0"+ port;
			var messageBodyProp = utils.intToHexStr(content.length / 2);
			if (messageBodyProp.length == 2) messageBodyProp = "00" + messageBodyProp;
			var packet = "8103" + messageBodyProp + owner + "0000" + content;
			packet = "7E" + packet + helper.calculateChecksum(packet) + "7E";
			//console.log(content, messageBodyProp, packet)
			sendToDevice(socket, packet);
			break;

		default:
			console.log("Unknown command")
			break;
	}

}

function sendToDevice(socket, packet) {
	if (!packet)
		return;

	logger.info("Sending response : " + packet);

	if (!socket)
		return;
	socket.write(new Buffer(packet, "hex"));
}

function isValidPacket(packet) {
	if (packet.length < 6)
		return false;

	var head = packet.substr(0, 2);
	var content = packet.substr(2, packet.length - 6);
	var checksum = packet.substr(packet.length - 4, 2);
	var tail = packet.substr(packet.length - 2, 2);

	if (head != "7E" || tail != "7E")
		return false;

	var expectedChecksum = helper.calculateChecksum(content).toUpperCase();
	if (checksum != expectedChecksum) {
		logger.info("Invalid checksum :: expected:" + expectedChecksum + " packet:" + packet);
		return false;
	}

	return true;
}

//login packet
//var loginPacket = "7E010000360340007257760A170000000000000000000000000000000000000000000000000000000000303732353737360031313131313131313131313131313131313D7E";
//var gpsObdPacket = "7E0200004003400072322704CA00000000000C00030124CD3A045956200017000000A5201102123134010400000BF72504000000002A0200002B040000000030011F310110E306006405480000D57E";
//var hearbeatPacket = "7E00020000013184610090000A4D7E";
//var authenticatePacket = "7E0102000C0131846100900003303133313834363130303930487E";
//var accelerationPacket = "7E090000200171941011810021F80008001B02200312101317000000B1011B56920465853400000000000016575E7E";
//var packet = "7E02000040017194101181007700000000000C0000011B5ABC046585FC024F0000000020031110230701040000147D012504000000002A0200002B0400000000300117310107E3060064050600002B7E";
//packet="7E020000EE0151942014470B5000000000000C00030119EA2E0466257D024B000001252002131612180104000017B4020201FF2504000000002A0200002B040000000030010E31010FE3060064057D0000F3A8000202000A000302083100040236EB0005040000163F00060203E700070203E70008010C0009020084000B02000F000C020068000D020134000E0144000F01AD010002000B0101040000163F0102020000010304000001FF0104020043010D0200D2010E020A7C010F020088011002379E011202000001130200000116020000005011000000000000000000000000000000000000510550303137320052040000014D010C02006EA27E";
//var metadataPacket = "7E0107004B034000723359005C00070000000000000000000000000000000000000000000000000030373233333539899127217309731926100456332E311756312E302E31315F5331365F42415F323032303037303103017D027E";
//processPacket(metadataPacket.toUpperCase(), undefined);
