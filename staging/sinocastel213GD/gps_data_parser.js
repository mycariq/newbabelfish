
//var str = "4040590004323133474C323031353031353430330000000000400100CF3F39BAE9E90E81BD080F00C3800000541E000085060C110400076400111F00030001090B0F0603014267FB03349CD40F750000000301AD19F1DF0D0A";

// 4040 5900 04 323133474C323031353031353430330000000000 4001 
// 00 CF3F39BAE9E90E81BD080F00C3800000541E000085060C110400076400111F000300
// 01 
// 09 0B 0F 06 03 01 4267FB03 349CD40F 7500 0000 03
// 01AD19
// F1DF0D0A

//console.log(processLocation(str));

// calculate data
var calculateLocation = function processLocation(packet){

    var response = [] ;

    // data packet
    var dataPacket = getDataString(packet);

    if(dataPacket.length <= 0)
        return response;
    //e.g = 090B0F0603014267FB03349CD40F7500000003

    // set packet type
    response[0] = "GPS";

    // get device id
    response[1] = getDeviceId(packet);

    // get time stamp 
    response[2] = getTimeStamp(dataPacket);

    //  get latitude
    response[3] = getLatitude(dataPacket);

    // get longitude
    response[4] = getLongitude(dataPacket);

    // get speed
    response[5] = getSpeed(dataPacket);

    return response;
}

// get data string 
function getDataString(packet){
    return packet.substr(126, 38);
}

// get deviceId
function getDeviceId(packet){
    var devId =  packet.substr(10,40);
    return devId.match(/.{1,2}/g).map(function(v){
                return v !== '00' ? String.fromCharCode(parseInt(v, 16)) : '';
            }).join('');
}


// get latitude
function getLatitude(packet){
    var latitude = packet.substr(12, 8);
    latitude = hexToInt(reverseHex(latitude));
    return latitude / 3600000;
}

// get longitude
function getLongitude(packet){
    var longitude = packet.substr(20, 8);
    longitude = hexToInt(reverseHex(longitude));
    return longitude / 3600000;
}

// get speed
function getSpeed(packet){
    var speedString = packet.substr(28,4);
    speedString = reverseHex(speedString);
    return hexToInt(speedString) * 0.036;
}

// get timestamp
function getTimeStamp(packet){

    // get date 
    var date = getDate(packet);

    // get time
    var time = getTime(packet);

    return date + "%20"+time;
}

// get date 
function getDate(packet){
    var dateString = packet.substr(0,6);
    return "20"+hexToInt(dateString.substr(4,2))+ "-" 
    +hexToInt(dateString.substr(2,2)) + "-" 
    +hexToInt(dateString.substr(0,2));
}

// get time 
function getTime(packet){
    var timeString = packet.substr(6,6);
    return hexToInt(timeString.substr(0,2)) + ":"
    + hexToInt(timeString.substr(2,2)) + ":"
    + hexToInt(timeString.substr(4,2));
}

// hex to int
function hexToInt(hexStr){
    hexStr = "0x"+hexStr;
    var result = "" + parseInt(hexStr);
    if(result.length < 2)
        return "0"+result;
    return result;    
}   

// reverse hex string
function reverseHex(hexStr){
    var result = [];
    for(var i = 0; i < hexStr.length/2; i++){
        result[i] = hexStr.substring(i*2, (i*2)+2);
    }
    return result.reverse().join("");
}

// exports module
exports.calculateLocation = calculateLocation;
