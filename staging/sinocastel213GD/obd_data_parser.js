var moment = require('moment');
var pidJson = require('./pidDetail.json');
/**
 **  Parse OBD data packet
 **/
//var str = "4040680004323133474C3230313530313534303300000000004002CF3F39BAFCE90E81BD080F0063840000541E0000BC060C110400076400111F00030003000A05210C210D210F2110210421132121212F214221010E7DAE19B119066E1401E80332E02ECAC70D0A";

// 4040 6800 04 323133474C323031353031353430330000000000 
// 4002
// CF3F39BAFCE90E81BD080F0063840000541E0000BC060C110400076400111F000300
// 03000A05210C210D210F2110210421132121212F214221010E7DAE19B119066E1401E80332E02E
// CAC7 0D0A

//console.log(processOBD(str));

// calculate data
var calculateOBDdata = function processOBD(packet){
    
    var response = [] ;

    // data packet
    var dataPacket = getDataString(packet);

    if(dataPacket.length <= 0)
        return response;
    //e.g = 03000A05210C210D210F2110210421132121212F214221010E7DAE19B119066E1401E80332E02E

    // set packet type
    response[0] = "OBD";

    // get device id
    response[1] = getDeviceId(packet);

    // get time stamp 
    response[2] = getTimeStamp(packet);
    
    // get number of pid
    var noOfPid = getNumberOfPid(dataPacket);
    
    // get pid string from data packet
    var obdDataPid = getObdDataPid(dataPacket, noOfPid);
        
    // get lenght of obd data    
    var lengthOfOBDData = getOBDdataLength(dataPacket, obdDataPid.length);
    
    // get obd data string
    var obdData = getOBDData(dataPacket, obdDataPid.length, lengthOfOBDData);
    
    response[3] = parsedData(obdDataPid, obdData);

    return response;
}

// get pid array
function parsedData(obdPid, obdData){
    var dataArray = [];
    var position = 0, startPosition = 0;
    while(position < obdPid.length){
        var pid = obdPid.substr(position, 4);
        dataArray[pidJson[pid][0]] = evaluate(obdData.substr(startPosition, pidJson[pid][1] * 2), pidJson[pid][2]);
        position += 4;
        startPosition += (pidJson[pid][1] * 2);
    }
    return dataArray;
}

// function calculate value
function evaluate(value, formula){
    var expression = formula.replace("A", hexToInt(value));
    return eval(expression); 
}

// get obd data
function getOBDData(packet, lengthObdPid, noOfBytes){
    return reverseHex(packet.substr(lengthObdPid+10, noOfBytes));
}

// get obd data length
function getOBDdataLength(packet, lengthObdPid){
    return hexToInt(packet.substr(lengthObdPid + 8,2)) * 2;
}

// get obd data pids
function getObdDataPid(packet, noOfPid){
    return reverseHex(packet.substr(6, noOfPid * 4));
}

// get number of pid
function getNumberOfPid(packet){
    return hexToInt(packet.substr(4,2));
}

// get data string 
function getDataString(packet){
    return packet.substring(122, packet.length-8);
}

// get deviceId
function getDeviceId(packet){
    var devId =  packet.substr(10,40);
    return devId.match(/.{1,2}/g).map(function(v){
                return v !== '00' ? String.fromCharCode(parseInt(v, 16)) : '';
            }).join('');
}

// get timestamp
function getTimeStamp(packet){
    // get timetsmp string
	var timeStampString = packet.substr(62,8);
    
    // reverse hex
	timeStampString = reverseHex(timeStampString);

    // get epoch
    var packetTime = hexToInt(timeStampString) * 1000;
        
    return moment.utc(new Date(packetTime)).format("YYYY-MM-DD HH:mm:ss").replace(/ /g, '%20');
}

// hex to int
function hexToInt(hexStr){
    hexStr = "0x"+hexStr;
    var result = "" + parseInt(hexStr);
    if(result.length < 2)
        return "0"+result;
    return result;    
}   

// reverse hex string
function reverseHex(hexStr){
    var result = [];
    for(var i = 0; i < hexStr.length/2; i++){
        result[i] = hexStr.substring(i*2, (i*2)+2);
    }
    return result.reverse().join("");
}

// exports module
exports.calculateOBDdata = calculateOBDdata;
