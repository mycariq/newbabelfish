const
	request = require('request');
const
	locParser = require('./gps_data_parser');
const
	obdParser = require('./obd_data_parser');
// prod base url
const
	DAS_BASEURL = "http://gearbox.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/";

var config = require("../config")["sinocastel213GD"];

var port = config["port"];

var logger = require("../logger.js");
var mylogger = logger.getLogger("sinocastel213GD", "parser.log");

// parsed data
function processPacket(packetData) {
	try {

		mylogger.info("(processPacket)-Reading packet =" + packetData);

		// get parsed data
		var parsedData = getParsedData(packetData);

		if (parsedData.length <= 0)
			return;

		// create url
		var Url = createUrl(parsedData);

		// send packet to server
		sendPacket(Url);
		mylogger.info("(processPacket)-Send to DAS = " + Url);

	} catch (err) {
		mylogger.error("(processPacket)-readPacket has error for packet = " + packetData);
		mylogger.error("(processPacket)-" + err.stack);
	}
}

// get parsed data
function getParsedData(packet) {
	var commandType = packet.substr(50, 4);

	if (commandType === "4004")
		return supportedPid.calculateSupportedPid(packet);
	else if (commandType === "4001")
		return locParser.calculateLocation(packet);
	else if (commandType === "4002")
		return obdParser.calculateOBDdata(packet);
	return "";
}

// function to send packet to DAS
function sendPacket(URL) {
	mylogger.info("(sendPacket)-Sending to " + URL);
	request.shouldKeepAlive = false;

	request(URL, function (error, response, body) {
		if (!error && response.statusCode == 200)
			mylogger.info("(sendPacket)-" + body);

		if (error)
			mylogger.error('(sendPacket)-InValid Request to DAS');

	});
}

// function to create URL
function createUrl(parsedData) {
	if (parsedData[0] === "GPS")
		return formGPSPacket(parsedData);
	else if (parsedData[0] === "OBD")
		return formOBDPacket(parsedData);
}

// function to formed OBD data
function formOBDPacket(parsedData) {
	var dataString = "";
	for (var index in parsedData[3]) {
		dataString += index + "=" + parsedData[3][index] + ",";
	}
	return DAS_BASEURL + parsedData[1] + "/" + parsedData[2] + "/"
		+ dataString.substr(0, dataString.length - 1);
}

// function to get formed GPS data
function formGPSPacket(parsedData) {
	return DAS_BASEURL + parsedData[1] + "/" + parsedData[2] + "/speed="
		+ parsedData[5] + ",Cariq_Latitude=" + parsedData[3]
		+ ",Cariq_Longitude=" + parsedData[4];
}
