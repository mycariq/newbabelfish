//import server module
var Server = require("../server.js");

var utils = require("../utils.js");

var crc = require("./crc.js");

var config = require("../config")["sinocastel213GD"];

var port = config["port"];

var SERVER_IP = "FFFFFFFF", SERVER_PORT = "0000";

var logger = require("../logger.js");
var mylogger = logger.getLogger("sinocastel213GD", "receiver.log");


new Server("Sino213GD", port).start(function(err, data) {
	var packet = new Buffer(data, "utf8").toString("hex").toUpperCase();

	// if has error then handles
	mylogger.info("(Server)-received=" + packet);
	if (err)
		mylogger.error("(Server)-" + err.stack);
	else {
		if (isValidPacket(packet)) {
			var command = packet.substr(50, 4);
			mylogger.info("(Server)-command=" + command);
			// if queueable then write into queue
			if (isQueueable(command)) {
				mylogger.info("(Server)-Packet is queueable so writing into queue");
			} else
				mylogger.info("(Server)-Packet is not queueable");
			// get response for packet and return it
			var response = getPacketResponse(command, packet);
			return response;
		}
	}
});

// checks whether the packet is valid or not
function isValidPacket(packet) {
	var header = packet.substr(0, 4);
	if (header === "4040")
		return true;
	return false;
}

// gets response for packet as per command
function getPacketResponse(command, packet) {
	var response = [];
	if (command === "1001") {
		response["message"] = new Buffer(getLoginResponse(packet), "hex");
		response["socket_action"] = "HOLD";
	}
	if (command === "1003") {
		response["message"] = new Buffer(getHeartBeatResponse(packet), "hex");
		response["socket_action"] = "HOLD";
	}
	return response;
}

// checks whether packet is queueable or not
function isQueueable(command) {
	if (command === "1001" || command === "1003")
		return false;
	return true;
}

// gets login response
function getLoginResponse(packet) {
	var response = "40402900" + getProtocol(packet) + getDeviceId(packet)
			+ "9001" + SERVER_IP + SERVER_PORT + getUTC();
	return response + crc.calculateChecksum(response) + "0d0a";
}

// gets heart beat response
function getHeartBeatResponse(packet) {
	var response = "40401f00" + getProtocol(packet) + getDeviceId(packet)
			+ "9003";
	return response + crc.calculateChecksum(response) + "0d0a";
}

// gets protocol
function getProtocol(packet) {
	return packet.substr(8, 2);
}

// gets deviceId in hex
function getDeviceId(packet) {
	return packet.substr(10, 40);
}

// gets utc timestamp in hex format
function getUTC() {
	var time = Math.floor(new Date().getTime() / 1000);
	return utils.reverseHex(time.toString(16));
}
