var helper = require("./helper");
var utils = require("../utils");
var alertJson = require("./alert.json");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(data) {
        var index = 0;
        var document = { "type": "alert" };
        document.serialNo = utils.revHexToInt(data.substr(index, 8));
        var statData = data.substr(index += 8, 68);
        var gpsCount = utils.hexToInt(data.substr(index += 68, 2));
        utils.extendJson(document, helper.parseGpsItem(data.substr(index += 2, gpsCount * 38)));
        var noOfAlert = utils.hexToInt(data.substr(index += gpsCount * 38, 2));
        var alertData = data.substr(index += 2, noOfAlert * 12);
        document.alertCount = noOfAlert;

        for (var i = 0; i < noOfAlert; i++) {
            var startIndex = i * 12;
            document.mark = utils.hexToInt(alertData.substr(startIndex, 2));
            if (document.mark != 1)
                return undefined;
            document.subtype = alertJson[alertData.substr(startIndex += 2, 2)];
            document.value = utils.revHexToInt(alertData.substr(startIndex += 2, 4));
            document.threshold = alertData.substr(startIndex += 4, 4);
            return document;
        }
        return undefined;
    }
}

module.exports = AlertParser;