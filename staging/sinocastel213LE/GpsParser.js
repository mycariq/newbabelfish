var helper = require("./helper");
var utils = require("../utils");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(data) {
        var index = 0;
        var document = { type: "data", subtype: "gps" };
        document.isLive = (data.substr(index, 2) == "00") ? true : false;
        var stateData = data.substr(index += 2, 68);
        var gpsCount = utils.hexToInt(data.substr(index += 68, 2));
        utils.extendJson(document, helper.parseGpsItem(data.substr(index += 2, 38)));

        return document;
    }
}

module.exports = GpsParser;