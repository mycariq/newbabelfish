'use strict'
class HeartBeatParser {
    constructor() {

    }

    parse(data) {
        return { timestamp: new Date(), type: "data", subtype: "heartbeat" };
    }
}

module.exports = HeartBeatParser;