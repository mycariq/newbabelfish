var helper = require("./helper");
var utils = require("../utils");

'use strict'
class LoginParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "gps" };
        var index = 0;
        var statData = data.substr(0, 68);
        var gpsCount = utils.hexToInt(data.substr(index += 68, 2));
        var gpsData = data.substr(index += 2, 38 * gpsCount);
        utils.extendJson(document, helper.parseGpsItem(gpsData));
        return document;
    }
}

module.exports = LoginParser;