var helper = require("./helper");
var utils = require("../utils");
var pidDetails = require("./pidDetail");

'use strict'
class ObdParser {
    constructor() {

    }

    parse(data) {
        var index = 0;
        var document = { type: "data", subtype: "obd" };
        var statData = data.substr(index, 68);
        document.timestamp = helper.parseHexEpochToDate(statData.substr(8, 8));
        document.interval = utils.revHexToInt(data.substr(index += 68, 4));
        var noOfPid = utils.hexToInt(data.substr(index += 4, 2));
        var pids = data.substr(index += 2, 4 * noOfPid).match(/.{1,4}/g).map(function (v) {
            return "41" + utils.reverseHex(v).substr(2, 2);
        });
        var noOfGroups = utils.hexToInt(data.substr(index += 4 * noOfPid, 2));
        var lenOfGoups = utils.hexToInt(data.substr(index += 2, 2));
        var pidData = data.substr(index += 2, noOfGroups * lenOfGoups * 2);

        var pidDataIndex = 0;
        pids.forEach(function (pid) {
            utils.parsePid(document, pid, utils.reverseHex(pidData.substr(pidDataIndex, 2 * pidDetails[pid])));
            pidDataIndex += 2 * pidDetails[pid];
        });

        return document;
    }
}

module.exports = ObdParser;