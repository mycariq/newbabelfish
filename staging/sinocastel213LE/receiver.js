var Server = require("../server");
var utils = require("../utils");
var helper = require("./helper");
var parser = new (require("./PacketParser"))();
var configLoader = require("../ConfigurationLoader");
var configCmdTLV = { "WIFI_NAME": "0234", "WIFI_SWITCH": "0134", "WIFI_ENCRYPTION_WPA2_PSK": "0434", "CLEAN_FLASH": "3003" };

var SERVER_IP = "FFFFFFFF", SERVER_PORT = "0000";
var logger = require("../logger.js").getLogger("sinocastel213LE", "receiver.log");

new Server("sinocastel213LE", require("../config.json").sinocastel213LE.port).start(function (err,
	data, id, socket) {
	var rawPacket = data.toString("hex").toUpperCase();
	logger.info("Received raw packet " + rawPacket);

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function	
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	processPacket(socket, rawPacket);

});
/**
fetches command from packet.
if this command need response then it prepares response and writes into socket
*/
function processPacket(socket, rawPacket) {
	//parse packet into base format
	var packetJson = {};
	packetJson.head = rawPacket.substr(0, 4);
	packetJson.protocolLength = utils.revHexToInt(rawPacket.substr(4, 4));
	packetJson.protocolVersion = utils.hexToInt(rawPacket.substr(8, 2));
	packetJson.deviceId = utils.hexToText(rawPacket.substr(10, 40));
	packetJson.rawDeviceId = rawPacket.substr(10, 40);
	packetJson.command = rawPacket.substr(50, 4);
	packetJson.data = rawPacket.substring(54, rawPacket.length - 8);
	packetJson.crc = rawPacket.substr(rawPacket.length - 8, 4);
	packetJson.tail = rawPacket.substr(rawPacket.length - 4);

	//validate packet by head and trail
	if (!isValid(packetJson)) {
		logger.info("Invalid packet " + rawPacket);
		return;
	}

	//pass packet for parsing
	parser.parse(packetJson.deviceId, packetJson.command, packetJson.data);

	//send response if required
	var response = prepareResponse(packetJson);
	if (response && socket) {
		logger.info("Sending response," + response);
		socket.write(new Buffer(response, "hex"));
	}
}

function isValid(packetJson) {
	return (packetJson.head == "4040" && packetJson.tail == "0D0A");
}

function prepareResponse(packetJson) {
	switch (packetJson.command) {
		case "1001":
			return getLoginResponse(packetJson);
		case "1003":
			return getHeartBeatResponse(packetJson);
		default:
			return getConfigResponse(packetJson);
	}
}

// gets login response
function getLoginResponse(packetJson) {
	var response = "40402900" + utils.intToHexStr(packetJson.protocolVersion) + packetJson.rawDeviceId
		+ "9001" + SERVER_IP + SERVER_PORT + helper.getCurrentSecEpochHex();
	return response + helper.calculateChecksum(response) + "0d0a";
}

// gets heart beat response
function getHeartBeatResponse(packetJson) {
	var response = "40401f00" + utils.intToHexStr(packetJson.protocolVersion) + packetJson.rawDeviceId
		+ "9003";
	return response + helper.calculateChecksum(response) + "0d0a";
}

function getConfigResponse(packetJson) {
	//check whether config is present or not
	var tlvArray = [];
	var cleanFlashResponse;

	Object.keys(configCmdTLV).forEach(function (command) {
		var configJson = configLoader.get(packetJson.deviceId, command);
		if (!configJson) return;
		if (command == "CLEAN_FLASH") {
			var response = "4040" + "2100" + utils.intToHexStr(packetJson.protocolVersion) + packetJson.rawDeviceId + "3003" + "0200";
			cleanFlashResponse = response + helper.calculateChecksum(response) + "0d0a";
		}
		tlvArray.push(getTLV(command, configJson));
		configLoader.remove(packetJson.deviceId, command);
	});

	if (cleanFlashResponse)
		return cleanFlashResponse;

	if (tlvArray.length == 0) return;
	var tlv = tlvArray.join('');
	var packetLen = utils.intToHexStr((tlv.length / 2) + 34/*fixed length*/);
	packetLen = utils.reverseHex((packetLen.length == 2) ? "00" + packetLen : packetLen);

	var tlvCount = utils.intToHexStr(tlvArray.length);


	var response = "4040" + packetLen + utils.intToHexStr(packetJson.protocolVersion) + packetJson.rawDeviceId
		+ "2001" + "0100" + tlvCount + tlv;
	return response + helper.calculateChecksum(response) + "0d0a";
}

function getTLV(command, configJson) {
	var value = undefined;
	switch (command) {
		case "WIFI_NAME":
			value = utils.textToHex(configJson.SSID);
			break;
		case "WIFI_SWITCH":
			if (configJson.WIFI_ONOFF == "1")
				value = "01";
			if (configJson.WIFI_ONOFF == "0")
				value = "00";
			break;
		case "WIFI_ENCRYPTION_WPA2_PSK":
			value = utils.textToHex(configJson.PWD);
			break;
		default:
			value = undefined;
	}
	if (!value) return;

	var len = utils.intToHexStr(value.length / 2);
	len = utils.reverseHex((len.length == 2) ? "00" + len : len);

	return configCmdTLV[command] + len + value;
}

// var login = "40407F000431303031313132353239393837000000000000001001C1F06952FDF069529C91110000000000698300000C0000000000036401014C00030001190A0D04121A1480D60488C5721800000000AF4944445F3231364730325F532056312E322E31004944445F3231364730325F482056312E322E31000000DF640D0A";
// var heartbeat = "40401F00043130303131313235323939383700000000000000100303320D0A";
// var gps = "40405900033231334C3230313730303032343300000000000040010166F4A05A84F4A05A00000000000000000000000000000002040000231C7C1E00800401040B11011C0EE08CD60414AC7218000000000301000008000D0A";
// var obd = "40405600033231334C32303137303030323433000000000000400278A6AB5AE9A6AB5AEA140000000000003C0000000000000204000339197C1E0083060A000505210B210C210F210D2101063C1FB8032700309B0D0A";
// var alert = "40406000043130303131313235323939383700000000000000400705000000C1F0695249F469529C9111000000000069830000D80040000400036401014C04030001190A0D04201E1480D60488C5721800000000AF0101060F000F00EA1E0D0A";

// processPacket(undefined, alert);
