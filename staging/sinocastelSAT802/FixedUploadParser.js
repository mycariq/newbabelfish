var helper = require("./helper");
var utils = require("../utils");

'use strict'
class FixedUploadParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "gps" };
        utils.extendJson(document, helper.parseVehicleInfo(data));
        return document;
    }
}

module.exports = FixedUploadParser;