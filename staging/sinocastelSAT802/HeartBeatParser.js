var helper = require("./helper");
var utils = require("../utils");

'use strict'
class HeartBeatParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "heartbeat" };
        document.timestamp = new Date()
        return document;
    }
}

module.exports = HeartBeatParser;