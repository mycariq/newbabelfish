var helper = require("./helper");
var utils = require("../utils");

'use strict'
class PowerFailureAlarmParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "alert", subtype: "powerFailure" };
        utils.extendJson(document, helper.parseVehicleInfo(data));
        return document;
    }
}

module.exports = PowerFailureAlarmParser;