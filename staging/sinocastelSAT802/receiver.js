var Server = require("../server");
var utils = require("../utils");
var helper = require("./helper");
var packetParser = new (require("./PacketParser"))();
var logger = require("../logger.js").getLogger("sinocastelSAT802", "receiver.log");

new Server("sinocastelSAT802", require("../config.json").sinocastelSAT802.port).start(function (err,
	data, id, socket) {
	var rawPacket = data.toString("hex").toUpperCase();
	logger.info("Received raw packet " + rawPacket);

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	//validate packet
	if (!isValid(rawPacket)) {
		logger.info("Invalid packet " + rawPacket);
		return;
	}

	return processPacket(id, socket, rawPacket);
});

function processPacket(id, socket, rawPacket) {
	//parse packet in protocol format
	var packetJson = {}
	packetJson.head = rawPacket.substr(0, 4);
	packetJson.packetLength = rawPacket.substr(4, 4);
	packetJson.command = rawPacket.substr(8, 2);
	if (packetJson.command == "01") {
		var bcdValues = helper.parseBCD(rawPacket.substring(10, rawPacket.length - 42));
		packetJson.deviceId = bcdValues[0];
		packetJson.data = rawPacket.substring(rawPacket.length - 42, rawPacket.length - 2)
	}
	else {
		packetJson.data = rawPacket.substring(10, rawPacket.length - 2);
		packetJson.deviceId = id;
	}
	packetJson.checksum = rawPacket.substr(rawPacket.length - 2);
	//pass packet for parsing
	packetParser.parse(packetJson.deviceId, packetJson.command, packetJson.data);

	//send response if required
	var response = prepareResponse(packetJson);
	if (response && socket) {
		logger.info("Sending response," + response);
		socket.write(new Buffer(response, "hex"));
	}

	//logger.info("Raw packet json," + JSON.stringify(packetJson));
	return { id: packetJson.deviceId };;
}

function isValid(rawPacket) {
	if (rawPacket.substr(0, 4) != "2424")
		return false;
	return true;
}

function prepareResponse(packetJson) {
	switch (packetJson.command) {
		case "01":
			return getLoginResponse(packetJson);
		default: undefined;
	}
	return undefined;
}

function getLoginResponse(packetJson) {
	return "40400700010006";
}

//var login = "2424200001123456FF00FF586DBB53C9DC0ECF1A4B0000350041000E00000073";
var login = "24242300018031173500050F00FF40EE0200E6DC0EBC1A4B0000140000000000000025";
var heartBeat = "242406000402";
var powerFailure = "24241A000872650800E6DC0EBC1A4B00001400000000000000C0";
var fixedUpload = "24241A0003736B0900E6DC0EBC1A4B00001400000000000000C5";

//processPacket("8031173500050", undefined, heartBeat);