var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "alert", subtype: "overSpeed" };
        // document.timestamp = helper.parseTimestamp(data.substr(0, 12));
        // document.satellite = data.substr(12, 2);
        // var latitude = (utils.hexToInt(data.substr(14, 8)) / 30000) / 60;
        // var longitude = (utils.hexToInt(data.substr(22, 8)) / 30000) / 60;
        // document.location = utils.getGeoJson(latitude, longitude, true);
        // document.speed = utils.hexToInt(data.substr(30, 2));
        // document.course = data.substr(32, 2);
        // document.status = data.substr(34, 2);
        document.lbsLength = data.substr(36, 2);
        document.mcc = data.substr(38, 4);
        document.mnc = data.substr(42, 2);
        document.lac = data.substr(44, 4);
        document.cellId = data.substr(48, 6);
        document.terminalInfoCenter ="44";// data.substr(54, 2);
        // var binAlert = utils.hexToBin(document.terminalInfoCenter);
        // console.log(binAlert);
        document.voltageLevel = data.substr(56, 2);
        document.gsmSignalStrength = data.substr(58, 2);
        document.alarmLang = data.substr(60, 4);
        return document;
    }
}

module.exports = AlertParser;