var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "gps" };
        document.timestamp = helper.parseTimestamp(data.substr(0, 12));
        document.satellite = data.substr(12, 2);
        var latitude = (utils.hexToInt(data.substr(14, 8)) / 30000) / 60;
        var longitude = (utils.hexToInt(data.substr(22, 8)) / 30000) / 60;
        document.location = utils.getGeoJson(latitude, longitude, true);
        document.speed = utils.hexToInt(data.substr(30, 2));
        document.course = data.substr(32, 2);
        document.status = data.substr(34, 2);
        return document;
    }
}

module.exports = GpsParser;