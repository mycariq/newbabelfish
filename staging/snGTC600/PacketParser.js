var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = console;

var parsers = {
    "12": new (require("./GpsParser"))(),
    "16": new (require("./AlertParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(deviceId, command, data) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for " + command);
            return;
        }

        //parse data
        var document = parser.parse(data);
        if (!document)
            return;

        //add owner and defaults
        document = utils.extendJson({ owner: deviceId }, document);
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;