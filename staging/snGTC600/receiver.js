var Server = require("../../server");
var utils = require("../../utils");
var parser = new (require("./PacketParser"))();
var helper = require("./helper");
var logger = console;

new Server("snGTC600", require("../../config.json").snGTC600.port).start(function (err,
	data, id, socket) {
	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet : " + packet);

	processPacket(socket, socket["deviceId"], packet);
});

function processPacket(socket, deviceId, packet) {
	var packetJson = {};
	packetJson.head = packet.substr(0, 4);
	packetJson.contentLength = utils.hexToInt(packet.substr(4, 2));
	packetJson.command = packet.substr(6, 2);
	packetJson.content = packet.substr(8, (packetJson.contentLength - 5) * 2);
	packetJson.serialNo = utils.hexToInt(packet.substr(packet.length - 12, 4));
	packetJson.rawSerialNo = packet.substr(packet.length - 12, 4);
	packetJson.checkSum = packet.substr(packet.length - 8, 4);
	packetJson.crc = helper.calcChecksum(packet.substr(4, packet.length - 12));
	packetJson.tail = packet.substr(packet.length - 4, 4);
	logger.info("Raw packet json " + JSON.stringify(packetJson));

	//parse packet
	parser.parse(deviceId, packetJson.command, packetJson.content);

	//return response
	switch (packetJson.command) {
		case "01":
			socket["deviceId"] = packetJson.content.substr(1);
			sendToDevice(socket, createLoginResponse(packetJson.rawSerialNo));
			break;
		case "13":
			sendToDevice(socket, createHearbeatResponse(packetJson.rawSerialNo));
			break;
		default:
			break;
	}
}

function sendToDevice(socket, packet) {
	if (socket) {
		logger.info("Sending to device : " + packet);
		socket.write(new Buffer(packet, "hex"));
	}
}

function createLoginResponse(serialNo) {
	var packet = "";
	packet += "05";//length
	packet += "01";//command
	packet += serialNo; //serial number
	return "7878" + packet + helper.calcChecksum(packet) + "0D0A";
}

function createHearbeatResponse(serialNo) {
	var packet = "";
	packet += "05";//length
	packet += "13";//command
	packet += serialNo; //serial number
	return "7878" + packet + helper.calcChecksum(packet) + "0D0A";
}


// var login = "78780D010861359033877166001EDE820D0A";
// var gps = "78781F1212041E0E0133CE01FE0A4007E9F3900014BD01945A0C27006BAF006D4DB10D0A";
//var alert = "787825160B0B0F0E241DCF027AC8870C4657E60014020901CC00287D001F726506040101003656A40D0A";
//processPacket(undefined, "861359033877166", alert);
