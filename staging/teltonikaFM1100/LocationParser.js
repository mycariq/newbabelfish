var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LocationParser {
    constructor() {
    }

    parse(data) {
        var documents = [];
        // removing code id and no of data from data packet
        var codeId = data.substr(0, 2);
        var avlDataCount = data.substr(2, 2);
        var avlData = data.slice(4, data.length - 2);

        var avlArray = helper.createAVLArray(avlData);
        avlArray.forEach(avl => {
            var document = { type: "data", subtype: "gps" };

            document.timestamp = new Date(utils.hexToSInt(avl.substr(0, 16)));
            document.priority = utils.hexToInt(avl.substr(16, 2));
            var gpsElements = avl.substr(18, 30);
            var ioElements = avl.substr(48);
         
            document = gpsElement(document, gpsElements);
            // get voltage
            document.voltage = getVoltage(ioElements);
            // get iccid
            document.iccid = getICCID(ioElements);

            var alertDocument = ioElement(document, ioElements);
            documents.push(document);
            if (alertDocument)
                documents.push(alertDocument);
        });

        return documents;
    }
}

function gpsElement(document, info) {
    var long = info.substr(0, 8);
    var lat = info.substr(8, 8);
    document.location = helper.getGeoJson(lat, long);
    document.altitude = info.substr(16, 4);
    document.angle = info.substr(20, 4);
    document.satellites = utils.hexToInt(info.substr(24, 2));
    document.speed = utils.hexToInt(info.substr(26));

    return document;
}

function getVoltage(info) {
    // event id for voltage 66 default
    var eventId = 66;
    var oneByteData = utils.hexToInt(info.substr(4, 2)) * 4;
    var twoByteData = utils.hexToInt(info.substr(6 + oneByteData, 2)) * 6;

    var data = info.substr(6 + oneByteData, twoByteData + 2);
    return getEventValue(data, eventId, 6, 4) / 1000;
}

function getICCID(info) {
    // event id for iccid 11 & 14 default
    var firstEventId = 11, secondEventId = 14;

    var oneByteData = utils.hexToInt(info.substr(4, 2)) * 4;
    var twoByteData = utils.hexToInt(info.substr(6 + oneByteData, 2)) * 6;
    var fourByteData = utils.hexToInt(info.substr(8 + oneByteData + twoByteData, 2)) * 10;
    var eightByteData = utils.hexToInt(info.substr(10 + oneByteData + twoByteData + fourByteData, 2)) * 18;

    // collecting icc ids
    var data = info.substr(10 + oneByteData + twoByteData + fourByteData, twoByteData + fourByteData + eightByteData + 2);
    var firstIccid = getEventValue(data, firstEventId, 18, 16);
    var secondIccid = getEventValue(data, secondEventId, 18, 16);
    if(!firstIccid || !secondIccid)
        return;

    // merging two iccids into correct iccid format
    var addZeros = 0;
    if(secondIccid.length < 10)
        addZeros = 10 - secondIccid.length;
    
    // add zero before second icc id
    var i = 0;
    while (i < addZeros) {
        secondIccid = 0 + "" + secondIccid;
        i++;
    }
    
    // returning concatinated iccid
    return firstIccid + "" + secondIccid;
}

function ioElement(document, info) {
    if (!info || info.length < 0)
        return;

    var priority = document.priority;
    if (priority > 0)
        return parseAlert(document.timestamp, document.location, info);
}

function parseAlert(timestamp, location, info) {
    var eventId = utils.hexToInt(info.substr(0, 2));

    var oneByteData = utils.hexToInt(info.substr(4, 2)) * 4;
    var twoByteData = utils.hexToInt(info.substr(6 + oneByteData, 2)) * 6;
    var fourByteData = utils.hexToInt(info.substr(8 + oneByteData + twoByteData, 2)) * 10;

    var alertName = helper.alertJson[eventId];
    if (!alertName)
        return;

    // TODO: currently only for 1 bytes alert supported
    var data = info.substr(4, oneByteData + 2);
    var subType = getAlertDesc(alertName, getEventValue(data, eventId, 4, 2));
    if (!subType)
        return;

    // for 4 byte
    // var data = info.substr((8 + oneByteData + twoByteData), fourByteData + 2);
    // document.alertValue = getEventValue(data, eventId, 10, 8);

    var document = { type: "alert", timestamp: timestamp, location: location, subtype: subType };
    return document;
}

function getEventValue(data, event, byteSize, valueSize) {
    data = data.substr(2);

    var index = 0;
    var val;
    for (; index < data.length;) {
        if (event != utils.hexToInt(data.substr(index, 2))) {
            index += byteSize;
            continue;
        } else {
            val = utils.hexToInt(data.substr(index + 2, valueSize));
            break;
        }
    }
    return val;
}

function getAlertDesc(alertName, value) {
    var alertDesc = {
        /*"digitalInput2": {
            "0": "digitalOutput1Off",
            "1": "digitalOutput1On"
        },
        "digitalOutput1": {
            "0": "digitalOutput1Off",
            "1": "digitalOutput1On"
        },*/
        "digitalOutput2": {
            "0": "mobilize",
            "1": "immobilize"
        },
        /*"sleepMode": {
            "0": "noSleepMode",
            "1": "gpsSleepMode",
            "2": "deepSleepMode",
            "3": "onlineSleepMode"
        },*/
        "ignition": {
            "0": "ignitionOff",
            "1": "ignitionOn"
        },
        /*"movement": {
            "0": "movementOff",
            "1": "movementOn"
        },*/
        "towing": {
            //"0": "steady",
            "1": "towing"
        },
        "crashDetection": {
            //"0": "noCrashDetection",
            "1": "collision"
        },
        "pullOutReminder": {
            "0": "plugInReminder",
            "1": "pullOutReminder"
        },
        "greenDrivingType": {
            "1": "suddenSpeededUp",
            "2": "rapidDeceleration",
            "3": "sharpTurn"
        }
    }

    var valueJson = alertDesc[alertName];
    return valueJson[value];
}

module.exports = LocationParser;