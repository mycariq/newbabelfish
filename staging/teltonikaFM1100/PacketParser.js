var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("teltonikaFM1100", "receiver.log");

var parsers = {
    "8": new (require("./LocationParser"))(),
};

'use strict'
class PacketParser {
    constructor() {
    }

    parse(command, owner, data) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  data:" + data);
            return;
        }

        //parse data
        var document = parser.parse(data);
        if (!document)
            return;

        if (!(document instanceof Array))
            document = [document];

        document.forEach(docs => {
            docs.owner = owner;
            helper.addDefaults(docs);

            //persist document
            persist(docs);
        });
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketParser;