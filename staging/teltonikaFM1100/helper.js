var utils = require("../../utils");
var origin = require("os").hostname();

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-Teltonika-FM1100";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;

}

var alertJson = {
    //"2": "digitalInput2",
    //"24": "overSpeed",
    "66": "voltage",
    //"67": "lowBatteryVoltage",
    //"179": "digitalOutput1",
    "180": "digitalOutput2",
    //"200": "sleepMode",
    "239": "ignition",
    //"240": "movement",
    "246": "towing",
    "247": "crashDetection",
    //"248": "immobilizer",
    "252": "pullOutReminder",
    "253": "greenDrivingType",
    // "72": "temperature",
    // "11": "ICCID 1st part",
    // "14": "ICCID 2nd part",
}

// make crc
function calculateChecksum(str) {
    var crc = 0; var offset = 0;

    var buf = new Buffer.from(str, "hex");
    var length = buf.length;

    for (var i = 0; i < length; i++) {
        var data = buf[(i + offset) % length] & 0xFF;
        crc ^= data;

        for (var j = 0; j < 8; j++) {
            if ((crc & 0x0001) != 0)
                crc = (crc >> 1) ^ 0xA001;
            else
                crc = crc >> 1;
        }
    }

    crc = (crc & 0xFFFF).toString(16);
    if (crc.length % 2 == 1)
        crc = "0" + crc;

    // adding first 2 byte zero's 
    return "0000" + crc;
}

function getGeoJson(latHex, longHex) {
    var latitude = utils.hexToInt(latHex) / 10000000;
    var longitude = utils.hexToInt(longHex) / 10000000;
    return utils.getGeoJson(latitude, longitude, true);
}

function createAVLArray(avlData) {
    if (!avlData || avlData.length < 0)
        return undefined;

    var avlArray = [];
    var index = 0;
    for (; index < avlData.length;) {
        var saltedData = "";
       
        saltedData += avlData.substr(index, 8 * 2); // timestamp
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // priority
        saltedData += avlData.substr((index + saltedData.length), 15 * 2); // gps element
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // io element id
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // io element total count

        var oneByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 1 byte
        saltedData += oneByteIOElement;
        // now add io element 1 bytes(id = 1 byte, value = 1 bytes)  
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(oneByteIOElement) * 4);

        var twoByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 2 byte
        saltedData += twoByteIOElement;
        // now add io element 2 bytes(id = 1 byte, value = 2 bytes) 
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(twoByteIOElement) * 6);

        var fourByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 4 byte
        saltedData += fourByteIOElement;
        // now add io element 4 bytes(id = 1 byte, value = 4 byes) 
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(fourByteIOElement) * 10);

        var eightByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 8 byte
        saltedData += eightByteIOElement;
        // now add io element 8 bytes(id = 1 byte, value = 8 bytes)
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(eightByteIOElement) * 18);

        // adding to array list
        avlArray.push(saltedData);

        // adding current index for next packet from this index
        index += saltedData.length;
        continue;
    }

    return avlArray;
}

// latest new avl array parsing for new packet byte changes happened
function createAVLArrayV2(avlData) {
    if (!avlData || avlData.length < 0)
        return undefined;

    var avlArray = [];
    var index = 0;
    for (; index < avlData.length;) {
        var saltedData = "";
       
        saltedData += avlData.substr(index, 8 * 2); // timestamp
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // priority
        saltedData += avlData.substr((index + saltedData.length), 15 * 2); // gps element
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // io element id
        saltedData += avlData.substr((index + saltedData.length), 1 * 2); // io element total count

        var oneByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 1 byte
        saltedData += oneByteIOElement;
        // now add io element 1 bytes(id = 1 byte, value = 1 bytes)  
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(oneByteIOElement) * 4);

        var twoByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 2 byte
        saltedData += twoByteIOElement;
        // now add io element 2 bytes(id = 1 byte, value = 2 bytes) 
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(twoByteIOElement) * 6);

        var fourByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 4 byte
        saltedData += fourByteIOElement;
        // now add io element 4 bytes(id = 1 byte, value = 4 byes) 
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(fourByteIOElement) * 10);

        var eightByteIOElement = avlData.substr((index + saltedData.length), 1 * 2); // io elements count 8 byte
        saltedData += eightByteIOElement;
        // now add io element 8 bytes(id = 1 byte, value = 8 bytes)
        saltedData += avlData.substr((index + saltedData.length), utils.hexToInt(eightByteIOElement) * 18);

        // adding to array list
        avlArray.push(saltedData);

        // adding current index for next packet from this index
        index += saltedData.length;
        continue;
    }

    return avlArray;
}

exports.addDefaults = addDefaults;
exports.alertJson = alertJson;
exports.calculateChecksum = calculateChecksum;
exports.getGeoJson = getGeoJson;
exports.createAVLArray = createAVLArray;