var xor = require('bitwise-xor');

var utils = require("../../utils");
var jAlarmFile = require("./alarm");
var config = require("../../config")["thinkrace"];
var dasDetails = require("../../config")["DAS"];
var mongoConfig = require("../../config")["mongo"];
var kafkaConfig = require("../../config")["kafka"];
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

// const DAS_BASEURL =
// "http://gearbox-dev.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/pushToBC/{id}/{timeStamp}/{packet}";
// const DAS_BASEURL_PARSED =
// "http://gearbox-dev.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/{id}/{timeStamp}/{packet}";
var port = config["port"];

var throughputConfig = require("../../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];

var dasDNS = "http://" + dasDetails["DNS"] + ":" + dasDetails["port"];

var ThroughputRecord = require("../../ThroughputRecord.js").ThroughputRecord;
var parserQThroughputRecord = new ThroughputRecord("parserQThroughputRecord", new Date, 0, threshold);

var logger = require("../../logger.js");
var mylogger = logger.getLogger("thinkrace", "parser.log");

const DAS_BASEURL = dasDNS + "/DataAcquisitionModule/dataacquisitions/pushToBC/{id}/{timeStamp}/{packet}";
const DAS_BASEURL_PARSED = dasDNS + "/DataAcquisitionModule/dataacquisitions/push/parsedData/{id}/{timeStamp}/{packet}";

const DAS_TOPIC = "das-urls";
var mongoTopic = mongoConfig["topic"];

const mongo_document_agent = "CIQ-Thinkrace-VT200";
const derived_towing_alert_speed = 15;

var os = require("os");
var origin = os.hostname();
var HashMap = require('hashmap');
var mapOfAlertNames = new HashMap();

mapOfAlertNames.set("over_speed", "overSpeed");
mapOfAlertNames.set("fatigue_driving", "fatigueDriving");
mapOfAlertNames.set("gps_module_fault", "gpsModuleFault");
mapOfAlertNames.set("low_battery_voltage", "lowBatteryVoltage");
mapOfAlertNames.set("abnormal_charging_circuit", "abnormalChargingCircuit");
mapOfAlertNames.set("ecm_abnormal", "ecmAbnormal");
mapOfAlertNames.set("hight_temperature_of_coolant", "highTemperatureOfCoolant");
mapOfAlertNames.set("low_temperature_of_coolant", "lowTemperatureOfCoolant");
mapOfAlertNames.set("maintenance_reminder", "maintenanceReminder");
mapOfAlertNames.set("throttle_reminder", "throttleReminder");
mapOfAlertNames.set("pull_out_reminder", "pullOutReminder");
mapOfAlertNames.set("illegal_door", "illegalDoor");
mapOfAlertNames.set("illegal_ignition", "illegalIgnition");
mapOfAlertNames.set("life_less_than_50km", "lifeLessThan50km");
mapOfAlertNames.set("refueling_report", "refuelingReport");
mapOfAlertNames.set("gasoline_abnormal_decrease_remind", "gasolineAbnormalDecreaseRemind");
mapOfAlertNames.set("vibration", "vibration");
mapOfAlertNames.set("no_protocol_read", "noProtocolRead");
mapOfAlertNames.set("sudden_speeded_up", "suddenSpeededUp");
mapOfAlertNames.set("rapid_deceleration", "rapidDeceleration");
mapOfAlertNames.set("sharp_turn", "sharpTurn");
mapOfAlertNames.set("towing", "towing");
mapOfAlertNames.set("collision", "collision");
mapOfAlertNames.set("rollover", "rollover");
mapOfAlertNames.set("sudden_lane_change", "suddenLaneChange");
mapOfAlertNames.set("sudden_refuel", "suddenRefuel");
mapOfAlertNames.set("idling", "idling");

mapOfAlertNames.set("Dormancy-Voltage", "dormancyVoltage");
mapOfAlertNames.set("Dormancy-Energize", "dormancyEnergize");
mapOfAlertNames.set("Dormancy-SMS", "dormancySMS");
mapOfAlertNames.set("Dormancy-Vibration", "dormancyVibration");
mapOfAlertNames.set("Dormancy-RTC", "dormancyRTC");
mapOfAlertNames.set("Wakeup-Voltage", "wakeupVoltage");
mapOfAlertNames.set("Wakeup-Energize", "wakeupEnergize");
mapOfAlertNames.set("Wakeup-SMS", "wakeupSMS");
mapOfAlertNames.set("Wakeup-Vibration", "wakeupVibration");
mapOfAlertNames.set("Wakeup-RTC", "wakeupRTC");


mylogger.info("DAS_BASEURL : " + DAS_BASEURL);
mylogger.info("DAS_BASEURL_PARSED : " + DAS_BASEURL_PARSED);

var myPath = require('path');
var myScriptName = myPath.basename(__filename) + " : ";

var START_START = 0;
var START_LENGTH = 2;
var ID_START = START_START + START_LENGTH;
var ID_LENGTH = 12;
var COMMAND_BYTE_START = ID_START + ID_LENGTH;
var COMMAND_BYTE_LENGTH = 4;
var CONTENT_LENGTH_START = COMMAND_BYTE_START + COMMAND_BYTE_LENGTH;
var CONTENT_LENGTH_LENGTH = 4;
var CONTENT_START = CONTENT_LENGTH_START + CONTENT_LENGTH_LENGTH;
var CHKSUM_LENGTH = 2;
var END_LENGTH = 2;

var DATETIME_LENGTH = 12;
var LOCATION_LENGTH = 18;
var ALARM_ID_LENGTH = 2;
var ALARM_DATA_LENGTH = 4;

var COMMON_CMD = "00";
var GSM_CMD = "10";
var GPS_CMD = "20";
var OBD_CMD = "30";

var ALARM_DEV_TO_SERVER = "0088";
var FIRMWARE_VERSION_COMMAND = "0082";
var WAKEUP_COMMAND = "008E";
var DEVICE_INFO_COMMAND = "0092";
var CLEAN_FLASH_COMMAND = "0094";
var GSENSOR_COMMAND = "0095";

var QUERY_IP_RESPONSE_CMD = "1081";
var HEARTBEAT_COMMAND = "1088";

var GPS_INFO_ACC_OFF_COMMAND = "2082";
var GPS_INFO_ACC_ON_COMMAND = "2086";
var GPS_OBD_INFO_COMMAND = "2084";

var OBD_INFO_COMMAND = "3082";
var DTC_ERROR_COMMAND = "3087";
var VIN_COMMAND = "3084";
var ACC_REPORT_COMMAND = "3089";
var TRIP_COMMAND = "3088";
var WIFI_HOTSPOT_OPEN_CLOSE = "1089";
var WIFI_ENCRYP_PWD = "108B";
var WIFI_NAME = "108C";

/*
 * Parse the Raw packet Create JSON which will hold the Packet Structure as per
 * the Protocol document Start - 2 , DevID - 12 , CMDByte - 4 , ContentLength -
 * 4 , Contect - ( HexToDec of ContentLength * 2) , ChkSum - 2 , End - 2
 */
function parseRawPacket(packet) {

	var jPacketJSON = {};
	var result = {};

	jPacketJSON.Start = packet.substr(START_START, START_LENGTH);
	jPacketJSON.DevID = packet.substr(ID_START, ID_LENGTH);
	jPacketJSON.CMDByte = packet.substr(COMMAND_BYTE_START, COMMAND_BYTE_LENGTH);
	jPacketJSON.ContentLength = packet.substr(CONTENT_LENGTH_START, CONTENT_LENGTH_LENGTH);


	/* Fine Content Length, which will vary packet to packet */
	iContentLength = parseInt(jPacketJSON.ContentLength, 16) * 2;
	jPacketJSON.Content = packet.substr(CONTENT_START, iContentLength);

	jPacketJSON.CkSum = packet.substr(packet.length - 4, CHKSUM_LENGTH); // Second
	// Last
	// BYTE
	// of
	// Packet
	jPacketJSON.End = packet.substr(packet.length - 2, END_LENGTH); // Last
	// BYTE
	// of
	// Packet

	//console.log(myScriptName + new Date + ": " + " Packet JSON = " + JSON.stringify(jPacketJSON));
	return jPacketJSON;
}

/*
 * Compute CheckSum using XOR Addition of all bytes
 */
function findChkSum(frame) {

	var ckSum = xor(new Buffer('00', 'hex'), new Buffer('00', 'hex'))

	// console.log("frame=" + frame);
	for (var i = 0; i < frame.length; i++) {
		var tmp = frame[i] + frame[i + 1];
		i = i + 1;
		// console.log(tmp);
		ckSum = xor(ckSum, new Buffer(tmp, 'hex'));
		// console.log(ckSum);
	}
	// console.log("Checksum=" + ckSum.toString('hex'));
	return ckSum.toString('hex');
}

// 28101010101010208200210100120506160954523D152083760771893447006A081300000138001000007C007B5D29
/*
 * Verify If checksum is right
 */
function verifyCheckSum(packet) {

	var length = packet.length;
	var frame = packet.substr(2, length - 6);


	// console.log("Checksum in packet =" + packet.substr(length -4 ,2));
	if (findChkSum(frame) == packet.substr(length - 4, 2))
		return true;
	else
		return false;
}

/*
 * Validate if this is GOOD packet or BAD 1) Check for START & END 2) Check for
 * CheckSum
 */
function validatePacket(jPacket) {
	if ((jPacket.Start == "28") && (jPacket.End == "29"))
		return true;
	else
		return false;
}

/*
 * Remove escape sequnce
 */
function escapePackets(packet) {

	/* If there is no 3D in packet, return it no need to process for escape */
	if (packet.indexOf("3D") < 0)
		return packet;

	var escapePacket = "";

	for (var i = 0; i < packet.length; i += 2) {
		if (packet.substr(i, 2) === "3D") {
			escapePacket += utils.xorHex(packet.substr(i, 2), packet.substr(i + 2, 2));
			i += 2;
		}
		else
			escapePacket += packet.substr(i, 2);
	}
	return escapePacket.toUpperCase();
}


function mongoProducer(document) {

	// set default values
	document.origin = require("os").hostname();
	document.serverTimestamp = new Date();

	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        //mylogger.info("KafkaResult:" + JSON.stringify(result));
    });
}

/*
 * Here we are mainitaining another Queue of "URL" Strings with "Topic"
 * DAS_TOPIC
 */
function processURL(URL) {
	/*
	 * 22 May 2017 - <kumar> Don't process URL, we are retireing DAS now
	 * 
	 * URL = URL.replace(/ /g, "%20"); // Replace SPACES with PERCENTAGE var job =
	 * jobsURL.create(DAS_TOPIC, { URL: URL }); // console.log(myScriptName + new
	 * Date + ": " + "Creating URLJob for : " + // URL ); job.save();
	 */

}


/*
 * Parse the RAW Packet and then Based on CMDByte , decide packet paerser
 */
function processPacket(packet) {

	/* Check for Escape Sequqnce */
	// packet = escapePackets(packet);//
	// packet.replace(/3D15/g,"28").replace(/3D14/g,"29");
	// console.log(myScriptName + new Date + ": " + "Raw Packet After escape=" +
	// packet);

	/* Do the basic Parsing */
	var jPacketJSON = parseRawPacket(packet);


	/* Validate Packet is GOOD - As per protocol */
	if (!validatePacket(jPacketJSON)) {
		//mylogger.error("(processPacket)-Packet is NOT Valid..returning from here,ANOMALY,INVALID_PACKET," + packet);
		return;
	}

	/* Verify CheckSum */
	// if (!verifyCheckSum(packet)) {
	// console.error(myScriptName + new Date + ": " + "[ERROR], Packet CheckSum
	// is NOT Valid..returning from here : " + packet);
	// return;
	// }

	switch (jPacketJSON.CMDByte) {
		case QUERY_IP_RESPONSE_CMD:
			getIPAddress(jPacketJSON);
			break;

		case ACC_REPORT_COMMAND:
			accReport(jPacketJSON);
			break;

		case GPS_INFO_ACC_OFF_COMMAND:
			parseGpsPacket(jPacketJSON, GPS_INFO_ACC_OFF_COMMAND);
			break;

		case GPS_OBD_INFO_COMMAND:
			parseGpsOBDPacket(jPacketJSON);
			break;

		case GPS_INFO_ACC_ON_COMMAND:
			parseGpsPacket(jPacketJSON, GPS_INFO_ACC_ON_COMMAND);
			break;

		case OBD_INFO_COMMAND:
			parseOBDPacket(jPacketJSON);
			break;

		case DTC_ERROR_COMMAND:
			parseErrorPacket(jPacketJSON);
			break;

		case TRIP_COMMAND:
			parseTripPacket(jPacketJSON);
			break;

		case HEARTBEAT_COMMAND:
			parseHeartBeatPacket(jPacketJSON);
			break;

		case ALARM_DEV_TO_SERVER:
			parseAlarmPacket(jPacketJSON);
			break;

		case VIN_COMMAND:
			parseVINPacket(jPacketJSON);
			break;

		case FIRMWARE_VERSION_COMMAND:
			parseFirmwareVersionPacket(jPacketJSON);
			break;

		case WAKEUP_COMMAND:
			parseWakeupPacket(jPacketJSON);
			break;

		case WIFI_HOTSPOT_OPEN_CLOSE:
			parseWifiHostspotOpenClosePacket(jPacketJSON);
			break;

		case WIFI_ENCRYP_PWD:
			parseWifiEncrypPwdPacket(jPacketJSON);
			break;

		case WIFI_NAME:
			parseWifiNamePacket(jPacketJSON);
			break;

		case GSENSOR_COMMAND:
			parseGSensorPacket(jPacketJSON);
			break;

		case CLEAN_FLASH_COMMAND:
			parseCleanFlashPacket(jPacketJSON);
			break;

		case DEVICE_INFO_COMMAND:
			parseDeviceInfoPacket(jPacketJSON);
			break;

		default:
		//mylogger.error("(processPacket)-Unparsed packet,ANOMALY,UNPARSED_PACKET," + JSON.stringify(jPacketJSON));
	}
	parserQThroughputRecord.increment();
}


/*
 * Sample Trip Report for new firmware which includes IMEI and IMSI number Raw
 * Command :
 * 28601603080423308800620070867273026722772F404909061825523F0601800001390601800002000014000001DA000003429471263C0000001400000342000001DA000000000000000000000000000000000000000000000000000000000000000000000000000000000000FB29
 * Received from Q
 * =>{"Start":"28","DevID":"601603080423","CMDByte":"3088","ContentLength":"0062","Content":"0070867273026722772F404909061825523F0601800001390601800002000014000001DA000003429471263C0000001400000342000001DA000000000000000000000000000000000000000000000000000000000000000000000000000000000000","CkSum":"FB","End":"29"}
 * 
 * Sample Trip without IMEI & IMSI Raw Command :
 * 2810101010101030880052000005018023595626051610384600C3000002420000097C492B2A83000000000000000000000000000000000000000000000073000007A8000001840050000001D4000000BD000000000000000000000000BC29
 * Received from Q
 * =>{"Start":"28","DevID":"101010101010","CMDByte":"3088","ContentLength":"0052","Content":"000005018023595626051610384600C3000002420000097C492B2A83000000000000000000000000000000000000000000000073000007A8000001840050000001D4000000BD000000000000000000000000","CkSum":"BC","End":"29"}
 */
function parseTripPacket(jPacket) {

	var jTripPacket = {};
	var iOffSet = 0;
	jTripPacket.DevID = jPacket.DevID;
	jTripPacket.TimeStamp = utils.getServerTimeStamp();

	if (jPacket.ContentLength == "0062")
		jTripPacket.IsIMEIDataPresent = true;
	else
		jTripPacket.IsIMEIDataPresent = false;

	// parseInt(jPacketJSON.ContentLength, 16)
	jTripPacket.trip_id = parseInt(jPacket.Content.substr(0, 4), 16);

	if (jTripPacket.IsIMEIDataPresent) {
		iOffSet = 32;
		jTripPacket.IMEI = jPacket.Content.substr(4, 16);
		jTripPacket.IMSI = jPacket.Content.substr(20, 16);
	}
	else
		iOffSet = 0;

	jTripPacket.ignition_timestamp = getTimeStamp(jPacket.Content.substr(4 + iOffSet, 12));
	jTripPacket.flameout_timestamp = getTimeStamp(jPacket.Content.substr(16 + iOffSet, 12));
	jTripPacket.tavel_time = parseInt(jPacket.Content.substr(28 + iOffSet, 4), 16);
	jTripPacket.fuel_comsumption = parseInt(jPacket.Content.substr(32 + iOffSet, 8), 16);
	jTripPacket.mileage = parseInt(jPacket.Content.substr(40 + iOffSet, 8), 16);
	jTripPacket.max_speed = parseInt(jPacket.Content.substr(48 + iOffSet, 2), 16);
	jTripPacket.max_rpm = parseInt(jPacket.Content.substr(50 + iOffSet, 4), 16) / 4;
	jTripPacket.max_coolant_temperature = parseInt(jPacket.Content.substr(54 + iOffSet, 2), 16) - 40;
	jTripPacket.rapid_acceleration_times = parseInt(jPacket.Content.substr(56 + iOffSet, 2), 16);
	jTripPacket.rapid_deceleration_times = parseInt(jPacket.Content.substr(58 + iOffSet, 2), 16);
	jTripPacket.over_speeding_time = parseInt(jPacket.Content.substr(60 + iOffSet, 4), 16);
	jTripPacket.over_speeding_mileage = parseInt(jPacket.Content.substr(64 + iOffSet, 8), 16);
	jTripPacket.fuel_consumption_over_speeding = parseInt(jPacket.Content.substr(72 + iOffSet, 8), 16);
	jTripPacket.high_speed_time = parseInt(jPacket.Content.substr(80 + iOffSet, 4), 16);
	jTripPacket.high_speed_mileage = parseInt(jPacket.Content.substr(84 + iOffSet, 8), 16);
	jTripPacket.fuel_consumption_high_speeds = parseInt(jPacket.Content.substr(92 + iOffSet, 8), 16);

	jTripPacket.normal_speed_time = parseInt(jPacket.Content.substr(100 + iOffSet, 4), 16);
	jTripPacket.normal_speed_mileage = parseInt(jPacket.Content.substr(104 + iOffSet, 8), 16);
	jTripPacket.fuel_consumption_normal_speed = parseInt(jPacket.Content.substr(112 + iOffSet, 8), 16);
	jTripPacket.low_speed_time = parseInt(jPacket.Content.substr(120 + iOffSet, 4), 16);
	jTripPacket.low_speed_mileage = parseInt(jPacket.Content.substr(124 + iOffSet, 8), 16);
	jTripPacket.fuel_consumption_low_speed = parseInt(jPacket.Content.substr(132 + iOffSet, 8), 16);

	jTripPacket.idle_speed_time = parseInt(jPacket.Content.substr(140 + iOffSet, 4), 16);
	jTripPacket.fuel_consumption_idle_speed = parseInt(jPacket.Content.substr(144 + iOffSet, 8), 16);
	jTripPacket.sharp_turn_times = parseInt(jPacket.Content.substr(152 + iOffSet, 2), 16);
	jTripPacket.over_speeding_times = parseInt(jPacket.Content.substr(154 + iOffSet, 2), 16);
	jTripPacket.hot_car_time = parseInt(jPacket.Content.substr(156 + iOffSet, 4), 16);
	jTripPacket.fast_lane_change = parseInt(jPacket.Content.substr(160 + iOffSet, 2), 16);
	jTripPacket.emergency_refueling = parseInt(jPacket.Content.substr(162 + iOffSet, 2), 16);

	// console.log(myScriptName + new Date + ": " + "TRIP JSON =>" +
	// JSON.stringify(jTripPacket));

	/*
	 * Construct the appropriate URL and send it to DAS , as per following
	 * sample
	 * 
	 * http://localhost:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/213GL2015016616/2016-06-06%2000:00:00/trip.trip_id=1.1,trip.ignition_timestamp=2016-01-01%2000:00:00,trip.flameout_timestamp=2016-01-02%2000:00:00,trip.tavel_time=2.2,trip.fuel_comsumption=3.3,trip.mileage=4.4,trip.max_speed=5.5,trip.max_rpm=6.6,trip.max_coolant_temperature=7.7,trip.rapid_acceleration_times=8.8,trip.rapid_deceleration_times=9.9,trip.over_speeding_time=10.10,trip.over_speeding_mileage=11.11,trip.fuel_consumption_over_speeding=12.12,trip.high_speed_time=13.13,trip.high_speed_mileage=14.14,trip.fuel_consumption_high_speeds=15.15,trip.normal_speed_time=16.16,trip.normal_speed_mileage=17.17,trip.fuel_consumption_normal_speed=18.18,trip.low_speed_time=19.19,trip.low_speed_mileage=20.20,trip.fuel_consumption_low_speed=21.21,trip.idle_speed_time=22.22,trip.fuel_consumption_idle_speed=23.23,trip.sharp_turn_times=24.24,trip.over_speeding_times=25.25,trip.hot_car_time=26.26,trip.fast_lane_change=27.27,trip.emergency_refueling=28.28
	 */

	var fieldNames = ["trip_id", "ignition_timestamp", "flameout_timestamp", "tavel_time", "fuel_comsumption", "mileage", "max_speed", "max_rpm", "max_coolant_temperature", "rapid_acceleration_times", "rapid_deceleration_times", "over_speeding_time", "over_speeding_mileage", "fuel_consumption_over_speeding", "high_speed_time", "high_speed_mileage", "fuel_consumption_high_speeds", "normal_speed_time", "normal_speed_mileage", "fuel_consumption_normal_speed", "low_speed_time", "low_speed_mileage", "fuel_consumption_low_speed", "idle_speed_time", "fuel_consumption_idle_speed", "sharp_turn_times", "over_speeding_times", "hot_car_time", "fast_lane_change", "emergency_refueling"];

	var tripData = "";
	for (var iTest = 0; iTest < fieldNames.length; iTest++) {
		var key = fieldNames[iTest];
		tripData += "trip." + fieldNames[iTest] + "=" + jTripPacket[key] + ",";
	}

	/*
	 * If IMEI data is present then put it in following format
	 * Signature.Smartplug.SIM_number Signature.Smartplug.IMEI_number
	 */
	if (jTripPacket.IsIMEIDataPresent) {
		tripData += "Signature.Smartplug.SIM_number=" + jTripPacket.IMSI + ",";
		tripData += "Signature.Smartplug.IMEI_number=" + jTripPacket.IMEI + ",";
	}

	// console.log("tripData=" + tripData);

	var result = {};
	result.id = jTripPacket.DevID;
	result.timeStamp = jTripPacket.TimeStamp;
	result.all_data = tripData;

	// console.log(myScriptName + new Date + ": " + "URL To DAS ==>" +
	// urlToDas);

	var urlToDas = createUrl(result, true);
	processURL(urlToDas);

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "trip";
	mongo_document.subtype = "trip";

	mongo_document.tripId = jTripPacket.trip_id;
	mongo_document.ignitionTimestamp = getISODate(jTripPacket.ignition_timestamp);
	mongo_document.flameoutTimestamp = getISODate(jTripPacket.flameout_timestamp);
	mongo_document.tavelTime = jTripPacket.tavel_time;
	mongo_document.fuelComsumption = jTripPacket.fuel_comsumption;
	mongo_document.mileage = jTripPacket.mileage;
	mongo_document.maxSpeed = jTripPacket.max_speed;
	mongo_document.maxRpm = jTripPacket.max_rpm;
	mongo_document.maxCoolantTemperature = jTripPacket.max_coolant_temperature;
	mongo_document.rapidAccelerationTimes = jTripPacket.rapid_acceleration_times;
	mongo_document.rapidDecelerationTimes = jTripPacket.rapid_deceleration_times;
	mongo_document.overSpeedingTime = jTripPacket.over_speeding_time;
	mongo_document.overSpeedingMileage = jTripPacket.over_speeding_mileage;
	mongo_document.fuelConsumptionOverSpeeding = jTripPacket.fuel_consumption_over_speeding;
	mongo_document.highSpeedTime = jTripPacket.high_speed_time;
	mongo_document.highSpeedMileage = jTripPacket.high_speed_mileage;
	mongo_document.fuelConsumptionHighSpeeds = jTripPacket.fuel_consumption_high_speeds;
	mongo_document.normalSpeedTime = jTripPacket.normal_speed_time;
	mongo_document.normalSpeedMileage = jTripPacket.normal_speed_mileage;
	mongo_document.fuelConsumptionNormalSpeed = jTripPacket.fuel_consumption_normal_speed;
	mongo_document.lowSpeedTime = jTripPacket.low_speed_time;
	mongo_document.lowSpeedMileage = jTripPacket.low_speed_mileage;
	mongo_document.fuelConsumptionLowSpeed = jTripPacket.fuel_consumption_low_speed;
	mongo_document.idleSpeedTime = jTripPacket.idle_speed_time;
	mongo_document.fuelConsumptionIdleSpeed = jTripPacket.fuel_consumption_idle_speed;
	mongo_document.sharpTurnTimes = jTripPacket.sharp_turn_times;
	mongo_document.overSpeedingTimes = jTripPacket.over_speeding_times;
	mongo_document.hotCarTime = jTripPacket.hot_car_time;
	mongo_document.fastLaneChange = jTripPacket.fast_lane_change;
	mongo_document.emergencyRefueling = jTripPacket.emergency_refueling;
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);


	/*
	 * Check for imei & imsi number and if present create metadata type
	 * mongodocument and push in mongo Queue
	 */
	if (jTripPacket.IsIMEIDataPresent) {
		var mongo_document = {};
		mongo_document.origin = origin;
		mongo_document.owner = result.id;
		mongo_document.timestamp = getISODate(result.timeStamp);
		mongo_document.agent = mongo_document_agent;
		mongo_document.type = "metadata";
		mongo_document.subtype = "imeiimsi";
		mongo_document.imsi = jTripPacket.IMSI;
		mongo_document.imei = jTripPacket.IMEI;
		mongo_document.location = getGeoJSON(0, 0, false);

		mongoProducer(mongo_document);
	}
}


function parseHeartBeatPacket(jPacket) {
	var result = {};
	result.id = jPacket.DevID;
	result.speed = "00";
	result.timeStamp = utils.getServerTimeStamp();

	if (result) {
		//mylogger.debug("(parseHeartBeatPacket)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "heartbeat";
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);

}

/*
 * Received Raw Packet
 * =281010101010102084003401001805061612274128209178077189770723B1081D0000013800100120676F10841F006289550445000000000000002400621BD129
 * Packet =
 * {"Start":"28","DevID":"101010101010","CMDByte":"2084","ContentLength":"0034","Content":"01001805061612274128209178077189770723B1081D0000013800100120676F10841F006289550445000000000000002400621B","CkSum":"D1","End":"29"}
 * 
 */
function parseGpsOBDPacket(jPacket) {

	var result = {};

	result.id = jPacket.DevID;
	result.datatype = jPacket.Content.substr(0, 2);
	result.tripid = jPacket.Content.substr(2, 4);
	result.timeStamp = getTimeStamp(jPacket.Content.substr(6, 12));
	result.location = getLocation(jPacket.Content.substr(18, 18));
	result.gps_speed = jPacket.Content.substr(36, 2);
	result.direction = jPacket.Content.substr(38, 2);
	result.gpsSateliteNumber = jPacket.Content.substr(40, 2);
	result.gsmSignal = jPacket.Content.substr(42, 2);
	result.mileage = jPacket.Content.substr(44, 8);
	result.deviceStatus = jPacket.Content.substr(52, 8);
	result.engineLoad = jPacket.Content.substr(60, 2);
	result.coolantTemparature = jPacket.Content.substr(62, 2);
	result.rpm = jPacket.Content.substr(64, 4);
	result.obdspeed = jPacket.Content.substr(68, 2);
	result.speed = result.gps_speed; // Fix 29Aug2016 : Treat gps_speed as speed
	result.Ignition_advance_angle = jPacket.Content.substr(70, 2);
	result.Intake_manifold_absolute_pressure = jPacket.Content.substr(72, 2);
	result.Voltage_control_module = jPacket.Content.substr(74, 2);
	result.Intake_air_temperature = jPacket.Content.substr(76, 2);
	result.Intake_air_flow = jPacket.Content.substr(78, 4);
	result.relative_position_of_the_throttle_valve = jPacket.Content.substr(82, 2);
	result.longterm_fuel_trim = jPacket.Content.substr(84, 2);
	result.Fuel_ratio_coefficient = jPacket.Content.substr(86, 4);
	result.Absolute_throttle_position = jPacket.Content.substr(90, 2);
	result.Fuel_Pressure = jPacket.Content.substr(92, 2);
	result.Instantaneous_fuel_consumption_1 = jPacket.Content.substr(94, 4);
	result.Instantaneous_fuel_consumption_2 = jPacket.Content.substr(98, 4);
	result.serial_no = jPacket.Content.substr(102, 2);

	if (result) {
		//mylogger.debug("(parseGpsOBDPacket)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	/* Send obd type data */
	var mongo_document = {};
	mongo_document.owner = result.id;
	mongo_document.origin = origin;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "obdgps";

	mongo_document.mileage = parseInt(result.mileage, 16);
	mongo_document.deviceStatus = result.deviceStatus;
	mongo_document.engineLoad = parseInt(result.engineLoad, 16) * 100 / 255;
	mongo_document.coolantTemparature = parseInt(result.coolantTemparature, 16) - 40;
	mongo_document.rpm = parseInt(result.rpm, 16) / 4;
	mongo_document.obdspeed = parseInt(result.obdspeed, 16);
	mongo_document.ignitionAdvanceAngle = (parseInt(result.Ignition_advance_angle, 16) / 2) - 64;
	mongo_document.intakeManifoldAbsolutePressure = parseInt(result.Intake_manifold_absolute_pressure, 16);
	mongo_document.voltage = parseInt(result.Voltage_control_module, 16) / 10;
	mongo_document.intakeAirTemperature = parseInt(result.Intake_air_temperature, 16) - 40;
	mongo_document.intakeAirFlow = parseInt(result.Intake_air_flow, 16) / 100;
	mongo_document.relativePositionOfTheThrottleValve = parseInt(result.relative_position_of_the_throttle_valve, 16) * 100 / 255;
	mongo_document.longtermFuelTrim = (parseInt(result.longterm_fuel_trim, 16) - 128) * (100 / 128);
	mongo_document.fuelRatioCoefficient = parseInt(result.Fuel_ratio_coefficient, 16) * 0.0000305;
	mongo_document.absoluteThrottlePosition = parseInt(result.Absolute_throttle_position, 16) * 100 / 255;
	mongo_document.fuelPressure = parseInt(result.Fuel_Pressure, 16) * 3;
	mongo_document.instantaneousFuelConsumption1 = parseInt(result.Instantaneous_fuel_consumption_1, 16) / 10;
	mongo_document.instantaneousFuelConsumption2 = parseInt(result.Instantaneous_fuel_consumption_2, 16) / 10;
	mongo_document.serialNo = parseInt(result.serial_no, 16);
	mongo_document.location = getGeoJSON(getLatitude(result.location.latitude), getLongitude(result.location.longitude), true);
	mongo_document.speed = parseInt(result.gps_speed, 16);
	mongo_document.direction = parseInt(result.direction, 16);
	mongo_document.gpsSateliteNumber = parseInt(result.gpsSateliteNumber, 16);
	mongo_document.gsmSignal = parseInt(result.gsmSignal, 16);

	mongoProducer(mongo_document);
}

function getISODate(stringDate) {
	// console.log("====> Remove me : input date:" + stringDate);
	// console.log("====> Remove me : input date:" + new

	return new Date(stringDate);
}

/*
 * Sample RAW Packet :
 * 286016030804233087002A010001020001030001430001820001C10001010102010702020702030702430702010202D60002A24502B629
 * Received from Q
 * =>{"Start":"28","DevID":"601603080423","CMDByte":"3087","ContentLength":"002A","Content":"010001020001030001430001820001C10001010102010702020702030702430702010202D60002A24502","CkSum":"B6","End":"29"}
 */
function parseErrorPacket(jPacket) {
	/*
	 * Following is SAMPLE URL
	 * http://gearbox.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/601603080423/2016-06-22%2000:59:42/ErrorCode_DTC=P0100|P0102|P0001|P0300|P0143|P0001|B0200|P01C1|P0001|P0101|P0201|P0702|P0207|P0203|P0702|C0307|P0201|P0202|U1600|P02A2|C0502|,
	 */

	var result = {};
	result.id = jPacket.DevID;
	result.timeStamp = utils.getServerTimeStamp();

	var errorCodeString;
	if (jPacket.Content)
		errorCodeString = getErrors(jPacket.Content);
	else
		errorCodeString = "P0000";

	result.all_data = "ErrorCode_DTC=" + errorCodeString;

	var urlToDas = createUrl(result, true);
	//mylogger.debug("(parseErrorPacket)-URL To DAS DTC ****** ==>" + urlToDas);
	processURL(urlToDas);

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "dtc";
	mongo_document.errorCodeDTC = errorCodeString;
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);
}

/*
 * This is response we get
 * 28601603080423308400113147314A43353434345237323532333637D229
 */
function parseVINPacket(jPacket) {
	/*
	 * Following is SAMPLE URL
	 * http://gearbox.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/601603080423/2016-06-22%2009:51:30/vin=1G1JC5444R7252367
	 */
	var result = {};
	var tmp = jPacket.Content.toString('ascii');
	result.all_data = "vin=" + new Buffer(tmp, 'hex');
	result.id = jPacket.DevID;
	result.timeStamp = utils.getServerTimeStamp();

	var urlToDas = createUrl(result, true);
	//mylogger.debug("(parseVINPacket)-URL To DAS VIN ****** ==>" + urlToDas);
	processURL(urlToDas);

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "metadata";
	mongo_document.subtype = "vin";
	mongo_document.vin = "" + new Buffer(tmp, 'hex');
	mongo_document.location = getGeoJSON(0, 0, false);
	mongoProducer(mongo_document);

}

/*
 * This is response we get 28101010101010008E000801030506160506452E29
 */
function parseWakeupPacket(jPacket) {

	var typeOfWakeup = "Wakeup-";
	var firstByte = jPacket.Content.substr(0, 2);
	var secondByte = jPacket.Content.substr(2, 2);

	if (firstByte == "00")
		typeOfWakeup = "Dormancy-";

	switch (secondByte) {
		case "00":
			typeOfWakeup += "Energize"; break;
		case "01":
			typeOfWakeup += "Voltage"; break;
		case "02":
			typeOfWakeup += "SMS"; break;
		case "03":
			typeOfWakeup += "Vibration"; break;
		case "04":
			typeOfWakeup += "RTC"; break;
	}
	// console.log(myScriptName + new Date + ": " + "WakeupEventPacket = " +
	// typeOfWakeup);

	var wakeupTime = getTimeStamp(jPacket.Content.substr(4, DATETIME_LENGTH));

	var result = {};
	result.all_data = "alert.wakeup=" + typeOfWakeup;
	result.id = jPacket.DevID;
	result.timeStamp = wakeupTime;

	var urlToDas = createUrl(result, true);
	processURL(urlToDas);

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "alert";
	mongo_document.subtype = mapOfAlertNames.get(typeOfWakeup);
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);

}


/*
 * This is response we get
 * 28301404140001008200104D5036303030415F3230313430343034DA29 Sample URL is
 * http://gearbox.mycariq.com:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/301404140001/2016-06-22%2010:15:34/Signature.Smartplug.Firmware_version=MP6000A_20140404,
 */
function parseFirmwareVersionPacket(jPacket) {

	var result = {};
	var tmp = jPacket.Content.toString('ascii');
	result.all_data = "Signature.Smartplug.Firmware_version=" + new Buffer(tmp, 'hex');
	result.id = jPacket.DevID;
	result.timeStamp = utils.getServerTimeStamp();

	var urlToDas = createUrl(result, true);
	//mylogger.debug("(parseFirmwareVersionPacket)-URL To DAS Firmware ****** ==>" + urlToDas);
	processURL(urlToDas);

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "metadata";
	mongo_document.subtype = "firmwareversion";
	mongo_document.firmwareVersion = "" + new Buffer(tmp, 'hex');
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);
}

function parseWifiHostspotOpenClosePacket(jPacket) {
	// if (jPacket.Content == "00")
	// 	mylogger.debug("(parseWifiHostspotOpenClosePacket)-DevID " + jPacket.DevID + " OFF");
	// else
	// 	mylogger.debug("(parseWifiHostspotOpenClosePacket)-DevID " + jPacket.DevID + " ON");
}

function parseWifiNamePacket(jPacket) {
	//mylogger.debug("(parseWifiNamePacket)-DevID :" + jPacket.DevID + " WifiName in Hex:" + jPacket.Content);
}

function parseWifiEncrypPwdPacket(jPacket) {
	var encode = jPacket.Content.substr(0, 2);
	var pwd = null;

	if (encode != "00")
		pwd = jPacket.Content.substr(2, 16);

	// if (encode == "00")
	// 	mylogger.debug("(parseWifiEncrypPwdPacket)-DevID " + jPacket.DevID + " NO PASSWORD");

	// if (encode == "01")
	// 	mylogger.debug("(parseWifiEncrypPwdPacket)-DevID " + jPacket.DevID + " WPA_TKIP" + " PWD :" + pwd);

	// if (encode == "02")
	// 	mylogger.debug("(parseWifiEncrypPwdPacket)-DevID " + jPacket.DevID + " WPA2-PSK" + " PWD :" + pwd);

}

// get errors from error string
function getErrors(errorsString) {

	//mylogger.debug("(getErrors)-** DTC Error String **" + errorsString);

	var errors = "";
	var codes = {};

	codes["0"] = "P0";
	codes["1"] = "P1";
	codes["2"] = "P2";
	codes["3"] = "P3";

	codes["4"] = "C0";
	codes["5"] = "C1";
	codes["6"] = "C2";
	codes["7"] = "C3";

	codes["8"] = "B0";
	codes["9"] = "B1";
	codes["A"] = "B2";
	codes["B"] = "B3";

	codes["C"] = "U0";
	codes["D"] = "U1";
	codes["E"] = "U2";
	codes["F"] = "U3";

	for (var i = 0; i < errorsString.length; i += 6) {
		var err = errorsString.substr(i, 4);
		// console.log(" Error : " + err);

		if (err != "0000")
			errors += codes[err.substr(0, 1)] + err.substr(1, 3) + "|";

		// console.log(" Error(s) : " + errors);

	}

	if (errors == "")
		errors = "P0000";

	//mylogger.debug("(getErrors)-** DTC Error(s) **" + errors);
	return errors;
}


function parseOBDPacket(jPacket) {

	var result = {};

	result.id = jPacket.DevID;
	result.datatype = jPacket.Content.substr(0, 2);
	result.tripid = jPacket.Content.substr(2, 4);
	result.timeStamp = getTimeStamp(jPacket.Content.substr(6, 12));
	result.engineLoad = jPacket.Content.substr(18, 2);
	result.coolantTemparature = jPacket.Content.substr(20, 2);
	result.rpm = jPacket.Content.substr(22, 4);
	result.speed = jPacket.Content.substr(26, 2);
	result.Ignition_advance_angle = jPacket.Content.substr(28, 2);
	result.Intake_manifold_absolute_pressure = jPacket.Content.substr(30, 2);
	result.Voltage_control_module = jPacket.Content.substr(32, 2);
	result.Intake_air_temperature = jPacket.Content.substr(34, 2);
	result.Intake_air_flow = jPacket.Content.substr(36, 4);
	result.relative_position_of_the_throttle_valve = jPacket.Content.substr(40, 2);
	result.longterm_fuel_trim = jPacket.Content.substr(42, 2);
	result.Fuel_ratio_coefficient = jPacket.Content.substr(44, 4);
	result.Absolute_throttle_position = jPacket.Content.substr(48, 2);
	result.Fuel_Pressure = jPacket.Content.substr(50, 2);
	result.mileage = jPacket.Content.substr(52, 8);
	result.deviceStatus = jPacket.Content.substr(60, 8);
	result.Instantaneous_fuel_consumption_1 = jPacket.Content.substr(68, 4);
	result.Instantaneous_fuel_consumption_2 = jPacket.Content.substr(72, 4);
	result.serial_no = jPacket.Content.substr(76, 2);

	if (result) {
		//mylogger.debug("(parseOBDPacket)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	/* Send obd type data */
	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "obd";

	mongo_document.mileage = parseInt(result.mileage, 16);
	mongo_document.deviceStatus = result.deviceStatus;
	mongo_document.engineLoad = parseInt(result.engineLoad, 16) * 100 / 255;
	mongo_document.coolantTemparature = parseInt(result.coolantTemparature, 16) - 40;
	mongo_document.rpm = parseInt(result.rpm, 16) / 4;
	mongo_document.obdspeed = parseInt(result.speed, 16);
	mongo_document.Ignition_advance_angle = (parseInt(result.Ignition_advance_angle, 16) / 2) - 64;
	mongo_document.Intake_manifold_absolute_pressure = parseInt(result.Intake_manifold_absolute_pressure, 16);
	mongo_document.voltage = parseInt(result.Voltage_control_module, 16) / 10;
	mongo_document.Intake_air_temperature = parseInt(result.Intake_air_temperature, 16) - 40;
	mongo_document.Intake_air_flow = parseInt(result.Intake_air_flow, 16) / 100;
	mongo_document.relative_position_of_the_throttle_valve = parseInt(result.relative_position_of_the_throttle_valve, 16) * 100 / 255;
	mongo_document.longterm_fuel_trim = (parseInt(result.longterm_fuel_trim, 16) - 128) * (100 / 128);
	mongo_document.Fuel_ratio_coefficient = parseInt(result.Fuel_ratio_coefficient, 16) * 0.0000305;
	mongo_document.Absolute_throttle_position = parseInt(result.Absolute_throttle_position, 16) * 100 / 255;
	mongo_document.Fuel_Pressure = parseInt(result.Fuel_Pressure, 16) * 3;
	mongo_document.Instantaneous_fuel_consumption_1 = parseInt(result.Instantaneous_fuel_consumption_1, 16) / 10;
	mongo_document.Instantaneous_fuel_consumption_2 = parseInt(result.Instantaneous_fuel_consumption_2, 16) / 10;
	mongo_document.serial_no = parseInt(result.serial_no, 16);
	mongo_document.location = getGeoJSON(0, 0, false);
	mongoProducer(mongo_document);

}

/*
 * Sample RAW Data :
 * 28101010101010208200210100080406161242073D151917230765377467001A0919000000B6001000007D00AF0D29
 * Received from Q =>
 * {"Start":"28","DevID":"101010101010","CMDByte":"2082","ContentLength":"0021","Content":"010008040616124207281917230765377467
 * 00 1A 0919000000B6001000007D00AF","CkSum":"0D","End":"29"}
 * 
 */
function parseGpsPacket(jPacket, accStatus) {

	var result = {};
	result.id = jPacket.DevID;
	var type = jPacket.Content.substr(0, 2);

	if (accStatus == GPS_INFO_ACC_OFF_COMMAND)
		result.type = "Acc_OFF_";
	else
		result.type = "Acc_ON_";

	if (type === "00")
		result.type += "gps_blind_data";
	if (type === "01")
		result.type += "gps_real_time";

	result.tripid = jPacket.Content.substr(2, 4);
	result.timeStamp = getTimeStamp(jPacket.Content.substr(6, 12));
	result.location = getLocation(jPacket.Content.substr(18, 18));
	result.gps_speed = parseInt(jPacket.Content.substr(36, 2), 16);
	result.speed = jPacket.Content.substr(36, 2); // Fix 29Aug2016 : Treat
	// gps_speed as speed

	result.type = result.type + "=" + result.gps_speed;

	/* Voltage - Start */
	var len = jPacket.Content.length;
	result.Voltage_control_module = jPacket.Content.substr(len - 6, 2);
	/* Voltage - End */
	// result.serial_no = packet.substr(86,2);
	if (result) {
		//mylogger.debug("(parseGpsPacket)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "gps";
	mongo_document.speed = result.gps_speed;

	if (accStatus == GPS_INFO_ACC_OFF_COMMAND)
		mongo_document.accOn = false;
	else
		mongo_document.accOn = true;

	mongo_document.location = getGeoJSON(getLatitude(result.location.latitude), getLongitude(result.location.longitude), true);
	var voltage = parseInt(result.Voltage_control_module, 16) / 10;
	mongo_document.voltage = (voltage > 0) ? voltage : undefined;

	mongoProducer(mongo_document);

	/*
	 * // This is Derived towing_alert if (accStatus == GPS_INFO_ACC_OFF_COMMAND &&
	 * result.gps_speed > derived_towing_alert_speed) { var mongo_document = {};
	 * mongo_document.owner = result.id; mongo_document.timestamp =
	 * result.timeStamp; mongo_document.agent = mongo_document_agent;
	 * mongo_document.type = "alert";
	 * 
	 * mongo_document.name = "towing"; mongo_document.data = result.gps_speed;
	 * mongo_document.location = getGeoJSON(
	 * getLatitude(result.location.latitude),
	 * getLongitude(result.location.longitude)); mongoProducer(mongo_document);
	 * 
	 * //**** To support DAS - We need to remove this code when DAS is retired
	 * 
	 * var tmpString = "alert.towing=" + result.gps_speed + ",alert.latitude=" +
	 * getLatitude(result.location.latitude) + ",alert.longitude=" +
	 * getLongitude(result.location.longitude); //console.log("==== KUMAR ===>" +
	 * tmpString); var resultTmp = {}; resultTmp.id = jPacket.DevID;
	 * resultTmp.timeStamp = result.timeStamp; resultTmp.all_data = tmpString ;
	 * //console.log("==== KUMAR resultTmp ===>" + JSON.stringify(resultTmp));
	 * var urlToDasTmp = createUrl(resultTmp, true); //console.log("==== KUMAR
	 * URL ===>" + urlToDasTmp); processURL(urlToDasTmp);
	 * 
	 * //**** To support DAS - We need to remove this code when DAS is retired }
	 */

}

function getIPAddress(jPacket) {
	var deviceId = jPacket.DevID;
	//mylogger.debug("(getIPAddress)-Device ID:" + deviceId);

	var sActualResponse = jPacket.Content;
	//mylogger.debug("(getIPAddress)-Actual Response:" + sActualResponse);

	var tmpBuffer = new Buffer(sActualResponse, 'hex');
	var sAsciiActualResponse = tmpBuffer.toString('ascii');
	//mylogger.debug("(getIPAddress)-Actual ASCII Response:" + sAsciiActualResponse);

	var tmpArr = sAsciiActualResponse.split(",");

	//for (var i = 0; i < tmpArr.length; i++)
	//mylogger.debug(" Arr[ " + i + "] = " + tmpArr[i]);
}

/*
 * Function to get GeoJSON useful for Mongo Note the location GeoJSON format
 * (longitude, latitude): { "type": "Point", "coordinates": [ 73.77462166666666,
 * 18.55815 ] }
 */
function getGeoJSON(myLatitude, myLongitude, isReal) {

	var locJson = {};
	var locArray = [];

	locJson.type = "Point";
	locJson.isReal = isReal;
	locArray[0] = myLongitude;
	locArray[1] = myLatitude;
	locJson.coordinates = locArray;

	return locJson;
}

/*
 * Sample Raw Packet from Device :
 * 28101010101010308900120000070406161241082819172307653774675929 Sample Packet
 * from Producer =
 * {"Start":"28","DevID":"101010101010","CMDByte":"3089","ContentLength":"0012","Content":"000007040616124108281917230765377467","CkSum":"59","End":"29"}
 */
function accReport(jPacket) {

	var isLocationDataPresent = false;
	var isAcclerationOn = false;

	var result = {};
	result.id = jPacket.DevID;

	if (jPacket.ContentLength == "0012")
		isLocationDataPresent = true;
	if (jPacket.Content[1] == "1")
		isAcclerationOn = true;

	if (isLocationDataPresent) {
		if (isAcclerationOn)
			result.type = "AccOnWithGPS";
		else
			result.type = "AccOffWithGPS";
	}
	else {
		if (isAcclerationOn)
			result.type = "AccOnWithoutGPS";
		else
			result.type = "AccOffWithoutGPS";
	}

	result.tripid = jPacket.Content.substr(2, 4);
	result.timeStamp = getTimeStamp(jPacket.Content.substr(6, 12));

	if (isLocationDataPresent)
		result.location = getLocation(jPacket.Content.substr(18, 18));

	// console.log(myScriptName + new Date + ": " + "TripID :" + result.tripid);
	// console.log(myScriptName + new Date + ": " + "TimeStamp :" +
	// result.timeStamp);
	// console.log(myScriptName + new Date + ": " + "Location :" +
	// result.location);

	if (result) {
		//mylogger.debug("(accReport)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	if (isLocationDataPresent) {
		var mongo_document = {};
		mongo_document.origin = origin;
		mongo_document.owner = result.id;
		mongo_document.timestamp = getISODate(result.timeStamp);
		mongo_document.agent = mongo_document_agent;
		mongo_document.type = "alert";

		if (isAcclerationOn) {
			mongo_document.subtype = "ignitionOn";
			mongo_document.ignition = 1;
		}
		else {
			mongo_document.subtype = "ignitionOff";
			mongo_document.ignition = 0;
		}

		//mongo_document.ignition = mongo_document.accOn;

		mongo_document.location = getGeoJSON(getLatitude(result.location.latitude), getLongitude(result.location.longitude), true);

		mongoProducer(mongo_document);
	}

}

function getLatitude(packet) {
	// console.log("Raw Latitide : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	// console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 2));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(2, strLength - 3));
	remaining = remaining / 60;
	// console.log("remaining : " + remaining);

	var actualLatitide = degree + remaining;
	if (direction == 'S')
		actualLatitide = actualLatitide * (-1);

	// console.log("actualLatitide : " + actualLatitide);
	return actualLatitide;
}


function getLongitude(packet) {
	//console.log("Raw Longitude : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	//console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 3));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(3, strLength - 4));
	remaining = remaining / 60;

	var actualLongitude = degree + remaining;
	if (direction == 'W')
		actualLongitude = actualLongitude * (-1);

	// console.log("actualLatitide : " + actualLongitude);
	return actualLongitude;
}


function getRealLocation(locationStr) {
	var realLoaction = {};

	var location = getLocation(locationStr);
	realLoaction.latitude = getLatitude(location.latitude);
	realLoaction.longitude = getLongitude(location.longitude);

	// mylogger.debug("(getRealLocation)-Real Location : " +
	// JSON.stringify(realLoaction));
	return realLoaction;

}

function getLocation(locationStr) {
	var location = {};
	var direction = utils.hexToBin(locationStr.substr(17, 1));
	var latDirection = (direction.substr(2, 1) === "1") ? "N" : "S";
	var longDirection = (direction.substr(1, 1) === "1") ? "E" : "W";
	location.latitude = (locationStr.substr(0, 4) + "." + locationStr.substr(4, 4))
		+ latDirection;
	location.longitude = (locationStr.substr(8, 5) + "." + locationStr.substr(13, 4))
		+ longDirection;
	// console.log("Location For DAS : " + JSON.stringify(location));
	return location;
}

function getTimeStamp(timestamp) {
	return "20" + timestamp.substr(4, 2) + "-" + timestamp.substr(2, 2) + "-"
		+ timestamp.substr(0, 2) + " " + timestamp.substr(6, 2) + ":"
		+ timestamp.substr(8, 2) + ":" + timestamp.substr(10, 2);
}

/*
 * This function is responsible for creating required URL which will be send to
 * DAS
 */
function createUrl(json, isParsedUrl) {

	var url;
	if (isParsedUrl) {
		url = DAS_BASEURL_PARSED.replace("{id}", json.id).replace("{timeStamp}", json.timeStamp.replace(" ", "%20"));
		url = url.replace("{packet}", json.all_data);
	}
	else {
		url = DAS_BASEURL.replace("{id}", json.id).replace("{timeStamp}", json.timeStamp.replace(" ", "%20"));

		packet = "";
		if (json.type)
			packet += json.type + ",";
		if (json.location) {
			if (json.location.latitude != "0000.0000N" && json.location.latitude != "0000.0000S")
				packet += "41Y1" + json.location.latitude + ",41Y2" + json.location.longitude + ",";
		}
		if (json.engineLoad)
			packet += "4104" + json.engineLoad + ",";
		if (json.coolantTemparature)
			packet += "4105" + json.coolantTemparature + ",";
		if (json.rpm)
			packet += "410C" + json.rpm + ",";
		if (json.speed) {
			if (json.speed.toString().length == 1) // If speed come as single
				// digit e.g. 9 instead of
				// 09 then prepend 0 before
				// digit
				json.speed = "0" + json.speed;
			packet += "410D" + json.speed + ",";
		}

		// TODO what does it mean
		// result.Ignition_advance_angle=packet.substr(92,2);

		if (json.Intake_manifold_absolute_pressure)
			packet += "410B" + json.Intake_manifold_absolute_pressure + ",";
		if (json.Voltage_control_module)
			packet += "TR01" + json.Voltage_control_module + ",";
		if (json.Intake_air_temperature)
			packet += "410F" + json.Intake_air_temperature + ",";
		// TODO what does it mean
		// result.Intake_air_flow=packet.substr(100,4);
		if (json.relative_position_of_the_throttle_valve)
			packet += "4145" + json.relative_position_of_the_throttle_valve + ",";
		// TODO what does it mean
		// result.longterm_fuel_trim=packet.substr(106,2);
		// TODO
		// result.Fuel_ratio_coefficient=packet.substr(108,4);
		// result.Fuel_ratio_coefficient=packet.substr(112,2);
		// result.Fuel_Pressure=packet.substr(114,2);
		// result.Instantaneous_fuel_consumption_1=packet.substr(116,4);
		// result.Instantaneous_fuel_consumption_1=packet.substr(120,4);
		// result.serial_no=packet.substr(124,2);
		url = url.replace("{packet}", packet);
	}
	return url;
}

/* Parse Alarm Packets */
function parseAlarmPacket(jPacket) {

	var jAlarmPacket = {};

	// console.log(myScriptName + new Date + ": " + "Alarm Packet Device ID :" +
	// jPacket.DevID);
	// console.log(myScriptName + new Date + ": " + "Alarm Packet Content :" +
	// jPacket.Content);

	jAlarmPacket.DevID = jPacket.DevID;
	jAlarmPacket.AlarmID = jPacket.Content.substr(0, ALARM_ID_LENGTH);


	if (jAlarmFile[jAlarmPacket.AlarmID])
		jAlarmPacket.AlarmDescription = jAlarmFile[jAlarmPacket.AlarmID];
	else
		jAlarmPacket.AlarmDescription = "NotDefinedYet";

	if (jPacket.ContentLength == "0009")
		jAlarmPacket.IsLocationPresent = false;
	else
		jAlarmPacket.IsLocationPresent = true;

	jAlarmPacket.AlarmData = jPacket.Content.substr(jPacket.Content.length - 4, ALARM_DATA_LENGTH);
	jAlarmPacket.TimeStamp = getTimeStamp(jPacket.Content.substr(2, DATETIME_LENGTH));

	// console.log("====== Alarm ID:" + jAlarmPacket.AlarmID);
	// console.log("====== Alarm Description:" + jAlarmPacket.AlarmDescription);
	// console.log("====== Alarm Data:" + jAlarmPacket.AlarmData);


	if (jAlarmPacket.IsLocationPresent == true)
		jAlarmPacket.Location = getRealLocation(jPacket.Content.substr(14, LOCATION_LENGTH));

	// console.log(myScriptName + new Date + ": " + "Alarm JSON =>" +
	//mylogger.debug("(parseAlarmPacket) " + JSON.stringify(jAlarmPacket));

	/*
	 * Construct the appropriate URL and send it to DAS , as per following
	 * sample
	 * http://localhost:8080/DataAcquisitionModule/dataacquisitions/push/parsedData/213GL2015016/2016-06-06%2000:00:00/alert.gps_module_fault=10.10,alert.latitude=value1,alert.longitude=value2;
	 */

	var result = {};
	result.id = jPacket.DevID;
	result.timeStamp = jAlarmPacket.TimeStamp;
	result.all_data = "alert." + jAlarmPacket.AlarmDescription + "=" + parseInt(jAlarmPacket.AlarmData, 16);
	if (jAlarmPacket.IsLocationPresent == true) {
		if (jAlarmPacket.Location.latitude != 0)
			result.all_data += ",alert.latitude=" + jAlarmPacket.Location.latitude + ",alert.longitude=" + jAlarmPacket.Location.longitude;
	}

	var urlToDas = createUrl(result, true);
	processURL(urlToDas);


	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "alert";
	mongo_document.subtype = mapOfAlertNames.get(jAlarmPacket.AlarmDescription);

	mongo_document.data = parseInt(jAlarmPacket.AlarmData, 16);
	if (mongo_document.subtype == "lowBatteryVoltage" && mongo_document.data) {
		mongo_document.voltage = mongo_document.data / 10;
	}
	if (jAlarmPacket.IsLocationPresent == true) {
		mongo_document.location = getGeoJSON(jAlarmPacket.Location.latitude, jAlarmPacket.Location.longitude, true);
	} else {
		mongo_document.location = getGeoJSON(0, 0, false);
	}

	mongoProducer(mongo_document);

}

/*
 * This is response we get
 * 2844503232182500940001013D29
 */
function parseCleanFlashPacket(jPacket) {

	var mongo_document = {};
	mongo_document.owner = jPacket.DevID;
	mongo_document.timestamp = new Date;
	mongo_document.type = "alert";
	mongo_document.subtype = "clean_flash";
	mongo_document.data = parseInt(jPacket.Content, 16);
	mongo_document.location = getGeoJSON(0, 0, false);
	mongo_document.agent = mongo_document_agent;
	mongo_document.origin = origin;

	mongoProducer(mongo_document);
}

/*
 * This is response we get
 * 286310365526010095006301011000000100020F00B883D0006500B383CF007100B783D2006600B883D2006300BA83D5005400B683D5006300B783D1005E00B783CD006800B183A8007E00B383CB006600B283D1006400B783D5005F00B483D0006100B583D0006A00B683D0006A0029
 */
function parseGSensorPacket(jPacket) {

	var interval = utils.hexToInt(jPacket.Content.substr(12, 4)) * 500;
	var count = utils.hexToInt(jPacket.Content.substr(16, 2));
	var baseTimestamp = utils.minusMilliSeconds(getISODate(getTimeStamp(jPacket.Content.substr(0, DATETIME_LENGTH))), interval * (count - 1));
	var gSensorData = jPacket.Content.substr(18);

	for (var index = 0; index < count; index++) {

		var document = {};
		document.origin = origin;
		document.owner = jPacket.DevID;
		document.agent = mongo_document_agent;
		document.type = "data";
		document.subtype = "gsensor";
		document.timestamp = utils.addMilliSeconds(baseTimestamp, index * interval);
		document.accelerometerX = [parseAccelerometerAxisValue(gSensorData.substr(index * 12, 4))];
		document.accelerometerY = [parseAccelerometerAxisValue(gSensorData.substr(index * 12 + 4, 4))];
		document.accelerometerZ = [parseAccelerometerAxisValue(gSensorData.substr(index * 12 + 8, 4))];
		document.location = utils.getGeoJson(0, 0, false);

		mongoProducer(document);
		break;
	}
}

function parseAccelerometerAxisValue(hex) {
	var firstByte = utils.hexToBin(hex.substr(0, 2));
	var secondByte = hex.substr(2, 2);
	if (firstByte[0] == '1')
		return -1 * (parseInt(firstByte.substr(1), 2) * 256 + utils.hexToInt(secondByte)) / 1000;
	return (parseInt(firstByte.substr(1), 2) * 256 + utils.hexToInt(secondByte)) / 1000;
}

function parseDeviceInfoPacket(jPacket) {
	var document = { owner: jPacket.DevID, timestamp: new Date(), type: "metadata", subtype: "imeiimsi", location: getGeoJSON(0, 0, false) };

	document.from = "DeviceInfoQuery";
	var content = jPacket.Content;

	//mylogger.info("**********[DeviceInfo:" + jPacket.DevID + "]***************");

	for (var index = 0; index < content.length;) {

		var type = content.substr(index, 2);
		var length = utils.hexToInt(content.substr(index + 2, 2)) * 2;
		var value = content.substr(index + 4, length);

		//mylogger.info("[DeviceInfo:" + jPacket.DevID + "]  type:" + type + " length:" + length + " value:" + value);

		switch (type) {
			case "01":
				document.firmwareVersion = utils.hexToText(value);
				break;
			case "02":
				document.imei = utils.hexToText(value);
				//document.imeiTLV = type+":"+length+":"+value;
				break;

			case "03":
				var imsi = utils.hexToText(value);
				if (imsi == "" || imsi == "Revision:1418B0" || imsi == "10.180.145.56")
					break;
				document.imsi = utils.hexToText(value);
				//document.imsiTLV = type+":"+length+":"+value;
				break;

			case "04":
				document.iccid = utils.hexToText(value).replace("f","");
				break;

			default:
				break;
		}

		index += 4 + length;
	}
	//mylogger.info("[DeviceInfo:" + jPacket.DevID + "] document:" + JSON.stringify(document));
	mongoProducer(document);
}


exports.processPacket = processPacket;


//sample packets
var obdGpsPacket = "284450359986782084003401001C020518014014195257890440205211306C0A12000000910020010050701D9031A7009244040C278380004100002200485D7029";
var gsensorPacket = "285340005592820095006301011000000100020F00388069834000388064837700398058836A003C805F835C00388058836300F2009483BC00828058836A00758058837700758061835500798058834700798058834700758061835C007A80588355007A80578347007A805C835C6C29";
var deviceInfoPacket = "285340005463960092004A01104D4B363130305F323031383039323872020F393131353334303030353436333936030F343034323737323932303438313035041438393931323732313732393230343831303538661A29";
// setInterval(function () {
// 	processPacket(obdGpsPacket);
// 	console.log("Sending");
// }, 10000000000);