var Server = require("../../server");
var utils = require("../../utils");
var xor = require('bitwise-xor');
var fs = require("fs");
var logger = require("../../logger.js").getLogger("thinkraceFota", "receiver.log");
var isUpgrade = true;
var parser = require("../thinkrace/parser");
var firmwareToUpgrade = "MK6000_20180118r";
//var firmwareToUpgrade="MK6000_20161026r";
var port=require("../../config")["thinkraceFota"].port;

new Server("thinkraceFota", port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var rawPacket = data.toString("hex").toUpperCase();
	rawPacket = escapePacketsRecv(rawPacket);
	logger.info("Received raw packet " + rawPacket);

	processPacket(rawPacket, socket);

});


function escapePacketsRecv(packet) {

	/* If there is no 3D in packet, return it no need to process for escape */
	if (packet.indexOf("3D") < 0)
		return packet;

	var escapePacket = "";

	for (var i = 0; i < packet.length; i += 2) {
		if (packet.substr(i, 2) === "3D") {
			escapePacket += utils.xorHex(packet.substr(i, 2), packet.substr(i + 2, 2));
			i += 2;
		} else
			escapePacket += packet.substr(i, 2);
	}
	return escapePacket.toUpperCase();
}

function processPacket(packet, socket) {

	//parse packet
	parser.processPacket(packet);

	var packetJson = {};
	packetJson.head = packet.substr(0, 2);
	packetJson.deviceId = packet.substr(2, 12);
	packetJson.command = packet.substr(14, 4);
	packetJson.contentLength = utils.hexToInt(packet.substr(18, 4));
	packetJson.content = packet.substr(22, packetJson.contentLength * 2);
	packetJson.checkSum = packet.substr(packet.length - 4, 2);
	packetJson.tail = packet.substr(packet.length - 2, 2);
	var content = packetJson.content;

	logger.info("Raw packet json : " + JSON.stringify(packetJson));

	if (isUpgrade) {
		var upgradePacket = createUpgradeNotificationPacket(packetJson.deviceId);
		logger.info("UPGRADE, Upgrade available. sending upgrade notification packet == " + upgradePacket);
		sendPacketToDevice(socket, upgradePacket);
		socket["firmwareToUpgrade"] = firmwareToUpgrade;
		isUpgrade = false;
	}

	switch (packetJson.command) {
		case "4081":
			if (content.substr(0, 2) == "01") {
				logger.info("UPGRADE, Upgrading device:" + packetJson.deviceId + " from:" + utils.hexToText(content.substr(2)) + " to:" + socket["firmwareToUpgrade"]);
				var firmwarePackage = createUpgradePacket(packetJson.deviceId, socket["firmwareToUpgrade"], 1);
				logger.info("UPGRADE, Sending 1st firmware package of " + socket["firmwareToUpgrade"] + ". Device: " + packetJson.deviceId);
				sendPacketToDevice(socket, firmwarePackage);
			}
			else
				logger.info("UPGRADE, Rejected upgrade device:" + packetJson.deviceId + " from:" + utils.hexToText(content.substr(2)) + " to:" + socket["firmwareToUpgrade"]);
			break;

		case "4082":
			var success = (content.substr(0, 2) == "01") ? true : false;
			var received = utils.hexToInt(content.substr(2, 4));
			var querying = utils.hexToInt(content.substr(6, 4));
			if (success)
				logger.info("UPGRADE, Firmware package " + received + " written. Querying for " + querying + ". Device: " + packetJson.deviceId);
			else
				logger.info("UPGRADE, Firmware package " + received + " not written. Querying for " + querying + ". Device: " + packetJson.deviceId);

			var firmwarePackage = createUpgradePacket(packetJson.deviceId, socket["firmwareToUpgrade"], querying);
			if (firmwarePackage) {
				logger.info("UPGRADE, Sending " + querying + " firmware package of " + socket["firmwareToUpgrade"] + ". Device: " + packetJson.deviceId);
				sendPacketToDevice(socket, firmwarePackage);
			}
			else {
				logger.info("UPGRADE, All packages of " + socket["firmwareToUpgrade"] + " are sent to " + packetJson.deviceId + ". Sending command to start upgradation.");
				sendPacketToDevice(socket, createStartUpgradePacket(packetJson.deviceId));
			}
			break;

		case "4084":
			if (content == "01")
				logger.info("UPGRADE, Starting upgrade " + packetJson.deviceId + " to" + socket["firmwareToUpgrade"]);
			else
				logger.info("UPGRADE, Not starting upgrade " + packetJson.deviceId + " to" + socket["firmwareToUpgrade"]);
			break;
		case "4085":
			// if (socket["firmwareToUpgrade"]) {
			// 	var upgradePacket = createUpgradePacket(packetJson.deviceId, socket["firmwareToUpgrade"], 1);
			// 	logger.info("Sending 1st batch of " + socket["firmwareToUpgrade"] + " to " + packetJson.deviceId + " packet:" + upgradePacket);
			// 	sendPacketToDevice(socket, upgradePacket);
			// }
			break;
		case "4086":
			if (content.substr(0, 2) == "01") {
				logger.info("UPGRADE, " + packetJson.deviceId + " is upgraded to " + socket["firmwareToUpgrade"] + ". Version:" + utils.hexToText(content.substr(2)));
				logger.info("UPGRADE, Sending factory reset packet to " + packetJson.deviceId);
				sendPacketToDevice(socket, createRestoreFactoryPacket(packetJson.deviceId));
			}
			else {
				logger.info("UPGRADE, " + packetJson.deviceId + " is not upgraded to " + socket["firmwareToUpgrade"] + ". Version:" + utils.hexToText(content.substr(2)));
			}
			break;
		default:
			return;
	}
}

function createStartUpgradePacket(deviceId) {
	var packet = deviceId;
	packet += "4004"; //command
	packet += "0001"; //content length
	packet += "01"; //start upgrade
	packet += computeChkSum(packet); //checksum
	packet = "28" + addEscapeSeq(packet) + "29";
	return packet;
}

function createRestoreFactoryPacket(deviceId) {
	var packet = deviceId;
	packet += "0004"; //command
	packet += "0001"; //content length
	packet += "01"; //restore factory
	packet += computeChkSum(packet); //checksum
	packet = "28" + addEscapeSeq(packet) + "29";
	return packet;
}

function createUpgradePacket(deviceId, firmwareVersion, batchNo) {
	var packet = deviceId;
	packet += "4002"; //command
	packet += "0200"; //content length
	packet += "01"; //write to deivce
	packet += (utils.intToHexStr(batchNo).length == 2) ? "00" + utils.intToHexStr(batchNo) : utils.intToHexStr(batchNo);//batch no in 2 bytes
	var firmwarePackage = getFirmwareBatch(firmwareVersion, batchNo);
	if (!firmwarePackage)
		return;
	packet += firmwarePackage;
	packet += GetCrc(firmwarePackage);
	packet += computeChkSum(packet); //checksum
	packet = "28" + addEscapeSeq(packet) + "29";
	return packet;
}

function addEscapeSeq(packet) {
	packet = packet.toUpperCase();
	var escPacket = "";
	for (var i = 0; i < packet.length; i += 2) {
		var byte = packet.substr(i, 2);

		if (byte == "5B") {
			escPacket += "3D66";
		}
		else if (byte == "5D") {
			escPacket += "3D60";
		}
		else if (byte == "3D") {
			escPacket += "3D00";
		}
		else if (byte == "2C") {
			escPacket += "3D11";
		}
		else if (byte == "29") {
			escPacket += "3D14";
		}
		else if (byte == "28") {
			escPacket += "3D15";
		}
		else {
			escPacket += byte;
		}
	}
	return escPacket;
}

function createNextFirmwareNoPacket(deviceId, firmwareVersion) {
	var packet = deviceId;
	packet += "4005"; //command
	packet += "0011"; //content length
	packet += "00"; //query from device
	packet += utils.textToHex(firmwareVersion);
	packet += computeChkSum(packet); //checksum
	packet = "28" + addEscapeSeq(packet) + "29";
	return packet;
}

function createUpgradeNotificationPacket(deviceId) {
	var packet = deviceId;
	packet += "4001"; //command
	packet += "0013"; //content length
	packet += "00"; //write
	packet += utils.textToHex(firmwareToUpgrade); //firmware version to upgrade
	packet += utils.intToHexStr(getFirmwareBatchCount(firmwareToUpgrade)); //firmware batch count
	packet += computeChkSum(packet); //checksum
	packet = "28" + addEscapeSeq(packet) + "29";
	return packet;
}


function computeChkSum(frame) {

	var ckSum = xor(new Buffer('00', 'hex'), new Buffer('00', 'hex'))

	for (var i = 0; i < frame.length; i++) {
		var tmp = frame[i] + frame[i + 1];
		i = i + 1;
		// console.log(tmp);
		ckSum = xor(ckSum, new Buffer(tmp, 'hex'));
		// console.log(ckSum);
	}
	return ckSum.toString('hex');
}

function sendPacketToDevice(socket, packet) {

	if (socket.write(new Buffer(packet, 'hex')))
		logger.info("Send to device :" + packet);
	else
		logger.info("Send to device failed :" + packet);
}

function getFirmwareBatchCount(firmwareVersion) {
	var path = require('path').basename(__dirname) + "/" + firmwareVersion + ".bin";
	var binary = fs.readFileSync(path);
	var size = binary.length;
	return Math.ceil(size / 508);
}

function getFirmwareBatch(firmwareVersion, batchNo) {
	var path = require('path').basename(__dirname) + "/" + firmwareVersion + ".bin";
	var binary = fs.readFileSync(path);
	var batch = binary.toString("hex", (batchNo - 1) * 508, batchNo * 508);
	if (batch.length == 0)
		return undefined;

	if (batch.length < 508 * 2) {
		for (var i = (batch.length / 2) + 1; i <= 508; i++)
			batch += "FF";
	}
	return batch;
}

var crctable = [ //reversed, 8-bit, poly=0x07
	0x00, 0x91, 0xE3, 0x72, 0x07, 0x96, 0xE4, 0x75, 0x0E, 0x9F, 0xED, 0x7C, 0x09, 0x98, 0xEA, 0x7B,
	0x1C, 0x8D, 0xFF, 0x6E, 0x1B, 0x8A, 0xF8, 0x69, 0x12, 0x83, 0xF1, 0x60, 0x15, 0x84, 0xF6, 0x67,
	0x38, 0xA9, 0xDB, 0x4A, 0x3F, 0xAE, 0xDC, 0x4D, 0x36, 0xA7, 0xD5, 0x44, 0x31, 0xA0, 0xD2, 0x43,
	0x24, 0xB5, 0xC7, 0x56, 0x23, 0xB2, 0xC0, 0x51, 0x2A, 0xBB, 0xC9, 0x58, 0x2D, 0xBC, 0xCE, 0x5F,
	0x70, 0xE1, 0x93, 0x02, 0x77, 0xE6, 0x94, 0x05, 0x7E, 0xEF, 0x9D, 0x0C, 0x79, 0xE8, 0x9A, 0x0B,
	0x6C, 0xFD, 0x8F, 0x1E, 0x6B, 0xFA, 0x88, 0x19, 0x62, 0xF3, 0x81, 0x10, 0x65, 0xF4, 0x86, 0x17,
	0x48, 0xD9, 0xAB, 0x3A, 0x4F, 0xDE, 0xAC, 0x3D, 0x46, 0xD7, 0xA5, 0x34, 0x41, 0xD0, 0xA2, 0x33,
	0x54, 0xC5, 0xB7, 0x26, 0x53, 0xC2, 0xB0, 0x21, 0x5A, 0xCB, 0xB9, 0x28, 0x5D, 0xCC, 0xBE, 0x2F,
	0xE0, 0x71, 0x03, 0x92, 0xE7, 0x76, 0x04, 0x95, 0xEE, 0x7F, 0x0D, 0x9C, 0xE9, 0x78, 0x0A, 0x9B,
	0xFC, 0x6D, 0x1F, 0x8E, 0xFB, 0x6A, 0x18, 0x89, 0xF2, 0x63, 0x11, 0x80, 0xF5, 0x64, 0x16, 0x87,
	0xD8, 0x49, 0x3B, 0xAA, 0xDF, 0x4E, 0x3C, 0xAD, 0xD6, 0x47, 0x35, 0xA4, 0xD1, 0x40, 0x32, 0xA3,
	0xC4, 0x55, 0x27, 0xB6, 0xC3, 0x52, 0x20, 0xB1, 0xCA, 0x5B, 0x29, 0xB8, 0xCD, 0x5C, 0x2E, 0xBF,
	0x90, 0x01, 0x73, 0xE2, 0x97, 0x06, 0x74, 0xE5, 0x9E, 0x0F, 0x7D, 0xEC, 0x99, 0x08, 0x7A, 0xEB,
	0x8C, 0x1D, 0x6F, 0xFE, 0x8B, 0x1A, 0x68, 0xF9, 0x82, 0x13, 0x61, 0xF0, 0x85, 0x14, 0x66, 0xF7,
	0xA8, 0x39, 0x4B, 0xDA, 0xAF, 0x3E, 0x4C, 0xDD, 0xA6, 0x37, 0x45, 0xD4, 0xA1, 0x30, 0x42, 0xD3,
	0xB4, 0x25, 0x57, 0xC6, 0xB3, 0x22, 0x50, 0xC1, 0xBA, 0x2B, 0x59, 0xC8, 0xBD, 0x2C, 0x5E, 0xCF
];

function GetCrc(data) {
	// console.log("In");
	var val = 0xFF;
	for (var i = 0; i < data.length; i += 2) {
		val = crctable[val ^ "0x" + data.substr(i, 2)];
		// console.log("xxx", val ^ "0x" + data.substr(i, 2));
	}
	return utils.intToHexStr(0xFF - val);
}

//console.log(0xFF ^ "0xAA");
// for (var i = 1; i <= getFirmwareBatchCount("MK6000_20180118R") + 1; i++)
// 	console.log(i + " firmware ", createUpgradePacket("694036106688", "MK6000_20180118R", i));


console.log(getFirmwareBatch("MK6000_20180118r", 2));