// import required modules
var config = require("../config").mongo;
var MongoClient = require("./mongo-client");
var logger = require("../logger.js");
var mylogger = logger.getLogger("thinkraceLoadTest", "mongo-batch.log");
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;
var mongoThroughputRecord = new ThroughputRecord("mongoThroughputRecord",
	new Date, 0, require("../config").ThroughputCalculator.threshold);

var insertedBatch = 0;
var filledBatch = 0;

// instantiates mongo batch
var MongoBatch = function (collection) {
	this.batchSize = config.batchSize;
	this.mongo = new MongoClient().connect();
	this.batch = [];
	this.index = 0;
	this.collection = collection;
}

// define methods
MongoBatch.prototype = {
	add: function (document) {

		if (!document) {
			mylogger.error("(add)-document is undefined so excluding it.");
			return;
		}

		//mylogger.debug("(add)-added into batch", JSON.stringify(document));
		// add document into batch
		this.batch[this.index++] = document;
		// if batch is full then insert batch in mongo, reset index and batch
		if (this.index >= this.batchSize) {
			filledBatch++;
			mylogger.debug("FILLED-BATCH filledBatch:" + filledBatch
				+ " insertedBatch:" + insertedBatch + " batch contains " + this.batch.length + " documents");

			var oldBatch = [];
			for (var i = 0; i < this.batch.length; i++) {
				oldBatch[i] = this.batch[i];
			}

			this.mongo.insertMany(this.collection, oldBatch, function (err, result) {
				insertedBatch++;
				mylogger.debug("INSERTED-BATCH filledBatch:" + filledBatch
					+ " insertedBatch:" + insertedBatch + " batch contains " + oldBatch.length + " documents");
				if (err) {
					mylogger.error("(add)-error while inserting batch:" + err);
					mylogger.error(err);
					throw Error(err);
				}
				for (var i = 0; i < oldBatch.length; i++) {
					//mylogger.debug("(INSERTED)-inserted document", JSON.stringify(oldBatch[i]));
					mongoThroughputRecord.increment();
				}
			});
			this.index = 0;
			this.batch = [];
		}
	}
}

module.exports = MongoBatch;
