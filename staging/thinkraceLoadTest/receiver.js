var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
	// Fork workers.
	for (var i = 0; i < numCPUs; i++) {
		cluster.fork();
	}

	Object.keys(cluster.workers).forEach(function (id) {
		console.log("I am running with ID : " + cluster.workers[id].process.pid);
	});

	cluster.on('exit', function (worker, code, signal) {
		console.log('Worker ' + worker.process.pid + ' died.');
	});
	return;
}

var heapdump = require('heapdump');
const
	https = require('https');

var Server = require("./server.js");

var parser = require("./parser");

var config = require("../config")["thinkraceLoadTest"];
var utils = require("../utils");
var xor = require('bitwise-xor');

var port = config["port"];
var ThroughputRecord = require("../ThroughputRecord.js").ThroughputRecord;

var configurator = require("../config")["Configurator"];
var frequncy = configurator["frequency"];

var configCmds = ["SWITCH_MAIN_SERVER", "WIFI_SWITCH", "WIFI_ENCRYPTION_NOPWD", "WIFI_ENCRYPTION_WPA_TKIP", "WIFI_ENCRYPTION_WPA2_PSK", "WIFI_NAME", "CLEAN_FLASH"];

var HashMap = require('hashmap');
var mapOfConfigurations = new HashMap();

//setInterval(checkConfiguration, frequncy);

var apiDetails = require("../config")["API"];

var optionsHost = apiDetails["DNS"];
var optionsPort = apiDetails["port"];
var optionsPath = '/Cariq/devices/admin/feedback/after/';

var logger = require("../logger.js");
var mylogger = logger.getLogger("thinkraceLoadTest", "receiver.log");

//mylogger.debug("(Server)- Started on port :" + port);
//mylogger.info("Options Host - Port - Path :" + optionsHost + "-" + optionsPort + "-" + optionsPath);
var beginDate = new Date();
// beginDate.setHours(beginDate.getHours() - 5); // Need to remove this code
// beginDate.setMinutes(beginDate.getMinutes() - 30); // Need to remove this
// code
var beginDateTime = utils.covertDateToString(beginDate).replace(/ /g, "%20");
//mylogger.info("beginDateTime:" + beginDateTime);
var updatedOn = beginDateTime;

var options = {
	host: '',
	port: '',
	path: '',
	// authentication headers
	headers: {
		'Authorization': 'Basic dGhpbmtkajowZGQ1NDkzOGQ2ZTllMzc5MzJlOTcxNTk1NDRiODMyNg=='
	}
};

function getConfigJSON() {

	var configJSON = {};

	options.path = optionsPath + updatedOn;
	options.host = optionsHost;
	options.port = optionsPort;
	////mylogger.debug("(getConfigJSON)-optionsHost:" + options.host + " " + options.port + " " + options.path);
	request = https.get(options, function (res) {
		var body = "";

		res.on('data', function (data) {
			body += data;
		});

		res.on('end', function () {
			if (res.statusCode == 200) {
				configJSON = JSON.parse(body);

				//mylogger.debug("(getConfigJSON)-configJSON:" + JSON.stringify(configJSON));

				if (configJSON.length > 0) {
					//mylogger.debug("(getConfigJSON)-configJSON:" + JSON.stringify(configJSON));
					// PUSH in MAP

					for (var myKey in configJSON) {
						// console.log("--------------");
						var subJson = configJSON[myKey];
						// console.log("\n DEV:"
						// + subJson.deviceId);
						// console.log("\n CMD:"
						// + subJson.command);
						var paramJson = subJson.parameter;

						/*
						 * if (paramJson.SSID) console.log("SSID:" +
						 * paramJson.SSID);
						 * 
						 * if (paramJson.SERVER_NAME) console.log("SERVER_NAME:" +
						 * paramJson.SERVER_NAME);
						 * 
						 * if (paramJson.PORT) console.log("PORT:" +
						 * paramJson.PORT);
						 * 
						 * if (paramJson.APN) console.log("APN:" +
						 * paramJson.APN);
						 * 
						 * if (paramJson.WIFI_ONOFF) console.log("WIFI_ONOFF:" +
						 * paramJson.WIFI_ONOFF);
						 * 
						 * if (paramJson.PWD) console.log("PWD:" +
						 * paramJson.PWD);
						 */

						var myJson = {};
						myJson.id = subJson.deviceId;
						// myJson.id =
						// "323022528993"; //
						// This is HACK for
						// testing....
						myJson.cmdname = subJson.command;
						myJson.cmdparam = "";

						switch (subJson.command) {
							case "WIFI_NAME":
								myJson.cmdparam = paramJson.SSID;
								break;
							case "SWITCH_MAIN_SERVER":
								myJson.cmdparam = paramJson.SERVER_NAME + "," + paramJson.PORT + "," + paramJson.APN;
								break;
							case "WIFI_SWITCH":
								myJson.cmdparam = paramJson.WIFI_ONOFF;
								break;
							case "WIFI_ENCRYPTION_WPA_TKIP":
							case "WIFI_ENCRYPTION_WPA2_PSK":
								myJson.cmdparam = paramJson.PWD;
								break;
						}
						//mylogger.info("(getConfigJSON)-Inserting this JSON in MAP: " + JSON.stringify(myJson));
						mapOfConfigurations.set(myJson.id + myJson.cmdname, myJson);
					}
					// End : PUSH in MAP
				} // else {
				// //console.log(myScriptName+
				// "" + new Date + " : No NEW
				// Configuration to Apply !!!");
				// }
			} else {
				//mylogger.error("(getConfigJSON)-Non 200 statusCode,ANOMALY,GETCONFIG_ERROR," + res.statusCode);
			}
		})
		res.on('error', function (e) {
			//mylogger.error("(getConfigJSON)-Got Error for request,ANOMALY,GETCONFIG_ERROR," + e.message);
		});
	});

	request.on('error', function (e) {
		//mylogger.error("(getConfigJSON)-Problem with request,ANOMALY,GETCONFIG_PROBLEM," + e.message);
	});

	// console.log("\n Options :" + options.path);
	var d2 = new Date();
	//d2.setHours(d2.getHours() - 5); // Need to remove this code
	//d2.setMinutes(d2.getMinutes() - 30); // Need to remove this code
	var currDateTime = utils.covertDateToString(d2).replace(/ /g, "%20");
	// console.log("currDateTime:" + currDateTime);
	// console.log("\n Options :" + options.path);
	updatedOn = currDateTime;

}

function checkConfiguration() {
	////mylogger.info("(checkConfiguration)-Get Configuration JSON from Platform");
	//var configJSON = {};
	getConfigJSON();
}

// var TRIP_COMMAND = "3088";
// var DTC_ERROR_COMMAND = "3087";
// var VIN_COMMAND = "3084";

var throughputConfig = require("../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];

var receiverThroughputRecord = new ThroughputRecord("receiverThroughputRecord", new Date, 0, threshold);

/*
 * Sample Error Query Looks like this ""28 601603080423 1001 0001 00 4A 29" 28 +
 * DeviceID + 1001 + length + HEX(ciqtrvt200.mycariq.com,49999,airtelgprs.com) +
 * CheckSUM + 29
 */
function constructSetDN(devID, serverConfig) {

	var tmpBuffer = new Buffer(serverConfig, 'ascii');
	var hexServerConfig = "01" + tmpBuffer.toString('hex');
	// console.log("Ascii:" + serverConfig);
	// console.log("Hex:" + hexServerConfig);

	var lenServerConfig = hexServerConfig.length / 2;
	// console.log("Length Decimal ="+ lenServerConfig );
	var hexLenServerConfig = Number(lenServerConfig).toString(16);
	// console.log("Length Hex ="+ hexLenServerConfig);

	var sQuery = devID;
	sQuery += "1001";
	sQuery += "00" + hexLenServerConfig;
	sQuery += hexServerConfig;
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructSetDN)-DomainName SET :" + sQuery);

	return sQuery;
}

function sendQueryToDevice(socket, sQuery) {
	//if (socket.write(new Buffer(sQuery, 'hex')))
		//mylogger.info("(sendQueryToDevice)-sQuery: write done,ACTION,WRITE_TO_DEVICE_PASS," + sQuery);
	//else
		//mylogger.info("(sendQueryToDevice)-sQuery: write fail,ANOMALY,WRITE_TO_DEVICE_FAIL," + sQuery);
}

/*
 * Compute CheckSum using XOR Addition of all bytes
 */
function computeChkSum(frame) {

	var ckSum = xor(new Buffer('00', 'hex'), new Buffer('00', 'hex'))

	for (var i = 0; i < frame.length; i++) {
		var tmp = frame[i] + frame[i + 1];
		i = i + 1;
		// console.log(tmp);
		ckSum = xor(ckSum, new Buffer(tmp, 'hex'));
		// console.log(ckSum);
	}
	return ckSum.toString('hex');
}

/*
 * Sample Error Query Looks like this "28 601603080423 0002 0001 00 6F 29" 28 +
 * DeviceID + 3004 + 0001 + 00 (for Query Mode) + CheckSum + 29
 */
function constructVINQuery(jPacket) {

	var sQuery = jPacket.substr(2, 12);
	sQuery += "3004";
	sQuery += "0001";
	sQuery += "00";
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructVINQuery)-VIN Query :" + sQuery);

	return sQuery;
}

/*
 * Sample WifiName Packet looks like 28 301404140001 100C 0009 01
 * 3030303030303030 1B 29 //set WIFI name is 3030303030303030 ( in Hex) 28 +
 * DeviceID + cmd (i.e. 100C) + length(i.e. 0009) + mode (i.e. 01) + 8 Byte Name +
 * CheckSum + 29
 */
function constructWifiName(devID, wifiName) {
	var sQuery = devID;
	var length = "0009";
	var cmd = "100C";
	var mode = "01";

	sQuery += cmd;
	sQuery += length;
	sQuery += mode;
	var tmpBuffer = new Buffer(wifiName, 'ascii');
	var hexWifi = tmpBuffer.toString('hex');
	//mylogger.info("(constructWifiName)-HEX wifiName:" + hexWifi);
	sQuery += hexWifi;
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructWifiName)-WiFiSetName :" + sQuery);

	return sQuery;
}
/*******************************************************************************
 * Sample Wifi Encryption Packet like this 28 301404140001 100B 0002 01 00 1B 29
 * //set WIFI encryption is NO_PASSWORD 28 301404140001 100B 000A 01 01
 * 3030303030303030 1B 29 //set WIFI encryption is WPA_TKIP,password is 00000000
 * 28 301404140001 100B 000A 01 02 3030303030303030 1B 29 //set WIFI encryption
 * is WPA2-PSK,password is 00000000
 * 
 * 28 + DeviceID + 100B + length + 01 (for Setting Mode) + 00/01/02 ( As per cmd
 * mode) + 8 Byte Password + CheckSum + 29
 ******************************************************************************/
function constructWifiEncryption(devID, pwd, encryptionCmd) {

	var sQuery = devID;
	var lenght = null;
	var cmd = null;

	switch (encryptionCmd) {
		case "WIFI_ENCRYPTION_NOPWD":
			length = "0002";
			cmd = "00";
			break;

		case "WIFI_ENCRYPTION_WPA_TKIP":
			length = "000A";
			cmd = "01";
			break;

		case "WIFI_ENCRYPTION_WPA2_PSK":
			length = "000A";
			cmd = "02";
			break;
	}

	sQuery += "100B";
	sQuery += length;
	sQuery += "01";
	sQuery += cmd;

	var tmpBuffer = new Buffer(pwd, 'ascii');
	var hexPwd = tmpBuffer.toString('hex');
	//mylogger.info("(constructWifiEncryption)-HEX pwd:" + hexPwd);

	sQuery += hexPwd;

	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructWifiEncryption)-WiFiSetEncryption :" + sQuery + "Mode :" + encryptionCmd);

	return sQuery;
}

/*
 * Sample Wifi Hotspot OpenClose Packet like this 28 301404140001 1009 0002 01
 * 01 1B 29 // ON 28 301404140001 1009 0002 01 00 1B 29 // OFF 28 + DeviceID +
 * 1009 + 0002 + 01 (for Setting Mode) + ON/OFF + CheckSum + 29
 */
function constructWifiHostspotOpenClosePacket(devID, onOrOff) {

	var sQuery = devID;
	sQuery += "1009";
	sQuery += "0002";
	sQuery += "01";
	if (onOrOff)
		sQuery += "01";
	else
		sQuery += "00";
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructWifiHostspotOpenClosePacket)-WiFiHotspotOPenClose :" + sQuery);

	return sQuery;
}

/*
 * Sample Error Query Looks like this "28 601603080423 0002 0001 00 03 29" 28 +
 * DeviceID + 0002 + 0001 + 00 (for Query Mode) + CheckSum + 29
 */
function constructFirmwareVersionQuery(jPacket) {

	var sQuery = jPacket.substr(2, 12);
	sQuery += "0002";
	sQuery += "0001";
	sQuery += "00";
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructFirmwareVersionQuery)-FirmwareVersion Query :" + sQuery);

	return sQuery;
}

/*
 * Sample Error Query Looks like this "28 601603080423 3007 0001 00 6C 29" 28 +
 * DeviceID + 3007 + 0001 + 00 (for Query Mode) + CheckSum + 29
 */
function constructErrorQuery(jPacket) {

	var sQuery = jPacket.substr(2, 12);
	sQuery += "3007";
	sQuery += "0001";
	sQuery += "00";
	sQuery += computeChkSum(sQuery);

	sQuery = "28" + sQuery + "29";
	//mylogger.info("(constructErrorQuery)-DTC Query :" + sQuery);

	return sQuery;
}

/*
 * Sample Clean flash command Looks like this "28 301404140001 0014 0001 01 30 29" 
 * 28 + DeviceID + 0014 + 0001 + 01 (for clean) + CheckSum + 29
 */
function constructCleanFlash(devID) {
	var sQuery = devID;
	sQuery += "0014";
	sQuery += "0001";
	sQuery += "01";
	sQuery += computeChkSum(sQuery);
	sQuery = "28" + sQuery + "29";

	//mylogger.info("(constructCleanFlash)-CleanFlash Query :" + sQuery);

	return sQuery;
}

/*
 * Remove escape sequnce
 */
function escapePacketsRecv(packet) {

	/* If there is no 3D in packet, return it no need to process for escape */
	if (packet.indexOf("3D") < 0)
		return packet;

	var escapePacket = "";

	for (var i = 0; i < packet.length; i += 2) {
		if (packet.substr(i, 2) === "3D") {
			escapePacket += utils.xorHex(packet.substr(i, 2), packet.substr(i + 2, 2));
			i += 2;
		} else
			escapePacket += packet.substr(i, 2);
	}
	return escapePacket.toUpperCase();
}

new Server("thinkRace", port).start(function (err, data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		//mylogger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		//mylogger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		//mylogger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	//data contains ping then return response as pong
	if (data.toString() == "cq_ping")
		socket.write(new Buffer("cq_pong", "utf8"));

	var packet = new Buffer(data, "utf8").toString("hex").toUpperCase();
	packet = escapePacketsRecv(packet);// packet.replace(/3D15/g,"28").replace(/3D14/g,"29");
	//mylogger.debug("(Server) Received Raw Packet (Writing to Q),RAW_DATA,DATA," + packet);
	receiverThroughputRecord.increment();
	//	producer.produce({
	//		packet : packet
	//	});

	processPacket(packet);

	/*
	 * var bgDevID = packet.substr(2,12);
	 * 
	 * if (bgDevID == "445032246824" || bgDevID == "445032320439" ) // This is
	 * Mandar-Vishal's device { console.log(" ----------Reconfiguring " +
	 * bgDevID + "to BAGIC production batrvt200.mycariq.com -----");
	 * sendQueryToDevice(socket,
	 * constructSetDN(bgDevID,"batrvt200.mycariq.com,49999,airtelgprs.com"));
	 * console.log("------------BAGIC Reconfiguration DONE !!! ------"); return ; }
	 */
	switch (packet.substr(14, 4)) {
		case "3089":
			sendQueryToDevice(socket, constructErrorQuery(packet));
			break;

		case "3088":
			sendQueryToDevice(socket, constructVINQuery(packet));
			break;

		case "3084":
			sendQueryToDevice(socket, constructFirmwareVersionQuery(packet));
			break;
	}

	var devID = packet.substr(2, 12);

	// //mylogger.debug("(Server) Received DeviceID" + devID);

	var cmdsLength = configCmds.length;
	// Check for all possible deviceID-command combination , get out of
	// this loop once found Configuration....
	for (var iTest = 0; iTest < cmdsLength; iTest++) {
		var configObject = mapOfConfigurations.get(devID + configCmds[iTest]);
		if (configObject) {
			//mylogger.info("(Server)-We need to apply this configuration,ACTION,APPLY_CONFIG," + JSON.stringify(configObject));
			//mylogger.info("(Server)-CMD = " + configObject.cmdname);
			//mylogger.info("(Server)-CMDPARAM = " + configObject.cmdparam);
			mapOfConfigurations.remove(devID + configCmds[iTest]);

			switch (configObject.cmdname) {
				case "SWITCH_MAIN_SERVER":
					sendQueryToDevice(socket, constructSetDN(devID, configObject.cmdparam));
					break;

				case "WIFI_SWITCH":
					sendQueryToDevice(socket, constructWifiHostspotOpenClosePacket(devID, configObject.cmdparam));
					break;

				case "WIFI_ENCRYPTION_NOPWD":
					sendQueryToDevice(socket, constructWifiEncryption(devID, configObject.cmdparam, "WIFI_ENCRYPTION_NOPWD"));
					break;

				case "WIFI_ENCRYPTION_WPA_TKIP":
					sendQueryToDevice(socket, constructWifiEncryption(devID, configObject.cmdparam, "WIFI_ENCRYPTION_WPA_TKIP"));
					break;

				case "WIFI_ENCRYPTION_WPA2_PSK":
					sendQueryToDevice(socket, constructWifiEncryption(devID, configObject.cmdparam, "WIFI_ENCRYPTION_WPA2_PSK"));
					break;

				case "WIFI_NAME":
					sendQueryToDevice(socket, constructWifiName(devID, configObject.cmdparam));
					break;

				case "CLEAN_FLASH":
					sendQueryToDevice(socket, constructCleanFlash(devID));
					break;

				default:
					//mylogger.error("(Server),ANOMALY,UNDEFINED_CONFIG,This configuration command is YET to implement");
					break;
			}
			break; // One Configuration at one time :)
		}
	}
});

function processPacket(packet) {
	var packetToSend = "";
	for (var i = 0; i < packet.length; i = i + 2) {
		var separator = packet.substr(i, 4);
		if (separator == "2928") {
			packetToSend += "29";
			//mylogger.debug("separedted packet : " + packetToSend);
			parser.processPacket(packet);
			packetToSend = "";
		}
		else
			packetToSend += packet.substr(i, 2);
	}

	if (packetToSend.length > 0) {
		//mylogger.debug("separedted packet : " + packetToSend);
		parser.processPacket(packet);
	}
}


//processPacket("281000000003842084003401073E280818134345182945040734909017170C090800001F94002001005C812A3B1BAE54906000000D8800003100004E01227CB229281000000003842084003401073E2808181343501829485207349103571A08090800001F94002001005C811D2E1DAE0A906000000D8800003100000600177D2429281000000003842084003401073E2808181343551829489507349104771A05090800001F94002001005C811CF31DAE14906000000D8800003100000C002A7E3E29281000000003842084003401073E2808181344001829493607349105371A03090800001F94002001005C811BE01DAE0B906000000D8800003100000700187FE729281000000003842084003401073E28081813440518294975073491055719B3090800001F94002001005C811AE41BAE0A906000000D880000310000050015808429");
