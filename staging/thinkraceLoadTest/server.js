var net = require("net");
var utils = require("../utils");

var logger = require("../logger.js");
var mylogger = logger.getLogger("server", "server.log");


// constructor
var Server = function(name, port) {
	this.name = name;
	this.port = port;
}

Server.prototype = {

	// create and starts server and calls callback by error and data
	start : function(callback) {

		// creates server by port
		net.createServer(
				function(socket) {
					var establishedOn = utils.getServerTimeStamp();
					var id;
					// calls callback by error and destroys socket
					socket.on("error", function(error) {
						var response = callback(error, undefined);
						// if response is returned then write into socket
						if (response)
							socket.write(response);
						socket.destroy();
					});

					// calls callback by data and ends socket
					socket.on("data", function(data) {
						var response = callback(undefined, data, id, socket);
						// if response is returned then write into socket
						if (response) {
							//console.log(myScriptName + new Date + ": " + "========Response=======");
							//console.log(myScriptName + new Date + ": " + response);
							//console.log(myScriptName + new Date + ": " + "=======================");
							if (response["message"]) {
								mylogger.debug("(createServer)-Writing back to Device :" + response["message"]);
								socket.write(response["message"]);
							}
							if (response["socket_action"]
									&& response["socket_action"] === "close") {
								mylogger.debug("(createServer)-Closing socket");
								socket.end();
							}
							if (response["id"])
								id = response["id"];
						}
					});

					socket.setTimeout(600000);

					socket.on("timeout", function() {
						var timedOutOn = utils.getServerTimeStamp();
						mylogger.debug("(createServer)-Established on :" + establishedOn + " Timedout on :" + timedOutOn);
						socket.destroy();
					});

				}).listen(this.port);
		mylogger.debug("(createServer)-Server started on " + this.port);
	}
}

module.exports = Server;
