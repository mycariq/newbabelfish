var utils = require("../../utils");
var origin = require("os").hostname();

function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-transight";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function parseTimestamp(time) {
    if (!time)
        return;

    return new Date(time.concat(" UTC"));
}

function parseLocation(latitude, longitude) {
    var latitude = parseFloat(latitude);
    var longitude = parseFloat(longitude);

    return utils.getGeoJson(latitude, longitude, true);
}

exports.addDefaults = addDefaults;
exports.parseTimestamp = parseTimestamp;
exports.parseLocation = parseLocation;
