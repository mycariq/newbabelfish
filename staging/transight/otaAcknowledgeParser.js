var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class LocationParser {
    constructor() {
    }

    parse(data) {
        console.log("input: ", data );
        var document = { type: "data", subtype: "otaAcknowledge" };

        document.vendorId = data.VD;
        document.firmwareVersion = data.FV;
        document.ackMode = (data.ak == 1) ? "ACK" : "NACK";
        document.isValid = (data.v == "a");
        document.backlog = (data.PS == "H");

        document.location = helper.parseLocation(data.l, data.g);
        document.timestamp = helper.parseTimestamp(data.T);
        document.speed = parseInt(data.s);
        document.voltage = parseInt(data.MV);

        return document;
    }
}

module.exports = LocationParser;