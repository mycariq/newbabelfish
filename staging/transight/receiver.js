var Server = require("../../server");
var logger = require("../../logger.js").getLogger("transight", "receiver.log");
var parser = new (require("./packetParser"))();
var utils = require("../../utils");

new Server("transight", require("../../config.json").transight.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var inputData = data.toString("utf-8");
	logger.info("Received raw packet : " + inputData);
	
	var packets = inputData.replace(/},{/g, "}SEPERATOR{").split("SEPERATOR");
	packets.forEach(input => {
		if (input.startsWith('[{'))
			input = input.replace("[{", "{");

		if (input.endsWith('}]'))
			input = input.replace("}]", "}");

		if (input.startsWith('{') && input.endsWith('}')) {
			var packet = JSON.parse(input);

			// for batch packets not getting device id, firmware verion, vendor id, acknowledge
			// getting this data from first packet and adding this data into remaining packet
			if (packet.t) {
				socket.t = packet.t; socket.VD = packet.VD; socket.FV = packet.FV;
			}

			if (!packet.t) {
				packet.t = socket.t; packet.VD = socket.VD; packet.FV = socket.FV;
			}

			processPacket(socket, JSON.stringify(packet));
		} else {
			logger.info("invalid packet format : " + input);
		}
	});
});

function processPacket(socket, packet) {

	//validate packet
	if (!isValidPacket(packet))
		return;

	// parse input json
	packet = JSON.parse(packet);

	var command = (!packet.OTA) ? 1 : 2;
	parser.parse(command, packet.t, packet);

	// send acknowledgement to device
	// var acknowledge = parseInt(packet.ak);
	// if (acknowledge == 1) {
	// sendPacketResponse(socket, response);
	// }
}

function sendPacketResponse(socket, response) {
	logger.info("Sending response : " + response);
	if (!socket)
		return;

	//socket.write(new Buffer(response, "hex"));
}

function isValidPacket(packet) {
	if (!packet) {
		logger.info("invalid packet");
		return false;
	}

	return true;
}

var alert = "{\"t\":\"860697058538549\",\"VD\":\"TRANSI\",\"FV\":\"TSFWAPPXXXX\",\"ak\":0,\"AD\":25,\"PS\":\"L\",\"v\":\"v\",\"T\":\"2021-05-15 15:30:52\",\"l\":\"+000.000000\",\"g\":\"+000.000000\",\"s\":\"000.00\",\"CG\":\"000.00\",\"LC\":\"E484\",\"CD\":\"11464\",\"SN\":\"0\",\"HD\":\"0\",\"o\":\"0.000\",\"MC\":\"404\",\"MN\":\"x95\",\"MV\":\"013.6\",\"DI\":\"1110\",\"DO\":\"1000\",\"AIN\":[113,94,0],\"F\":128.4,\"SID\":1,\"SDA\":\"F=1630T=00.0\",\"TP\":26.4,\"HM\":56.3,\"ms\":\"000000\",\"SS\":\"19\",\"is\":1,\"i\":1,\"p\":1,\"FN\":\"6\",\"VM\":\"M\"}";
//processPacket({}, alert);