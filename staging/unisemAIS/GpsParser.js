var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(packet) {
        var document = {
            owner: packet[7],
            vendorId: packet[2],
            firmwareVersion: -packet[3],
            packetType: packet[4],
            alertId: packet[5],
            packetStatus: packet[6],
            imei: packet[7],
            registrationNo: packet[8],
            gpsFix: packet[9],
            timestamp: helper.parseTimestamp(packet[10], packet[11], packet[12], packet[13], packet[14], packet[15]),
            location: helper.parseLocation(packet[16], packet[17], packet[18], packet[19]),
            speed: parseFloat(packet[20]),
            degree: packet[21],
            noOfSattelite: packet[22],
            altitude: packet[23],
            pdop: packet[24],
            hdop: packet[25],
            networkOperator: packet[26],
            ignition: packet[27],
            powerStatus: packet[28],
            voltage: parseFloat(packet[29]),
            internalBatteryVoltage: packet[30],
            emergencyStatus: packet[31],
            tamperAlert: packet[32],
            gsmSignalStrength: packet[33],
            mcc: packet[34],
            mnc: packet[35],
            lac: packet[36],
            cellId: packet[37],
            nmr: packet[38],
            lac: packet[39],
            gsmSignalStrength: packet[40],
            cellId: packet[41],
            lac: packet[42],
            gsmSignalStrength: packet[43],
            cellId: packet[44],
            lac: packet[45],
            gsmSignalStrength: packet[46],
            lac: packet[47],
            gsmSignalStrength: packet[48],
            cellId: packet[49],
            digitalInputStatus: packet[50],
            digitalOutputStatus: packet[51],
            frameNo: packet[52]
        };

        addDocumentType(document, packet[4], packet[5]);

        return document;
    }
}

function addDocumentType(document, packetType, alertType) {
    switch (parseInt(alertType)) {
        case 1:
        case 2:
            document.type = "data";
            document.subtype = "gps";
            break;

        case 3:
            document.type = "alert";
            document.subtype = "pullOutReminder";
            break;

        case 4:
            document.type = "alert";
            document.subtype = "lowBatteryVoltageInternal";
            break;

        case 5:
            document.type = "alert";
            document.subtype = "lowBatteryVoltageResolvedInternal";
            break;

        case 6:
            document.type = "alert";
            document.subtype = "plugIn";
            break;

        case 7:
            document.type = "alert";
            document.subtype = "ignitionOn";
            break;

        case 8:
            document.type = "alert";
            document.subtype = "ignitionOff";
            break;

        case 9:
            document.type = "alert";
            document.subtype = "gpsBoxOpened";
            break;

        case 10:
            document.type = "alert";
            document.subtype = "emergencyStateOn";
            break;

        case 11:
            document.type = "alert";
            document.subtype = "emergencyStateOff";
            break;

        case 12:
            document.type = "alert";
            document.subtype = "overTheAirParamChanged";
            break;

        case 13:
            document.type = "alert";
            document.subtype = "rapidDeceleration";
            break;

        case 14:
            document.type = "alert";
            document.subtype = "suddenSpeededUp";
            break;

        case 15:
            document.type = "alert";
            document.subtype = "sharpTurn";
            break;

        case 16:
            document.type = "alert";
            document.subtype = "tamper";
            break;

        default:
            break;
    }
}

module.exports = GpsParser;