var utils = require("../../utils");
var helper = require("./helper");
'use strict'
class LoginParser {
    constructor() {

    }

    parse(packet) {
        var document = {
            imei: packet[4],
            type: "data",
            subtype: "heartbeat",
            timestamp: new Date(),
            vendorId: packet[2],
            firmwareVersion: packet[3],
            imei: packet[4],
            batteryPercentage: packet[5],
            lowBatteryThresholdValue: packet[6],
            memoryPercentage: packet[7],
            dataUploadRateWhenIgnitionOn: packet[8],
            dataUploadRateWhenIgnitionOff: packet[9],
            digitalIOStatus: packet[10],
            analogIOStatus: packet[11],
            adc1: packet[12],
            adc2: packet[13],
        };

        return document;
    }
}

module.exports = LoginParser;