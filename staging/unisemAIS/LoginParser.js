var utils = require("../../utils");
var helper = require("./helper");
'use strict'
class LoginParser {
    constructor() {

    }

    parse(packet) {
        var document = {
            imei: packet[4],
            type: "data",
            subtype: "login",
            timestamp: new Date(),
            vendorId: packet[2],
            firmwareVersion: packet[3],
            imei: packet[4],
            registrationNo: packet[5]
        };

        return document;
    }
}

module.exports = LoginParser;