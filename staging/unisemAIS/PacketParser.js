var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var utils = require("../../utils");
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("unisemAIS", "receiver.log");

var parsers = {
    "01": new (require("./LoginParser"))(),
    "02": new (require("./HealthParser"))(),
    "03": new (require("./GpsParser"))(),
    "04": new (require("./GpsParser"))()
};

'use strict'
class PacketParser {
    constructor() {

    }

    parse(packetType, packet) {
        //get parser from registry
        var parser = parsers[packetType];
        if (!parser) {
            logger.info("No parser found for ::" + packetType + "  packet:" + packet);
            return;
        }

        //parse data
        var document = parser.parse(packet);
        if (!document)
            return;

        //add owner and defaults
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}
module.exports = PacketParser;