var utils = require("../../utils");
var origin = require("os").hostname();;

//adds default values to mongo document
exports.addDefaults = function (document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-Unisem-AIS";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

exports.isValidPacket = function (packet) {
    return (packet.substr(0, 1) == "$" && packet.substr(packet.length - 1, 1));
}

exports.parseLocation = function (latitude, latitudeDir, longitude, longitudeDir) {
    var latitude = parseFloat(latitude);
    var longitude = parseFloat(longitude);
    if (latitudeDir == "S")
        latitude = latitude * -1;

    if (longitudeDir == "W")
        longitude = longitude * -1;

    return utils.getGeoJson(latitude, longitude, true);
}

exports.parseTimestamp = function (day, month, year, hour, minute, second) {
    var timestampStr = year + "-" + month + "-" + day;
    timestampStr += " " + hour + ":" + minute + ":" + second + " UTC";
    return new Date(timestampStr);
}