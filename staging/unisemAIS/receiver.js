var Server = require("../../server");
var utils = require("../../utils");
var packetParser = new (require("./PacketParser"))();
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("unisemAIS", "receiver.log");

new Server("unisemAIS", require("../../config.json").unisemAIS.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	logger.info(data);
	var packet = data.toString("utf-8").toUpperCase();
	logger.info("Received raw packet : " + packet);

	processPacket(socket, packet);
});

function processPacket(socket, packet) {
	if (!helper.isValidPacket(packet)) {
		logger.info("Invalid packet : " + packet);
		return;
	}

	var packetArray = packet.split(",");
	packetParser.parse(packetArray[1], packetArray);

	if (packetArray[1] == "01") {
		sendToDevice(socket, "$,1,*");
	}	
}

function sendToDevice(socket, packet) {
	if (socket) {
		logger.info("Sending to device : " + packet);
		socket.write(new Buffer(packet, "utf8"));
	}
}

//var login = "$,01,XYZ123,0.0.1,861359034137271,MH12AB1234,*";
//var health = "$,02,XYZ123,0.0.1,861359034137271,100,30,00.0,00005,00600,1000,01,00.1,00.0,*";
//var gps = "$,03,XYZ123,0.0.1,TA,16,L,861359034137271,MH12AB1234,0,00,00,0000,00,00,00,000.000000,N,000.000000,E,000.0,000.00,00,000.0,00.00,00.00,IDEAIN,1,1,00.0,4.0,1,O,16,404,22,2797,11b7,11b9,2797,-087,11b8,2797,-093,11b4,2797,-106,0000,0000,0000,1000,01,000032,8173e058,*";
//processPacket(undefined, gps);
