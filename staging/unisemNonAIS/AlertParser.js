var utils = require("../../utils");
var helper = require("./helper");
var voltageLevel = require("./Voltage.json");
var gsmSignal = require("./GsmSignal.json");
var alert = require("./Alert.json");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "alert", };
        document.timestamp = helper.parseTimestamp(content.substr(0, 12));
        document.noOfSatellite = utils.hexToInt(content.substr(12, 2));
        document.location = helper.getLocation(content, 14, 16);
        document.speed = utils.hexToInt(content.substr(30, 2));
        document.course = content.substr(32, 4);
        document.lbsLength = content.substr(36, 2);
        document.mcc = content.substr(38, 4);
        document.mnc = content.substr(42, 2);
        document.lac = content.substr(44, 4);
        document.cellId = content.substr(48, 6);
        document.terminalInfo = content.substr(54, 2);
        document.voltage = parseFloat(voltageLevel[content.substr(56, 2)]);
        document.gsmSignalStrength = gsmSignal[content.substr(58, 2)];
        document.subtype = (alert[content.substr(60, 2)]) ? alert[content.substr(60, 2)] : "unknown:" + content.substr(60, 2);
        document.language = content.substr(62, 2);

        return document;
    }
}

module.exports = AlertParser;