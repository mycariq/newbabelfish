var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(content) {
        var document = { type: "data", subtype: "gps" };
        document.timestamp =  helper.parseTimestamp(content.substr(0, 12));
        document.noOfSatellite = utils.hexToInt(content.substr(12, 2));
        document.location = helper.getLocation(content, 14, 16);
        document.speed = utils.hexToInt(content.substr(30, 2));
        document.course = content.substr(32, 4);
        document.mcc = content.substr(36, 4);
        document.mnc = content.substr(40, 2);
        document.lac = content.substr(42, 4);
        document.cellId = content.substr(46, 6);
        return document;
    }
}

module.exports = GpsParser;