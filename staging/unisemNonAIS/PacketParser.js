var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var helper = require("./helper");
var logger = require("../../logger.js").getLogger("unisemNonAIS", "receiver.log");;

var parsers = {
    "12": new (require("./GpsParser"))(),
    "13": new (require("./HeartbeatParser"))(),
    "26": new (require("./AlertParser"))()
};

'use strict'
class PacketParser {
    constructor() {
    }

    parse(command, owner, packet) {
        //get parser from registry
        var parser = parsers[command];
        if (!parser) {
            logger.info("No parser found for ::" + command + "  packet:" + packet);
            return;
        }

        //parse data
        var document = parser.parse(packet);
        if (!document)
            return;

        //add owner and defaults
        document.owner = owner;
        helper.addDefaults(document);

        //persist document
        persist(document);
    }
}

function persist(document) {
    logger.info("Parsed packet " + JSON.stringify(document));
    kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });

}

module.exports = PacketParser;