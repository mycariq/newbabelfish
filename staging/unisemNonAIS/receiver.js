var Server = require("../../server");
var utils = require("../../utils");
var logger = require("../../logger.js").getLogger("unisemNonAIS", "receiver.log");
var parser = new (require("./PacketParser"))();
var responseSender = new (require("./ResponseSender"))();
var helper = require("./helper");

new Server("unisemNonAIS", require("../../config.json").unisemNonAIS.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	//logger.info(data);
	var packet = data.toString("hex").toUpperCase();
	logger.info("Received raw packet : " + packet);

	// split muptiple packets saperated by comma
	var packets = packet.replace(/0D0A/gi, "0D0A,").split(","); 
	packets.pop();
	packets.forEach(function (packet) {	
		//validate packet. return if it is invalid
		if (!isValidPacket(packet))
			return;

		//process packet
		processPacket(socket, packet);
	});

});

function isValidPacket(packet) {
	//fetch head, tail crc from packet
	var head = packet.substr(0, 4);
	var tail = packet.substr(packet.length - 4, 4);
	var crc = packet.substr(packet.length - 8, 4);

	//validate head and tail
	if ("7878" != head || "0D0A" != tail) {
		logger.info("Head and Start are not matching. Hence ignoring packet: " + packet);
		return false;
	}

	//validate crc
	// if (crc != helper.calculateChecksum(packet.substring(4, packet.length - 8))) {
	// 	logger.info("CRC is not matching. hence ignoring packet: " + packet);
	// 	return false;
	// }
	return true;
}

function processPacket(socket, packet) {
	var command = packet.substr(6, 2);
	var serialNo = packet.substr(packet.length - 12, 4);
		
	if ("01" == command)
		socket.owner = packet.substr(8, 16);
	else {
		//var content = packet.slice(24, -4);
		var content = packet.slice(8, -4);
		parser.parse(command, socket.owner, content);
	}
	
	//send response
	responseSender.send(socket, socket.owner, command, serialNo);
}

var login = "78780D010861359036085338007B3BDA0D0A";
//var gps = "787827120861359036130779130B0D06022FCC01FE0AFE07E9F488001485019416ED2F00158D006853CD0D0A";
var gps = "78781F12130C12070E0ECD01FE0AA207E9F498001483019416ED2F0015880001DB8B0D0A";
var alert = "78782D260861359036130779130B0D07053BCD01FE09A207E9F4C00A14AB08000000000000000050060302020001008F170D0A";
//var alert = "787825260B0B0F0E241DCF027AC8870C4657E60014020901CC00287D001F726506040101003656A40D0A";
var heartbeat = "7878121308613590361307794606040002000471590D0A";
//var heartbeat = "78780A1310060200020170E0230D0A";

//processPacket({}, gps);
