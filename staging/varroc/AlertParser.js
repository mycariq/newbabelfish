var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class AlertParser {
    constructor() {

    }

    parse(data) {
        var document = {};

        document.owner = data[1];
        document.backlog = (data[2] == "H");
        document.publishDate = helper.parseTimeStamp(data[3]);
        document.timestamp = helper.parseTimeStamp(data[4]);
        document.isValid = data[5];
        document.location = helper.parseLocation(data[6], data[7], data[8], data[9]);
        document.speed = parseFloat(data[10]);
        addAlertType(document, parseInt(data[11]));
        document.frameNo = data[12];
        document.protocolVersion = data[13];
        document.checkSum = data[14];

        return document;
    }
}

/*01 --> Disconnect from main battery and running on its internal battery,
02 --> Low battery "If device internal battery has fallen below a defined threshold"
03 --> Low battery removed or battery fully charged "Indicates that device internal battery is charged again"
04 --> Connect back to main battery "Indicates that device is connected back to main battery"
05 --> Ignition ON "Indicates that Vehicle’s Ignition is switched ON"
06 --> Ignition OFF "Indicates that Vehicle’s Ignition is switched OFF"
07 --> GPS box opened
08 --> GPS box closed
09 --> Emergency state ON "When any of the emergency button is pressed"
10 --> Emergency State OFF "When emergency state of vehicle is removed"
11 --> Over the air parameter change "When any parameter is changed over the air"
12 --> Harsh Braking "Alert indicating for harsh braking."
13 --> Harsh Acceleration "Alert indicating for harsh acceleration."
14 --> Rash Turning "Alert indicating for Rash turning"
15 --> Device Tempered "Alert Indicating Emergency button wire disconnect/ wire cut"
16 --> Vehicle Start running
17 --> Vehicle Stop
18 --> Engine idling
19 --> Over speeding*/


function addAlertType(document, alertType) {

    // default for all cases
    document.type = "alert";

    switch (alertType) {
        case 01:
            document.subtype = "pullOutReminder";
            break;

        case 02:
            document.subtype = "internalLowBatteryVoltage";
            break;

        case 04:
            document.subtype = "wakeupEnergize";
            break;

        case 05:
            document.subtype = "wakeupVoltage";
            break;

        case 06:
            document.subtype = "dormancyVoltage";
            break;

        case 07:
            document.subtype = "tamperOn";
            break;

        case 08:
            document.subtype = "tamperOff";
            break;

        case 09:
            document.subtype = "imergencyOn";
            break;

        case 10:
            document.subtype = "imergencyOff";
            break;

        case 11:
            document.subtype = "OTAParamChanged";
            break;

        case 12:
            document.subtype = "rapidDeceleration";
            break;

        case 13:
            document.subtype = "suddenSpeededUp";
            break;

        case 14:
            document.subtype = "sharpTurn";
            break;

        case 15:
            document.subtype = "imergencyTemper";
            break;

        case 16:
            document.subtype = "vehicleStartRunnung";
            break;

        case 17:
            document.subtype = "vehicleStop";
            break;

        case 18:
            document.subtype = "ideling";
            break;

        case 19:
            document.subtype = "overspeed";
            break;

        default:
            return;
    }
}

module.exports = AlertParser;