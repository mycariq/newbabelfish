var utils = require("../../utils");
var helper = require("./helper");
var messageType = require("./MessageType.json");

'use strict'
class EmergencyParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "emergency" };

        document.owner = data[1];
        document.vendorId = data[2];
        document.firmwareVersion = data[3];
        document.backlog = (data[4] == "SP");
        document.publishDate = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.messageType = messageType[data[9]];
        document.gpsValid = (data[10] == "V");
        document.location = helper.parseLocation(data[11], data[12], data[13], data[14]);
        document.speed = parseFloat(data[15]);
        document.altitude = data[16];
        document.distance = data[17];
        document.gpsDataProvider = data[18];
        document.frameNo = data[19];
        document.replyNo = data[20];
        document.registationNo = data[21];
        document.protocolVersion = data[22];
        //document.checkSum = data[23];

        return document;
    }
}

module.exports = EmergencyParser;