var configurationLoader = require("../../ConfigurationLoader");
var logger = require("../../logger.js").getLogger("varroc", "receiver.log");

var commands = {
	immobilize: {
		on: "$SENGINEAUTH,__OWNER__,2,1,Elpro compound,VTS01001_20042019*",
		off: "$SENGINEAUTH,__OWNER__,1,2,Elpro compound,VTS01001_20042019*"
	}
};

function send(owner, socket) {
	//send mobiliser
	var immobiliseConf = configurationLoader.get(owner, "IMMOBILIZE_SWITCH");
	if (immobiliseConf) {
		if (immobiliseConf.IMMOBILIZE == "ON") {
			var response = commands.immobilize.on.replace("__OWNER__", owner);
			logger.info("Sending mobilise on command to device=" + owner + " command=" + response);
			socket.write(response);
		}
		if (immobiliseConf.IMMOBILIZE == "OFF") {
			var response = commands.immobilize.off.replace("__OWNER__", owner);
			logger.info("Sending mobilise off command to device=" + owner + " command=" + response);
			socket.write(response);
		}
		configurationLoader.remove(owner, "IMMOBILIZE_SWITCH");
	}
}

exports.send = send;
