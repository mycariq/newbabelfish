var utils = require("../../utils");
var helper = require("./helper");
var geofenceStatus = require("./GeofenceStatus.json");
var geofenceAction = require("./GeofenceAction.json");

'use strict'
class GeofenceParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "geofence" };

        document.owner = data[1];
        document.publishDate = helper.parseTimeStamp(data[2], data[3]);
        document.timestamp = helper.parseTimeStamp(data[4], data[5]);
        document.geofenceStatus = geofenceStatus[data[6]];
        document.geofenceAction = geofenceAction[data[7]];
        document.notUsed1 = data[8];
        document.notUsed2 = data[9];
        document.protocolVersion = data[10];
        //document.checkSum = data[20];

        return document;
    }
}

module.exports = GeofenceParser;