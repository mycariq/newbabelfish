var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class GpsParser {
    constructor() {

    }

    parse(data) {
        var document = {};
        document.owner = data[1];
        document.backlog = (data[2] == "H");
        document.publishDate = helper.parseTimeStamp(data[3]);
        document.timestamp = helper.parseTimeStamp(data[4]);
        addPacketType(document, data[5]);
        document.isValid = data[6];
        document.location = helper.parseLocation(data[7], data[8], data[9], data[10]);
        document.speed = parseFloat(data[11]);
        document.cog = data[12];
        document.satelliteUse = data[13];
        document.pdop = data[14];
        document.hdop = data[15];
        document.altitude = data[16];
        document.signalQuality = data[17];
        document.countryCode = data[18];
        document.networkCode = data[19];
        document.locationAreaCode = data[20];
        document.cellId = data[21];
        document.networkMeasurement = data[22];
        document.powerStatus = data[23];
        document.movingStatus = data[24];
        document.ignitionStatus = data[25];
        document.distance = data[26];
        document.odometerDistance = data[27];
        document.inputStatus = data[28];
        document.outputStatus = data[29];
        document.frameNo = data[30];
        document.operator = data[31];
        document.registationNo = data[32];
        document.protocolVersion = data[33];
        document.checkSum = data[34];

        return document;
    }
}

function addPacketType(document, packetType) {

    switch (packetType) {
        case "NR":
            document.type = "data";
            document.subtype = "gps";
            break;

        case "HP":
            document.type = "data";
            document.subtype = "heartbeat";
            break;

        case "IN":
            document.type = "alert";
            document.subtype = "wakeupVoltage";
            break;

        case "IF":
            document.type = "alert";
            document.subtype = "dormancyVoltage";
            break;

        case "BD":
            document.type = "alert";
            document.subtype = "pullOutReminder";
            break;

        case "BR":
            document.type = "alert";
            document.subtype = "wakeupEnergize";
            break;

        case "BL":
            document.type = "alert";
            document.subtype = "lowBatteryVoltage";
            break;

        default:
            return;
    }
}

module.exports = GpsParser;