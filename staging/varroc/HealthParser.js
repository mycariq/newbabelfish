var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class HealthParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "health" };

        document.owner = data[1];
        document.vendorId = data[2];
        document.firmwareVersion = data[3];
        document.backlog = (data[4] == "H");
        document.publishDate = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.chargingLevel = data[9];
        document.pendingLog = data[10];
        document.voltage = helper.getVoltage(data[11]);
        document.memoryPercentage = data[12];
        document.ignitionOn = data[13];
        document.ignitionOff = data[14];
        document.digitalInput = data[15];
        document.analogInput = data[16];
        document.frameNo = data[17];
        document.hardwareVersion = data[18];
        document.protocolVersion = data[19];
        //document.checkSum = data[20];

        return document;
    }
}

module.exports = HealthParser;