var utils = require("../../utils");
var helper = require("./helper");
var packetType = require("./PacketType.json");
var alertType = require("./AlertType.json");

'use strict'
class LocationParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "gps" };

        document.owner = data[1];
        document.vendorId = data[2];
        document.firmwareVersion = data[3];
        document.backlog = (data[4] == "H");
        document.publishDate = helper.parseTimeStamp(data[5], data[6]);
        document.timestamp = helper.parseTimeStamp(data[7], data[8]);
        document.packetType = packetType[data[9]];        
        document.alertType =  alertType[data[10]];
        document.isValid = data[11];
        document.location = helper.parseLocation(data[12], data[13], data[14], data[15]);
        document.speed = parseFloat(data[16]);
        document.cog = data[17];
        document.satelliteUse = data[18];
        document.pdop = data[19];
        document.hdop = data[20];
        document.altitude = data[21];
        document.signalQuality = data[22];
        document.countryCode = data[23];
        document.networkCode = data[24];
        document.locationAreaCode = data[25];
        document.cellId = data[26];
        document.networkMeasurement1 = data[27];
        document.networkMeasurement1 = data[28];
        document.networkMeasurement1 = data[29];
        document.networkMeasurement1 = data[30];
        document.powerStatus = data[31];
        document.movingStatus = data[32];
        document.ignitionStatus = data[33];
        document.emergencyStatus = data[34];
        document.tamperAlert = data[35];
        document.voltage = parseFloat(data[36]);
        document.batteryVoltage = parseFloat(data[37]);
        document.inputStatus = data[38];
        document.outputStatus = data[39];
        document.frameNo = data[40];
        document.operator = data[41];
        document.registationNo = data[42];
        document.protocolVersion = data[43];
        //document.checkSum = data[44];

        return document;
    }
}

module.exports = LocationParser;