var utils = require("../../utils");
var helper = require("./helper");

'use strict'
class MaintenanceParser {
    constructor() {

    }

    parse(data) {
        var document = { type: "data", subtype: "maintenance" };

        document.owner = data[1];
        document.backlog = (data[2] == "H");
        document.publishDate = helper.parseTimeStamp(data[3]);
        document.timestamp = helper.parseTimeStamp(data[4]);
        document.isValid = data[5];
        document.location = helper.parseLocation(data[6], data[7], data[8], data[9]);
        document.speed = parseFloat(data[10]);
        document.batteryStatus = data[11];
        document.chargingLevel = data[12];
        document.voltage = parseFloat(data[13]);
        document.pendingLog = data[14];
        document.memoryPercentage = data[15];
        document.notUsed2 = data[16];
        document.notUsed3 = data[17];
        document.frameNo = data[18];
        document.firmwareVer = data[19];
        document.hardwareVer = data[20];
        document.protocolVersion = data[21];
        document.checkSum = data[22];

        return document;
    }
}

module.exports = MaintenanceParser;