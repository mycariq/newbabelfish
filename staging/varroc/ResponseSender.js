var utils = require("../../utils");
var helper = require("./helper");
var logger = console;

'use strict';
class ResponseSender {
    constructor() {

    }

    send(socket, deviceId, status, action, version) {

        var response = undefined;

        var startToken = "$SGEOFENCE";
        //TODO: now packet not sending geofence name adding default
        var geofenceName = "Elpro compound";

        // create response packet
        response = startToken + ',' + deviceId + ',' + status + ',' + action + ',' + geofenceName + ',' + version;

        sendResponse(socket, deviceId, response)
    }
}

function sendResponse(socket, deviceId, response) {
    if (!response)
        return;

    //send response to device
    logger.info("Sending response to " + deviceId + " Response : " + response);

    if (socket)
        socket.write(new Buffer(response, "utf-8"));

}

module.exports = ResponseSender;