var utils = require("../../utils");

var origin = require("os").hostname();;

//adds default values to mongo document
function addDefaults(document) {
    if (document) {
        document.origin = origin;
        document.agent = "CIQ-Varroc";
        document.serverTimestamp = new Date;
        if (!document.location) {
            document.location = utils.getGeoJson(0.0, 0.0, false);
        }
    }
    return document;
}

function parseTimeStamp(dateStr, timeStr) {
    var timestampStr = parseInt(dateStr.substr(4)) + "-" + parseInt(dateStr.substr(2, 2)) + "-" + parseInt(dateStr.substr(0, 2));
    timestampStr += " " + parseInt(timeStr.substr(0, 2)) + ":" + parseInt(timeStr.substr(2, 2)) + ":" + parseInt(timeStr.substr(4) + " UTC");
    return new Date(timestampStr);
}

function parseLocation(latStr, latDir, longStr, longDir) {
    var latitude = parseFloat(latStr);
    var longitude = parseFloat(longStr);
    if (latDir == "S")
        latitude = latitude * -1;

    if (longDir == "W")
        longitude = longitude * -1;

    return utils.getGeoJson(latitude, longitude, true);
}

function getVoltage(value) {
    // Equation of a Straight Line: y = mx + c 
    // battery alert below 15% same like mobile 
    // m=gradient, angle of the line to x-axis
    // c=is intercept on y-axis 
    var m = 0.035;
    var c = 10.50;

    var batteryCapacity = parseFloat(value.substr(0, 2) + '.' + value.substr(2));
    return round(batteryCapacity * m + c, 2);
}

function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

exports.parseTimeStamp = parseTimeStamp;
exports.addDefaults = addDefaults;
exports.parseLocation = parseLocation;
exports.getVoltage = getVoltage;
exports.round = round;
