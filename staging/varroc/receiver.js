var Server = require("../../server");
var logger = require("../../logger.js").getLogger("varroc", "receiver.log");
var parser = new (require("./PacketParser"))();
var responseSender = new (require("./ResponseSender"))();
var feedbackSender = require("./FeedbackSender");

new Server("varroc", require("../../config.json").varroc.port).start(function (err,
	data, id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	var packet = data.toString("utf-8");
	logger.info("Received raw packet : " + packet);

	var packets = packet.split("\n");
	for (var i = 0; i < packets.length; i++) {
		if (packets[i])
			processPacket(socket, packets[i]);
	}
});

function processPacket(socket, packet) {

	logger.info("Parsing :" + packet);
	var packetArray = packet.split(",");

	var packetType = packetArray[0];

	// removed $ from packet type
	var packetType = packetType.substr(1);
	parser.parse(packetType, packetArray);

	var owner = packetArray[1];
	feedbackSender.send(owner, socket);
}

var gps = "$LOCATION,865734020000749,L,17-04-2006 14:22:48,17-04-2006 14:22:45,NR,0,18.556662,N,073.793187,E,120.4,087,12,2.24,3.64,0545.23,68,404,10,00D6,CFBD,0,0,0,0,010.5,000123,0000,0000,087965,MH-12AB-1234,VT01001_260718,4C*<CR><LF>";
var event = "$ALERT,865734020000749,L,17-04-2006 14:22:48,17-04-2006 14:22:45,0,18.556662,N,073.793187,E,120.4,01,087965,VT01001_260718,4C*<CR><LF>";
var maintenance = "$MAINTENANCE,865734020000749,L,17-04-2006 14:22:48,17-04-2006 14:22:45,0,18.556662,N,073.793187,E,120.4,0,75,12,00010,40,0,0,087965,VTSxxx_060818,VTHxx_060818,VT01001_260718,4C*<CR><LF>";

var health = "$HEALTH,869867030184735,VARROC,VT01001_20042017,L,31102019,152213,31102019,152207,50,000008,10,00,0040,0020,1010,0101,000853,VTH_20042017,VT01001,1D*<CR><LF>";
var emergency = "$EPB,865734020000749,VARROC,VTS01001_20042019,NM,01022019,142246,01022019,142246,EMR,A,18.556662,N,073.793187,E,120.4,0545.23,0010.52,G,087965,0,MH-12 AB-1234,VT01001,00004C12*";
var location = "$LOCATION,869867030184735,VARROC,VT01001_20042017,H,18112019,065137,18112019,064832,NR,02,0,0,N,0,E,0.00,0,0,3.93,0,0,68,404,22,ed07,e6d,1:CFAD:0198:010,2:CBAE:0258:030,3:CCFD:0303:040,4:DEFC:0632:010,1,0,0,0,O,12.5,0,0000,00,046303,Vodafone IN,MH-12 AB-1234,VT01001,74*";
//var geofence = "$SGEOFENCERSP,865734020000749,01022019,142246,01022019,142246,0,0,0,0,VT01001*<CR><LF>";
//processPacket({}, geofence);
