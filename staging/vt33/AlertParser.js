var helper = require("./helper");
var utils = require("../../utils");

'use strict'
class GpsParser {

	constructor() {
	}

	parse(owner, data) {
		var document = { owner: owner, "type": "alert" };
		document.timestamp = helper.getTimestamp(data.substr(0, 6), data.substr(33, 6));
		document.isLbs = data.substr(6, 1);
		document.location = utils.getGeoJson(helper.getLatitude(data.substr(7, 10)), helper.getLongitude(data.substr(17, 11)), true);
		document.speed = parseFloat(data.substr(28, 5));
		document.direction = parseFloat(data.substr(39, 6));
		document.gsmSignal = parseFloat(data.substr(45, 3));
		document.noOfSatellite = parseFloat(data.substr(48, 3));
		document.batteryLevel = parseFloat(data.substr(51, 3));
		document.accStatus = data.substr(54, 1);
		document.fortificationStatus = data.substr(55, 2);
		document.workingMode = data.substr(57, 2);
		var dataArray = data.split(",");
		document.lbsDetails =dataArray[1]+","+dataArray[2]+","+dataArray[3]+","+dataArray[4];
		document.subtype = getAlertType(dataArray[5]);
		return document;
	}
}

function getAlertType(alertCode) {
	var alertType;
	switch (alertCode) {
		case "01":
			alertType = "sos";
			break;
		case "02":
			alertType = "pullOutReminder";
			break;
		case "03":
			alertType = "vibration";
			break;
		case "04":
			alertType = "enterGeoFence";
			break;
		case "05":
			alertType = "exitGeoFence";
			break;
		case "09":
			alertType = "displacement";
			break;
		case "13":
			alertType = "overSpeed";
			break;
		case "14":
			alertType = "lowBatteryVoltage";
			break;
		default:
			break;
	}
	return alertType;
}


module.exports = GpsParser;