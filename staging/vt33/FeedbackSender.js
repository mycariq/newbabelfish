var configurationLoader = require("../../ConfigurationLoader");
var logger = require("../../logger.js").getLogger("vt33", "receiver.log");

var commands = {
	mobilize: {
		on: "TRVBP040000031#",
		off: "TRVBP030000021#"
	}
};

function send(owner, socket) {
	//send mobiliser
	var mobiliseConf = configurationLoader.get(owner, "MOBILIZE_SWITCH");
	if (mobiliseConf) {
		if (mobiliseConf.MOBILIZE == "ON") {
			logger.info("Sending mobilise on command to device=" + owner + " command=" + commands.mobilize.on);
			socket.write(commands.mobilize.on);
		}
		if (mobiliseConf.MOBILIZE == "OFF") {
			logger.info("Sending mobilise off command to device=" + owner + " command=" + commands.mobilize.off);
			socket.write(commands.mobilize.off);
		}
		configurationLoader.remove(owner, "MOBILIZE_SWITCH");
	}
}

exports.send = send;
