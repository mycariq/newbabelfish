var helper = require("./helper");
var utils = require("../../utils");

'use strict'
class GpsParser{
	
	constructor(){
	}
	
	parse(owner,data){
		var document = {owner:owner, "type":"data", "subtype":"gps"};
		document.timestamp = helper.getTimestamp( data.substr(0,6),data.substr(33,6));
		document.isLbs = data.substr(6,1);
		document.location = utils.getGeoJson(helper.getLatitude(data.substr(7,10)),helper.getLongitude(data.substr(17,11)),true);
		document.speed = parseFloat(data.substr(28,5));
		document.direction = parseFloat(data.substr(39,6));
		document.gsmSignal = parseFloat(data.substr(45,3));
		document.noOfSatellite = parseFloat(data.substr(48,3));
		document.batteryLevel = parseFloat(data.substr(51,3));
		return document;
	}
}



module.exports = GpsParser;