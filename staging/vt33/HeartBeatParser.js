'use strict'
class HeartBeatParser{
	
	constructor(){
	}
	
	parse(owner, data){
		var document = { owner:owner, type:"data", subtype:"heartbeat" };
		document.timestamp = new Date;
		document.gsmSignal = parseFloat(data.substr(1,3));
		document.noOfSatellite = parseFloat(data.substr(4,3));
		document.batteryLevel = parseFloat(data.substr(7,3));
		return document;
	}
}

module.exports = HeartBeatParser;