var gpsParser = new (require("./GpsParser"))();
var alertParser = new (require("./AlertParser"))();
var heartBeatParser = new (require("./HeartBeatParser"))();
var kafkaProducer = new (require("../../producer"))("GeoBit", 3);
var logger = require("../../logger.js").getLogger("vt33", "receiver.log");

'use strict'
class PacketProcessor {
	constructor() {
	}

	process(owner, command, data) {
		var response = {};

		switch (command) {
			case "AP00":
				response.id = data;
				response.message = new Buffer("TRVBP00#", "utf-8");
				break;
			case "AP01":
				persist(gpsParser.parse(owner, data));
				response.message = new Buffer("TRVBP01#", "utf-8");
				break;
			case "AP10":
				persist(alertParser.parse(owner, data));
				response.message = new Buffer("TRVBP10#", "utf-8");
				break;
			case "CP01":
				persist(heartBeatParser.parse(owner, data));
				response.message = new Buffer("TRVDP01#", "utf-8");
				break;
			default:
				logger.info("Parser not found owner:" + owner + " command:" + command + " data:" + data);
		}

		return response;
	}
}

function persist(document) {
	if (!document)
		return;
	require("./helper").addDefaults(document);
	logger.info("Persisting... " + JSON.stringify(document));
	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        logger.info("KafkaResult:" + JSON.stringify(result));
    });
}

module.exports = PacketProcessor;