function getTimestamp(dateStr, timeStr) {
	return new Date("20" + dateStr.substr(0, 2) + "-" + dateStr.substr(2, 2)
			+ "-" + dateStr.substr(4, 2) + "T" + timeStr.substr(0, 2) + ":"
			+ timeStr.substr(2, 2) + ":" + timeStr.substr(4, 2)+".000Z");
}

function addDefaults(document) {
	if (document) {
		document.origin = require("os").hostname();
		document.agent = "vt33";
		document.serverTimestamp = new Date;
		if (!document.location) {
			document.location = require("../utils").getGeoJson(0.0, 0.0, false);
		}
	}
}

function getLatitude(packet) {
	// console.log("Raw Latitide : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	// console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 2));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(2, strLength - 3));
	remaining = remaining / 60;
	// console.log("remaining : " + remaining);

	var actualLatitide = degree + remaining;
	if (direction == 'S')
		actualLatitide = actualLatitide * (-1);

	// console.log("actualLatitide : " + actualLatitide);
	return actualLatitide;
}

function getLongitude(packet) {
	// console.log("Raw Longitude : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	// console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 3));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(3, strLength - 4));
	remaining = remaining / 60;

	var actualLongitude = degree + remaining;
	if (direction == 'W')
		actualLatitide = actualLongitude * (-1);

	// console.log("actualLatitide : " + actualLongitude);
	return actualLongitude;
}

exports.getLongitude = getLongitude;
exports.getLatitude = getLatitude;
exports.addDefaults = addDefaults;
exports.getTimestamp = getTimestamp;