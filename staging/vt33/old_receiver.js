//import server module
var Server = require("../server.js");
var Producer = require("../producer.js");
var config = require("../config")["vt33"];
var utils = require("../utils.js");
var port = config["port"];

var producer = new Producer(port);
var logger = require("../logger.js");
var mylogger = logger.getLogger("vt33", "receiver.log");

mylogger.debug("Server()- Server stared on port:" + port);

function checkAndParseLoginPacket(mypacket) {
	var loginResponse = {};
	loginResponse.devId = "";


	loginResponse.cmdType = mypacket.substr(3,4);
	if (loginResponse.cmdType === "AP00") {
		loginResponse.devId = mypacket.substr(7,15);
	}
	//mylogger.debug("(checkAndParseLoginPacket)-Resonse from checkAndParseLoginPacket = " + JSON.stringify(loginResponse)); 
	return loginResponse;
}

function validatePacket(mypacket) {
	var begining = mypacket.substr(0,3);
	var end =  mypacket.substr(mypacket.length-1,1);
	
	if ((begining === "TRV") && (end === "#"))
		return true;
	return false;
}

new Server("vt33", port).start(function(err, data, id, socket) {

	//if error is occured then log it exit from function
	if(err){
		mylogger.error("(Server)-Unexpected Error,ANOMALY,UNEXPECTED_ERROR," + err);
		return ;
	}
	//if data is undefined then exit from function
	if(!data) {
		mylogger.error("(Server)-Received NULL Data,ANOMALY, NULL_DATA");
		return;
	}

	var packet = new Buffer(data, "utf8").toString("ascii").toUpperCase();
	var response= [];

	mylogger.debug("(Server)-Raw DATA from VT33,RAW_DATA,DATA," + packet);
	
	if(!validatePacket(packet)) {
		mylogger.error("(Server)-INVALID Packet,ANOMALY,INVALID_PACKET," + packet);
		return;
	}

	var packetWithDevID = {};
	var devId = id; // id coming from server.js
	
	var loginResponse = checkAndParseLoginPacket(packet);
	if (loginResponse.devId) {
		//mylogger.debug("(Server)-It is Login Packet : devIdFromLoginPacket = " + loginResponse.devId);
		devId = loginResponse.devId;
	} 

	if (devId) { 
		// We have DevID available here
		packetWithDevID.devId = devId;
		packetWithDevID.packet = packet;
		packetWithDevID.cmdType = loginResponse.cmdType;
	
		//mylogger.debug("(Server)-packetWithDevID " + JSON.stringify(packetWithDevID)); 

	} else
	{
		// We do not know from which device this packet came :(  Log it and forgot it
		mylogger.error("(Server)-Packet From UNKNOWN Device,ANOMALY,UNKNOWN_DEVICE," + packet);
	}

	response["id"] = devId;  // Loop back devId received from Login Packet
	mylogger.debug("#### Loopback id = " + devId);

	switch (loginResponse.cmdType) {
		case "AP00":
			//mylogger.debug("AP00 Login command");
			response["message"] = "TRVBP00#";
			break;

		case "AP01":
			//mylogger.debug("AP01 Location Packet");
			response["message"] = "TRVBP01#";
			break;
	
		case "CP01":
			//mylogger.debug("CP01 Heartbeat Packet");
			response["message"] = "TRVDP01#";
			break;

		case "AP10":
			//mylogger.debug("CP01 Heartbeat Packet");
			response["message"] = "TRVBP10#";
			break;
	
		default :
			mylogger.error("(Server)-Unknown COMMAND,ANOMALY,UNPARSED_PACKET" + packet);
			break;

	};

	if (devId) {
		//if devId present then ONLY Pass received Packet for further processing to parser via producer-consumer channel
		producer.produce({
			packet : packetWithDevID
		});
	}

	if (response["message"])
		mylogger.info("(Server)-Sending response to device,ACTION,SEND_ACK," + response["message"]);
	else
		mylogger.error("(Server)-Sending response to device,ANOMALY,NO_ACK, No ack is send");

	
	return response; // This reponse contains message-->  responseMsgToDevice and id --->  devId we received from Login Packet
});
