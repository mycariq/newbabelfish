var kueURL = require('kue'), jobsURL = kueURL.createQueue();

var xor = require('bitwise-xor');

var kafkaProducer = new (require("../../producer"))("GeoBit", 3);

var Consumer = require("../../consumer.js");
var utils = require("../../utils");
var config = require("../../config")["vt33"];
var dasDetails = require("../../config")["DAS"];
var mongoConfig = require("../../config")["mongo"];
var port = config["port"];

var throughputConfig = require("../../config")["ThroughputCalculator"];
var threshold = throughputConfig["threshold"];

var dasDNS = "http://" + dasDetails["DNS"] + ":" + dasDetails["port"];

var ThroughputRecord = require("../../ThroughputRecord.js").ThroughputRecord;
var mongoQThroughputRecord = new ThroughputRecord("mongoQThroughputRecord", new Date, 0, threshold);
var parserQThroughputRecord = new ThroughputRecord("parserQThroughputRecord", new Date, 0, threshold);

const DAS_BASEURL = dasDNS + "/DataAcquisitionModule/dataacquisitions/pushToBC/{id}/{timeStamp}/{packet}";
const DAS_BASEURL_PARSED = dasDNS + "/DataAcquisitionModule/dataacquisitions/push/parsedData/{id}/{timeStamp}/{packet}";

const DAS_TOPIC = "das-urls";
var mongoTopic = mongoConfig["topic"];

const mongo_document_agent = "ciqsmartplugVT33";
var logger = require("../../logger.js");
var mylogger = logger.getLogger("vt33", "parser.log");

var os = require("os");
var origin = os.hostname();
var HashMap = require('hashmap');

mylogger.debug("DAS_BASEURL : " + DAS_BASEURL);
mylogger.debug("DAS_BASEURL_PARSED : " + DAS_BASEURL_PARSED);


function getTimeStamp(timestamp) {
	return "20" + timestamp.substr(4, 2) + "-" + timestamp.substr(2, 2) + "-"
		+ timestamp.substr(0, 2) + " " + timestamp.substr(6, 2) + ":"
		+ timestamp.substr(8, 2) + ":" + timestamp.substr(10, 2);
}


function getLatitude(packet) {
	// console.log("Raw Latitide : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	// console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 2));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(2, strLength - 3));
	remaining = remaining / 60;
	// console.log("remaining : " + remaining);

	var actualLatitide = degree + remaining;
	if (direction == 'S')
		actualLatitide = actualLatitide * (-1);

	// console.log("actualLatitide : " + actualLatitide);
	return actualLatitide;
}


function getLongitude(packet) {
	// console.log("Raw Longitude : " + packet);
	var strLength = packet.length;

	// get direction
	var direction = packet.substr(strLength - 1, 1);
	// console.log("Direction : " + direction);

	var degree = parseInt(packet.substr(0, 3));
	// console.log("degree : " + degree);

	var remaining = parseFloat(packet.substr(3, strLength - 4));
	remaining = remaining / 60;

	var actualLongitude = degree + remaining;
	if (direction == 'W')
		actualLatitide = actualLongitude * (-1);

	// console.log("actualLatitide : " + actualLongitude);
	return actualLongitude;
}


/*
 * Function to get GeoJSON useful for Mongo Note the location GeoJSON format
 * (longitude, latitude): { "type": "Point", "coordinates": [ 73.77462166666666,
 * 18.55815 ] }
 */
function getGeoJSON(myLatitude, myLongitude, isReal) {

	var locJson = {};
	var locArray = [];

	locJson.type = "Point";
	locJson.isReal = isReal;
	locArray[0] = myLongitude;
	locArray[1] = myLatitude;
	locJson.coordinates = locArray;

	return locJson;
}


/*
 * This function is responsible for creating required URL which will be send to
 * DAS
 */
function createUrl(json, isParsedUrl) {

	var url;
	if (isParsedUrl) {
		url = DAS_BASEURL_PARSED.replace("{id}", json.id).replace("{timeStamp}", json.timeStamp.replace(" ", "%20"));
		url = url.replace("{packet}", json.all_data);
	}
	else {
		url = DAS_BASEURL.replace("{id}", json.id).replace("{timeStamp}", json.timeStamp.replace(" ", "%20"));

		packet = "";
		if (json.type)
			packet += json.type + ",";
		if (json.location) {
			if (json.location.latitude != "0000.0000N" && json.location.latitude != "0000.0000S")
				packet += "41Y1" + json.location.latitude + ",41Y2" + json.location.longitude + ",";
		}
		if (json.engineLoad)
			packet += "4104" + json.engineLoad + ",";
		if (json.coolantTemparature)
			packet += "4105" + json.coolantTemparature + ",";
		if (json.rpm)
			packet += "410C" + json.rpm + ",";
		if (json.speed) {
			if (json.speed.toString().length == 1) // If speed come as single
				// digit e.g. 9 instead of
				// 09 then prepend 0 before
				// digit
				json.speed = "0" + json.speed;
			packet += "410D" + json.speed + ",";
		}

		// TODO what does it mean
		// result.Ignition_advance_angle=packet.substr(92,2);

		if (json.Intake_manifold_absolute_pressure)
			packet += "410B" + json.Intake_manifold_absolute_pressure + ",";
		if (json.Voltage_control_module)
			packet += "TR01" + json.Voltage_control_module + ",";
		if (json.Intake_air_temperature)
			packet += "410F" + json.Intake_air_temperature + ",";
		// TODO what does it mean
		// result.Intake_air_flow=packet.substr(100,4);
		if (json.relative_position_of_the_throttle_valve)
			packet += "4145" + json.relative_position_of_the_throttle_valve + ",";
		// TODO what does it mean
		// result.longterm_fuel_trim=packet.substr(106,2);
		// TODO
		// result.Fuel_ratio_coefficient=packet.substr(108,4);
		// result.Fuel_ratio_coefficient=packet.substr(112,2);
		// result.Fuel_Pressure=packet.substr(114,2);
		// result.Instantaneous_fuel_consumption_1=packet.substr(116,4);
		// result.Instantaneous_fuel_consumption_1=packet.substr(120,4);
		// result.serial_no=packet.substr(124,2);
		url = url.replace("{packet}", packet);
	}
	return url;
}

function getISODate(stringDate) {
	// console.log("====> Remove me : input date:" + stringDate);
	// console.log("====> Remove me : input date:" + new

	return new Date(stringDate);
}

function dec2hex(i) {
	return (i + 0x10000).toString(16).substr(-2).toUpperCase();
}

/*
 * Sample RAW Data :
 * TRVAP01080524A2232.9806N11404.9355E000.1180630323.8706000908000102,460,0,9520,3671#
 * Packet for Parsing ={"devId":"TEST11111111111","packet":"TRVAP01080524A2232.9806N11404.9355E000.1180630323.8706000908000102,460,0,9520,3671#","cmdType":"AP01"}

	TRVAP01170324A1833.4588N07346.5352E000.1112815000.0010000810020101,404,900,3111,29888#

	TRVAP01  : Initial identifier (0,7)

	170324   : YYMMDD (7,6)    we need to make it DD-MM-YY

	A        : Valid data (13,1)  V means invalid 	data

	1833.4588N07346.5352E : Lat-Long (14,21) ..... we need to seperate Lat & Long along with  direction symball and pass it to  location.latitude and location.longitude

	000.1 : speed (35,5)

	112815 : GMT (40,6) hrMnSc 

	000.00 : Direction Angle (46,6)

	10000810020101 : GSMinfo (52,14)   ======= >  within GSM Info   ----> (0,3)  GSM Signal Strength-- (3,3) Number of Satelite--- (6,3) Battery level ---> (9,1) ACC_Status_0-NoACC,1-OpenACC,2-CloseACC ----> (10,2) fortification staus ---> (12,2) working mode

	, : comma (66,1)

	404,900,3111,29888# : LBS Data (67,)
 */
function parseAP01Packet(mypacket) {

	//console.log(myScriptName + new Date + ": " + "Packet for Parsing =" + JSON.stringify(mypacket));

	var result = {};
	var packet = mypacket.packet;
	var yy = packet.substr(7, 2);
	var mm = packet.substr(9, 2);
	var dd = packet.substr(11, 2);
	var ts = packet.substr(40, 6);
	var mlocation = {};
	mlocation.latitude = packet.substr(14, 10);
	mlocation.longitude = packet.substr(24, 11);

	result.id = mypacket.devId;
	result.timeStamp = getTimeStamp(dd + mm + yy + ts);
	result.location = mlocation;

	//var speedFromPacket = packet.substr(35,5);
	//console.log("#### speedFromPacket=" + speedFromPacket);
	//var speed2DecimalDigits = packet.substr(36,2);
	//console.log("#### speed2DecimalDigits=" + speed2DecimalDigits);
	//var int2Digit = parseInt(speed2DecimalDigits, 10);
	//console.log("#### int2Digit=" + int2Digit);
	//var int2Hex = dec2hex(int2Digit);
	//console.log("#### int2Hex=" + int2Hex);
	//result.speed = int2Hex; // Fix 29Aug2016 : Treat gps_speed as speed

	result.speed = dec2hex(parseInt(packet.substr(36, 2), 10)); //  User middle two digits from SPEED which is in integer and convert it to HEX 

	if (result) {
		mylogger.debug("(parseAP01Packet)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "gps";
	mongo_document.speed = result.speed;
	mongo_document.location = getGeoJSON(getLatitude(result.location.latitude), getLongitude(result.location.longitude), true);

	mongoProducer(mongo_document);
}

/*
 * Sample RAW Data :
 * TRVCP01,1000001002000010200110600110#
 * This is HeartBeat packet, treat it is SPEED 0 packet to show connectivity of Device
 */
function parseHeartBeatPacket(mypacket) {
	var result = {};
	result.id = mypacket.devId;
	result.speed = "00";
	result.timeStamp = utils.getServerTimeStamp();

	if (result) {
		mylogger.debug("(parseHeartBeatPacket)-Result JSON : " + JSON.stringify(result));
		var myURL = createUrl(result, false);
		processURL(myURL);
	}

	var mongo_document = {};
	mongo_document.origin = origin;
	mongo_document.owner = result.id;
	mongo_document.timestamp = getISODate(result.timeStamp);
	mongo_document.agent = mongo_document_agent;
	mongo_document.type = "data";
	mongo_document.subtype = "heartbeat";
	mongo_document.location = getGeoJSON(0, 0, false);

	mongoProducer(mongo_document);
}

function mongoProducer(document) {
	// set default values
	document.origin = require("os").hostname();
	document.serverTimestamp = new Date();

	//console.log("==========================================================");
	//console.log("MONGO==>" + JSON.stringify(document));
	//console.log("==========================================================");

	kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
        mylogger.info("KafkaResult:" + JSON.stringify(result));
    });
	mongoQThroughputRecord.increment();
}

/*
 * Here we are mainitaining another Queue of "URL" Strings with "Topic"
 * DAS_TOPIC
 */
function processURL(URL) {
	URL = URL.replace(/ /g, "%20"); // Replace SPACES with PERCENTAGE
	var job = jobsURL.create(DAS_TOPIC, { URL: URL });
	mylogger.debug("(processURL)-Creating URLJob for : " + URL);
	job.save();
	parserQThroughputRecord.increment();
}


new Consumer(port, function (data) {
	// console.log(myScriptName + new Date + ": " + "Received from Q =>" +
	// (data));
	var packet = data.packet;
	processPacket(packet);
});


/*
 * Parse the RAW Packet and then Based on CMDByte , decide packet paerser
 */
function processPacket(mypacket) {

	//console.log(myScriptName + new Date + ": " + "Packet for Parsing =" + JSON.stringify(mypacket));
	//console.log(myScriptName + new Date + ": " + mypacket.cmdType);

	switch (mypacket.cmdType) {
		case "CP01":
			parseHeartBeatPacket(mypacket);
			break;

		case "AP01":
			parseAP01Packet(mypacket);
			break;

		case "AP00":
			break;

		default:
			mylogger.error("(processPacket)-Unknown Command,ANOMALY,UNPARSED_PACKET," + JSON.stringify(mypacket));
			break;
	}
}

