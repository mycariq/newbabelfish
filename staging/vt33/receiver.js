var Server = require("../../server");
var utils = require("../../utils");
var logger = require("../../logger.js").getLogger("vt33", "receiver.log");
var packetProcessor = new (require("./PacketProcessor"))();

var feedbackSender = require("./FeedbackSender");

new Server("vt33", require("../../config").vt33.port).start(function (err, data,
	id, socket) {

	// if error is occured then log it exit from function
	if (err) {
		logger.error("(Server)-Unexpected Error, ANOMALY,UNEXPECTED_ERROR," + err);
		logger.error(err);
		return;
	}
	// if data is undefined then exit from function
	if (!data) {
		logger.error("(Server)-Undefined data, ANOMALY,NULL_DATA");
		return;
	}

	// convert hex buffer into ascii
	var rawPacket = data.toString("utf-8");
	logger.info("Received owner:" + id + " packet: " + rawPacket);

	// check whether packet is valid or not, if not then do not proceed
	if (!isValidPacket(rawPacket)) {
		logger.info("Invalid packet " + rawPacket);
		return;
	}

	// split packet into id, command, data
	var packet = {};
	packet.id = id;
	packet.command = rawPacket.substr(3, 4);
	packet.data = rawPacket.substr(7, rawPacket.length - 8);

	logger.info("After splitting: " + JSON.stringify(packet));

	if (id)
		feedbackSender.send(id, socket);

	// process packet and return response
	var response = packetProcessor.process(packet.id, packet.command,
		packet.data);
	logger.info("Responding: " + JSON.stringify(response));
	return response;
});

function isValidPacket(rawPacket) {
	if (rawPacket.substr(0, 3) == "TRV"
		&& rawPacket.substr(rawPacket.length - 1) == "#")
		return true;
	return false;
}

// var loginPacket = "TRVAP00357653050525281#";
// var gpsPacket = "TRVAP01170505A1831.7575N07347.4964E041.704495316.22008201610020101,404,900,3159,57852#";
// var alertPacket="TRVAP10080524A2232.9806N11404.9355E000.1061830323.8706000908000102,460,0,9520,3671,02,zh-cn,00#";
// var heartbeatPacket="TRVCP01,0990091002000010200110600112#";
// var packet=alertPacket;

// var response = packetProcessor.process("122355335433",packet.substr(3,4),packet.substr(7, packet.length - 8));
// console.log("Response :: "+response);