#!/bin/sh

basePath="/opt/log/forever"
dateStr=`date +"%Y%m%d"`

mkdir -p ${basePath}
   
#get parsers and receivers and start by forever 
for i in `ls services/*/receiver*.js`; do
    logPath=${basePath}"/application-"`echo ${i} | cut -d'/' -f 1`
    filePath=${logPath}"-"`echo $i | cut -d'/' -f 2`"-"${dateStr}
    outFile="${filePath}.out"
    logFile="${filePath}.log"
    errFile="${filePath}.err"
    uid=`echo $i | tr / _`
    #please do not remove these log file paths otherwise this will occupy space in ~/.forever. 
	#Hence user directory will get full and ssh to this system will get blocked.
	#This will require to why script is restarting and all
    forever -a -l "${logFile}" -o "${outFile}" -e "${errFile}" --uid "${uid}" start "${i}"  --max_old_space_size=8096 --stack_size=4096 --max_new_space_size=8096;    
done;
