var kue = require('kue');
var queue = kue.createQueue();
var logger = require('./log');

console.log("cleaner started");

queue.inactiveCount(function(err, total) {console.log("Inactive", total);});
queue.activeCount(function(err, total) {console.log("active", total);});
queue.completeCount(function(err, total) {console.log("complete", total);});
queue.failedCount(function(err, total) {console.log("failed", total);});
queue.delayedCount(function(err, total) {console.log("delayed", total);});

