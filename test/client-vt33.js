var net = require("net");
var fs = require("fs");
process.stdin.resume();
process.stdin.setEncoding('utf8');

var host = process.argv[2];
var port = process.argv[3];
var filename = process.argv[4];
//var mode = "hex";
var mode = "ascii";

var client = net.connect(port, host, function() {
	console.log("Connected to " + host + ":" + port);
	process.stdin.on('data', function(text) {
		console.log('input:' + text);

		client.write(getBuffer(text));
	});

	if ( filename ) {
	 console.log("Getting from File : " + filename);
	 fs.readFileSync(filename).toString().split('\n')
	 .forEach(function(line) {
	 console.log("Writing=" + line);
	 client.write(new Buffer(line, mode));
	 var stop = new Date().getTime();
	 while (new Date().getTime() < stop + 1000) {
	 ;
	 }
	 });
	}
});

client.on('data', function(data) {
	console.log("Message from server\n------------");
	console.log(data.toString(mode));
});

function getBuffer(text) {
	return new Buffer(text.substr(0, text.length - 1), mode);
}
