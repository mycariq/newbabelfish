console.log("*********************USAGE**************************");
console.log("nodejs client <host> <port> <mode> <filename>");
console
		.log("Mode is optional. If it is not provided then default will be hex.");
console
		.log("Filename is optional. If it is provided then it load packets from file first.")

var net = require("net");
var fs = require("fs");
process.stdin.resume();
process.stdin.setEncoding('utf8');

var host = process.argv[2];
var port = process.argv[3];
var mode = process.argv[4];
var filename = process.argv[5];

if (!mode) {
	mode = "hex";
}

var client = net.connect(port, host, function() {
	console.log("Connected to " + host + ":" + port);
	process.stdin.on('data', function(text) {
		console.log('input:' + text);

		client.write(getBuffer(text));
	});

	if (filename) {
		console.log("Getting from File : " + filename);
		fs.readFileSync(filename).toString().split('\n').forEach(
				function(line) {
					console.log("Writing=" + line);
					client.write(new Buffer(line, mode));
					var stop = new Date().getTime();
					while (new Date().getTime() < stop + 200) {
						;
					}
				});
	}
});

client.on('data', function(data) {
	console.log("Message from server\n------------");
	console.log(data.toString(mode));
});

function getBuffer(text) {
	return new Buffer(text.substr(0, text.length - 1), mode);
}
