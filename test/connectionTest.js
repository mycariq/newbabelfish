//forever -a -l /opt/log/forever/driven.log -o /opt/log/forever/driven.out -e /opt/log/forever/driven.err --uid driventest start test/connectionTest.js
//forever -a -l /opt/log/forever/bagic.log -o /opt/log/forever/bagic.out -e /opt/log/forever/bagic.err --uid bagictest start test/connectionTest.js

var sleep = require("sleep");
var net = require("net");

var client = undefined;
var startTime = undefined;

function openConnection() {

	console.log(new Date, "connecting");

	client = net.connect(49999, "localhost", function() {
		console.log(new Date, "connected");

		startTime = new Date;
		client.write(new Buffer("cq_ping", "utf8"));
	});
	client.on('data', function(data) {
		console.log(new Date, "Pong:" + (new Date - startTime) + " ms");
		client.destroy();
	});
	client.on('error', function(error) {
		console.log(new Date, "Error:", error);
	});
	client.on('close', function() {
		console.log(new Date, "Socket ended");
	});
}
openConnection();
setInterval(openConnection, 1000 * 60 * 10);
