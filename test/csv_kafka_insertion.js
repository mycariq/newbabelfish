if (process.argv.length < 3)
    printUsage();

function printUsage() {
    console.info("Required Arguments: " + 2);
    console.info("Provided Arguments: " + (process.argv.length - 2));
    console.info("===============USAGE============");
    console.info("nodejs " + process.argv[1] + " <file/dir path> <columns>");
    console.info("owner, timestamp, latitude, longitude columns are must.")
    console.info("================================\n");
    console.info("1 - " + process.argv[2]);
    console.info("2 - " + process.argv[3]);
    process.exit(-1);
}

var filename = process.argv[2];
var columns = process.argv[3];
var kafkaProducer = new (require("../producer"))("GeoBit", 3);
var logger = require("../logger").getLogger("test", "csv_kafka_insertion.log");
var utils = require("../utils");
var origin = require("os").hostname();
var deasync = require('deasync');
var fs = require("fs");

var startTimestamp = new Date();

var inserted = 0, inserting = 0;

if (!/owner|timestamp|latitude|longitude/.test(columns)) {
    throw new Error("owner|timestamp|latitude|longitude must present in the columns");
}

var columnPosition = {};
columns.split(",").forEach((col, index) => { columnPosition[index] = col; });

function processPath(path) {
    //if it is file then call process file
    if (fs.lstatSync(path).isFile()) {
        processFile(path);
        deasync.sleep(3000);
    }

    //if it is dir then call process dir
    else
        processDir(path);
}

function processDir(dirPath) {
    //Read dir
    var files = fs.readdirSync(dirPath);

    files.forEach(fileName => {
        processPath(dirPath + "/" + fileName);
    });
}

function processFile(filePath) {
    logger.info("Processing:::file:" + filePath + " columns:" + JSON.stringify(columnPosition));
    //read file sync
    fs.readFileSync(filePath).toString().split('\n').forEach((line, index) => {
        if (index == 0) return;
        line = line.replace("\r", "");
        //create document from line based on order of params
        var document = createDocument(line);

        //write into kafka
        persist(document);
        deasync.runLoopOnce();

    });
    logger.info("Processed:::file:" + filePath + " columns:" + JSON.stringify(columnPosition));
}

function persist(document) {

    if (document) {
        inserting++;
        //logger.info("Parsed packet " + JSON.stringify(document));
        logger.info("Inserting:" + inserting + "   Inserted:" + inserted);
        kafkaProducer.keyBasedProduce(JSON.stringify(document), document.owner, function (result) {
            logger.info("KafkaResult:" + JSON.stringify(result));
            inserted++;
            logger.info("Inserting:" + inserting + "   Inserted:" + inserted);
        });
    }
}

function createDocument(line) {

    if (line == undefined || line.replace(/ /g, "").length == 0)
        return;

    var document = { type: "data", subtype: "gps" };

    line.split(",").forEach((value, index) => {
        document[columnPosition[index]] = value;
    });

    // var dateAndTime = document.timestamp.split(" ");
    // var date = dateAndTime[0].split("-").reverse().join("-");

    document.timestamp = new Date(document.timestamp);
    document.location = utils.getGeoJson(parseFloat(document.latitude), parseFloat(document.longitude), true);
    document.serverTimestamp = new Date();
    document.agent = "DataInsertion";
    document.origin = origin;

    delete document.latitude;
    delete document.longitude;

    return document;
}

logger.info("********** STARTED ***********")

processPath(filename);

while (inserted < inserting) {
    deasync.sleep(10000);
    logger.info("Waiting for all callback to end script");
}

logger.info("********** ENDED ***********")
logger.info("StartedAt:" + startTimestamp + "  EndedAt:" + new Date());
