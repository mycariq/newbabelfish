var mongodb = require("mongodb");
var utils = require("../utils");

var url = require("../config").mongo.hotDBURL;
var logger = require("../logger.js").getLogger("scalability", "insertionTest.log");


var error = undefined;
var w4iqdb = undefined;
var collection;

// connect to mongodb
mongodb.MongoClient.connect(url, {
    reconnectTries: 600,
    // wait 1 second before retrying
    reconnectInterval: 1000,
    poolSize: 100,
    w: 1
}, function (err, db) {
    error = err;
    w4iqdb = db;
    collection = w4iqdb.collection("geobit");

    if (err) {
        console.error("(MongoClient.connect)-Error in connecting to ",
            url, err);
        return;
    }
    console.log("Connected to :", url);
});

// executes till callback from mongo gets called
while (!error && !w4iqdb) {
    require('deasync').runLoopOnce();
}

//inserts bathc into mongo
function insert() {

    for (var j = 1; j <= 20; j++) {
        var batch = [];
        var bulkBatch = [];


        //create batch

        for (var i = 0; i < 50; i++) {
            batch[i] = createDocuments()[i % 4];
            bulkBatch[i] = createDocuments()[i % 4];
        }

        insertBatch(batch);
        //bulkInsert(bulkBatch);

        //console.log("BEFORE");

    }
}

function insertBatch(batch) {
    var startTS = new Date;
    collection.insert(batch, { forceServerObjectId: true }, function (err, result) {
        var endTS = new Date;
        logger.debug("Inserted" + "," + startTS.toISOString() + "," + endTS.toISOString() + "," + (endTS.getTime() - startTS.getTime()) + "," + result.insertedCount);
    });
}

function bulkInsert(bulkBatch) {
    var bulk = collection.initializeOrderedBulkOp();
    bulkBatch.forEach(function (document) {
        bulk.insert(document);
    });
    var startTS = new Date;
    bulk.execute(function (err, result) {
        var endTS = new Date;
        // re-initialise batch operation           
        //callback();
        // console.log(result.nInserted);
        logger.debug("BulkInserted" + "," + startTS.toISOString() + "," + endTS.toISOString() + "," + (endTS.getTime() - startTS.getTime()) + "," + result.nInserted);
    });
}


//write header into log
logger.debug("TAG,startTimestamp,endTimestamp,time,count");

setInterval(insert, 60 * 60 * 1000);
insert();


function createDocuments() {
    var gpsDate = utils.minusMilliSeconds(new Date(), 300000);
    var alertDate = utils.minusMilliSeconds(new Date(), 600000);
    var tripDate = utils.minusMilliSeconds(new Date(), 900000);
    var obdgpsDate = utils.minusMilliSeconds(new Date(), 1200000);
    return [
        { "origin": "datta", "owner": "scalability", "timestamp": gpsDate, "agent": "ciqsmartplug", "type": "data", "subtype": "gps", "speed": 17, "accOn": true, "location": { "type": "Point", "isReal": true, "coordinates": [73.82023833333334, 18.50381] }, "voltage": 14.2, "serverTimestamp": new Date() },
        { "origin": "datta", "owner": "scalability", "timestamp": alertDate, "agent": "ciqsmartplug", "type": "alert", "subtype": "dormancyVoltage", "location": { "type": "Point", "isReal": false, "coordinates": [0, 0] }, "serverTimestamp": new Date(), "dormancyVoltage": "data" },
        { "origin": "datta", "owner": "scalability", "timestamp": tripDate, "agent": "ciqsmartplug", "type": "trip", "subtype": "trip", "tripId": 463, "ignitionTimestamp": new Date(), "flameoutTimestamp": new Date(), "tavelTime": 397, "fuelComsumption": 239, "mileage": 1594, "maxSpeed": 36, "maxRpm": 2856.5, "maxCoolantTemperature": 93, "rapidAccelerationTimes": 0, "rapidDecelerationTimes": 0, "overSpeedingTime": 0, "overSpeedingMileage": 0, "fuelConsumptionOverSpeeding": 0, "highSpeedTime": 0, "highSpeedMileage": 0, "fuelConsumptionHighSpeeds": 0, "normalSpeedTime": 0, "normalSpeedMileage": 0, "fuelConsumptionNormalSpeed": 0, "lowSpeedTime": 347, "lowSpeedMileage": 1594, "fuelConsumptionLowSpeed": 226, "idleSpeedTime": 50, "fuelConsumptionIdleSpeed": 12, "sharpTurnTimes": 0, "overSpeedingTimes": 0, "hotCarTime": 1, "fastLaneChange": 0, "emergencyRefueling": 0, "location": { "type": "Point", "isReal": false, "coordinates": [0, 0] }, "serverTimestamp": new Date() },
        { "owner": "scalability", "origin": "host", "timestamp": obdgpsDate, "agent": "ciqsmartplug", "type": "data", "subtype": "obdgps", "mileage": 931, "deviceStatus": "00200100", "engineLoad": 8.235294117647058, "coolantTemparature": 83, "rpm": 1342.5, "obdspeed": 17, "ignitionAdvanceAngle": 9, "intakeManifoldAbsolutePressure": 4, "voltage": 14.5, "intakeAirTemperature": 40, "intakeAirFlow": 0, "relativePositionOfTheThrottleValve": 5.490196078431373, "longtermFuelTrim": -17.1875, "fuelRatioCoefficient": 1.0000035, "absoluteThrottlePosition": 15.294117647058824, "fuelPressure": 0, "instantaneousFuelConsumption1": 0.2, "instantaneousFuelConsumption2": 1.4, "serialNo": 148, "location": { "type": "Point", "isReal": true, "coordinates": [76.75147333333334, 30.716505] }, "speed": 16, "direction": 120, "gpsSateliteNumber": 9, "gsmSignal": 24, "serverTimestamp": new Date() }
    ];
}