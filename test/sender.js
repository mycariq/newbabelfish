var net = require("net");
var sleep = require("sleep");
var async = require("async");
var utils = require("../utils");

console.log("===============USAGE============");
console
		.log("nodejs sender.js <host> <port> <no of connection> <time to live in seconds> <interval in mili seconds>");

// get command line arguments
var host = process.argv[2];
var port = process.argv[3];
var connCount = process.argv[4];
var timeToLive = process.argv[5];
var interval = process.argv[6];

// dummy packet
// var packet =
// "2860160308017520840034010011190516090712183247220735369076049C00110000001E0020010023820E6C008C268A61000007767FFC2200000F00000BD229";

var packet = "286016030801753082002701000328041409000128041409000128140900012804000000010000000023112211220123456789DF29";
// empty socket array
var sockets = [];

// declare end time of script
var endTime = new Date().getTime() + (timeToLive * 1000);

connect();
setInterval(send, interval);

// create connect to server and store sockets
function connect() {
	for (var i = 0; i < connCount; i++) {
		var socket = net.connect(port, host, function() {
			console.log("============= Connected =================");
		});
		sockets.push(socket);
	}
}

var writeCallback = function write(socket) {
	console.log("----------------");
	console.log(socket);

}

// sends packet to server
function send() {

	// // if time exceeds then kill the script
	if (endTime < new Date().getTime()) {
		sleep.sleep(300000);
		process.exit(0);
	}

	console.log(sockets.length, new Date);

	// sends packet on each socket
	sockets.forEach(function(socket) {
		process.nextTick(function() {
			var message = new Buffer(packet, "hex");
			socket.write(message);
		}.bind(socket));

	});
}
