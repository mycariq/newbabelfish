var fs = require("fs");
var utils = require("../utils");
var net = require("net");
var exec = require('child_process').exec;
var port = 1234;

var socketNo = 0;

var dir = "/tmp/benchmark";

// creates directory if it does not exist or if exists remove all files from it
if (!fs.existsSync(dir))
	fs.mkdirSync(dir);
else {
	var files = fs.readdirSync(dir);
	files.forEach(function(file) {
		fs.unlinkSync(dir + "/" + file);
	});
}

// create server and on connection get socket
net.createServer(function(socket) {
	var id = ++socketNo;

	var writer = fs.createWriteStream("/tmp/benchmark/" + id + ".csv");
	var file = dir + "/" + id + ".csv";
	var packetCount = 0;

	socket.on("data", function(data) {
		var message = utils.getServerTimeStamp() + "," + data;
		writer.write(message + "\n");
		// exec("echo \"" + message + "\" >> " + file);
		packetCount++;
	});

	setInterval(function() {
		console.log("id=" + id + " count=" + packetCount)
	}, 30000);

	socket.on("error", function(err) {
	});

}).listen(port);

console.log("Server started on " + port);