var tls = require("tls");
var utils = require("./utils");
var logger = require("./logger.js").getLogger("server", "tlsServer.log");


// constructor
var TLSServer = function (name, port, options) {
    this.name = name;
    this.port = port;
    this.options = options;
}

TLSServer.prototype = {

    // create and starts server and calls callback by error and data
    start: function (callback) {

        var serverName = this.name;
        var serverPort = this.port;

        // creates server by port
        var tlsServer = tls.createServer(this.options,
            function (socket) {
                logger.info("TLS Connection established for " + serverName + ":" + serverPort + ".  RemoteAddress=>" + socket.remoteAddress + ':' + socket.remotePort);

                var establishedOn = utils.getServerTimeStamp();
                var id;
                // calls callback by error and destroys socket
                socket.on("error", function (error) {
                    var response = callback(error, undefined);
                    // if response is returned then write into socket
                    if (response)
                        socket.write(response);
                    socket.destroy();
                });

                socket.on("close", () => {
                    logger.info("TLS Connection closed for " + serverName + ":" + serverPort + ".  RemoteAddress=>" + socket.remoteAddress + ':' + socket.remotePort);
                });

                // calls callback by data and ends socket
                socket.on("data", function (data) {
                    var response = callback(undefined, data, id, socket);
                    // if response is returned then write into socket
                    if (response) {
                        if (response["message"]) {
                            logger.debug("(createServer)-Writing back to Device :" + response["message"]);
                            socket.write(response["message"]);
                        }
                        if (response["socket_action"]
                            && response["socket_action"] === "close") {
                            logger.debug("(createServer)-Closing socket");
                            socket.end();
                        }
                        if (response["id"])
                            id = response["id"];
                    }
                });

                socket.setTimeout(1800000);

                socket.on("timeout", function () {
                    var timedOutOn = utils.getServerTimeStamp();
                    logger.debug("(createServer)-Established on :" + establishedOn + " Timedout on :" + timedOutOn);
                    socket.destroy();
                });

            }).listen(this.port);

        logger.debug("(createServer)-Server started on " + this.port);
    }
}

module.exports = TLSServer;