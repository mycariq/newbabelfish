const
	request = require('request');

var myPath = require('path');
var myScriptName = myPath.basename(__filename) + " : ";

function reverseHex(hexStr) {
	var result = [];
	for (var i = 0; i < hexStr.length / 2; i++) {
		result[i] = hexStr.substring(i * 2, (i * 2) + 2);
	}
	return result.reverse().join("");
}

function intToHexStr(num) {
	var hex = num.toString(16);
	if (hex.length % 2 == 1)
		return "0" + hex;
	return hex.toUpperCase();
}

function hexToText(hex) {
	return hex.match(/.{1,2}/g).map(function (v) {
		return v !== '00' ? String.fromCharCode(parseInt(v, 16)) : '';
	}).join('');
}

function textToHex(text) {
	return new Buffer(text, "utf8").toString("hex");
}

function hexToInt(hex) {
	return parseInt(hex, 16);
}

function binToInt(bin) {
	return parseInt(bin, 2);
}

function binToHex(bin) {
	return intToHexStr(binToInt(bin));
}

function binToSInt(bin) {
	return hexToSInt(binToHex(bin));
}

function hexToSInt(hex) {

	if (hex.length % 2 != 0) {
		hex = "0" + hex;
	}
	var num = parseInt(hex, 16);
	var maxVal = Math.pow(2, hex.length / 2 * 8);
	if (num > maxVal / 2 - 1) {
		num = num - maxVal;
	}
	return num;
}

function sIntToHex(num, noOfBytes) {
	var maxVal = Math.pow(2, noOfBytes * 8);
	if (num < 0)
		num = maxVal / 2 + (maxVal / 2 + num);
	return intToHexStr(num);
}

function hexToBin(hexStr) {

	var binaryStr = "";
	for (var index = 0; index < hexStr.length; index += 2) {
		var hex = hexStr.substr(index, 2);
		var bin = parseInt(hex, 16).toString(2);
		if (hex.length == 2)
			binaryStr += addLeadingChars(bin, "0", 8);
		else
			binaryStr += addLeadingChars(bin, "0", 4);
	}

	return binaryStr;
}

function revHexToBin(hex) {
	return hexToBin(reverseHex(hex));
}

function xorHex(hex1, hex2) {
	var num1 = hexToInt(hex1);
	var num2 = hexToInt(hex2);
	var result = num1 ^ num2;
	return intToHexStr(result);
}

// function to send packet to DAS
function sendPacket(URL) {
	console.log(myScriptName + "[INFO],Sending to " + URL);
	request.shouldKeepAlive = false;

	request(URL, function (error, response, body) {
		if (!error && response.statusCode == 200)
			console.log(myScriptName + '[INFO], ' + body);

		if (error) {
			console.log(myScriptName + '[ERROR], InValid Request to DAS');
			console.error(error.stack);
		}

	});
}

function getServerTimeStamp() {
	var date = new Date();
	return covertDateToString(date);
}

function covertDateToString(date) {
	var hour = date.getHours();
	hour = (hour < 10 ? "0" : "") + hour;

	var min = date.getMinutes();
	min = (min < 10 ? "0" : "") + min;

	var sec = date.getSeconds();
	sec = (sec < 10 ? "0" : "") + sec;

	var year = date.getFullYear();

	var month = date.getMonth() + 1;
	month = (month < 10 ? "0" : "") + month;

	var day = date.getDate();
	day = (day < 10 ? "0" : "") + day;

	return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}

function convertISTStrToUTCStr(dateStr) {
	var date = new Date(dateStr);

	//istTS.setHours(istTS.getHours()-5);
	date.setMinutes(date.getMinutes() - 330);
	return covertDateToString(date);
}

function getGeoJson(latitude, longitude, isReal) {
	var geoJson = {};
	if (!latitude || !longitude || latitude == 0 || longitude == 0)
		isReal = false;
	geoJson.type = "Point";
	geoJson.isReal = isReal;
	geoJson.coordinates = [longitude, latitude];
	return geoJson;
}

function hexToFloat(hex) {
	return Buffer(hex, 'hex').readFloatBE(0);
}

function extendJson(origin, add) {
	if (!add || (typeof add !== 'object' && add !== null)) {
		return origin;
	}

	var keys = Object.keys(add);
	keys.forEach(key => {
		var newKey = key.replace(/\./g, "_");
		origin[newKey] = add[key];
	});

	return origin;
};

function reverseStr(str) {
	return str.split("").reverse().join("");
}

function minusMilliSeconds(date, milliSeconds) {
	var newDate = new Date(date);
	newDate.setTime(newDate.getTime() - milliSeconds);
	return newDate;
}

function addMilliSeconds(date, milliSeconds) {
	var newDate = new Date(date);
	newDate.setTime(newDate.getTime() + milliSeconds);
	return newDate;
}

function revHexToInt(hex) {
	return hexToInt(reverseHex(hex));
}

function revHexToIntStr(hex) {
	return hexToInt(reverseHex(hex)).toString();
}

exports.parsePid = function (document, pid, pidValue) {
	var data = pidValue;
	switch (pid) {
		case "410C":
			document.rpm = ((hexToInt(data.substr(0, 2)) * 256) + hexToInt(data.substr(2, 2))) / 4;
			break;
		case "410D":
			document.obdSpeed = hexToInt(data);
			break;
		default:
			if (document.yetToImplement)
				document.yetToImplement = document.yetToImplement + "|" + pid + data;
			else
				document.yetToImplement = pid + data;
	}
}

function addLeadingChars(text, leadingChar, finalLength) {

	var leadsToAdd = (finalLength - text.length);

	for (var i = 1; i <= leadsToAdd; i++) {
		text = leadingChar + text;
	}
	return text;
}

// exports reverseHex function
exports.reverseHex = reverseHex;
exports.intToHexStr = intToHexStr;
exports.hexToText = hexToText;
exports.hexToInt = hexToInt;
exports.sendPacket = sendPacket;
exports.getServerTimeStamp = getServerTimeStamp;
exports.hexToBin = hexToBin;
exports.revHexToBin = revHexToBin;
exports.xorHex = xorHex;
exports.covertDateToString = covertDateToString;
exports.convertISTStrToUTCStr = convertISTStrToUTCStr;
exports.getGeoJson = getGeoJson;
exports.hexToFloat = hexToFloat;
exports.revHexToInt = revHexToInt;
exports.textToHex = textToHex;
exports.hexToSInt = hexToSInt;
exports.sIntToHex = sIntToHex;
exports.reverseStr = reverseStr;
exports.minusMilliSeconds = minusMilliSeconds;
exports.addMilliSeconds = addMilliSeconds;
exports.extendJson = extendJson;
exports.binToInt = binToInt;
exports.revHexToIntStr = revHexToIntStr;
exports.addLeadingChars = addLeadingChars;
exports.binToHex = binToHex;
exports.binToSInt = binToSInt;